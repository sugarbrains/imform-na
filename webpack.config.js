var path = require('path');
var miniCssExtractPlugin = require('mini-css-extract-plugin');
// var ngAnnotatePlugin = require('ng-annotate-webpack-plugin');

var config = {
    mode: 'development',
    output: {
        path: path.resolve(__dirname, "./dist"),
        filename: "[name].js"
    },
    module: {
        rules: [
            // {test: /\.scss$/, use: [
            //     'style-loader',
            //     'css-loader',
            //     'sass-loader'
            // ]},
            //
            {test: /\.scss$/, use: [
                {
                    loader: miniCssExtractPlugin.loader
                },
                "css-loader",
                "sass-loader"
            ]},
            {test: /\.css$/, use: [
                'style-loader',
                'css-loader'
            ]}
        ]
    },
    plugins: [
        new miniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[name].css'
        })
    ],
    watch: true
};

module.exports = config;