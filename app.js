const Koa = require('koa');
const koaBody = require('koa-body');
const userAgent = require('koa-useragent');
const CONFIG = require('./server/config');
// const serve = require('./server/methods/serve');
const sequelize = require('./server/methods/sequelize');
const initialize = require('./server/methods/initialize');
const cron = require('./server/cron');
const models = require('./server/models');

const Log = require('./server/middleware/log');
const Api = require('./server/middleware/api');
const Validator = require('./server/middleware/validator');
const Model = require('./server/middleware/model');
const Util = require('./server/middleware/util');
const Meta = require('./server/middleware/meta');
// const Verify = require('./server/middleware/verify');

const routes = require('./routes');

sequelize.defineAll(models).sync({
    force: false
}).then(async () => {
    const app = new Koa();

    // middleware
    app.use(koaBody({
        formidable: {uploadDir: './uploads'},
        multipart: true,
        urlencoded: true
    }));
    app.use(userAgent);
    app.use(Log);
    app.use(Validator);
    app.use(Util);
    app.use(Meta);
    // app.use(Verify);
    app.use(Model);
    app.use(Api);

    routes(app);

    // methods
    // serve(app);
    await initialize(app);

    // cron
    cron();

    const server = app.listen(CONFIG.port, () => {
        const host = server.address().address;
        const port = server.address().port;
        console.log(`Server listening at ${host}:${port}`);
    });

}).catch((e) => {
    console.error(e);
});
