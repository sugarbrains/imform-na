const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const rename = require('gulp-rename');
const inject = require('gulp-inject');
const clean = require('gulp-clean');
const webpackStream = require('webpack-stream');
const injectString = require('gulp-inject-string');

const now = new Date().getTime();

if (!fs.existsSync('dist')) fs.mkdirSync('dist');
if (!fs.existsSync('dist/analytics.css')) fs.writeFileSync('dist/analytics.css', '');
if (!fs.existsSync('dist/analytics.js')) fs.writeFileSync('dist/analytics.js', '');

console.log("v=", now);

gulp.task('build', ['webpack-watch']);

// gulp.task('clean', () => {
//     console.log('clean');
//     return gulp.src('./pages/analytics/views/index.php')
//         .pipe(clean({force: true}));
// });

gulp.task('inject', [], () => {
    console.log('inject');
    const path = './pages/analytics/views/';
    const array = ['./dist/analytics.js', './dist/analytics.css'];
    return gulp.src(path + 'index.origin.php')
        .pipe(inject(gulp.src(array, {read: false})))
        .pipe(injectString.replace('analytics.js', 'analytics.js?v=' + now))
        .pipe(injectString.replace('analytics.css', 'analytics.css?v=' + now))
        .pipe(rename('./index.php'))
        .pipe(gulp.dest(path));
});

gulp.task('webpack-watch', ['inject'], () => {
    console.log('webpack-watch');
    let webpackConfig = require('./webpack.config');

    webpackConfig.entry = {
        'analytics': './pages/analytics/module.js'
    };

    return gulp.src('')
        .pipe(webpackStream(webpackConfig))
        .pipe(gulp.dest('dist'));
});