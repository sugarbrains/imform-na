<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 9. 4.
 * Time: PM 6:06
 */
session_start();
if ($_SESSION['id']) {
    require_once(dirname(__FILE__).'/../../pages/analytics/views/index.php');
} else {
    header("Location: /routes/sign-in.php");
}