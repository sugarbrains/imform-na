<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 7. 30.
 * Time: AM 12:38
 */
session_start();
if ($_SESSION['id']) {
    require_once(dirname(__FILE__).'/../pages/sign/views/contents/select-service.php');
} else {
    header("Location: /routes/sign-in.php");
}