const fs = require('fs');
const Router = require('koa-router');
const router = new Router();

const target = '/etc/export';

module.exports = (app) => {
    router.get(`${target}/*`, redirect);

    app.use(router.routes(), router.allowedMethods());
};

async function redirect(ctx) {
    const path = ctx.request.originalUrl.replace(`${target}/`, '');
    const paths = path.split('/');

    ctx.body = fs.createReadStream(path);

    ctx.set('Content-disposition', 'attachment; filename=' + paths[paths.length - 1]);
}