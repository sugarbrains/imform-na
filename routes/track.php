<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 1:24
 */
require_once(dirname(__FILE__).'/../server/utils/Hash.php');
require_once(dirname(__FILE__).'/../server/utils/Response.php');
require_once(dirname(__FILE__).'/../server/db/OpenMessages.php');
require_once(dirname(__FILE__).'/../server/db/Messages.php');

$requestUri = explode('/', $_SERVER['REQUEST_URI']);

if (count($requestUri) === 3 && substr($requestUri[1],0,1) === 'Z') {
    $messageId = Hash::hashToId(substr($requestUri[1],1, strlen($requestUri[1])));
//    $receiverId = Hash::hashToId($requestUri[2]);

//    $OpenMessages = new OpenMessages();
//    $result = $OpenMessages->create($messageId, $receiverId);
//    $OpenMessages->close();

    $Messages = new Messages();
    $message = $Messages->get($messageId);
    $Messages->close();

    if ($message) {
        require_once(dirname(__FILE__).'/../routes/redirect.php');
    } else {
        $response = new Response(404,null,true);
    }
}