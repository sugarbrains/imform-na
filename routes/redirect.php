<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: PM 5:51
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title><?php echo isset($message) ? $message['title2'] : 'TITLE' ?></title>

    <meta property="og:title" content="<?php echo isset($message) ? $message['title2'] : 'TITLE' ?>">
    <meta property="og:image" content="<?php echo isset($message) ? $message['image'] : '' ?>">
    <meta name="description" content="">
    <meta property="og:type" content="website">
    <meta property="og:description" content="">

</head>
<body>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
    var url = '/api/analytics/open-messages';
    var body = {
        messageId: '<?php echo isset($messageId) ? $messageId : '' ?>'
    };
    var messageHash = '<?php echo isset($requestUri) && isset($requestUri[1]) ? $requestUri[1] : '' ?>';
    var userHash = '<?php echo isset($requestUri) && isset($requestUri[2]) ? $requestUri[2] : '' ?>';
    if (userHash && userHash !== 'undefined') {
        body.userHash = userHash;
    }
    console.log(body);
    $(document).ready(function() {
        $.post(url, body, function (data) {
            data = JSON.parse(data);
            if (data.url) {
                location.href = data.url + '?messageHash=' + messageHash + '&userHash=' + userHash;
            }
        });
    });
</script>
</body>
</html>