<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 8. 10.
 * Time: PM 11:23
 */
?>
    <script type="text/javascript">
        function openDialog(message, callback, isCancel) {
            if (isCancel) {
                $('<div id="lcAlertDialog">' + message + '</div>').dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        '취소': function() {
                            $(this).dialog( "close" );
                        },
                        '확인': function() {
                            $(this).dialog( "close" );
                            if (callback) callback();
                        }
                    }
                });
            } else {
                $('<div id="lcAlertDialog">' + message + '</div>').dialog({
                    resizable: false,
                    modal: true,
                    buttons: {
                        '확인': function() {
                            $(this).dialog( "close" );
                            if (callback) callback();
                        }
                    }
                });
            }
        }

        function alertError (status, data) {
            try {
                data = JSON.parse(data);
                if (data.message) {
                    openDialog(data.message);
                } else {
                    openDialog('잘못된 요청입니다.');
                }
            } catch (e) {
                openDialog('잘못된 요청입니다.');
            }
        }
    </script>
