<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 8. 10.
 * Time: PM 10:54
 */
?>
    <script type="text/javascript">
        var contentType = 'application/x-www-form-urlencoded; charset=UTF-8;';
        
        function post (url, body, callback, successStatusCode) {
            $.ajax({
                url: url,
                method: 'POST',
                data: body,
                contentType: contentType,
                success: function (data) {
                    callback(successStatusCode ? successStatusCode : 200, data);
                },
                error: function (jqXHR) {
                    callback(jqXHR.status, jqXHR.responseText);
                }
            });
        }
        
        function get (url, query, callback, successStatusCode) {
            $.ajax({
                url: url,
                method: 'GET',
                data: query,
                contentType: contentType,
                success: function (data) {
                    callback(successStatusCode ? successStatusCode : 200, data);
                },
                error: function (jqXHR) {
                    callback(jqXHR.status, jqXHR.responseText);
                }
            });
        }

        function put (url, body, callback) {
            $.ajax({
                url: url,
                method: 'PUT',
                data: body,
                contentType: contentType,
                success: function () {
                    callback(204);
                },
                error: function (jqXHR) {
                    callback(jqXHR.status, jqXHR.responseText);
                }
            });
        }

        function del (url, query, callback) {
            $.ajax({
                url: url,
                method: 'DELETE',
                data: query,
                contentType: contentType,
                success: function () {
                    callback(204);
                },
                error: function (jqXHR) {
                    callback(jqXHR.status, jqXHR.responseText);
                }
            });
        }
    </script>
