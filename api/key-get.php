<?php
/**
 * Created by PhpStorm.
 * User: Edward
 * Date: 2020-08-13
 * Time: 15:54
 */

header('Content-Type: application/json');
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: x-requested-with');

require_once(dirname(__FILE__).'/../server/utils/Hash.php');



// 입력값 확인
if (isset($_REQUEST['mid'])) { $get_mid = htmlspecialchars(trim($_REQUEST['mid'])); } else { $get_mid = ''; }
if (isset($_REQUEST['uid'])) { $get_uid = htmlspecialchars(trim($_REQUEST['uid'])); } else { $get_uid = ''; }

if (!$get_mid || !$get_uid) {
    http_response_code(400);
    echo json_encode([ 'error_code' => 400, 'error_text' => '필수 값이 누락되었습니다.' ], JSON_UNESCAPED_UNICODE);
    exit;
}


// 결과 조회 후 리턴
if (substr($get_mid, 0, 1) === 'Z') {
    $messageId = Hash::hashToId(substr($get_mid,1, strlen($get_mid)));
    $receiverId = Hash::hashToId($get_uid);

    //
    //
    // DB Connect Data
    //
    $db_host        = 'localhost';
    $db_id          = 'na-mk2';
    $db_pw          = 'imform123!!qwe';
    $db_database    = 'na-mk2';

    // DB Connect Try
    @ $db = new mysqli($db_host, $db_id, $db_pw, $db_database);

    // DB Connect Error Check
    if (mysqli_connect_errno()) {
        http_response_code(503);
        echo json_encode([ 'error_code' => 503, 'error_text' => "[".mysqli_connect_errno()."] Could not connect to database. Please try again later." ], JSON_UNESCAPED_UNICODE);
        exit;
    }

    $db_query1 = $db->query("SELECT * FROM Receivers WHERE id = {$receiverId}");
    $row_receivers = $db_query1->fetch_assoc();
    if ($row_receivers['key']) {
        http_response_code(200);
        echo json_encode([ 'code' => 200, 'mid' => $messageId, 'uid' => $receiverId, 'key' => $row_receivers['key'] ], JSON_UNESCAPED_UNICODE);
    } else {
        http_response_code(403);
        echo json_encode([ 'error_code' => 403, 'error_text' => '잘못된 uid 입니다.' ], JSON_UNESCAPED_UNICODE);
        exit;
    }

} else {
    http_response_code(403);
    echo json_encode([ 'error_code' => 403, 'error_text' => '잘못된 mid 입니다.' ], JSON_UNESCAPED_UNICODE);
    exit;
}

