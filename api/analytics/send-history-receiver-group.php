<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 12.
 * Time: PM 9:50
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/send-history-receiver-group/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $validator = new Validator([
            'sendHistoryIds',
            'searchField',
            'searchItem',
            'genders',
            'ages',
            'sido',
            'sigungu',
            'openCount',
            'diagramHour',
            'diagramDay',
            'state',
            'name'
        ], [
            'sendHistoryIds',
            'name'
        ], $CONTROLLER.'post.php');

        $validator->isEnum('searchField', ['key']);
        $validator->len('searchItem',1,191);
        $validator->len('sido',1,191);
        $validator->len('sigungu',1,191);
        $validator->isInt('openCount');
        $validator->isInt('diagramHour');
        $validator->isInt('diagramDay');
        $validator->isEnum('state', ['sendAnalysis', 'senderAnalysis']);
        $validator->len('name',1,191);
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}