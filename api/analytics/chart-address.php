<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 7.
 * Time: AM 1:05
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/chart-address/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $validator = new Validator([
            'sendHistoryIds',
            'searchField',
            'searchItem',
            'genders',
            'ages',
            'sido',
            'sigungu',
            'openCount',
            'diagramHour',
            'diagramDay',
            'state',
            'offset',
            'size'
        ], [], $CONTROLLER.'get.php');

        $validator->isEnum('searchField', ['key']);
        $validator->len('searchItem',1,191);
        $validator->len('sido',1,191);
        $validator->len('sigungu',1,191);
        $validator->isInt('openCount');
        $validator->isInt('diagramHour');
        $validator->isInt('diagramDay');
        $validator->isEnum('state', ['sendAnalysis', 'senderAnalysis']);
        $validator->isInt('offset');
        $validator->isInt('size');
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}