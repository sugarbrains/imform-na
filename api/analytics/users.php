<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 20.
 * Time: AM 5:26
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/users/';
$DYNAMIC = '/api/analytics/users/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
                'searchField',
                'searchItem',
                'startDate',
                'endDate'
            ], [
                'size'
            ], $CONTROLLER.'gets.php');

            $validator->isEnum('orderBy', ['id', 'uid', 'name', 'phoneNum', 'createdAt']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->isInt('offset');
            $validator->isEnum('searchField', ['uid', 'name', 'phoneNum']);
            $validator->len('searchItem',1,191);
            $validator->isExp('startDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->isExp('endDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->active();
        }
        break;
    case 'PUT':
        if (Filter::isDynamic($DYNAMIC)) {
            $_POST['id'] = Filter::requestId();
            $validator = new Validator([
                'id',
                'domainId',
                'uid',
                'role',
                'password',
                'name',
                'phoneNum',
                'url',
                'refuseMessage',
                'sms',
                'lms'
            ], [
                'id',
                'uid',
                'role',
                'name',
                'phoneNum'
            ],$CONTROLLER.'put.php');
            $validator->isInt('id');
            $validator->isInt('domainId');
            $validator->len('uid',1,191);
            $validator->isEnum('role', ['ADMIN', 'MANAGER', 'USER', 'API']);
            $validator->len('password',6,20);
            $validator->len('name',1,191);
            $validator->len('phoneNum',1,191);
            $validator->len('url',1,191);
            $validator->isFloat('sms');
            $validator->isFloat('lms');
            $validator->active();
        } else {
            new Response(404,null,true);
        }
        break;
    default:
        new Response(404,null,true);
        break;
}