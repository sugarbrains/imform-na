<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 5:19
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/message-send/';
$DYNAMIC = '/api/analytics/message-send/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'PUT':
        if (Filter::isDynamic($DYNAMIC)) {
            $_POST['id'] = Filter::requestId();
            $validator = new Validator([
                'id',
                'activeDate'
            ], [
                'id'
            ],$CONTROLLER.'put.php');
            $validator->isInt('id');
            $validator->isExp('w', '/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/');
            $validator->active();
        } else {
            new Response(404,null,true);
        }
        break;
    default:
        new Response(404,null,true);
        break;
}