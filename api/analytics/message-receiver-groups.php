<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 20.
 * Time: AM 2:00
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/message-receiver-groups/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $validator = new Validator([
            'orderBy',
            'sort',
            'size',
            'offset',
            'searchField',
            'searchItem',
            'receiverGroupId'
        ], [
            'size',
            'receiverGroupId'
        ], $CONTROLLER.'gets.php');

        $validator->isEnum('orderBy', ['id', 'title', 'redirectUrl', 'activeDate', 'createdAt']);
        $validator->isEnum('sort', ['ASC', 'DESC']);
        $validator->isInt('size');
        $validator->isInt('offset');
        $validator->isEnum('searchField', ['title']);
        $validator->len('searchItem',1,191);
        $validator->isInt('receiverGroupId');
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}