<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 11:08
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/meta-prices/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $validator = new Validator([], [],$CONTROLLER.'get.php');
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}