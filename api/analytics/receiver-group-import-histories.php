<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 18.
 * Time: AM 2:31
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/receiver-group-import-histories/';
$DYNAMIC = '/api/analytics/receiver-group-import-histories/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            new Response(404,null,true);
        }
        break;
    default:
        new Response(404,null,true);
        break;
}