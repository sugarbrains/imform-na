<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:35
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/messages/';
$DYNAMIC = '/api/analytics/messages/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
                'searchField',
                'searchItem',
                'startDate',
                'endDate',
                'state'
            ], [
                'size'
            ], $CONTROLLER.'gets.php');

            $validator->isEnum('orderBy', ['id', 'title', 'redirectUrl', 'activeDate', 'createdAt']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->isInt('offset');
            $validator->isEnum('searchField', ['title', 'name']);
            $validator->len('searchItem',1,191);
            $validator->isExp('startDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->isExp('endDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->isEnum('state', ['standby', 'send', 'sendComplete', 'cancel', 'export']);
            $validator->active();
        }
        break;
    case 'POST':
        $validator = new Validator([
            'title',
            'title2',
            'message',
            'refuseMessage',
            'redirectUrl',
            'receiverGroupIds',
            'buttons'
        ], [
            'title',
            'title2',
            'message',
            'redirectUrl'
        ],$CONTROLLER.'post.php');
        $validator->len('title',1,191);
        $validator->len('title2',1,191);
        $validator->len('redirectUrl',1,191);
        $validator->active();
        break;
    case 'PUT':
        if (Filter::isDynamic($DYNAMIC)) {
            $_POST['id'] = Filter::requestId();
            $validator = new Validator([
                'id',
                'title',
                'title2',
                'message',
                'refuseMessage',
                'redirectUrl',
                'receiverGroupIds',
                'buttons'
            ], [
                'id',
                'title',
                'title2',
                'message',
                'redirectUrl'
            ],$CONTROLLER.'put.php');
            $validator->isInt('id');
            $validator->len('title',1,191);
            $validator->len('title2',1,191);
            $validator->len('redirectUrl',1,191);
            $validator->active();
        } else {
            new Response(404,null,true);
        }
        break;
    case 'DELETE':
        if (Filter::isDynamic($DYNAMIC)) {
            $_POST['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'del.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            new Response(404,null,true);
        }
        break;
    default:
        new Response(404,null,true);
        break;
}