<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 23.
 * Time: PM 3:02
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/domains/';
$DYNAMIC = '/api/analytics/domains/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
            ], [], $CONTROLLER.'gets.php');

            $validator->isEnum('orderBy', ['url', 'createdAt']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->isInt('offset');
            $validator->active();
        }
        break;
    default:
        new Response(404,null,true);
        break;
}