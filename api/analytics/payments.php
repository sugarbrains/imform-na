<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 28.
 * Time: AM 4:43
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/payments/';
$DYNAMIC = '/api/analytics/payments/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
                'startDate',
                'endDate'
            ], [
                'size'
            ], $CONTROLLER.'gets.php');

            $validator->isEnum('orderBy', ['id', 'price', 'createdAt']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->isExp('startDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->isExp('endDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->active();
        }
        break;
    default:
        new Response(404,null,true);
        break;
}