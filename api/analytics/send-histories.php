<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: PM 3:48
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/send-histories/';
$DYNAMIC = '/api/analytics/send-histories/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'senderIds',
                'orderBy',
                'sort',
                'size',
                'offset',
                'searchField',
                'searchItem',
                'startDate',
                'endDate'
            ], [
                'size'
            ], $CONTROLLER.'gets.php');

            $validator->isEnum('searchField', ['id', 'name', 'title']);
            $validator->isEnum('orderBy', ['id', 'name', 'title', 'activeDate', 'createdAt', 'openRate']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->len('searchItem',1,191);
            $validator->isExp('startDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->isExp('endDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->active();
        }
        break;
    default:
        new Response(404,null,true);
        break;
}