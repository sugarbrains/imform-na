<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 1:49
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/button-time/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $validator = new Validator([
            'buttonId',
            'messageHash',
            'userHash',
            'time',
            'callback',
            '_'
        ], [
            'buttonId',
            'messageHash',
            'userHash',
            'time'
        ], $CONTROLLER.'get.php');
        $validator->isInt('buttonId');
        $validator->len('messageHash',2,10);
        $validator->len('userHash',1,10);
        $validator->isInt('time');
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}