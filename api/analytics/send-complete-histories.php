<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 28.
 * Time: AM 2:51
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/send-complete-histories/';
$DYNAMIC = '/api/analytics/send-complete-histories/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
                'searchField',
                'searchItem',
                'startDate',
                'endDate'
            ], [
                'size'
            ], $CONTROLLER.'gets.php');

            $validator->isEnum('searchField', ['id', 'title']);
            $validator->isEnum('orderBy', ['id', 'title', 'activeDate', 'createdAt']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->len('searchItem',1,191);
            $validator->isExp('startDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->isExp('endDate', '/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/');
            $validator->active();
        }
        break;
    default:
        new Response(404,null,true);
        break;
}