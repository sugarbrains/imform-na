<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 3. 5.
 * Time: AM 4:18
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/sender-histories/';
$DYNAMIC = '/api/analytics/sender-histories/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
                'searchField',
                'searchItem',
            ], [
                'size'
            ], $CONTROLLER.'gets.php');

            $validator->isEnum('searchField', ['id', 'name']);
            $validator->isEnum('orderBy', ['id', 'name', 'activeDate', 'createdAt']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->len('searchItem',1,191);
            $validator->active();
        }
        break;
    default:
        new Response(404,null,true);
        break;
}