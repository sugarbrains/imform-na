<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 7:14
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/receivers/';
$DYNAMIC = '/api/analytics/receivers/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
                'searchField',
                'searchItem',
                'receiverGroupId'
            ], [
                'size'
            ], $CONTROLLER.'gets.php');

            $validator->isEnum('orderBy', ['id', 'key', 'name']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->isInt('offset');
            $validator->isEnum('searchField', ['key', 'name']);
            $validator->len('searchItem',1,191);
            $validator->isInt('receiverGroupId');
            $validator->active();
        }
        break;
    default:
        new Response(404,null,true);
        break;
}