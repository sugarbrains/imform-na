<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 1:34
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/open-messages/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $validator = new Validator([
            'messageId',
            'userHash'
        ], [
            'messageId'
        ], $CONTROLLER.'post.php');

        $validator->isInt('messageId');
        $validator->len('userHash',1,10);
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}