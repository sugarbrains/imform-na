<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 9:54
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/analytics/receiver-groups/';
$DYNAMIC = '/api/analytics/receiver-groups/id';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        if (Filter::isDynamic($DYNAMIC)) {
            $_GET['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'get.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            $validator = new Validator([
                'orderBy',
                'sort',
                'size',
                'offset',
                'searchField',
                'searchItem',
                'size'
            ], [], $CONTROLLER.'gets.php');

            $validator->isEnum('orderBy', ['id', 'name', 'receiverCount', 'sentCount', 'createdAt']);
            $validator->isEnum('sort', ['ASC', 'DESC']);
            $validator->isInt('size');
            $validator->isInt('offset');
            $validator->isEnum('searchField', ['name']);
            $validator->len('searchItem',1,191);
            $validator->active();
        }
        break;
    case 'POST':
        $validator = new Validator([
            'name'
        ], [
            'name'
        ],$CONTROLLER.'post.php');
        $validator->len('name',1,191);
        $validator->active();
        break;
    case 'PUT':
        if (Filter::isDynamic($DYNAMIC)) {
            $_POST['id'] = Filter::requestId();
            $validator = new Validator([
                'id',
                'name'
            ], [
                'id',
                'name'
            ],$CONTROLLER.'put.php');
            $validator->isInt('id');
            $validator->len('name',1,191);
            $validator->active();
        } else {
            new Response(404,null,true);
        }
        break;
    case 'DELETE':
        if (Filter::isDynamic($DYNAMIC)) {
            $_POST['id'] = Filter::requestId();
            $validator = new Validator([
                'id'
            ], [
                'id'
            ],$CONTROLLER.'del.php');
            $validator->isInt('id');
            $validator->active();
        } else {
            new Response(404,null,true);
        }
        break;
    default:
        new Response(404,null,true);
        break;
}