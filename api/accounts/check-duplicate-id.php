<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: AM 10:44
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/accounts/check-duplicate-id/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $validator = new Validator([
            'uid'
        ], [
            'uid'
        ], $CONTROLLER.'get.php');
        $validator->len('uid',1,191);
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}