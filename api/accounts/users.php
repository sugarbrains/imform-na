<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: PM 12:15
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/accounts/users/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $validator = new Validator([
            'uid',
            'password',
            'name',
            'phoneNum'
        ], [
            'uid',
            'password',
            'name',
            'phoneNum'
        ], $CONTROLLER.'post.php');

        $validator->len('uid',1,191);
        $validator->len('password',6,20);
        $validator->len('name',1,191);
        $validator->len('phoneNum',1,191);
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}