<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: PM 3:38
 */
require_once(dirname(__FILE__).'/../../server/utils/Filter.php');
require_once(dirname(__FILE__).'/../../server/utils/Validator.php');

$CONTROLLER = dirname(__FILE__).'/../../server/controllers/accounts/session/';

switch($_SERVER['REQUEST_METHOD']) {
    case 'GET':
        $validator = new Validator([], [], $CONTROLLER.'get.php');
        $validator->active();
        break;
    default:
        new Response(404,null,true);
        break;
}