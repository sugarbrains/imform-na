<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 7. 30.
 * Time: AM 12:47
 */
require_once(dirname(__FILE__).'/../../server/utils/Session.php');
Session::logout();
header("Location: /routes/sign-in.php");
die();