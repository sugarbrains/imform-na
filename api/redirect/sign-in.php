<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 7. 30.
 * Time: AM 12:27
 */
require_once(dirname(__FILE__).'/../../server/db/Users.php');

if ($_POST['uid'] && $_POST['password']) {
    $Users = new Users();
    $user = $Users->login($_POST, $_SERVER['HTTP_HOST']);
    $Users->close();

    if ($user === 'API') {
        $prevPage = $_SERVER["HTTP_REFERER"];
        header("location: " . $prevPage . "?api=true");
    } else if ($user) {
        header("Location: /routes/analytics/index.php");
    } else {
        $prevPage = $_SERVER["HTTP_REFERER"];
        header("location: " . $prevPage . "?wrong=true");
    }
} else {
    header("Location: /routes/sign-in.php?wrong=true");
}
die();