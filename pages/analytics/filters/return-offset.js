export default function returnOffset() {
    'ngInject';

    return (size, page) => {
        return (parseInt(page) - 1) * parseInt(size);
    }
}