export default function noData() {
    'ngInject';

    return (data) => {
        return data ? data : '-';
    }
}