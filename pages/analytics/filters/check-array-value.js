export default function checkArrayValue () {
    'ngInject';

    return function (array, value = true, key = null) {
        for (let i=0; i<array.length; i++) {
            if ((key && array[i][key] != value) || (!key && array[i] != value)) {
                return false;
            }
        }
        return true;
    };
}