export default function attachZero() {
    'ngInject';

    return (data) => {
        if (data || data === 0) {
            if (typeof data == "string") {
                data = parseInt(data);
            }
            if (typeof data != 'number') {
                return null;
            }
            if (data < 10) {
                return '0' + data;
            } else {
                return data;
            }
        } else {
            return null;
        }
    }
}