export default function returnPage () {
    'ngInject';

    return (size, offset) => {
        if (offset) {
            return parseInt(offset / size) + 1;
        } else {
            return 1;
        }
    }
}