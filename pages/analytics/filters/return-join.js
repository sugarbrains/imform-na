export default function returnJoin () {
    'ngInject';

    return (array, key) => {
        return array.join(key);
    };
}