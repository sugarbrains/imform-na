export default function toISODate() {
    'ngInject';

    return (dateString) => {
        return new Date(dateString.replace(' ', 'T') + '.000Z');
    }
}