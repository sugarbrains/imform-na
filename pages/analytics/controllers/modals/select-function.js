export default function SelectFunctionCtrl ($scope, $rootScope, constant, modalHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#selectFunctionWrap');

    $scope.close = close;
    $scope.select = select;
    $scope.exportResult = exportResult;
    $scope.exportOpener = exportOpener;
    $scope.createReceiverGroup = createReceiverGroup;

    init();

    $scope.$on(constant.modalKey.selectFunction, () => {
        init();
        $scope.isOpen = true;
        modalHandler.focus($target);
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function exportResult() {
        $scope.form = {
            isExportResult: true
        };
    }

    function exportOpener() {
        $scope.form = {
            isExportOpener: true
        };
    }

    function createReceiverGroup() {
        $scope.form = {
            isCreateReceiverGroup: true
        };
    }

    function init() {
        $scope.isOpen = false;
        $scope.form = {};
    }

    function select() {
        let form = $scope.form;
        if (form.isExportResult || form.isExportOpener || form.isCreateReceiverGroup) {
            $rootScope.$broadcast(constant.eventKey.selectFunction, {
                select: angular.copy($scope.form)
            });
            close();
        } else {
            // todo dialog
        }
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}