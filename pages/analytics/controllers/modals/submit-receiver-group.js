export default function SubmitReceiverGroupCtrl($scope, $rootScope, constant, dialogHandler, modalHandler, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#submitReceiverGroupWrap');

    $scope.close = close;
    $scope.submit = submit;

    init();

    $scope.$on(constant.modalKey.submitReceiverGroup, (e, args) => {
        init(args.query);
        $scope.isOpen = true;
        modalHandler.focus($target);
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {};
        if (query) {
            if(query['#'] !== undefined) delete query['#'];
            $scope.form = angular.copy(query);
        }
    }

    function submit() {
        receiverGroupManager.createHistoryReceiverGroup($scope.form, (status, data) => {
            if (status == 201) {
                dialogHandler.show('생성이 완료되었습니다.', {
                    submitCallback: () => {
                        $rootScope.$broadcast(constant.eventKey.submitReceiverGroup);
                        close();
                    }
                });
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }



    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}