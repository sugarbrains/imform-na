export default function ReceiverListCtrl($scope, $rootScope, $filter, constant, dialogHandler, modalHandler, receiverManager, orderHandler) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    const $target = $('#receiverListWrap');

    $scope.close = close;
    $scope.goPage = goPage;
    $scope.refresh = refresh;
    $scope.search = search;

    orderHandler.init($scope, constant.orderKeys.receivers);
    $scope.headers = constant.listHeaders.receivers;

    init();

    orderHandler.setSort($scope, constant.listKeys.receiver);
    orderHandler.setOrderBy($scope, constant.listKeys.receiver, findReceivers);

    $scope.genderStates = constant.genderStates;
    $scope.enumSearchFields = [{
        value: 'key',
        text: 'KEY'
    }, {
        value: 'name',
        text: '이름'
    }];

    $scope.$on(constant.modalKey.receiverList, (e, args) => {
        init(args.query);
        findReceivers(() => {
            $scope.isOpen = true;
            modalHandler.focus($target);
        });
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function findReceivers(callback) {
        orderHandler.setSort($scope, constant.listKeys.receiver);

        let query = angular.copy($scope.form);
        if(query.searchItem !== undefined && query.searchItem !== null && query.searchItem.length === 0) delete query.searchItem;

        receiverManager.findAll(query, (status, data) => {
            if(status === 200) {
                data.rows.forEach((item) => {
                    if(item.birth != 9999) {
                        item.age = (new Date().getFullYear()) - item.birth + 1;
                    } else {
                        item.age = null;
                    }
                });
                $scope.receivers = data;
                if (callback) callback();
            } else if (status === 404) {
                $scope.receivers = null;;
                if (callback) callback();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {
            orderBy: 'id',
            sort: 'DESC'
        };
        $scope.receiverGroup = {};
        if (query) {
            if(query['#'] !== undefined) delete query['#'];
            $scope.receiverGroup = angular.copy(query);
            $scope.form.receiverGroupId = $scope.receiverGroup.id;
            $scope.form.size = 10;
            $scope.form.searchField = 'key';
        }
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }

    function goPage(page) {
        search(returnOffset($scope.form.size, page));
    }

    function refresh() {
        delete $scope.form.searchItem;
        $scope.form.searchField = 'key';
        search();
    }
    
    function search(offset) {
        $scope.form.offset = offset ? offset : 0;
        findReceivers();
    }
}