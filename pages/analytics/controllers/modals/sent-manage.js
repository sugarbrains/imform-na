export default function SentManageCtrl($scope, $rootScope, $filter, constant, loadingHandler, enumHandler, dialogHandler, modalHandler, messageManager) {
    'ngInject';

    let vm = $scope.vm;
    let $toISODate = $filter('toISODate');
    let $dateFilter = $filter('date');
    let $attachZero = $filter('attachZero');

    const $target = $('#sentManageWrap');

    $scope.enumHours = enumHandler.returnHours();
    $scope.enumMinutes = enumHandler.returnMinutes();
    let d = new Date();
    $scope.minDate = new Date(d.setDate(d.getDate() - 1)).toString();

    $scope.sendPost = sendPost;
    $scope.postReservation = postReservation;
    $scope.extractUrl = extractUrl;
    $scope.cancelPost = cancelPost;
    $scope.removePost = removePost;
    $scope.downloadCsv = downloadCsv;

    $scope.sendPostVisibleStates = {
        standby: true,
        send: false,
        sendComplete: false,
        cancel: false,
        export: false,
    };
    $scope.postReservationVisibleStates = {
        standby: true,
        send: false,
        sendComplete: false,
        cancel: false,
        export: false,
    };
    $scope.cancelPostVisibleStates = {
        standby: false,
        send: true,
        sendComplete: false,
        cancel: false,
        export: false,
    };

    $scope.extractUrlVisibleStates = {
        standby: true,
        send: false,
        sendComplete: false,
        cancel: false,
        export: false,
    };

    $scope.removePostVisibleStates = {
        standby: true,
        send: true,
        sendComplete: true,
        cancel: true,
        export: true,
    };

    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.sentManage, (e, args) => {
        init(args.query);
        $scope.isOpen = true;
        modalHandler.focus($target);
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        // $scope.isOpen = false;
        $scope.form = {};
        $scope.reservationForm = {};
        if (query) {
            if (query['#'] !== undefined) delete query['#'];
            findMessageInfo(query.id);
        }
    }

    function sendPost() {
        messageManager.send({id: $scope.form.id}, (status, data) => {
            if (status === 204) {
                dialogHandler.show('메세지 발송을 시작합니다.', {
                    submitCallback: () => {
                        findMessageInfo($scope.form.id);
                        $rootScope.$broadcast(constant.eventKey.sendPost);
                    }
                });
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function postReservation() {
        let body = {
            id: $scope.form.id,
        };

        if (!$scope.reservationForm.sendDate) {
            return dialogHandler.show('예약발송일을 선택해주세요.');
        }

        body.activeDate = $scope.reservationForm.sendDate;

        if ($scope.reservationForm.sendHour) {
            body.activeDate += " " + $scope.reservationForm.sendHour;
            if ($scope.reservationForm.sendMinute) {
                body.activeDate += ":" + $scope.reservationForm.sendMinute + ":00";
            } else {
                body.activeDate += ":00:00";
            }
        } else {
            body.activeDate += " 00:00:00";
        }

        return messageManager.send(body, (status, data) => {
            if (status === 204) {
                dialogHandler.show('메세지 발송을 예약되었습니다.', {
                    submitCallback: () => {
                        findMessageInfo($scope.form.id);
                        $rootScope.$broadcast(constant.eventKey.sendPost);
                        // close();
                        // $rootScope.$broadcast(constant.eventKey.sendPost);
                    }
                });
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function extractUrl() {
        let body = {
            id: $scope.form.id,
        };
        loadingHandler.startLoading('extract-url');
        messageManager.extract(body, (status, data) => {
            if (status === 204) {
                let interval = setInterval(() => {
                    messageManager.getExtractHistory(data.id, (status, data) => {
                        if (status === 200) {
                            loadingHandler.loadingText('extract-url', `${data.progress}%...`);
                            if (data.state === "complete") {
                                window.open(`/${data.fileName}`, '_blank');
                                clearInterval(interval);
                                loadingHandler.endLoading('extract-url');
                                findMessageInfo($scope.form.id);
                                $rootScope.$broadcast(constant.eventKey.sendPost);
                            } else if (data.state === "error") {
                                clearInterval(interval);
                                loadingHandler.endLoading('extract-url');
                                dialogHandler.show(`추출 실패!\n사유: ${data.errorMessage}`);
                            }
                        } else {
                            loadingHandler.endLoading('extract-url');
                            dialogHandler.alertError(status, data);
                        }
                    });
                }, 1000);
            } else {
                loadingHandler.endLoading('extract-url');
                dialogHandler.alertError(status, data);
            }
        });
    }

    function cancelPost() {
        let now = new Date().getTime();
        let activeDate = $toISODate($scope.form.activeDate);

        if (now <= activeDate) {
            messageManager.cancel({id: $scope.form.id}, (status, data) => {
                if (status === 204) {
                    dialogHandler.show('메세지 발송(예약)이 취소되었습니다.', {
                        submitCallback: () => {
                            findMessageInfo($scope.form.id);
                            $rootScope.$broadcast(constant.eventKey.sendPost);
                        }
                    });
                } else {
                    dialogHandler.alertError(status, data);
                }
            });
        } else {
            dialogHandler.show('발송일이 지나지 않은 문자만 취소가 가능합니다.');
        }

    }

    function removePost() {
        dialogHandler.show('메세지를 삭제하시겠습니까?', {
            submitCallback: () => {
                messageManager.remove($scope.form, (status, data) => {
                    if (status === 204) {
                        dialogHandler.show('메세지가 삭제되었습니다.', {
                            submitCallback: () => {
                                close();
                                $rootScope.$broadcast(constant.eventKey.sendPost);
                            }
                        });
                    } else {
                        dialogHandler.alertError(status, data);
                    }
                });
            }
        }, {
            cancelText: "취소",
            cancelCallback: () => {

            }
        });
    }

    function downloadCsv() {
        window.open(`/${$scope.form.exportHistory.fileName}`);
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }

    function findMessageInfo(id) {
        messageManager.findById(id, (status, data) => {
            if (status === 200) {
                $scope.form = data;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    $scope.$watch('reservationForm.sendDate', (n, o) => {
        if (n !== o) {
            let now = $dateFilter(new Date(), 'yyyy-MM-dd');
            let newVal = $dateFilter(n, 'yyyy-MM-dd');
            if (newVal == now) {
                let currentHour = $attachZero(new Date().getHours());
                let array = enumHandler.returnHours();
                $scope.enumHours = array.splice(array.indexOf(currentHour), 24);
            } else {
                $scope.enumHours = enumHandler.returnHours();
            }
        }
    }, true);

    function a() {

    }
}