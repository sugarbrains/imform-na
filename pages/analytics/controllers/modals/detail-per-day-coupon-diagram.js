export default function DetailPerDayCouponDiagramCtrl($scope, constant, modalHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailPerDayCouponDiagramWrap');

    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailPerDayCouponDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = angular.copy(query);
    }

    function findPerDayCouponDiagram() {
        // todo find per day coupon diagram
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}