export default function DetailPageTimeDiagramCtrl($scope, constant, modalHandler, chartManager, messageListManager, dialogHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailPageTimeDiagramWrap');
    $scope.isSelected = false;
    $scope.selectHistoryId = null;

    $scope.headers = constant.listHeaders.detailPageTimes;
    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailPageTimeDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {};
        if (query) {
            setSelectEnum(query.sendHistoryIds);
            $scope.form = query;
        }
    }

    function setSelectEnum(keys) {
        messageListManager.findByIds(keys, (status, data) => {
            if(status === 200) {
                $scope.selectEnums = data;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findPageTimeDiagram(sendHistoryId) {
        let query = Object.assign({}, $scope.form);
        query.sendHistoryIds = sendHistoryId;
        chartManager.getChartPageTime(query, (status, data) => {
            if (status === 200) {
                data.forEach(item => {
                    item.time = parseInt(item.time / 1000);
                });
                $scope.detailPageTimes = data;
                $scope.isSelected = true;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.form = {};
            $scope.selectHistoryId = null;
            $scope.isSelected = false;
            $scope.isOpen = false;
        }
    }

    $scope.$watch('selectHistoryId', (n, o) => {
        if(n !== o && n !== null) {
            findPageTimeDiagram(n);
        }
    }, true);
}