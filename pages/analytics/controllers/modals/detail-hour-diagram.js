export default function DetailHourDiagramCtrl ($scope, constant, modalHandler, messageListManager, dialogHandler, chartManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailHourDiagramWrap');
    $scope.isSelected = false;
    $scope.selectHistoryId = null;

    $scope.headers = constant.listHeaders.detailHours;

    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailHourDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {};
        if (query) {
            setSelectEnum(query.sendHistoryIds);
            $scope.form = query;
        }
    }

    function setSelectEnum(keys) {
        messageListManager.findByIds(keys, (status, data) => {
            if(status === 200) {
                $scope.selectEnums = data;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findHourDiagram(sendHistoryId) {
        let query = Object.assign({}, $scope.form);
        query.sendHistoryIds = sendHistoryId;
        chartManager.getChartHour(query, (status, data) => {
            if(status === 200) {
                let hours = [];
                let hourDiagram = [];

                for(let i=0; i<24; i++) {
                    hours.push(i*3);
                    hourDiagram.push(0);
                }

                for (let i=0; i<hours.length; i++) {
                    for (let j=0; j<data.length; j++) {
                        if (hours[i] === parseInt(data[j].hour)) {
                            hourDiagram[i] = parseInt(data[j].openCount);
                            data.splice(j, 1);
                            j--;
                            break;
                        }
                    }
                }

                $scope.detailHours = hourDiagram;
                $scope.isSelected = true;
            } else {
                dialogHandler.alertError(status, data);
            }
        })
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.form = {};
            $scope.selectHistoryId = null;
            $scope.isSelected = false;
            $scope.isOpen = false;
        }
    }

    $scope.$watch('selectHistoryId', (n, o) => {
        if(n !== o && n !== null) {
            findHourDiagram(n);
        }
    }, true);
}