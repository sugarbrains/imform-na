export default function DetailCouponTypeCountDiagramCtrl($scope, constant, modalHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailCouponTypeCountDiagramWrap');

    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailCouponTypeCountDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = angular.copy(query);
    }

    function findCouponTypeCountDiagram() {
        // todo find CouponTypeCount diagram
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}