export default function SentMessageListCtrl($scope, $rootScope, $filter, orderHandler, constant, dialogHandler, modalHandler, messageReceiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    const $target = $('#sentMessageListWrap');

    $scope.close = close;
    $scope.goPage = goPage;

    orderHandler.init($scope, constant.orderKeys.sentMessageList);
    $scope.headers = constant.listHeaders.sentMessageList;

    init();

    orderHandler.setSort($scope, constant.listKeys.sentMessageList);
    orderHandler.setOrderBy($scope, constant.listKeys.sentMessageList, findMessageReceiverGroup);

    $scope.$on(constant.modalKey.sentMessageList, (e, args) => {
        init(args.query);
        findMessageReceiverGroup(() => {
            $scope.isOpen = true;
            modalHandler.focus($target);
        });
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function findMessageReceiverGroup(callback) {
        orderHandler.setSort($scope, constant.listKeys.sentMessageList);

        let query = angular.copy($scope.form);

        messageReceiverGroupManager.findAll(query, (status, data) => {
            if(status === 200) {
                $scope.messageReceiverGroup = data;
                if (callback) callback();
            } else if (status === 404) {
                if (callback) callback();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {
            orderBy: 'id',
            sort: 'DESC'
        };
        $scope.receiverGroup = {};
        if (query) {
            if(query['#'] !== undefined) delete query['#'];
            $scope.receiverGroup = angular.copy(query);
            $scope.form.receiverGroupId = $scope.receiverGroup.id;
            $scope.form.size = 10;
        }
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        search();
    }
}