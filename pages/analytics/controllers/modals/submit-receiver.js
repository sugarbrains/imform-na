export default function SubmitReceiverCtrl($scope, constant, dataValue, dialogHandler, modalHandler, FileUploader, receiverManager, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#submitReceiverWrap');

    FileUploader.FileSelect.prototype.isEmptyAfterSelection = function () {
        return true;
    };

    $scope.close = close;
    $scope.selectSubmitOne = selectSubmitOne;
    $scope.selectSubmitFile = selectSubmitFile;
    $scope.clearFile = clearFile;
    $scope.submit = submit;

    $scope.enumGenders = dataValue.ENUM_SELECT_GENDERS;
    $scope.enumSidos = [];
    $scope.enumSigungus = [];
    $scope.enumEupmyeondongs = [];
    $scope.receiverGroups = {
        count: 0,
        rows: []
    };

    init();

    $scope.uploader = new FileUploader({
        onAfterAddingAll: (items) => {
            $scope.form.file = items[0]._file;
            $scope.uploader.clearQueue();
        }
    });

    $scope.$on(constant.modalKey.submitReceiver, () => {
        init();
        findReceiverGroups(() => {
            $scope.isOpen = true;
            modalHandler.focus($target);
        });
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function findReceiverGroups(callback) {
        let query = {
            orderBy: 'groupName',
            sort: 'ASC'
        };

        receiverGroupManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.receiverGroups = data;
                callback();
            } else if (status == 404) {

                callback();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }
    
    function submit () {
        // todo submit receiver
        receiverManager.create($scope.form, (status, data) => {
            if (status == 201) {
                $rootScope.$broadcast(constant.eventKey.submitReceiver);
                close();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function init() {
        $scope.isOpen = false;
        $scope.isSubmitOne = true;
        $scope.isSubmitFile = false;
        $scope.receiverGroups = {
            count: 0,
            rows: []
        };
        $scope.form = {};
    }

    function selectSubmitOne() {
        $scope.isSubmitOne = true;
        $scope.isSubmitFile = false;
    }

    function selectSubmitFile() {
        $scope.isSubmitOne = false;
        $scope.isSubmitFile = true;
    }

    function clearFile () {
        delete $scope.form.file;
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}