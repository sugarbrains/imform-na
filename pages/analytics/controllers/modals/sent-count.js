export default function SentCountCtrl ($scope, constant, modalHandler, orderHandler, sendHistoryManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#sentCountWrap');

    $scope.close = close;

    orderHandler.init($scope, constant.orderKeys.sentCount);
    $scope.headers = constant.listHeaders.sentCount;

    init();

    orderHandler.setSort($scope, constant.listKeys.sentCount);
    orderHandler.setOrderBy($scope, constant.listKeys.sentCount, findSendHistories);

    $scope.$on(constant.modalKey.sentCount, (e, args) => {
        if (args.receiverId) {
            init(args.receiverId);
            findSendHistories(() => {
                $scope.isOpen = true;
                modalHandler.focus($target);
            });
        }
    });

    modalHandler.eventBind($target, function () {
        vm.apply(function () {
            close();
        });
    });

    function findSendHistories(callback) {
        orderHandler.setSort($scope, constant.listKeys.sentCount);

        let query = angular.copy($scope.form);

        sendHistoryManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.sendHistories = data;
                if (callback) callback();
            } else if (status == 404) {

                if (callback) callback();
            } else {
                // todo dialog
            }
        });
    }

    function init(receiverId) {
        $scope.isOpen = false;
        $scope.sendHistories = {
            count: 0,
            rows: []
        };
        $scope.form = {
            receiverId: receiverId,
            orderBy: 'serialNum',
            sort: 'DESC'
        };
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}