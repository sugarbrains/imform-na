export default function RegisterReceiverGroupCtrl($scope, $rootScope, constant, dialogHandler, modalHandler, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#registerReceiverGroupWrap');

    $scope.close = close;
    $scope.register = register;

    init();

    $scope.$on(constant.modalKey.registerReceiverGroup, (e, args) => {
        if (args.receiverId) {
            init();
            findReceiverGroups(args.receiverId, () => {
                $scope.isOpen = true;
                modalHandler.focus($target);
            });
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init() {
        $scope.isOpen = false;
        $scope.receiverGroups = {
            count: 0,
            rows: []
        };
        $scope.form = {};
    }

    function register() {

    }

    function findReceiverGroups(receiverId, callback) {
        receiverGroupManager.findAll({
            receiverId: receiverId,
            orderBy: 'serialNum',
            sort: 'DESC'
        }, function (status, data) {
            if (status == 200) {
                $scope.receiverGroups = data;
                if (callback) callback();
            } else if (status == 404) {

                if (callback) callback();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}