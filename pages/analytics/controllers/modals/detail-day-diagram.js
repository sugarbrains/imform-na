export default function DetailDayDiagramCtrl ($scope, constant, modalHandler, chartManager, messageListManager, dialogHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailDayDiagramWrap');
    $scope.isSelected = false;
    $scope.selectHistoryId = null;

    $scope.headers = constant.listHeaders.detailDays;
    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailDayDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {};
        if (query) {
            setSelectEnum(query.sendHistoryIds);
            $scope.form = query;
        }
    }

    function setSelectEnum(keys) {
        messageListManager.findByIds(keys, (status, data) => {
            if(status === 200) {
                $scope.selectEnums = data;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findDayDiagram(sendHistoryId) {
        let query = Object.assign({}, $scope.form);
        query.sendHistoryIds = sendHistoryId;
        chartManager.getChartDay(query, (status, data) => {
            if (status === 200) {
                let days = [0, 1, 2, 3, 4, 5, 6];
                let dayArray = {
                    0: false,
                    1: false,
                    2: false,
                    3: false,
                    4: false,
                    5: false,
                    6: false,
                };

                for (let i=0; i<days.length; i++) {
                    for (let j=0; j<data.length; j++) {
                        if (days[i] === parseInt(data[j].day)) {
                            dayArray[i] = data[j].openCount;
                            data.splice(j, 1);
                            j--;
                            break;
                        }
                    }
                }

                for (let key in dayArray) {
                    if(!dayArray[key]) {
                        dayArray[key] = 0;
                    }
                }

                $scope.detailDay = dayArray;
                $scope.isSelected = true;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.form = {};
            $scope.selectHistoryId = null;
            $scope.isSelected = false;
            $scope.isOpen = false;
        }
    }

    $scope.$watch('selectHistoryId', (n, o) => {
        if(n !== o && n !== null) {
            findDayDiagram(n);
        }
    }, true);
}