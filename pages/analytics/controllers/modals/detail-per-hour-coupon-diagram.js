export default function DetailPerHourCouponDiagramCtrl($scope, constant, modalHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailPerHourCouponDiagramWrap');

    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailPerHourCouponDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = angular.copy(query);
    }

    function findPerHourCouponDiagram() {
        // todo find per hour coupon diagram
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}