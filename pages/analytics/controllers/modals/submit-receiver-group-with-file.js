import dialog from "../dialog";

export default function SubmitReceiverGroupWithFileCtrl($scope, $rootScope, constant, loadingHandler, dialogHandler, modalHandler, receiverGroupManager, receiverGroupImportHistoryManger) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#submitReceiverGroupWrap');

    $scope.close = close;
    $scope.submit = submit;

    init();

    $scope.$on(constant.modalKey.submitReceiverGroupWithFile, (e, args) => {
        init();
        $scope.isOpen = true;
        modalHandler.focus($target);
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init() {
        $scope.isOpen = false;
        $scope.form = {};
    }

    function submit() {
        if(!$scope.form.name) {
            return dialogHandler.show('그룹명을 입력해주세요.');
        }
        let formData = new FormData();
        formData.append("name", $scope.form.name);
        formData.append("file", $scope.form.file);

        dialogHandler.show('등록하시겠습니까?', {
            submitCallback: () => {
                loadingHandler.startLoading('submit-receiver-group');
                receiverGroupManager.create(formData, (status, data) => {
                    if (status == 201) {
                        let interval = setInterval(() => {
                            receiverGroupImportHistoryManger.findById(data.id, (status, data) => {
                                if(status === 200) {
                                    loadingHandler.loadingText('submit-receiver-group', `${data.progress}%...`);
                                    if(data.state === "complete") {
                                        clearInterval(interval);
                                        loadingHandler.endLoading('submit-receiver-group');
                                        dialogHandler.show('생성이 완료되었습니다.', {
                                            submitCallback: () => {
                                                $rootScope.$broadcast(constant.eventKey.submitReceiverGroupWithFile);
                                                close();
                                            }
                                        });
                                    } else if (data.state === "error") {
                                        clearInterval(interval);
                                        loadingHandler.endLoading('submit-receiver-group');
                                        dialogHandler.show(`그룹 생성 실패!\n사유: ${data.errorMessage}`);
                                    }
                                } else {
                                    loadingHandler.endLoading('submit-receiver-group');
                                    dialogHandler.alertError(status, data);
                                }
                            });
                        }, 1000);
                    } else {
                        loadingHandler.endLoading('submit-receiver-group');
                        dialogHandler.alertError(status, data);
                    }
                });
            }
        }, {
            cancelText: "취소",
            cancelCallback: () => {

            }
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
            $scope.form = null;
        }
    }
}