export default function DetailAddressDiagramCtrl($scope, constant, modalHandler, messageListManager, chartManager, dialogHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailAddressDiagramWrap');
    $scope.isSelected = false;
    $scope.selectHistoryId = null;

    $scope.headers = constant.listHeaders.detailAddresses;
    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailAddressDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {};
        if (query) {
            setSelectEnum(query.sendHistoryIds);
            $scope.form = query;
        }
    }

    function setSelectEnum(keys) {
        messageListManager.findByIds(keys, (status, data) => {
            if(status === 200) {
                $scope.selectEnums = data;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findAddressDiagram(sendHistoryId) {
        let query = Object.assign({}, $scope.form);
        query.sendHistoryIds = sendHistoryId;
        chartManager.getChartAddress(query, (status, data) => {
            if (status === 200) {
                $scope.detailAddresses = data;
                $scope.isSelected = true;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.form = {};
            $scope.selectHistoryId = null;
            $scope.isSelected = false;
            $scope.isOpen = false;
        }
    }

    $scope.$watch('selectHistoryId', (n, o) => {
        if(n !== o && n !== null) {
            findAddressDiagram(n);
        }
    }, true);
}