export default function ReceiverGroupCtrl($scope, constant, modalHandler, dialogHandler, orderHandler, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#receiverGroupWrap');

    $scope.close = close;
    $scope.openRegisterReceiverGroup = openRegisterReceiverGroup;
    $scope.withdraw = withdraw;

    orderHandler.init($scope, constant.orderKeys.receiverGroup);
    $scope.headers = constant.listHeaders.receiverGroup;

    init();

    orderHandler.setSort($scope, constant.listKeys.receiverGroup);
    orderHandler.setOrderBy($scope, constant.listKeys.receiverGroup, findReceiverGroups);

    $scope.$on(constant.modalKey.receiverGroup, (e, args) => {
        if (args.receiverId) {
            init(args.receiverId);
            findReceiverGroups(() => {
                $scope.isOpen = true;
                modalHandler.focus($target);
            });
        }
    });

    modalHandler.eventBind($target, function () {
        vm.apply(function () {
            close();
        });
    });

    function findReceiverGroups(callback) {
        orderHandler.setSort($scope, constant.listKeys.receiverGroup);

        let query = angular.copy($scope.form);

        receiverGroupManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.receiverGroups = data;
                if (callback) callback();
            } else if (status == 404) {
                if (callback) callback();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function openRegisterReceiverGroup() {
        modalHandler.openRegisterReceiverGroup($scope.form.receiverId);
    }

    function withdraw(receiverGroup, index) {
        // todo withdraw
    }

    function init(receiverId) {
        $scope.isOpen = false;
        $scope.receiverGroups = {
            count: 0,
            rows: []
        };
        $scope.form = {
            receiverId: receiverId,
            orderBy: 'serialNum',
            sort: 'DESC'
        };
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}