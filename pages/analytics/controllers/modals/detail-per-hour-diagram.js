export default function DetailPerHourDiagramCtrl($scope, constant, modalHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailPerHourDiagramWrap');

    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailPerHourDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = angular.copy(query);
    }

    function findPerHourDiagram() {
        // todo find per hour diagram
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}