export default function UpdateReceiverGroupCtrl ($scope, $rootScope, constant, dialogHandler, modalHandler, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#updateReceiverGroupWrap');

    $scope.close = close;
    $scope.update = update;
    $scope.remove = remove;

    init();

    $scope.$on(constant.modalKey.updateReceiverGroup, (e, args) => {
        findById(args.receiverGroupId, () => {
            // init();
            $scope.isOpen = true;
            modalHandler.focus($target);
        });
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init() {
        $scope.isOpen = false;
        $scope.form = {};
    }

    function findById(receiverGroupId, callback) {
        receiverGroupManager.findById(receiverGroupId, (status, data) => {
            if (status == 200) {
                $scope.form = data;
                if (callback) callback();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function update() {
        receiverGroupManager.updateById($scope.form, (status, data) => {
            if (status == 204) {
                dialogHandler.show('수정이 완료되었습니다.', {
                    submitCallback: () => {
                        $rootScope.$broadcast(constant.eventKey.updateReceiverGroup, {
                            receiverGroup: angular.copy($scope.form)
                        });
                        close();
                    }
                });
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function remove() {
        dialogHandler.show('해당 그룹을 삭제하시겠습니까?', {
            submitCallback: () => {
                receiverGroupManager.remove($scope.form, (status, data) => {
                    if(status === 204) {
                        dialogHandler.show('그룹이 삭제되었습니다.', {
                            submitCallback: () => {
                                $rootScope.$broadcast(constant.eventKey.removeReceiverGroup);
                                close();
                            }
                        });
                    } else {
                        dialogHandler.alertError(status, data);
                    }
                });
            }
        }, {
            cancelText: "취소"
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            init();
        }
    }
}