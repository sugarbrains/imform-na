export default function InsertPhoneNumCtrl ($scope, $rootScope, constant, modalHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#insertPhoneNumWrap');

    $scope.close = close;
    $scope.send = send;

    init();

    $scope.$on(constant.modalKey.insertPhoneNum, () => {
        init();
        $scope.isOpen = true;
        modalHandler.focus($target);
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init() {
        $scope.isOpen = false;
        $scope.form = {};
    }

    function send() {
        /**
         * phoneNum exp
         */
        $rootScope.$broadcast(constant.eventKey.insertPhoneNum, {
            phoneNum: $scope.form.phoneNum
        });
        close();
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}