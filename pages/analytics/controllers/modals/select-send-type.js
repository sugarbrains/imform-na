export default function SelectSendTypeCtrl($scope, constant, dataValue, dialogHandler, modalHandler, enumHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#selectSendTypeWrap');

    $scope.close = close;
    $scope.selectSendTypeDirect = selectSendTypeDirect;
    $scope.selectSendTypeBook = selectSendTypeBook;
    $scope.send = send;

    $scope.enumHours = enumHandler.returnHours();
    $scope.enumMinutes = enumHandler.returnMinutes();

    init();

    $scope.$on(constant.modalKey.selectSendType, (e, args) => {
        if (args.sendHistoryId) {
            init();
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function selectSendTypeDirect() {
        $scope.form = {
            isSendTypeDirect: true
        };
    }

    function selectSendTypeBook() {
        $scope.form = {
            isSendTypeBook: true
        };
    }

    function init() {
        $scope.isOpen = false;
        $scope.form = {
            isSendTypeDirect: true
        };
    }

    function send() {
        let form = $scope.form;
        if (form.isSendTypeDirect) {
            // todo send direct
        } else if (form.isSendTypeBook && form.sendDate && form.sendHour && form.sendMinute) {
            // todo send book
        } else {
            dialogHandler.show('필수 입력값이 없습니다.');
        }
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}