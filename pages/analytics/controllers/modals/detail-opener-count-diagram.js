export default function DetailOpenerCountDiagramCtrl($scope, constant, modalHandler, chartManager, messageListManager, orderHandler, dialogHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailOpenerCountDiagramWrap');

    $scope.close = close;
    $scope.isSelected = false;
    $scope.selectHistoryId = null;

    orderHandler.init($scope, constant.orderKeys.detailOpenerCount);
    $scope.headers = constant.listHeaders.detailOpenerCountDiagram;

    init();

    orderHandler.setSort($scope, constant.listKeys.detailOpenerCount);
    orderHandler.setOrderBy($scope, constant.listKeys.detailOpenerCount);

    $scope.$on(constant.modalKey.detailOpenerCountDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {};
        if (query) {
            setSelectEnum(query.sendHistoryIds);
            $scope.form = query;
        }
    }

    function setSelectEnum(keys) {
        messageListManager.findByIds(keys, (status, data) => {
            if(status === 200) {
                $scope.selectEnums = data;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findOpenerCountDiagram(sendHistoryId) {
        let query = Object.assign({}, $scope.form);
        query.sendHistoryIds = sendHistoryId;
        chartManager.getChartOpenerCount(query, (status, data) => {
            if (status === 200) {
                $scope.detailOpenerCountData = data;
                $scope.isSelected = true;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.form = {};
            $scope.selectHistoryId = null;
            $scope.isSelected = false;
            $scope.isOpen = false;
        }
    }

    $scope.$watch('selectHistoryId', (n, o) => {
        if(n !== o && n !== null) {
            findOpenerCountDiagram(n);
        }
    }, true);
}