export default function DetailPerDayDiagramCtrl($scope, constant, modalHandler) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailPerDayDiagramWrap');

    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailPerDayDiagram, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = angular.copy(query);
    }

    function findPerDayDiagram() {
        // todo find per day diagram
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.isOpen = false;
        }
    }
}