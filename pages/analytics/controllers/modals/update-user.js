export default function UpdateUserCtrl ($scope, $rootScope, constant, dialogHandler, modalHandler, messageHandler, domainManager, userManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#updateUserWrap');
    const ROLE_ADMIN = 'ADMIN';

    $scope.close = close;
    $scope.update = update;

    init();


    $scope.$on(constant.modalKey.updateUser, (e, args) => {
        findById(args.userId, () => {
            // init(args.userId);
            $scope.isOpen = true;
            if(vm.session.role === ROLE_ADMIN) findDomains();
            $scope.enumRoles = constant.enumRolesByRole[vm.session.role];
            modalHandler.focus($target);
        });
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init() {
        $scope.isOpen = false;
        $scope.form = {};
        $scope.domains = null;
    }

    function findDomains() {
        domainManager.findAll((status, data) => {
            if(status === 200) {
                $scope.domains = data;
            } else if (status === 404) {
                $scope.domains = null;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findById(userId, callback) {
        userManager.findById(userId, (status, data) => {
            if (status == 200) {
                if(data.sms) data.sms = parseFloat(data.sms);
                if(data.lms) data.lms = parseFloat(data.lms);
                $scope.form = data;
                if (callback) callback();
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function update() {
        if(!messageHandler.checkUrl($scope.form.url)) {
            return dialogHandler.show('URL 형식이 잘못됐습니다.');
        }
        userManager.updateById($scope.form, (status, data) => {
            if (status == 204) {
                dialogHandler.show('수정이 완료되었습니다.', {
                    submitCallback: () => {
                        $rootScope.$broadcast(constant.eventKey.updateUser, {});
                        close();
                    }
                });
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }


    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            init();
        }
    }
}