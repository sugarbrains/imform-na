export default function DetailOpenerCountCtrl($scope, constant, modalHandler, chartManager, dialogHandler, messageListManager) {
    'ngInject';

    let vm = $scope.vm;
    const $target = $('#detailOpenerCountWrap');
    $scope.isSelected = false;
    $scope.selectHistoryId = null;

    $scope.headers = constant.listHeaders.detailOpenerCount;
    $scope.close = close;

    init();

    $scope.$on(constant.modalKey.detailOpenerCount, (e, args) => {
        if (args.query) {
            init(args.query);
            $scope.isOpen = true;
            modalHandler.focus($target);
        }
    });

    modalHandler.eventBind($target, () => {
        vm.apply(() => {
            close();
        });
    });

    function init(query) {
        $scope.isOpen = false;
        $scope.form = {};
        if (query) {
            setSelectEnum(query.sendHistoryIds);
            $scope.form = query;
        }
    }

    function setSelectEnum(keys) {
        messageListManager.findByIds(keys, (status, data) => {
            if(status === 200) {
                $scope.selectEnums = data;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }
    
    function findOpenerCount(sendHistoryId) {
        let query = Object.assign({}, $scope.form);
        query.sendHistoryIds = sendHistoryId;
        chartManager.getChartOpenerCount(query, (status, data) => {
            if (status === 200) {
                data.avgOpenCount = parseInt(data.openerCount) ? (data.openCount / parseInt(data.openerCount)) : 0
                $scope.detailOpenerCountData = data;
                $scope.isSelected = true;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function close(isOverlay) {
        if ((isOverlay && constant.modalOverlayClose) || !isOverlay) {
            $scope.form = {};
            $scope.selectHistoryId = null;
            $scope.isSelected = false;
            $scope.isOpen = false;
        }
    }

    $scope.$watch('selectHistoryId', (n, o) => {
        if(n !== o && n !== null) {
            findOpenerCount(n);
        }
    }, true);
}