export default function NavigationCtrl ($scope, constant, navigator) {
    'ngInject';

    let vm = $scope.vm;

    $scope.toggle = toggle;
    $scope.goToReceiverGroupManage = goToReceiverGroupManage;
    $scope.goToReceiverManage = goToReceiverManage;
    $scope.goToUserManage = goToUserManage;
    $scope.goToSendManage = goToSendManage;
    $scope.goToMessageManage = goToMessageManage;
    $scope.goToSendAnalysis = goToSendAnalysis;
    $scope.goToSenderAnalysis = goToSenderAnalysis;
    $scope.goToCalculationManage = goToCalculationManage;

    $scope.possibleAdministrationRoles = constant.possibleAdministrationRoles;

    function toggle () {
        vm.toggleNav = false;
    }

    function goToReceiverGroupManage() {
        navigator.goToReceiverGroupManage(null, true);
    }

    function goToReceiverManage() {
        navigator.goToReceiverManage(null, true);
    }

    function goToUserManage() {
        navigator.goToUserManage(null, true);
    }

    function goToSendManage() {
        navigator.goToSendManage(null, true);
    }

    function goToMessageManage() {
        navigator.goToMessageManage(null, true);
    }

    function goToSendAnalysis() {
        navigator.goToSendAnalysis(null, true);
    }

    function goToSenderAnalysis() {
        navigator.goToSenderAnalysis(null, true);
    }

    function goToCalculationManage() {
        navigator.goToCalculationManage(null, true);
    }

    $scope.$on('$locationChangeStart', function () {
        toggle();
    });
}