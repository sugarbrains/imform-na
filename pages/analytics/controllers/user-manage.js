export default function UserManageCtrl ($scope, $stateParams, $filter, constant, dataValue, navigator, dialogHandler, modalHandler, stateHandler, orderHandler, userManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    const possibleAdministrationRoles = constant.possibleAdministrationRoles;

    stateHandler.setState(vm);
    stateHandler.setGroupTitle(vm);

    $scope.search = search;
    $scope.refresh = refresh;
    $scope.goPage = goPage;
    $scope.openUpdateUserModal = openUpdateUserModal;
    $scope.pagination = pagination;

    $scope.templatePath = '/pages/analytics/views/contents/user-manage/';

    $scope.enumSearchFields = [{
        value: 'uid',
        text: '아이디'
    }, {
        value: 'name',
        text: '이름'
    }];
    $scope.currentTab = 1;
    $scope.enumSizes = dataValue.ENUM_SIZES;

    orderHandler.init($scope, constant.orderKeys.userManage);
    $scope.headers = constant.listHeaders.userManage;

    $scope.users = {
        count: 0,
        rows: []
    };

    let selectQuery = generateForm();

    vm.getSession(() => {
        if(!possibleAdministrationRoles[vm.session.role]) {
            return dialogHandler.show('관리자만 접근 가능합니다.', {
                submitCallback: () => {
                    navigator.goToReceiverGroupManage();
                }
            })
        }
        else {
            findUsers(selectQuery);

            $scope.$watch('form.size', (n, o) => {
                if (n != o) {
                    search();
                }
            }, true);

            $scope.$on(constant.eventKey.updateUser, () => {
                search();
            });

            orderHandler.setOrderBy($scope, constant.listKeys.userManage, search);
        }
    });

    function findUsers(query) {
        userManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.users = data;
            } else if (status == 404) {

            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (!form.size) form.size = dataValue.DEFAULT_SIZE;

        $scope.form = form;

        orderHandler.setSort($scope, constant.listKeys.userManage);

        return angular.copy($scope.form);
    }

    function generateQuery() {
        return angular.copy($scope.form);
    }

    function openUpdateUserModal(userId) {
        modalHandler.openUpdateUser(userId);
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        pagination();
    }

    function refresh() {
        let form = angular.copy(constant.defaultQuery.userManage);
        form.size = $scope.form.size;
        $scope.form = form;
        search();
    }

    function search() {
        $scope.form.offset = 0;
        navigator.goToUserManage(generateQuery(), true);
    }

    function pagination() {
        navigator.goToUserManage(generateQuery(), true);
    }
}