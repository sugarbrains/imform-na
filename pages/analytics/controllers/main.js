export default function MainCtrl ($scope, $rootScope, dialogHandler, loadingHandler, modalHandler, sessionManager) {
    'ngInject';

    let vm = $scope.vm = {};

    modalHandler.init($rootScope);
    dialogHandler.init($rootScope);
    loadingHandler.init($rootScope);

    vm.apply = apply;
    vm.getSession = getSession;

    vm.toggleNav = false;
    vm.session = null;

    $scope.templatePath = '/pages/analytics/views/';
    $scope.modalTemplatePath = '/pages/analytics/views/modals/';

    function getSession (successCallback, failCallback) {
        sessionManager.getSession((status, data) => {
            if (status === 200) {
                vm.session = data;
                if (successCallback) successCallback();
            } else {
                vm.session = null;
                if (failCallback) failCallback();
                else location.href = '/';
            }
        });
    }

    function apply (callback) {
        if ($scope.$$phase === '$apply' || $scope.$$phase === '$digest') {
            callback();
        } else {
            $scope.$apply(function () {
                callback();
            });
        }
    }
}