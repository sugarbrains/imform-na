export default function MessageManageCtrl($scope, $stateParams, $filter, navigator, constant, dataValue, dialogHandler, modalHandler, stateHandler, orderHandler, messageManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    let dateFilter = $filter('date');
    let selectedIndex = null;

    stateHandler.setState(vm);
    stateHandler.setGroupTitle(vm);

    $scope.search = search;
    $scope.refresh = refresh;
    $scope.goToSendManage = goToSendManage;
    $scope.openSelectSendType = openSelectSendType;
    $scope.goPage = goPage;
    $scope.activeTab = activeTab;
    $scope.pagination = pagination;

    $scope.templatePath = '/pages/analytics/views/contents/message-manage/';

    $scope.enumSearchFields = [{
        value: 'title',
        text: '메시지 이름'
    }, {
        value: 'name',
        text: '그룹 이름'
    }];
    $scope.currentTab = 1;

    $scope.enumSizes = dataValue.ENUM_SIZES;
    $scope.enumMessageManageStates = constant.enumMessageManageStates;

    orderHandler.init($scope, constant.orderKeys.messageManage);
    $scope.headers = constant.listHeaders.messageManage;

    $scope.sendHistories = {
        count: 0,
        rows: []
    };

    let selectQuery = generateForm();

    vm.getSession(() => {
        findSendHistories(selectQuery);

        $scope.$watch('form.size', (n, o) => {
            if (n != o) {
                search();
            }
        }, true);

        $scope.$on(constant.eventKey.sendPost, () => {
            search();
        });

        orderHandler.setOrderBy($scope, constant.listKeys.messageManage, search);
    });

    function findSendHistories(query) {
        messageManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.messages = data;
            } else if (status == 404) {

            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function openSelectSendType(sendHistory, index) {
        selectedIndex = index;
        modalHandler.openSentManage(sendHistory);
    }

    function goToSendManage(sendHistory, index) {
        if (sendHistory) {
            selectedIndex = index;
            navigator.goToSendManage({
                id: sendHistory.id,
                state: vm.state,
            }, true);
        } else {
            navigator.goToSendManage({
                state: vm.state
            }, true);
        }
    }

    function setDateMinLimit(startDate) {
        let d = new Date(startDate);
        d.setDate(d.getDate()-1);
        $scope.dateMinLimit = dateFilter(new Date(d), 'yyyy-MM-dd');
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (!form.size) form.size = dataValue.DEFAULT_SIZE;

        $scope.form = form;

        if($scope.form.startDate) {
            setDateMinLimit($scope.form.startDate);
        }

        orderHandler.setSort($scope, constant.listKeys.messageManage);

        return angular.copy($scope.form);
    }

    function generateQuery() {
        return angular.copy($scope.form);
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        pagination();
    }

    function refresh() {
        let form = angular.copy(constant.defaultQuery.messageManage);
        form.size = $scope.form.size;
        $scope.form = form;
        search();
    }

    function search() {
        $scope.form.offset = 0;
        navigator.goToMessageManage(generateQuery(), true);
    }

    function activeTab(tab) {
        $scope.currentTab = tab;
    }

    function pagination() {
        navigator.goToMessageManage(generateQuery(), true);
    }

    $scope.$watch('form.startDate', (n, o) => {
        if(n !== o && n !== undefined) {
            setDateMinLimit(n);
            if($scope.form.endDate !== undefined) {
                let startDate = new Date(n).getTime();
                let endDate = new Date($scope.form.endDate).getTime();

                if(startDate > endDate) {
                    $scope.form.endDate = null;
                }
            }
        }
    });
}