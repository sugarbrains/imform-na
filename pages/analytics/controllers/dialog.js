export default function DialogCtrl($scope, $timeout, constant, dataValue) {
    'ngInject';

    let vm = $scope.vm;
    const dialogKey = constant.dialogKey;
    const STATES = dataValue.STATES;
    const SUBMIT_TEXT = dataValue.defaultDialogSubmitText;
    
    $scope.cancel = cancel;
    $scope.submit = submit;

    init();

    $scope.$on(dialogKey.show, (e, args) => {
        if (args.text) {
            initShow(args);
        }
    });

    $scope.$on(dialogKey.alertError, (e, args) => {
        if (args.status) {
            initAlertError(args);
        }
    });

    function initShow(options) {
        $scope.text = options.text;
        if (options.submitOptions) {
            $scope.submitText = options.submitOptions.submitText ? options.submitOptions.submitText : SUBMIT_TEXT;
            $scope.submitCallback = options.submitOptions.submitCallback;
        }
        if (options.cancelOptions) {
            $scope.cancelText = options.cancelOptions.cancelText;
            $scope.cancelCallback = options.cancelOptions.cancelCallback;
        }
        $scope.isOpen = true;
    }

    function initAlertError(options) {
        $scope.text = options.data ? options.data.message || STATES[options.status] : STATES[options.status];
        $scope.isOpen = true;
    }

    function init() {
        $scope.isOpen = false;
        $scope.text = null;
        $scope.submitText = SUBMIT_TEXT;
        $scope.submitCallback = null;
        $scope.cancelText = null;
        $scope.cancelCallback = null;
    }
    
    function cancel() {
        if ($scope.cancelCallback) {
            $scope.cancelCallback();
        }
        init();
    }
    
    function submit() {
        if ($scope.submitCallback) {
            $scope.submitCallback();
        }
        init();
    }
}