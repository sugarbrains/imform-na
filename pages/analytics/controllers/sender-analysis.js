export default function SenderAnalysisCtrl ($scope, $stateParams, $filter, $cookies, $timeout, $document, $state, constant, dataValue, navigator, dialogHandler, stateHandler, modalHandler, orderHandler, senderHistoryManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    let checkArrayValue = $filter('checkArrayValue');
    const CHECK_SIZE = constant.checkItemStates.senderAnalysis.size;
    const CHECK_KEY = constant.checkItemStates.senderAnalysis.key;
    const CHECK_OBJ_KEY = constant.checkItemStates.senderAnalysis.objKey;

    stateHandler.setState(vm);
    stateHandler.setGroupTitle(vm);

    $scope.search = search;
    $scope.refresh = refresh;
    $scope.goPage = goPage;
    $scope.totalCheck = totalCheck;
    $scope.checkItem = checkItem;
    $scope.goToSendAnalysisWithSenderIds = goToSendAnalysisWithSenderIds;
    $scope.goToSingleSendAnalysis = goToSingleSendAnalysis;

    $scope.templatePath = '/pages/analytics/views/contents/sender-analysis/';

    $scope.enumSearchFields = [{
        value: 'name',
        text: '이름'
    }];
    $scope.enumSizes = dataValue.SENDER_ANALYSIS_ENUM_SIZES;

    orderHandler.init($scope, constant.orderKeys.senderAnalysis);
    $scope.headers = constant.listHeaders.senderAnalysis;

    $scope.currentTab = 1;
    $scope.check = {
        total: false
    };
    $scope.senders = {
        count: 0,
        rows: []
    };

    let selectQuery = generateForm();

    vm.getSession(() => {
        findSenders(selectQuery);

        $scope.$watch('form.size', (n, o) => {
            if (n != o) {
                search();
            }
        }, true);

        orderHandler.setOrderBy($scope, constant.listKeys.senderAnalysis, search);
    });

    function findSenders(query) {
        senderHistoryManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.senders = data;
                checkGetCookies();
            } else if (status == 404) {
                $scope.sender = null;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function goToSendAnalysisWithSenderIds() {
        let senderIds = [];
        let getCookies = $cookies.get(CHECK_KEY);

        if(getCookies) {
            senderIds = getCookies.split(',');
        }

        if (senderIds.length) {
            let query = angular.copy(constant.defaultQuery.sendAnalysis);
            query.senderIds = senderIds.join(',');
            $state.go('sendAnalysis', query, {
                reload: true
            }).then(() => {
                $document.scrollTop(0, 500);
            })
        } else {
            dialogHandler.show('선택된 발송자가 없습니다.');
        }
    }

    function goToSingleSendAnalysis(item) {
        let query = angular.copy(constant.defaultQuery.sendAnalysis);

        query.senderIds = item.id;
        $cookies.put(CHECK_OBJ_KEY, JSON.stringify([item]));
        $state.go('sendAnalysis', query, {
            reload: true
        }).then(() => {
            $document.scrollTop(0, 500);
        });
    }

    function totalCheck(isTotalCheck) {
        if(isTotalCheck) {
            let getCookies = $cookies.get(CHECK_KEY);
            let getObjCookies = $cookies.get(CHECK_OBJ_KEY);
            let beforeCheckSize = 0;
            if(getCookies) {
                getCookies = getCookies.split(',');
                getObjCookies = JSON.parse(getObjCookies);
            } else {
                getCookies = [];
                getObjCookies = [];
            }
            const getCookieSize = getCookies ? getCookies.length : 0;
            for(let i=0; i<$scope.senders.rows.length; i++) {
                if($scope.senders.rows[i].isCheck) {
                    beforeCheckSize++;
                }
            }
            const newCheckSize = $scope.senders.rows.length - beforeCheckSize;
            if((getCookieSize + newCheckSize) > CHECK_SIZE) {
                $timeout(() => {
                    $scope.check.total = !isTotalCheck;
                });
                return dialogHandler.show(`항목은 최대 ${CHECK_SIZE}개까지 선택 가능합니다.`);
            }

            let rows = $scope.senders.rows;
            for (let i=0; i<rows.length; i++) {
                if(getCookies.indexOf(rows[i].id) === -1) {
                    getCookies.push(rows[i].id);
                    getObjCookies.push({
                        id: rows[i].id,
                        name: rows[i].name
                    });
                }
                rows[i].isCheck = isTotalCheck;
            }
            $cookies.put(CHECK_KEY, getCookies.join(','));
            $cookies.put(CHECK_OBJ_KEY, JSON.stringify(getObjCookies));
        } else {
            let rows = $scope.senders.rows;
            let getCookies = $cookies.get(CHECK_KEY);
            let getObjCookies = $cookies.get(CHECK_OBJ_KEY);

            if(getCookies) {
                getCookies = getCookies.split(',');
                getObjCookies = JSON.parse(getObjCookies);
            }

            for (let i=0; i<rows.length; i++) {
                if(getCookies) {
                    if(getCookies.indexOf(rows[i].id) !== -1) {
                        const index = getCookies.indexOf(rows[i].id);
                        getCookies.splice(index, 1);
                        getObjCookies.splice(index, 1);
                    }
                }
                rows[i].isCheck = isTotalCheck;
            }
            $cookies.put(CHECK_KEY, getCookies.join(','));
            $cookies.put(CHECK_OBJ_KEY, JSON.stringify(getObjCookies));
        }
    }

    function checkItem(isCheck, index) {
        let getCookies = $cookies.get(CHECK_KEY);
        if(getCookies) {
            getCookies = getCookies.split(',');
            if(getCookies.length > CHECK_SIZE || (isCheck && getCookies.length === CHECK_SIZE)) {
                $scope.senders.rows[index].isCheck = false;
                return dialogHandler.show(`항목은 최대 ${CHECK_SIZE}개까지 선택 가능합니다.`);
            }
        }
        $scope.senders.rows[index].isCheck = isCheck;
        $scope.check.total = checkArrayValue($scope.senders.rows, true, 'isCheck');
        checkSetCookies(isCheck, $scope.senders.rows[index]);
    }

    //쿠키 셋 하는 로직, ids와 별도로 obj배열도 저장함
    function checkSetCookies(isCheck, item) {
        let cookieArr = [];
        let getCookies = $cookies.get(CHECK_KEY);
        let objCookiesArr = [];
        let getObjCookies = $cookies.get(CHECK_OBJ_KEY);

        if(getCookies) {
            cookieArr = getCookies.split(',');
            objCookiesArr = JSON.parse(getObjCookies);
        }

        if(isCheck) {
            cookieArr.push(item.id);
            objCookiesArr.push({
                id: item.id,
                name: item.name
            });
        } else {
            cookieArr.splice(cookieArr.indexOf(item.id), 1);
            for(let i=0; i<objCookiesArr.length; i++) {
                if(objCookiesArr[i].id === item.id) {
                    objCookiesArr.splice(i, 1);
                    break;
                }
            }
        }

        $cookies.put(CHECK_KEY, cookieArr.join(','));
        $cookies.put(CHECK_OBJ_KEY, JSON.stringify(objCookiesArr));
    }

    //첨에체크시켜주는로직, find돌때 같이돔
    function checkGetCookies() {
        let getCookies = $cookies.get(CHECK_KEY);
        if(getCookies) {
            getCookies = getCookies.split(',');
            for(let i=0; i<$scope.senders.rows.length; i++) {
                if(getCookies.indexOf($scope.senders.rows[i].id) !== -1) {
                    $scope.senders.rows[i].isCheck = true;
                }
            }
        }
        $scope.check.total = checkArrayValue($scope.senders.rows, true, 'isCheck');
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (!form.size) form.size = dataValue.SENDER_ANALYSIS_DEFAULT_SIZE;

        $scope.form = form;

        orderHandler.setSort($scope, constant.listKeys.senderAnalysis);

        return angular.copy($scope.form);
    }

    function generateQuery() {
        return angular.copy($scope.form);
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        pagination();
    }

    function refresh() {
        let form = angular.copy(constant.defaultQuery.senderAnalysis);
        form.size = $scope.form.size;
        $scope.form = form;
        search();
    }

    function goToSenderAnalysis(query, reload) {
        let param = query ? query : generateQuery();

        $state.go('senderAnalysis', param, {
            reload: reload
        }).then(() => {
            $document.scrollTop(0, 500);
        });
    }

    function search() {
        $scope.form.offset = 0;
        goToSenderAnalysis();
    }

    function pagination() {
        goToSenderAnalysis();
    }
}