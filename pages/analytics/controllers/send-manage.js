import dialog from "./dialog";

export default function SendManageCtrl($scope, $timeout, $stateParams, navigator, constant, dataValue, stateHandler, dialogHandler, modalHandler, enumHandler, messageHandler, sendHistoryManager, messageManager, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    const AD_MESSAGE = '(광고)';

    stateHandler.setState(vm, $stateParams);
    stateHandler.setGroupTitle(vm, $stateParams);
    messageHandler.setPosition();

    $scope.saveMessage = saveMessage;
    $scope.insertUrl = insertUrl;
    $scope.setPosition = setPosition;
    $scope.removeItem = removeItem;
    $scope.addPageButton = addPageButton;
    $scope.removePageButton = removePageButton;

    $scope.sizeUrl = dataValue.SIZE_URL;
    $scope.templatePath = '/pages/analytics/views/contents/send-manage/';

    $scope.pageButtons = [];
    $scope.selectedReceiverGroups = [];
    $scope.receiverGroups = {
        count: 0,
        rows: []
    };
    $scope.bytes = 0;
    $scope.preview = '';
    $scope.form = {
        isAd: false,
        message: '',
        url: 'http://'
    };

    vm.getSession(() => {
        if ($stateParams.id) {
            findSendHistory($stateParams.id);
        } else {
            $scope.form.isRefuse = !!vm.session.refuseMessage;
            $scope.form.refuseMessage = vm.session.refuseMessage || '';
            if(window.buttonPresets.length) {
                $scope.pageButtons = window.buttonPresets;
            }
            findReceiverGroups();
        }
    });

    function findSendHistory(sendHistoryId) {
        messageManager.findById(sendHistoryId, (status, data) => {
            if (status === 200) {
                setMessageData(data, () => {
                    findReceiverGroups();
                });
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findReceiverGroups() {
        let query = {};
        receiverGroupManager.findAll(query, (status, data) => {
            if (status === 200) {
                let receiverGroups = data;
                if($stateParams.id) {
                    if($scope.selectedReceiverGroups.length) {
                        let ids = $scope.selectedReceiverGroups.map((item) => {
                            return item.id;
                        });
                        for(let i=0; i<receiverGroups.rows.length; i++) {
                            if(ids.indexOf(receiverGroups.rows[i].id) !== -1) {
                                ids.splice(ids.indexOf(receiverGroups.rows[i].id), 1);
                                receiverGroups.rows.splice(i, 1);
                                i--;
                            }
                        }
                    }
                }
                $scope.receiverGroups = receiverGroups;
            } else if (status === 404) {

            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    $scope.$watch('form.groupId', (n, o) => {
        if (n != o && n) {
            let rows = $scope.receiverGroups.rows;
            for (let i=0; i<rows.length; i++) {
                if (rows[i].id == n) {
                    $scope.selectedReceiverGroups.push(rows.splice(i, 1)[0]);
                    $scope.form.groupId = '';
                    break;
                }
            }
        }
    }, true);

    $scope.$watch('form.isAd', (n, o) => {
        if (n != o) {
            let message = $scope.form.message;
            let isRefuse = $scope.form.isRefuse;
            let refuseMessage = $scope.form.refuseMessage;
            $scope.preview = messageHandler.generatePreview(message, undefined, isRefuse ? refuseMessage : '', n ? AD_MESSAGE : '');
            $scope.bytes = messageHandler.getBytes(message, isRefuse ? refuseMessage : '', n ? AD_MESSAGE : '');
        }
    }, true);

    $scope.$watch('form.message', (n, o) => {
        if (n != o) {
            let isRefuse = $scope.form.isRefuse;
            let refuseMessage = $scope.form.refuseMessage;
            let isAd = $scope.form.isAd;
            messageHandler.setPosition();
            $scope.preview = messageHandler.generatePreview(n, undefined, isRefuse ? refuseMessage : '', isAd ? AD_MESSAGE : '');
            $scope.bytes = messageHandler.getBytes(n, isRefuse ? refuseMessage : '', isAd ? AD_MESSAGE : '');
        }
    }, true);

    $scope.$watch('form.refuseMessage', (n, o) => {
        if (n != o) {
            let message = $scope.form.message;
            let isRefuse = $scope.form.isRefuse;
            let isAd = $scope.form.isAd;
            $scope.preview = messageHandler.generatePreview(message, undefined, isRefuse ? n : '', isAd ? AD_MESSAGE : '');
            $scope.bytes = messageHandler.getBytes(message, isRefuse ? n : '', isAd ? AD_MESSAGE : '');
        }
    }, true);

    $scope.$watch('form.isRefuse', (n, o) => {
        if (n != o) {
            let message = $scope.form.message;
            let refuseMessage = $scope.form.refuseMessage;
            let isAd = $scope.form.isAd;
            $scope.preview = messageHandler.generatePreview(message, undefined, n ? refuseMessage : '', isAd ? AD_MESSAGE : '');
            $scope.bytes = messageHandler.getBytes(message, n ? refuseMessage : '', isAd ? AD_MESSAGE : '');
        }
    }, true);

    function setMessageData(data, callback) {
        $scope.form = {
            state: data.state,
            isAd: (data.message.indexOf(AD_MESSAGE) === 0),
            message: data.message.replace(AD_MESSAGE + '\n', ''),
            url: data.redirectUrl ? data.redirectUrl : 'http://',
            isRefuse: !!data.refuseMessage,
            refuseMessage: data.refuseMessage ? data.refuseMessage : "",
            title: data.title ? data.title : "",
            title2: data.title2 ? data.title2 : ""
        };

        if(data.buttons && data.buttons.length) {
            $scope.pageButtons = data.buttons;
        }

        if(data.receiverGroups) {
            $scope.selectedReceiverGroups = [];
            data.receiverGroups.forEach(item => {
                $scope.selectedReceiverGroups.push(item);
            });
        }

        callback();
    }

    function saveMessage() {
        if(!$scope.form.message) {
            return dialogHandler.show('메시지 내용이 없습니다.');
        }
        let form = {};

        form.message = $scope.form.message;
        if ($scope.form.isAd) form.message = AD_MESSAGE + '\n' + form.message;
        if($scope.form.title !== undefined) form.title = $scope.form.title;
        if($scope.form.title2 !== undefined) form.title2 = $scope.form.title2;
        if(!messageHandler.checkUrl($scope.form.url)) {
            return dialogHandler.show('URL 형식이 잘못됐습니다.');
        }
        if($scope.form.url !== undefined) form.redirectUrl = $scope.form.url;
        if($scope.form.isRefuse && $scope.form.refuseMessage) {
            form.refuseMessage = $scope.form.refuseMessage;
        }
        if($scope.selectedReceiverGroups.length) {
            let receiverGroupArrays = $scope.selectedReceiverGroups.map((item, index) => {
                return item.id;
            });

            form.receiverGroupIds = receiverGroupArrays.join(',');
        }

        if($scope.pageButtons.length) {
            let buttons = [];
            $scope.pageButtons.forEach(item => {
                if(item.title) {
                    buttons.push(item.title);
                }
            });
            form.buttons = buttons.join(',');
        }

        if($stateParams.id) {
            if($scope.form.state !== 'standby') {
                dialogHandler.show('발송 및 추출 전 상태에서만 수정이 가능합니다');
            } else {

                messageManager.update($stateParams.id, form, (status, data) => {
                    if(status === 204) {
                        dialogHandler.show('메시지가 수정되었습니다.', {
                            submitCallback: () => {
                                navigator.goToMessageManage();
                            }
                        });
                    } else {
                        dialogHandler.alertError(status, data);
                    }
                });
            }
        } else {
            messageManager.create(form, (status, data) => {
                if(status === 201) {
                    dialogHandler.show('메시지가 저장되었습니다.', {
                        submitCallback: () => {
                            navigator.goToMessageManage();
                        }
                    });
                } else {
                    dialogHandler.alertError(status, data);
                }
            });
        }
    }

    function insertUrl() {
        if (!$scope.form.url || !messageHandler.checkUrl($scope.form.url)) {
            dialogHandler.show('URL 형식이 잘못됐습니다.');
            return false;
        }

        if (messageHandler.checkHasUrl($scope.form.message)) {
            dialogHandler.show('이미 URL을 입력하셨습니다.');
        } else {
            const message = messageHandler.getUrlMessage($scope.form.message);
            if (message) {
                $scope.form.message = message;
            } else {
                return false;
            }
        }
    }

    function setPosition() {
        $timeout(messageHandler.setPosition);
    }

    function removeItem(index) {
        $scope.receiverGroups.rows.push($scope.selectedReceiverGroups.splice(index, 1)[0]);
    }

    function addPageButton() {
        $scope.pageButtons.push({
            title: ''
        });
    }

    function removePageButton(item, index) {
        $scope.pageButtons.splice(index, 1);
    }
}