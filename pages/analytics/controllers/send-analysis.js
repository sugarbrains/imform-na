export default function SendAnalysisCtrl ($scope, $stateParams, $rootScope, $filter, $timeout, $cookies, $state, $document, constant, dataValue, navigator, dialogHandler, modalHandler, stateHandler, orderHandler, dateHandler, sendHistoryManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    let dateFilter = $filter('date');
    let checkArrayValue = $filter('checkArrayValue');
    const CHECK_SIZE = constant.checkItemStates.sendAnalysis.size;
    const CHECK_KEY = constant.checkItemStates.sendAnalysis.key;
    const CHECK_OBJ_KEY = constant.checkItemStates.senderAnalysis.objKey;

    stateHandler.setState(vm);
    stateHandler.setGroupTitle(vm);

    $scope.search = search;
    $scope.refresh = refresh;
    $scope.activeTab = activeTab;
    $scope.goPage = goPage;
    $scope.totalCheck = totalCheck;
    $scope.checkItem = checkItem;
    $scope.goToChart = goToChart;
    $scope.goToSingleChart = goToSingleChart;
    $scope.pagination = pagination;

    $scope.templatePath = '/pages/analytics/views/contents/send-analysis/';
    $scope.enumMessageManageStates = constant.enumMessageManageStates;
    $scope.enumSearchFields = [{
        value: 'id',
        text: 'No.'
    }, {
        value: 'name',
        text: '발송자'
    }, {
        value: 'title',
        text: '메시지 이름'
    }];
    $scope.enumSizes = dataValue.ENUM_SIZES;

    orderHandler.init($scope, constant.orderKeys.sendAnalysis);
    $scope.headers = constant.listHeaders.sendAnalysis;

    $scope.currentTab = 1;
    $scope.check = {
        total: false
    };
    $scope.sendHistories = {
        count: 0,
        rows: []
    };

    $rootScope.$on('$stateChangeStart',() => {
        console.log("start");
    });

    let selectQuery = generateForm();

    vm.getSession(() => {
        findSendHistories(selectQuery);

        $scope.$watch('form.size', (n, o) => {
            if (n != o) {
                search();
            }
        }, true);

        orderHandler.setOrderBy($scope, constant.listKeys.sendAnalysis, search);
    });

    function findSendHistories(query) {
        sendHistoryManager.findAll(query, (status, data) => {
            if (status === 200) {
                data.rows.map((item) => {
                    item.openRate = item.sendCount == 0 ? null : (item.openCount / item.sendCount * 100);
                    item.createdAt = dateHandler.toISO(item.createdAt);
                });
                $scope.sendHistories = data;
                checkGetCookies();
            } else if (status === 404) {

            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }
    
    function goToChart() {
        let sendHistoryIds = [];
        let getCookies = $cookies.get(CHECK_KEY);
        if(getCookies) {
            getCookies = getCookies.split(',');
        } else {
            getCookies = [];
        }
        for(let i=0; i<getCookies.length; i++) {
            sendHistoryIds.push(getCookies[i]);
        }

        if (sendHistoryIds.length) {
            navigator.goToChart({
                state: vm.state,
                sendHistoryIds: sendHistoryIds.join(',')
            });
        } else {
            dialogHandler.show('항목을 선택해주세요.');
        }
    }

    function goToSingleChart(item) {
        navigator.goToChart({
            state: vm.state,
            sendHistoryIds: item.id
        });
    }

    function totalCheck(isTotalCheck) {
        if(isTotalCheck) {
            let getCookies = $cookies.get(CHECK_KEY);
            let beforeCheckSize = 0;
            if(getCookies) {
                getCookies = getCookies.split(',');
            } else {
                getCookies = [];
            }
            const getCookieSize = getCookies ? getCookies.length : 0;
            for(let i=0; i<$scope.sendHistories.rows.length; i++) {
                if($scope.sendHistories.rows[i].isCheck) {
                    beforeCheckSize++;
                }
            }
            const newCheckSize = $scope.sendHistories.rows.length - beforeCheckSize;
            if((getCookieSize + newCheckSize) > CHECK_SIZE) {
                $timeout(() => {
                    $scope.check.total = !isTotalCheck;
                });
                return dialogHandler.show(`항목은 최대 ${CHECK_SIZE}개까지 선택 가능합니다.`);
            }

            let rows = $scope.sendHistories.rows;
            for (let i=0; i<rows.length; i++) {
                if(getCookies.indexOf(rows[i].id) === -1) {
                    getCookies.push(rows[i].id);
                }
                rows[i].isCheck = isTotalCheck;
            }
            $cookies.put(CHECK_KEY, getCookies.join(','));
        } else {
            let rows = $scope.sendHistories.rows;
            let getCookies = $cookies.get(CHECK_KEY);
            if(getCookies) {
                getCookies = getCookies.split(',');
            }

            for (let i=0; i<rows.length; i++) {
                if(getCookies) {
                    if(getCookies.indexOf(rows[i].id) !== -1) {
                        getCookies.splice(getCookies.indexOf(rows[i].id), 1);
                    }
                }
                rows[i].isCheck = isTotalCheck;
            }
            $cookies.put(CHECK_KEY, getCookies.join(','));
        }
    }

    function checkItem(isCheck, index) {
        let getCookies = $cookies.get(CHECK_KEY);
        if(getCookies) {
            getCookies = getCookies.split(',');
            if(getCookies.length > CHECK_SIZE || (isCheck && getCookies.length === CHECK_SIZE)) {
                $scope.sendHistories.rows[index].isCheck = false;
                return dialogHandler.show(`항목은 최대 ${CHECK_SIZE}개까지 선택 가능합니다.`);
            }
        }
        $scope.sendHistories.rows[index].isCheck = isCheck;
        $scope.check.total = checkArrayValue($scope.sendHistories.rows, true, 'isCheck');
        checkSetCookies();
    }

    function checkSetCookies() {
        let cookieArr = [];
        let getCookies = $cookies.get(CHECK_KEY);
        if(getCookies) {
            cookieArr = getCookies.split(',');
        }

        for(let i=0; i<$scope.sendHistories.rows.length; i++) {
            if($scope.sendHistories.rows[i].isCheck && cookieArr.indexOf($scope.sendHistories.rows[i].id) === -1) {
                cookieArr.push($scope.sendHistories.rows[i].id);
            }
        }
        $cookies.put(CHECK_KEY, cookieArr.join(','));
    }

    function checkGetCookies() {
        let getCookies = $cookies.get(CHECK_KEY);
        if(getCookies) {
            getCookies = getCookies.split(',');
            for(let i=0; i<$scope.sendHistories.rows.length; i++) {
                if(getCookies.indexOf($scope.sendHistories.rows[i].id) !== -1) {
                    $scope.sendHistories.rows[i].isCheck = true;
                }
            }
        }
        $scope.check.total = checkArrayValue($scope.sendHistories.rows, true, 'isCheck');
    }

    function setDateMinLimit(startDate) {
        let d = new Date(startDate);
        d.setDate(d.getDate()-1);
        $scope.dateMinLimit = dateFilter(new Date(d), 'yyyy-MM-dd');
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (!form.size) form.size = dataValue.DEFAULT_SIZE;

        $scope.form = form;

        if($scope.form.senderIds) {
            $scope.senders = JSON.parse($cookies.get(CHECK_OBJ_KEY));
        }

        if($scope.form.startDate) {
            setDateMinLimit($scope.form.startDate);
        }

        orderHandler.setSort($scope, constant.listKeys.sendAnalysis);

        return angular.copy($scope.form);
    }

    function generateQuery() {
        return angular.copy($scope.form);
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        pagination();
    }

    function activeTab(tab) {
        $scope.currentTab = tab;
    }

    function refresh() {
        let form = angular.copy(constant.defaultQuery.sendAnalysis);
        form.size = $scope.form.size;
        $scope.form = form;
        search();
    }

    function search() {
        $scope.form.offset = 0;
        goToSendAnalysis();
    }

    function pagination() {
        goToSendAnalysis();
    }

    function goToSendAnalysis(query, reload) {
        let param = query ? query : generateQuery();

        $state.go('sendAnalysis', param, {
            reload: reload
        }).then(() => {
            $document.scrollTop(0, 500);
        });
    }

    $scope.$watch('form.startDate', (n, o) => {
        if(n !== o && n !== undefined) {
            setDateMinLimit(n);
            if($scope.form.endDate !== undefined) {
                let startDate = new Date(n).getTime();
                let endDate = new Date($scope.form.endDate).getTime();

                if(startDate > endDate) {
                    $scope.form.endDate = null;
                }
            }
        }
    });
}