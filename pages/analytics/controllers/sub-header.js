export default function SubHeaderCtrl ($scope) {
    'ngInject';

    let vm = $scope.vm;

    $scope.toggle = toggle;

    function toggle () {
        vm.toggleNav = !vm.toggleNav;
    }
}