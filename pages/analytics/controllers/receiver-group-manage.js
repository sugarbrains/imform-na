export default function ReceiverGroupManageCtrl($scope, $stateParams, $filter, navigator, constant, dataValue, dialogHandler, modalHandler, stateHandler, orderHandler, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    let selectedIndex = null;

    stateHandler.setState(vm);
    stateHandler.setGroupTitle(vm);

    $scope.search = search;
    $scope.refresh = refresh;
    $scope.openSubmitReceiverGroupWithFile = openSubmitReceiverGroupWithFile;
    $scope.openUpdateReceiverGroup = openUpdateReceiverGroup;
    $scope.openReceiverListModal = openReceiverListModal;
    $scope.openSentMessageListModal = openSentMessageListModal;
    $scope.goPage = goPage;
    $scope.pagination = pagination;

    $scope.templatePath = '/pages/analytics/views/contents/receiver-group-manage/';

    $scope.enumSearchFields = [{
        value: 'name',
        text: '그룹 이름'
    }];

    $scope.enumSizes = dataValue.ENUM_SIZES;

    orderHandler.init($scope, constant.orderKeys.receiverGroupManage);
    $scope.headers = constant.listHeaders.receiverGroupManage;

    $scope.receiverGroups = {
        count: 0,
        rows: []
    };

    let selectQuery = generateForm();

    vm.getSession(() => {
        findReceiverGroups(selectQuery);

        $scope.$watch('form.size', (n, o) => {
            if (n != o) {
                search();
            }
        }, true);

        $scope.$on(constant.eventKey.submitReceiverGroup, () => {
            search();
        });

        $scope.$on(constant.eventKey.updateReceiverGroup, (e, args) => {
            if (args.receiverGroup) {
                $scope.receiverGroups.rows[selectedIndex] = args.receiverGroup;
            }
        });

        $scope.$on(constant.eventKey.submitReceiverGroupWithFile, (e, args) => {
            search();
        });

        $scope.$on(constant.eventKey.removeReceiverGroup, () => {
            search($scope.form.page);
        });

        orderHandler.setOrderBy($scope, constant.listKeys.receiverGroupManage, search);
    });

    function findReceiverGroups(query) {

        receiverGroupManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.receiverGroups = data;
            } else if (status == 404) {
                $scope.receiverGroups = null;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function openSubmitReceiverGroupWithFile() {
        modalHandler.openSubmitReceiverGroupWithFile();
    }

    function openUpdateReceiverGroup(receiverGroup, index) {
        selectedIndex = index;
        modalHandler.openUpdateReceiverGroup(receiverGroup.id);
    }

    function openReceiverListModal(receiverGroup, index) {
        //todo open Receiver Modal
        modalHandler.openReceiverListModal(receiverGroup, index);
    }

    function openSentMessageListModal(receiverGroup, index) {
        //todo open Message list modal
        modalHandler.openSentMessageListModal(receiverGroup, index);
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (!form.size) form.size = dataValue.DEFAULT_SIZE;

        $scope.form = form;

        orderHandler.setSort($scope, constant.listKeys.receiverGroupManage);

        return angular.copy($scope.form);
    }

    function generateQuery() {
        return angular.copy($scope.form);
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        pagination();
    }

    function pagination() {
        navigator.goToReceiverGroupManage(generateQuery(), true);
    }

    function refresh() {
        let form = angular.copy(constant.defaultQuery.receiverGroupManage);
        form.size = $scope.form.size;
        $scope.form = form;
        search();
    }

    function search() {
        $scope.form.offset = 0;
        navigator.goToReceiverGroupManage(generateQuery(), true);
    }
}