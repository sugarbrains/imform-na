export default function LoadingCtrl($scope, $timeout, constant) {
    "ngInject";

    let vm = $scope.vm;
    const loadingKey = constant.loadingKey;

    init();

    $scope.$on(loadingKey.startLoading, (e, args) => {
        if(args.key) {
            const key = args.key;
            if ($scope.keyHash[key]) {
                return false;
            } else {
                $scope.keyHash[key] = true;
                $scope.isOpen = true;
            }
        }
    });

    $scope.$on(loadingKey.loadingText, (e, args) =>  {
        if(args.key && args.loadingText) {
            vm.apply(() => {
                $scope.loadingText = args.loadingText;
            })
        }
    });

    $scope.$on(loadingKey.endLoading, (e, args) => {
        if(args.key) {
            const key = args.key;
            delete $scope.keyHash[key];
            vm.apply(() => {
                $scope.isOpen = !!(Object.keys($scope.keyHash).length);
                $scope.loadingText = null;
            });
        }
    });

    function init() {
        $scope.isOpen = false;
        $scope.keyHash = {};
        $scope.loadingText = null;
    }
}