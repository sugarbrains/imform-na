export default function ChartCtrl ($scope, $stateParams, $filter, $timeout, navigator, constant, dataValue, addressHandler, loadingHandler, dialogHandler, modalHandler, chartHandler, orderHandler, stateHandler, checkHandler, addressManager, chartManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnJoin = $filter('returnJoin');
    let checkArrayValue = $filter('checkArrayValue');
    let number = $filter('number');
    const CHART_KEY = constant.chartKey;
    const FORM = 'form';
    const COMMA = ',';

    const HOUR_DIAGRAM_SCOPE_KEY = 'hourDiagramScope';
    const DAY_DIAGRAM_SCOPE_KEY = 'dayDiagramScope';
    const PER_HOUR_DIAGRAM_SCOPE_KEY = 'perHourDiagramScope';
    const PER_DAY_DIAGRAM_SCOPE_KEY = 'perDayDiagramScope';

    const HOUR_DIAGRAM_FORM_KEY = 'hourDiagramForm';
    const DAY_DIAGRAM_FORM_KEY = 'dayDiagramForm';
    const PER_HOUR_DIAGRAM_FORM_KEY = 'perHourDiagramForm';
    const PER_DAY_DIAGRAM_FORM_KEY = 'perDayDiagramForm';

    stateHandler.setState(vm, $stateParams);
    stateHandler.setGroupTitle(vm, $stateParams);

    $scope.search = search;
    $scope.refresh = refresh;
    $scope.activeTab = activeTab;
    $scope.openSelectFunction = openSelectFunction;
    $scope.exportResult = exportResult;
    $scope.exportOpener = exportOpener;
    $scope.open = open;
    $scope.openCreateReceiverGroup = openCreateReceiverGroup;
    $scope.openDetailOpenerCount = openDetailOpenerCount;
    $scope.openDetailOpenerCountDiagram = openDetailOpenerCountDiagram;
    $scope.openDetailHourDiagram = openDetailHourDiagram;
    $scope.openDetailDayDiagram = openDetailDayDiagram;
    $scope.openDetailPerHourDiagram = openDetailPerHourDiagram;
    $scope.openDetailPerDayDiagram = openDetailPerDayDiagram;
    $scope.openDetailGenderDiagram = openDetailGenderDiagram;
    $scope.openDetailAgeDiagram = openDetailAgeDiagram;
    $scope.openDetailAddressDiagram = openDetailAddressDiagram;
    $scope.openDetailPageCountDiagram = openDetailPageCountDiagram;
    $scope.openDetailPageTimeDiagram = openDetailPageTimeDiagram;
    $scope.openDetailPerHourCouponDiagram = openDetailPerHourCouponDiagram;
    $scope.openDetailPerDayCouponDiagram = openDetailPerDayCouponDiagram;
    $scope.openDetailCouponCountDiagram = openDetailCouponCountDiagram;
    $scope.openDetailCouponTypeCountDiagram = openDetailCouponTypeCountDiagram;

    $scope.templatePath = '/pages/analytics/views/contents/chart/';
    $scope.chartKey = CHART_KEY;

    chartHandler.init($scope, [
        CHART_KEY.openCount,
        CHART_KEY.reopenCount,
        CHART_KEY.hourDiagram,
        CHART_KEY.dayDiagram,
        CHART_KEY.perHourDiagram,
        CHART_KEY.perDayDiagram,
        CHART_KEY.genderDiagram,
        CHART_KEY.ageDiagram,
        CHART_KEY.addressDiagram,
        CHART_KEY.pageCountDiagram,
        CHART_KEY.pageTimeDiagram
    ]);

    $scope[HOUR_DIAGRAM_SCOPE_KEY] = {headers: constant.listHeaders.hourDiagram};
    $scope[DAY_DIAGRAM_SCOPE_KEY] = {headers: constant.listHeaders.dayDiagram};
    $scope[PER_HOUR_DIAGRAM_SCOPE_KEY] = {headers: constant.listHeaders.perHourDiagram};
    $scope[PER_DAY_DIAGRAM_SCOPE_KEY] = {headers: constant.listHeaders.perDayDiagram};


    // orderHandler.init($scope, constant.orderKeys.hourDiagram, HOUR_DIAGRAM_SCOPE_KEY);
    // orderHandler.init($scope, constant.orderKeys.dayDiagram, DAY_DIAGRAM_SCOPE_KEY);
    // orderHandler.init($scope, constant.orderKeys.perHourDiagram, PER_HOUR_DIAGRAM_SCOPE_KEY);
    // orderHandler.init($scope, constant.orderKeys.perDayDiagram, PER_DAY_DIAGRAM_SCOPE_KEY);

    $scope.checkGenderTotal = checkHandler.check($scope, FORM, dataValue.GENDER_TOTAL);
    $scope.checkGenderMale = checkHandler.check($scope, FORM, dataValue.GENDER_MALE);
    $scope.checkGenderFemale = checkHandler.check($scope, FORM, dataValue.GENDER_FEMALE);
    $scope.checkGenderNone = checkHandler.check($scope, FORM, dataValue.GENDER_NONE);

    $scope.checkAgeTotal = checkHandler.check($scope, FORM, dataValue.AGE_TOTAL);
    $scope.checkAge20 = checkHandler.check($scope, FORM, dataValue.AGE_20);
    $scope.checkAge30 = checkHandler.check($scope, FORM, dataValue.AGE_30);
    $scope.checkAge40 = checkHandler.check($scope, FORM, dataValue.AGE_40);
    $scope.checkAge50 = checkHandler.check($scope, FORM, dataValue.AGE_50);
    $scope.checkAgeNone = checkHandler.check($scope, FORM, dataValue.AGE_NONE);

    $scope.enumSidos = [];
    $scope.enumSigungus = [];
    $scope.enumEupmyeondongs = [];
    $scope.enumSearchFields = [{
        value: 'key',
        text: 'KEY'
    }];

    $scope.currentTab = 1;
    $scope.isOpen = {
        hourDiagram: false,
        dayDiagram: false
    };
    $scope.listResult = {
        hourDiagram: [],
        dayDiagram: []
    };

    $scope.openerCount = {
        openCount: 0,
        reopenCount: 0,
        avgOpenCount: 0
    };

    let selectedQuery = generateForm();

    // orderHandler.setSort($scope, constant.listKeys.hourDiagram, HOUR_DIAGRAM_SCOPE_KEY, HOUR_DIAGRAM_FORM_KEY);
    // orderHandler.setSort($scope, constant.listKeys.dayDiagram, DAY_DIAGRAM_SCOPE_KEY, DAY_DIAGRAM_FORM_KEY);
    // orderHandler.setSort($scope, constant.listKeys.perHourDiagram, PER_HOUR_DIAGRAM_SCOPE_KEY, PER_HOUR_DIAGRAM_FORM_KEY);
    // orderHandler.setSort($scope, constant.listKeys.perDayDiagram, PER_DAY_DIAGRAM_SCOPE_KEY, PER_DAY_DIAGRAM_FORM_KEY);

    // orderHandler.setOrderBy($scope, constant.listKeys.hourDiagram, findHourDiagramList, HOUR_DIAGRAM_SCOPE_KEY, HOUR_DIAGRAM_FORM_KEY);
    // orderHandler.setOrderBy($scope, constant.listKeys.dayDiagram, findDayDiagramList, DAY_DIAGRAM_SCOPE_KEY, DAY_DIAGRAM_FORM_KEY);
    // orderHandler.setOrderBy($scope, constant.listKeys.perHourDiagram, findPerHourDiagramList, PER_HOUR_DIAGRAM_SCOPE_KEY, PER_HOUR_DIAGRAM_FORM_KEY);
    // orderHandler.setOrderBy($scope, constant.listKeys.perDayDiagram, findPerDayDiagramList, PER_DAY_DIAGRAM_SCOPE_KEY, PER_DAY_DIAGRAM_FORM_KEY);

    vm.getSession(() => {
        $scope.$on(constant.eventKey.selectFunction, (e, args) => {
            if (args.isExportResult) {
                exportResult();
            } else if (args.isExportOpener) {
                exportOpener();
            } else if (args.isCreateReceiverGroup) {
                openCreateReceiverGroup();
            }
        });
        $scope.$on(constant.eventKey.submitReceiverGroup, (e, args) => {

        });
        findAddresses(selectedQuery);
        findOpenerCountDiagram(selectedQuery);
        findHourDiagram(selectedQuery);
        findDayDiagram(selectedQuery);
        // findPerHourDiagram();
        // findPerDayDiagram();
        findGenderDiagram(selectedQuery);
        findAgeDiagram(selectedQuery);
        findAddressDiagram(selectedQuery);
        findPageCountDiagram(selectedQuery);
        findPageTimeDiagram(selectedQuery);
    });

    function findAddresses({sendHistoryIds, sido, sigungu}) {
        addressManager.getAddresses({
            sendHistoryIds
        }, (status, data) => {
            if (status === 200) {
                const addresses = addressHandler.generateAddresses(data);
                $scope.enumSidos = Object.keys(addresses);
                if (sido && addresses[sido]) {
                    $scope.enumSigungus = Object.keys(addresses[sido]);
                }
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findOpenerCountDiagram(query) {
        chartManager.getChartOpenerCount(query, (status, {receiverCount, openerCount, openCount, reopenerCount, reopenCount}) => {
            if (status === 200) {
                chartHandler.drawOpenCount({
                    openerCount: openerCount,
                    notOpenCount: receiverCount - openerCount
                });
                chartHandler.drawReopenCount({
                    reopenCount: reopenerCount,
                    notReopenCount: receiverCount - reopenerCount
                });

                $scope.openerCount = {
                    openCount,
                    reopenCount,
                    avgOpenCount: parseInt(openerCount) ? (openCount / parseInt(openerCount)) : 0
                };
                $scope.openerCountLabelForm = {
                    openerCount: openerCount,
                    notOpenCount: receiverCount - openerCount,
                    reopenCount: reopenerCount,
                    notReopenCount: receiverCount - reopenerCount
                };
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }
    
    function findHourDiagram(query) {
        chartManager.getChartHour(query, (status, data) => {
            if(status === 200) {
                let hourDiagram = setHourDiagramChartData(data);
                $scope.listResult.hourDiagram = hourDiagram;
                chartHandler.drawHourDiagram(hourDiagram);
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }
    
    function findDayDiagram(query) {
        chartManager.getChartDay(query, (status, data) => {
           if(status === 200) {
               let dayDiagram = setDayDiagramChartData(data);
               $scope.listResult.dayDiagram = dayDiagram;
               chartHandler.drawDayDiagram(dayDiagram);
           } else {
               dialogHandler.alertError(status, data);
           }
        });
    }

    function setDayDiagramChartData(data) {
        let days = [0, 1, 2, 3, 4, 5, 6];
        let dayDiagram = [0, 0, 0, 0, 0, 0, 0];

        for (let i=0; i<days.length; i++) {
            for (let j=0; j<data.length; j++) {
                if (days[i] === parseInt(data[j].day)) {
                    dayDiagram[i] = parseInt(data[j].openCount);
                    data.splice(j, 1);
                    j--;
                    break;
                }
            }
        }
        return dayDiagram;
    }

    function setDayDiagramTableData(data) {
        let days = [0, 1, 2, 3, 4, 5, 6];
        let dayArray = {
            0: false,
            1: false,
            2: false,
            3: false,
            4: false,
            5: false,
            6: false,
        };

        for (let i=0; i<days.length; i++) {
            for (let j=0; j<data.length; j++) {
                if (days[i] === parseInt(data[j].day)) {
                    dayArray[i] = data[j].openCount;
                    data.splice(j, 1);
                    j--;
                    break;
                }
            }
        }

        for (let key in dayArray) {
            if(!dayArray[key]) {
                dayArray[key] = 0;
            }
        }

        return dayArray;
    }

    function setHourDiagramChartData(data) {
        let hours = [];
        let hourDiagram = [];

        for(let i=0; i<24; i++) {
            hours.push(i*3);
            hourDiagram.push(0);
        }

        for (let i=0; i<hours.length; i++) {
            for (let j=0; j<data.length; j++) {
                if (hours[i] === parseInt(data[j].hour)) {
                    hourDiagram[i] = parseInt(data[j].openCount);
                    data.splice(j, 1);
                    j--;
                    break;
                }
            }
        }
        return hourDiagram;
    }

    function setHourDiagramTableData(data) {
        let hours = [];
        let hourDiagram = {};

        for(let i=0; i<24; i++) {
            hours.push(i);
            hourDiagram[i] = false;
        }

        for (let i=0; i<hours.length; i++) {
            for (let j=0; j<data.length; j++) {
                if (hours[i] === parseInt(data[j].hour)) {
                    hourDiagram[i] = parseInt(data[j].openCount);
                    data.splice(j, 1);
                    j--;
                    break;
                }
            }
        }

        for (let key in hourDiagram) {
            if(!hourDiagram[key]) {
                hourDiagram[key] = 0;
            }
        }
        return hourDiagram;
    }
    
    function findPerHourDiagram() {
        let perHourDiagram = [];
        for (let i=0; i<24; i++) {
            perHourDiagram.push(Math.floor(Math.random() * 1000));
        }
        chartHandler.drawPerHourDiagram(perHourDiagram);
    }

    function findPerDayDiagram() {
        let perDayDiagram = [];
        for (let i=0; i<7; i++) {
            perDayDiagram.push(Math.floor(Math.random() * 5000));
        }
        chartHandler.drawPerDayDiagram(perDayDiagram);
    }

    function findGenderDiagram(query) {
        chartManager.getChartGender(query, (status, {genderMale, genderFemale, genderNone}) => {
            if (status === 200) {
                chartHandler.drawGenderDiagram({
                    genderMale,
                    genderFemale,
                    genderNone
                });
                $scope.labelGenderForm = {
                    genderMale: genderMale,
                    genderFemale: genderFemale,
                    genderNone: genderNone
                };
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findAgeDiagram(query) {
        chartManager.getChartAge(query, (status, {age20, age30, age40, age50, ageNone}) => {
            if (status === 200) {
                chartHandler.drawAgeDiagram({
                    age20,
                    age30,
                    age40,
                    age50,
                    ageNone
                });
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findAddressDiagram(query) {
        let temp = Object.assign({
            size: 10
        }, query);
        chartManager.getChartAddress(temp, (status, data) => {
            if (status === 200) {
                let addressDiagram = [];
                let addressDiagramLabels = [];
                data.forEach((item) => {
                    let label;
                    if (query.sido && query.sigungu) {
                        label = `${item.address1} ${item.address2} ${item.address3}`;
                    } else if (query.sido) {
                        label = `${item.address1} ${item.address2}`;
                    } else {
                        label = `${item.address1}`;
                    }
                    addressDiagram.push(item.count);
                    addressDiagramLabels.push(label);
                });
                chartHandler.drawAddressDiagram(addressDiagram, addressDiagramLabels);
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findPageCountDiagram(query) {
        let customQuery = angular.copy(query);
        customQuery.size = 10;
        chartManager.getChartPageCount(customQuery, (status, data) => {
            if (status === 200) {
                let pageCount = [];
                let pageCountLabels = [];
                data.forEach((item) => {
                    pageCount.push(item.count);
                    pageCountLabels.push(item.title);
                });
                chartHandler.drawPageCountDiagram(pageCount, pageCountLabels);
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findPageTimeDiagram(query) {
        let customQuery = angular.copy(query);
        customQuery.size = 10;
        chartManager.getChartPageTime(customQuery, (status, data) => {
            if (status === 200) {
                let pageTime = [];
                let pageTimeLabels = [];
                data.forEach((item) => {
                    pageTime.push(parseInt(item.time / 1000));
                    pageTimeLabels.push(item.title);
                });
                chartHandler.drawPageTimeDiagram(pageTime, pageTimeLabels);
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findHourDiagramList() {
        orderHandler.setSort($scope, constant.listKeys.hourDiagram, HOUR_DIAGRAM_SCOPE_KEY, HOUR_DIAGRAM_FORM_KEY);

        let query = angular.copy($scope[HOUR_DIAGRAM_FORM_KEY]);

        // todo find hour diagram list
    }

    function findDayDiagramList() {
        orderHandler.setSort($scope, constant.listKeys.dayDiagram, DAY_DIAGRAM_SCOPE_KEY, DAY_DIAGRAM_FORM_KEY);

        let query = angular.copy($scope[DAY_DIAGRAM_FORM_KEY]);

        // todo find day diagram list
    }

    function findPerHourDiagramList() {
        orderHandler.setSort($scope, constant.listKeys.perHourDiagram, PER_HOUR_DIAGRAM_SCOPE_KEY, PER_HOUR_DIAGRAM_FORM_KEY);

        let query = angular.copy($scope[PER_HOUR_DIAGRAM_FORM_KEY]);

        // todo find per hour diagram list
    }

    function findPerDayDiagramList() {
        orderHandler.setSort($scope, constant.listKeys.perDayDiagram, PER_DAY_DIAGRAM_SCOPE_KEY, PER_DAY_DIAGRAM_FORM_KEY);

        let query = angular.copy($scope[PER_DAY_DIAGRAM_FORM_KEY]);

        // todo find per day diagram list
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (form.genders) {
            let genders = form.genders.split(COMMA);
            form.genderMale = genders.indexOf(dataValue.GENDER_MALE) !== -1;
            form.genderFemale = genders.indexOf(dataValue.GENDER_FEMALE) !== -1;
            form.genderNone = genders.indexOf(dataValue.GENDER_NONE) !== -1;
            form.genderTotal = checkArrayValue([
                form.genderMale,
                form.genderFemale,
                form.genderNone
            ]);
        }

        if (form.ages) {
            let ages = form.ages.split(COMMA);
            form.age20 = ages.indexOf(dataValue.AGE_20) !== -1;
            form.age30 = ages.indexOf(dataValue.AGE_30) !== -1;
            form.age40 = ages.indexOf(dataValue.AGE_40) !== -1;
            form.age50 = ages.indexOf(dataValue.AGE_50) !== -1;
            form.ageNone = ages.indexOf(dataValue.AGE_NONE) !== -1;
            form.ageTotal = checkArrayValue([
                form.age20,
                form.age30,
                form.age40,
                form.age50,
                form.ageNone
            ]);
        }

        if (form.receiverCount) {
            form.receiverCount = parseInt(form.receiverCount);
        }

        if (form.openCount) {
            form.openCount = parseInt(form.openCount);
        }

        $scope.form = form;

        generateOrderForm();

        let query = angular.copy($stateParams);
        if (form.ageTotal) {
            query.ages = '';
        }

        return query;
    }

    function generateOrderForm() {
        $scope[HOUR_DIAGRAM_FORM_KEY] = angular.copy($scope.form);
        $scope[HOUR_DIAGRAM_FORM_KEY].orderBy = 'hour';
        $scope[HOUR_DIAGRAM_FORM_KEY].sort = 'ASC';

        $scope[DAY_DIAGRAM_FORM_KEY] = angular.copy($scope.form);
        $scope[DAY_DIAGRAM_FORM_KEY].orderBy = 'day';
        $scope[DAY_DIAGRAM_FORM_KEY].sort = 'ASC';

        $scope[PER_HOUR_DIAGRAM_FORM_KEY] = angular.copy($scope.form);
        $scope[PER_HOUR_DIAGRAM_FORM_KEY].orderBy = 'hour';
        $scope[PER_HOUR_DIAGRAM_FORM_KEY].sort = 'ASC';

        $scope[PER_DAY_DIAGRAM_FORM_KEY] = angular.copy($scope.form);
        $scope[PER_DAY_DIAGRAM_FORM_KEY].orderBy = 'day';
        $scope[PER_DAY_DIAGRAM_FORM_KEY].sort = 'ASC';
    }

    function generateQuery() {
        let query = angular.copy($scope.form);

        if (query.genderTotal) {
            query.genders = returnJoin([
                dataValue.GENDER_MALE,
                dataValue.GENDER_FEMALE,
                dataValue.GENDER_NONE
            ], COMMA);
        } else {
            query.genders = '';
            if (query.genderMale) query.genders += dataValue.GENDER_MALE + COMMA;
            if (query.genderFemale) query.genders += dataValue.GENDER_FEMALE + COMMA;
            if (query.genderNone) query.genders += dataValue.GENDER_NONE + COMMA;
            query.genders = query.genders.substr(0, query.genders.length - 1);
        }

        if (query.ageTotal) {
            query.ages = returnJoin([
                dataValue.AGE_20,
                dataValue.AGE_30,
                dataValue.AGE_40,
                dataValue.AGE_50,
                dataValue.AGE_NONE,
            ], COMMA);
        } else {
            query.ages = '';
            if (query.age20) query.ages += dataValue.AGE_20 + COMMA;
            if (query.age30) query.ages += dataValue.AGE_30 + COMMA;
            if (query.age40) query.ages += dataValue.AGE_40 + COMMA;
            if (query.age50) query.ages += dataValue.AGE_50 + COMMA;
            if (query.ageNone) query.ages += dataValue.AGE_NONE + COMMA;
            query.ages = query.ages.substr(0, query.ages.length - 1);
        }

        return query;
    }

    function openSelectFunction() {
        modalHandler.openSelectFunction();
    }

    function exportResult() {
        // todo export result
    }

    function exportOpener() {
        if($scope.openerCountLabelForm.openerCount != 0) {
            let url = location.protocol + "//" + location.host;
            const apiUrl = "/api/analytics/opener-excel.php";
            let querystring = '?';
            Object.keys(selectedQuery).forEach((key, index) => {
                if(selectedQuery[key]) {
                    querystring += key;
                    querystring += '=';
                    querystring += selectedQuery[key];
                    querystring += '&';
                }
            });

            querystring = querystring.slice(0, -1);

            window.open(url+apiUrl+querystring);
        } else {
            dialogHandler.show('열람자가 없습니다.');
        }
    }

    function openCreateReceiverGroup() {
        modalHandler.openSubmitReceiverGroup(selectedQuery);
    }

    function openDetailOpenerCount() {
        modalHandler.openDetailOpenerCount(selectedQuery);
    }

    function openDetailOpenerCountDiagram() {
        modalHandler.openDetailOpenerCountDiagram(selectedQuery);
    }

    function openDetailHourDiagram() {
        modalHandler.openDetailHourDiagram(selectedQuery);
    }

    function openDetailDayDiagram() {
        modalHandler.openDetailDayDiagram(selectedQuery);
    }

    function openDetailPerHourDiagram() {
        modalHandler.openDetailPerHourDiagram(selectedQuery);
    }

    function openDetailPerDayDiagram() {
        modalHandler.openDetailPerDayDiagram(selectedQuery);
    }

    function openDetailGenderDiagram() {
        modalHandler.openDetailGenderDiagram(selectedQuery);
    }

    function openDetailAgeDiagram() {
        modalHandler.openDetailAgeDiagram(selectedQuery);
    }

    function openDetailAddressDiagram() {
        modalHandler.openDetailAddressDiagram(selectedQuery);
    }

    function openDetailPageCountDiagram() {
        modalHandler.openDetailPageCountDiagram(selectedQuery);
    }

    function openDetailPageTimeDiagram() {
        modalHandler.openDetailPageTimeDiagram(selectedQuery);
    }

    function openDetailPerHourCouponDiagram() {
        modalHandler.openDetailPerHourCouponDiagram(selectedQuery);
    }

    function openDetailPerDayCouponDiagram() {
        modalHandler.openDetailPerDayCouponDiagram(selectedQuery);
    }

    function openDetailCouponCountDiagram() {
        modalHandler.openDetailCouponCountDiagram(selectedQuery);
    }

    function openDetailCouponTypeCountDiagram() {
        modalHandler.openDetailCouponTypeCountDiagram(selectedQuery);
    }

    function open(key) {
        $scope.isOpen[key] = !$scope.isOpen[key];
    }

    function activeTab(tab) {
        $scope.currentTab = tab;
    }

    function refresh() {
        let form = angular.copy(constant.defaultQuery.chart);
        form.state = $stateParams.state;
        form.senderIds = $stateParams.senderIds;
        form.senderHistoryIds = $stateParams.senderHistoryIds;

        form.age20 = form.ages.indexOf('age20') !== -1;
        form.age30 = form.ages.indexOf('age30') !== -1;
        form.age40 = form.ages.indexOf('age40') !== -1;
        form.age50 = form.ages.indexOf('age50') !== -1;
        form.ageNone = form.ages.indexOf('ageNone') !== -1;
        form.ageTotal = form.age20 && form.age30 && form.age40 && form.age50 && form.ageNone;

        form.genderMale = form.genders.indexOf('genderMale') !== -1;
        form.genderFemale = form.genders.indexOf('genderFemale') !== -1;
        form.genderNone = form.genders.indexOf('genderNone') !== -1;
        form.genderTotal = form.genderMale && form.genderFemale && form.genderNone;

        $scope.form = form;
    }

    function search() {
        navigator.goToChart(generateQuery(), true);
    }
}