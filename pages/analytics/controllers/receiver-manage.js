export default function ReceiverManageCtrl($scope, $stateParams, $filter, navigator, constant, dataValue, dialogHandler, modalHandler, stateHandler, checkHandler, orderHandler, receiverManager, receiverGroupManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    let returnJoin = $filter('returnJoin');
    let checkArrayValue = $filter('checkArrayValue');
    let selectedIndex = null;
    const FORM = 'form';
    const COMMA = ',';

    stateHandler.setState(vm);
    stateHandler.setGroupTitle(vm);

    $scope.search = search;
    $scope.refresh = refresh;
    $scope.activeTab = activeTab;
    $scope.openSubmitReceiver = openSubmitReceiver;
    $scope.openUpdateReceiver = openUpdateReceiver;
    $scope.openReceiverGroup = openReceiverGroup;
    $scope.openSentCount = openSentCount;
    $scope.goPage = goPage;

    $scope.checkGenderTotal = checkHandler.check($scope, FORM, dataValue.GENDER_TOTAL);
    $scope.checkGenderMale = checkHandler.check($scope, FORM, dataValue.GENDER_MALE);
    $scope.checkGenderFemale = checkHandler.check($scope, FORM, dataValue.GENDER_FEMALE);
    $scope.checkGenderNone = checkHandler.check($scope, FORM, dataValue.GENDER_NONE);

    $scope.checkAgeTotal = checkHandler.check($scope, FORM, dataValue.AGE_TOTAL);
    $scope.checkAge20 = checkHandler.check($scope, FORM, dataValue.AGE_20);
    $scope.checkAge30 = checkHandler.check($scope, FORM, dataValue.AGE_30);
    $scope.checkAge40 = checkHandler.check($scope, FORM, dataValue.AGE_40);
    $scope.checkAge50 = checkHandler.check($scope, FORM, dataValue.AGE_50);
    $scope.checkAgeNone = checkHandler.check($scope, FORM, dataValue.AGE_NONE);

    $scope.templatePath = '/pages/analytics/views/contents/receiver-manage/';

    $scope.enumSidos = [];
    $scope.enumSigungus = [];
    $scope.enumEupmyeondongs = [];
    $scope.enumSearchFields = [];
    $scope.enumSizes = dataValue.ENUM_SIZES;

    orderHandler.init($scope, constant.orderKeys.receiverManage);
    $scope.headers = constant.listHeaders.receiverManage;

    $scope.currentTab = 1;
    $scope.receiverGroups = {
        count: 0,
        rows: []
    };
    $scope.receivers = {
        count: 0,
        rows: []
    };

    let selectQuery = generateForm();

    vm.getSession(() => {
        findReceiverGroups();
        findReceivers(selectQuery);

        $scope.$watch('form.size', (n, o) => {
            if (n != o) {
                search();
            }
        }, true);

        $scope.$on(constant.eventKey.submitReceiver, () => {
            search();
        });

        $scope.$on(constant.eventKey.updateReceiver, (e, args) => {
            if (args.receiver) {
                $scope.receivers.rows[selectedIndex] = args.receiver;
            }
        });

        $scope.$on(constant.eventKey.deleteReceiver, () => {
            search();
        });

        orderHandler.setOrderBy($scope, constant.listKeys.receiverManage, search);
    });

    function findReceivers(query) {

        receiverManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.receivers = data;
            } else if (status == 404) {

            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function findReceiverGroups () {
        let query = {
            orderBy: 'groupName',
            sort: 'ASC'
        };
        receiverGroupManager.findAll(query, (status, data) => {
            if (status == 200) {
                $scope.receiverGroups = data;
            } else if (status == 404) {

            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function openSubmitReceiver() {
        modalHandler.openSubmitReceiver();
    }

    function openUpdateReceiver(receiver, index) {
        selectedIndex = index;
        modalHandler.openUpdateReceiver(receiver);
    }

    function openReceiverGroup(receiver) {
        modalHandler.openReceiverGroup(receiver.id);
    }

    function openSentCount(receiver) {
        modalHandler.openSentCount(receiver.id);
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (form.genders) {
            let genders = form.genders.split(COMMA);
            form.genderMale = genders.indexOf(dataValue.GENDER_MALE) != -1;
            form.genderFemale = genders.indexOf(dataValue.GENDER_FEMALE) != -1;
            form.genderNone = genders.indexOf(dataValue.GENDER_NONE) != -1;
            form.genderTotal = checkArrayValue([
                form.genderMale,
                form.genderFemale,
                form.genderNone
            ]);
        }

        if (form.ages) {
            let ages = form.ages.split(COMMA);
            form.age20 = ages.indexOf(dataValue.AGE_20) != -1;
            form.age30 = ages.indexOf(dataValue.AGE_30) != -1;
            form.age40 = ages.indexOf(dataValue.AGE_40) != -1;
            form.age50 = ages.indexOf(dataValue.AGE_50) != -1;
            form.ageNone = ages.indexOf(dataValue.AGE_NONE) != -1;
            form.ageTotal = checkArrayValue([
                form.age20,
                form.age30,
                form.age40,
                form.age50,
                form.ageNone
            ]);
        }

        if (!form.size) form.size = dataValue.DEFAULT_SIZE;

        $scope.form = form;

        orderHandler.setSort($scope, constant.listKeys.receiverManage);

        return angular.copy($stateParams);
    }

    function generateQuery() {
        let query = angular.copy($scope.form);

        if (query.genderTotal) {
            query.genders = returnJoin([
                dataValue.GENDER_MALE,
                dataValue.GENDER_FEMALE,
                dataValue.GENDER_NONE
            ], COMMA);
        } else {
            query.genders = '';
            if (query.genderMale) query.genders += dataValue.GENDER_MALE + COMMA;
            if (query.genderFemale) query.genders += dataValue.GENDER_FEMALE + COMMA;
            if (query.genderNone) query.genders += dataValue.GENDER_NONE + COMMA;
            query.genders = query.genders.substr(0, query.genders.length - 1);
        }

        if (query.ageTotal) {
            query.ages = returnJoin([
                dataValue.AGE_20,
                dataValue.AGE_30,
                dataValue.AGE_40,
                dataValue.AGE_50,
                dataValue.AGE_NONE,
            ], COMMA);
        } else {
            query.ages = '';
            if (query.age20) query.ages += dataValue.AGE_20 + COMMA;
            if (query.age30) query.ages += dataValue.AGE_30 + COMMA;
            if (query.age40) query.ages += dataValue.AGE_40 + COMMA;
            if (query.age50) query.ages += dataValue.AGE_50 + COMMA;
            if (query.ageNone) query.ages += dataValue.AGE_NONE + COMMA;
            query.ages = query.ages.substr(0, query.ages.length - 1);
        }

        return query;
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        search();
    }

    function activeTab(tab) {
        $scope.currentTab = tab;
    }

    function refresh() {
        let form = angular.copy(constant.defaultQuery.receiverManage);
        form.size = $scope.form.size;
        $scope.form = form;
    }

    function search() {
        navigator.goToReceiverManage(generateQuery(), true);
    }
}