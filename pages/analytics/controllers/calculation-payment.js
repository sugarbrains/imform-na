export default function CalculationPaymentCtrl($rootScope, $scope, $filter, constant, dataValue, dialogHandler, stateHandler, orderHandler, calculationManager) {
    "ngInject";

    let vm = $scope.vm;

    vm.getSession(() => {
        $scope.payments = null;
        findPayments();

        $rootScope.$broadcast('set-calc-order-key', {
            orderKeys: constant.listKeys.calcPayment
        })
    });

    function findPayments() {
        let query = {};

        if($scope.form.orderBy !== undefined) query.orderBy = $scope.form.orderBy;
        if($scope.form.sort !== undefined) query.sort = $scope.form.sort;
        if($scope.form.size !== undefined) query.size = $scope.form.size;
        if($scope.form.offset !== undefined) query.offset = $scope.form.offset;
        if($scope.form.startDate !== undefined) query.startDate = $scope.form.startDate;
        if($scope.form.endDate !== undefined) query.endDate = $scope.form.endDate;

        calculationManager.findAllPayments(query, (status, data) => {
            if(status === 200) {
                $scope.payments = data;
            } else if (status === 404) {
                $scope.payments = null;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }
}