export default function CalculationSentMessageCtrl($rootScope, $scope, $filter, constant, dataValue, dialogHandler, stateHandler, orderHandler, calculationManager) {
    "ngInject";

    let vm = $scope.vm;

    vm.getSession(() => {
        $scope.sentMessage = null;
        findSentMessages();

        $rootScope.$broadcast('set-calc-order-key', {
            orderKeys: constant.listKeys.calcSentMessage
        })
    });

    function findSentMessages() {
        let query = {};

        if($scope.form.orderBy !== undefined) query.orderBy = $scope.form.orderBy;
        if($scope.form.sort !== undefined) query.sort = $scope.form.sort;
        if($scope.form.size !== undefined) query.size = $scope.form.size;
        if($scope.form.offset !== undefined) query.offset = $scope.form.offset;
        if($scope.form.startDate !== undefined) query.startDate = $scope.form.startDate;
        if($scope.form.endDate !== undefined) query.endDate = $scope.form.endDate;

        calculationManager.findAllSentMessages(query, (status, data) => {
            if(status === 200) {
                $scope.sentMessage = data;
            } else if (status === 404) {
                $scope.sentMessage = null;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }
}