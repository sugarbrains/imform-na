export default function HeaderCtrl ($scope, navigator, sessionManager, modalHandler) {
    'ngInject';

    let vm = $scope.vm;

    $scope.toggle = toggle;
    $scope.goToIndex = goToIndex;
    $scope.openMyInfo = openMyInfo;
    $scope.signOut = sessionManager.signOut;

    $scope.session = sessionManager.getSessionData();

    function toggle () {
        vm.toggleNav = !vm.toggleNav;
    }

    function goToIndex () {
        navigator.goToIndex();
    }

    function openMyInfo() {
        modalHandler.openMyProfile(vm.session.id);
    }
}