export default function CalculationManageCtrl($scope, $stateParams, $filter, constant, navigator, dataValue, dialogHandler, stateHandler, orderHandler, calculationManager) {
    'ngInject';

    let vm = $scope.vm;
    let returnOffset = $filter('returnOffset');
    let dateFilter = $filter('date');

    stateHandler.setState(vm);
    stateHandler.setGroupTitle(vm);

    $scope.activeTab = activeTab;
    $scope.search = search;
    $scope.refresh = refresh;
    $scope.goPage = goPage;
    $scope.pagination = pagination;

    $scope.templatePath = '/pages/analytics/views/contents/calculation-manage/';

    $scope.enumSizes = dataValue.ENUM_SIZES;
    $scope.dashboard = {};

    let selectQuery = generateForm();

    function activeTab(tab) {
        $scope.form.activeTab = tab;
    }

    vm.getSession(() => {
        $scope.$watch('form.size', (n, o) => {
            if (n != o) {
                search();
            }
        }, true);

        findDashboardData();

        $scope.$on('set-calc-order-key', (e, args) => {
            if(args.orderKeys) {
                $scope.childOrderKeys = args.orderKeys;
                orderHandler.setOrderBy($scope, args.orderKeys, search);
            }
        });
    });

    function initOrderHeader(tabName) {
        if(tabName === 'sentMessages') {
            orderHandler.init($scope, constant.orderKeys.calcSentMessage);
            $scope.headers = constant.listHeaders.calcSentMessage;
        } else if (tabName === 'payments') {
            orderHandler.init($scope, constant.orderKeys.calcPayment);
            $scope.headers = constant.listHeaders.calcPayment;
        }
    }

    function findDashboardData() {
        calculationManager.findAllChargeCount({}, (status, data) => {
            if(status === 200) {
                $scope.dashboard = data;
            } else if (status === 404) {
                $scope.dashboard = null;
            } else {
                dialogHandler.alertError(status, data);
            }
        });
    }

    function setDateMinLimit(startDate) {
        let d = new Date(startDate);
        d.setDate(d.getDate()-1);
        $scope.dateMinLimit = dateFilter(new Date(d), 'yyyy-MM-dd');
    }

    function generateForm() {
        let form = angular.copy($stateParams);

        if (!form.size) form.size = dataValue.DEFAULT_SIZE;

        $scope.form = form;
        initOrderHeader(form.activeTab);

        if($scope.form.startDate) {
            setDateMinLimit($scope.form.startDate);
        }

        if($stateParams.activeTab === 'sentMessages') {
            orderHandler.setSort($scope, constant.listKeys.calcSentMessage);
        } else if ($stateParams.activeTab === 'payments') {
            orderHandler.setSort($scope, constant.listKeys.calcPayment);
        }

        return angular.copy($scope.form);
    }

    function goPage(page) {
        $scope.form.offset = returnOffset($scope.form.size, page);
        pagination();
    }

    function refresh(activeTab) {
        let form = angular.copy(constant.defaultQuery.calculationManage);
        form.size = $scope.form.size;
        if(activeTab) form.activeTab = activeTab;
        $scope.form = form;
        search();
    }

    function generateQuery() {
        return angular.copy($scope.form);
    }

    function search() {
        $scope.form.offset = 0;
        navigator.goToCalculationManage(generateQuery(), true);
    }


    function pagination() {
        navigator.goToCalculationManage(generateQuery(), true);
    }

    $scope.$watch('form.activeTab', (n, o) => {
        if(n !== o) {
            refresh(n);
        }
    });

    $scope.$watch('form.startDate', (n, o) => {
        if(n !== o && n !== undefined) {
            setDateMinLimit(n);
            if($scope.form.endDate !== undefined) {
                let startDate = new Date(n).getTime();
                let endDate = new Date($scope.form.endDate).getTime();

                if(startDate > endDate) {
                    $scope.form.endDate = null;
                }
            }
        }
    });
}