export default function fileread() {
    "ngInject";

    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            if($(element).attr('type') === 'file') {
                element.on("change", function (changeEvent) {
                    if(changeEvent.target.files.length) {
                        var reader = new FileReader();
                        reader.onload = function (loadEvent) {
                            scope.$apply(function () {
                                // scope.fileread = loadEvent.target.result;
                                scope.fileread = changeEvent.target.files[0];
                            });
                        };
                        reader.readAsDataURL(changeEvent.target.files[0]);
                    } else {
                        scope.$apply(function () {
                            scope.fileread = {};
                        });
                    }
                });
            }
        }
    }
}