export default function checkBox () {
    'ngInject';

    const templatePath = '/pages/analytics/directives/';
    const directory = 'check-box';
    const url = templatePath + directory + '/' + directory + '.html';

    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            ngText: '@',
            ngFunc: '=',
            ngIndex: '@'
        },
        templateUrl: url,
        link: (scope, element, attr) => {
            scope.check = check;

            function check () {
                scope.ngModel = !scope.ngModel;
                if (scope.ngFunc) {
                    scope.ngFunc(scope.ngModel, scope.ngIndex);
                }
            }
        }
    }
}