export default function selectBox () {
    'ngInject';

    const templatePath = '/pages/analytics/directives/';
    const directory = 'select-box';
    const url = templatePath + directory + '/' + directory + '.html';

    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            ngEnum: '=',
            ngPlaceholder: '@',
            ngValueKey: '@',
            ngTextKey: '@'
        },
        templateUrl: url,
        link: (scope, element, attr) => {
        }
    }
}