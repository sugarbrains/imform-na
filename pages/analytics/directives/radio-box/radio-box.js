export default function radioBox () {
    'ngInject';

    const templatePath = '/pages/analytics/directives/';
    const directory = 'radio-box';
    const url = templatePath + directory + '/' + directory + '.html';

    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            ngFunc: '=',
            ngText: '@'
        },
        templateUrl: url,
        link: (scope, element, attr) => {

        }
    }
}