export default function fileUpload() {
    'ngInject';

    const templatePath = '/pages/analytics/directives/';
    const directory = 'file-upload';
    const url = templatePath + directory + '/' + directory + '.html';

    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            ngId: '@'
        },
        templateUrl: url,
        link: (scope, element, attr) => {
        }
    }
}