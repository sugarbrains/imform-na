export default function searchInput () {
    'ngInject';

    const templatePath = '/pages/analytics/directives/';
    const directory = 'search-input';
    const url = templatePath + directory + '/' + directory + '.html';

    return {
        restrict: 'E',
        scope: {
            ngKeyword: '=',
            ngEnum: '=',
            ngKeywordPlaceholder: '@',
            ngValueKey: '@',
            ngTextKey: '@',
            ngItem: '=',
            ngItemPlaceholder: '@',
            ngSearch: '='
        },
        templateUrl: url,
        link: (scope, element, attr) => {
            scope.submit = submit;

            if(scope.ngEnum && scope.ngEnum.length === 1) {
                if(scope.ngValueKey && scope.ngTextKey) {
                    scope.ngKeyword = scope.ngEnum[0][scope.ngValueKey];
                } else {
                    scope.ngKeyword = scope.ngEnum[0];
                }
            }

            function submit() {
                if (scope.ngSearch) scope.ngSearch();
            }
        }
    }
}