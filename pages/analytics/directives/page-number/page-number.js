export default function pageNumber($rootScope) {
    "ngInject";

    const templatePath = '/pages/analytics/directives/';
    const directory = 'page-number';
    const url = templatePath + directory + '/' + directory + '.html';
    const countPerPageList = 5;

    function setParams(size, count, currentPageString) {

        let currentPage = parseInt(currentPageString);

        let totalPage = Math.ceil(count / size);

        let isFirstPage = currentPage <= countPerPageList;
        let rest = totalPage % countPerPageList;

        if (rest == 0) {
            rest = 5;
        }

        let isLastPage = currentPage > (totalPage - rest);

        if (totalPage <= countPerPageList) {
            isLastPage = true;
        }

        let pageNumbers = [];
        let selectedPageIndex;

        let frontNumber;
        let rearNumber;
        let temp = currentPageString.split('');

        let currentStartPage;
        let currentEndPage;

        if (currentPageString != '10' && currentPageString.length > 1) {

            frontNumber = currentPageString.substr(0, currentPageString.length - 1);
            rearNumber = temp[temp.length - 1];

            if (rearNumber > 5) {
                currentStartPage = parseInt(frontNumber + '6');
                currentEndPage = parseInt(parseInt(frontNumber) + 1 + '0');
            } else {

                if (parseInt(rearNumber) == 0) {
                    currentEndPage = parseInt(currentPageString);
                    currentStartPage = parseInt(currentPageString) - 4;
                } else {
                    currentStartPage = parseInt(frontNumber + '1');
                    currentEndPage = parseInt(frontNumber + '5');
                }

            }

        } else {

            frontNumber = currentPageString;

            if (currentPageString == '10' || frontNumber > 5) {
                currentStartPage = 6;
                currentEndPage = 10;
            } else {
                currentStartPage = 1;
                currentEndPage = 5;
            }

        }


        for (let i = currentStartPage; i <= currentEndPage && i <= totalPage; i++) {
            pageNumbers.push(i);

            if (currentPage == i) {
                selectedPageIndex = i;
            }
        }

        return {
            pageNumbers: pageNumbers,
            selectedPageIndex: selectedPageIndex,
            isFirstPage: isFirstPage,
            isLastPage: isLastPage,
            totalPage: totalPage
        };

    }

    return {
        restrict: 'E',
        scope: {
            size: '=',
            total: '=',
            currentPageString: '@',
            func: '='
        },
        templateUrl: url,
        link: (scope, element, attr) => {
            active();

            scope.$watch('size', (newVal, oldVal) => {
                if (newVal != oldVal) {
                    active();
                }
            }, true);

            scope.$watch('total', (newVal, oldVal) => {
                if (newVal != oldVal) {
                    active();
                }
            }, true);

            scope.$watch('currentPageString', (newVal, oldVal) => {
                if (newVal != oldVal) {
                    active();
                }
            }, true);

            function active () {
                scope.pagination = setParams(scope.size, scope.total, scope.currentPageString.toString());
            }

            scope.goPage = scope.func;

        }
    }
}