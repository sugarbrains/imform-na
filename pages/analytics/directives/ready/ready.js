export default function ready($rootScope) {
    'ngInject';

    return {
        restrict: 'A',
        scope: {
            id: '@'
        },
        link: (scope, element, attr) => {
            $rootScope.$broadcast(scope.id);
        }
    }
}