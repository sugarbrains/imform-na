export default function fileOver () {
    "ngInject";

    return {
        restrict: 'A',
        link: (scope, element, attr) => {
            element.on('dragleave', function () {
                $(this).removeClass('file-over');
            });
        }
    }
}