export default function listHeader($rootScope) {
    'ngInject';

    const templatePath = '/pages/analytics/directives/';
    const directory = 'list-header';
    const url = templatePath + directory + '/' + directory + '.html';

    return {
        restrict: 'E',
        scope: {
            headers: '=',
            events: '=',
            orderBys: '=',
            isCheck: '=',
            checkFunc: '='
        },
        templateUrl: url,
        link: (scope, element, attr) => {
            scope.click = click;

            scope.form = {
                isCheck: scope.isCheck
            };

            scope.$watch('form.isCheck', (n, o) => {
                if (n != o) {
                    scope.isCheck = n;
                }
            }, true);

            scope.$watch('isCheck', (n, o) => {
                if (n != o) {
                    scope.form.isCheck = scope.isCheck;
                }
            }, true);

            if(scope.orderBys) {
                if (scope.headers.length != scope.events.length ||
                    scope.headers.length != scope.orderBys.length) {
                    console.error("list header error");
                }
            }

            function click(index) {
                if(scope.events && scope.events.length) {
                    let event = scope.events[index];
                    if (event) {
                        $rootScope.$broadcast(event);
                    }
                }
            }
        }
    }
}