export default function preventTouch () {
    "ngInject";

    return {
        restrict: 'AE',
        link: function (scope, element, attrs) {
            element[0].addEventListener('touchmove', function (e) {
                e.preventDefault();
            });
        }
    }
}