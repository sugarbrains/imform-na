route.$inject = ['$stateProvider', '$urlRouterProvider', 'constant'];

export default function route($stateProvider, $urlRouterProvider, constant) {
    const enumQueries = constant.enumQueries;
    const pathContents = '/pages/analytics/views/contents/';

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('index', {
            url: '/',
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'index.html';
                    }
                }
            }
        })
        .state('receiverGroupManage', {
            url: '/receiver-group-manage.php' + returnQueries(enumQueries.receiverGroupManage),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'receiver-group-manage/index.html';
                    }
                }
            }
        })
        .state('receiverManage', {
            url: '/receiver-manage.php' + returnQueries(enumQueries.receiverManage),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'receiver-manage/index.html';
                    }
                }
            }
        })
        .state('userManage', {
            url: '/user-manage.php' + returnQueries(enumQueries.userManage),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'user-manage/index.html';
                    }
                }
            }
        })
        .state('sendManage', {
            url: '/send-manage.php' + returnQueries(enumQueries.sendManage),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'send-manage/index.html';
                    }
                }
            }
        })
        .state('messageManage', {
            url: '/message-manage.php' + returnQueries(enumQueries.messageManage),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'message-manage/index.html';
                    }
                }
            }
        })
        .state('sendAnalysis', {
            url: '/send-analysis.php' + returnQueries(enumQueries.sendAnalysis),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'send-analysis/index.html';
                    }
                }
            }
        })
        .state('senderAnalysis', {
            url: '/sender-analysis.php' + returnQueries(enumQueries.senderAnalysis),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'sender-analysis/index.html';
                    }
                }
            }
        })
        .state('chart', {
            url: '/chart.php' + returnQueries(enumQueries.chart),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'chart/index.html';
                    }
                }
            }
        })
        .state('calculationManage', {
            url: '/calculation-manage.php' + returnQueries(enumQueries.calculationManage),
            views: {
                'contents': {
                    templateUrl: () => {
                        return pathContents + 'calculation-manage/index.html';
                    }
                }
            }
        });

    function returnQueries (enums) {
        let query = '';
        enums.forEach((key, index) => {
            if (index) {
                query += '&' + key;
            } else {
                query += '?' + key;
            }
        });
        return query;
    }
}