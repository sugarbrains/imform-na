config.$inject = ['$locationProvider', '$sceProvider', '$httpProvider'];

export default function config($locationProvider, $sceProvider, $httpProvider) {
    $locationProvider.html5Mode(true);
    $sceProvider.enabled(true);
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    if (!$httpProvider.defaults.headers.delete) {
        $httpProvider.defaults.headers.delete = {};
    }
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

    $httpProvider.defaults.headers.delete = {'Content-Type': 'application/json;charset=utf-8'};
}