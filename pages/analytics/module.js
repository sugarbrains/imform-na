// preload
import uiRouter from "@uirouter/angularjs";
import ngResource from "angular-resource";
import 'angularjs-datepicker';
import 'angular-file-upload';
import './assets/locale';
import 'angular-cookies';

// config
import config from './config/config';
import routing from './config/route';
import configInput from './config/config-input';
import configTextarea from './config/config-textarea';

// constant
import constant from './services/contants/constant';
import resource from './services/contants/resource';
import dataValue from './services/contants/data-value';

// controller
import MainCtrl from './controllers/main';
import DialogCtrl from './controllers/dialog';
import LoadingCtrl from './controllers/loading';
import HeaderCtrl from './controllers/header';
import SubHeaderCtrl from './controllers/sub-header';
import NavigationCtrl from './controllers/navigation';
import IndexCtrl from './controllers/index';
import ReceiverGroupManageCtrl from './controllers/receiver-group-manage';
import ReceiverManageCtrl from './controllers/receiver-manage';
import UserManageCtrl from './controllers/user-manage';
import SendManageCtrl from './controllers/send-manage';
import MessageManageCtrl from './controllers/message-manage';
import SendAnalysisCtrl from './controllers/send-analysis';
import SenderAnalysisCtrl from './controllers/sender-analysis';
import ChartCtrl from './controllers/chart';
import CalculationManageCtrl from './controllers/calculation-manage';

import SubmitReceiverCtrl from './controllers/modals/submit-receiver';
import UpdateReceiverCtrl from './controllers/modals/update-receiver';
import SubmitReceiverGroupCtrl from './controllers/modals/submit-receiver-group';
import SubmitReceiverGroupWithFileCtrl from './controllers/modals/submit-receiver-group-with-file';
import UpdateReceiverGroupCtrl from './controllers/modals/update-receiver-group';
import ReceiverGroupCtrl from './controllers/modals/receiver-group';
import RegisterReceiverGroupCtrl from './controllers/modals/register-receiver-group';
import SentCountCtrl from './controllers/modals/sent-count';
import InsertPhoneNumCtrl from './controllers/modals/insert-phone-num';
import SelectSendTypeCtrl from './controllers/modals/select-send-type';
import SelectFunctionCtrl from './controllers/modals/select-function';
import DetailOpenerCountCtrl from './controllers/modals/detail-opener-count';
import DetailOpenerCountDiagramCtrl from './controllers/modals/detail-opener-count-diagram';
import DetailHourDiagramCtrl from './controllers/modals/detail-hour-diagram';
import DetailDayDiagramCtrl from './controllers/modals/detail-day-diagram';
import DetailPerDayDiagramCtrl from './controllers/modals/detail-per-day-diagram';
import DetailPerHourDiagramCtrl from './controllers/modals/detail-per-hour-diagram';
import DetailGenderDiagramCtrl from './controllers/modals/detail-gender-diagram';
import DetailAgeDiagramCtrl from './controllers/modals/detail-age-diagram';
import DetailAddressDiagramCtrl from './controllers/modals/detail-address-diagram';
import DetailPageCountDiagramCtrl from './controllers/modals/detail-page-count-diagram';
import DetailPageTimeDiagramCtrl from './controllers/modals/detail-page-time-diagram';
import DetailPerHourCouponDiagramCtrl from './controllers/modals/detail-per-hour-coupon-diagram';
import DetailPerDayCouponDiagramCtrl from './controllers/modals/detail-per-day-coupon-diagram';
import DetailCouponCountDiagramCtrl from './controllers/modals/detail-coupon-count-diagram';
import DetailCouponTypeCountDiagramCtrl from './controllers/modals/detail-coupon-type-count-diagram';
import SentMessageListCtrl from './controllers/modals/sent-message-list';
import ReceiverListCtrl from './controllers/modals/receiver-list';
import SentManageCtrl from './controllers/modals/sent-manage';
import UpdateUserCtrl from './controllers/modals/update-user';
import MyProfileCtrl from './controllers/modals/my-profile';
import CalculationSentMessageCtrl from './controllers/calculation-sent-message';
import CalculationPaymentCtrl from './controllers/calculation-payment';

// directive
import selectBox from './directives/select-box/select-box';
import checkBox from './directives/check-box/check-box';
import radioBox from './directives/radio-box/radio-box';
import searchInput from './directives/search-input/search-input';
import pageNumber from './directives/page-number/page-number';
import listHeader from './directives/list-header/list-header';
import fileOver from './directives/file-over/file-over';
import preventTouch from './directives/prevent-touch/prevent-touch';
import ready from './directives/ready/ready';
import fileUpload from './directives/file-upload/file-upload';
import fileread from './directives/fileread.directive';

// service
import navigator from './services/navigator';
import dialogHandler from './services/dialog-handler';
import loadingHandler from './services/loading-handler';
import stateHandler from './services/state-handler';
import orderHandler from './services/order-handler';
import modalHandler from './services/modal-handler';
import checkHandler from './services/check-handler';
import enumHandler from './services/enum-handler';
import messageHandler from './services/message-handler';
import chartHandler from './services/chart-handler';
import dateHandler from './services/date-handler';
import addressHandler from './services/address-handler';

// provider
import metaManager from './services/providers/meta-manager';

// manager
import sessionManager from './services/managers/session-manager';
import receiverManager from './services/managers/receiver-manager';
import userManager from './services/managers/user-manager';
import receiverGroupManager from './services/managers/receiver-group-manager';
import sendHistoryManager from './services/managers/send-history-manager';
import addressManager from './services/managers/address-manager';
import chartManager from './services/managers/chart-manager';
import messageManager from './services/managers/message-manager';
import messageListManager from './services/managers/message-list-manager';
import receiverGroupImportHistoryManger from './services/managers/receiver-group-import-history-manager';
import messageReceiverGroupManager from './services/managers/message-receiver-group-manager';
import domainManager from './services/managers/domain-manager';
import calculationManager from './services/managers/calculation-manager';
import senderHistoryManager from './services/managers/sender-history-manager';

// model
import Session from './services/models/session';
import Receiver from './services/models/receiver';
import User from './services/models/user';
import ReceiverGroup from './services/models/receiver-group';
import SendHistory from './services/models/send-history';
import Sender from './services/models/sender';
import Address from './services/models/address';
import ChartOpenerCount from './services/models/chart-opener-count';
import ChartGender from './services/models/chart-gender';
import ChartAge from './services/models/chart-age';
import ChartAddress from './services/models/chart-address';
import ChartPageCount from './services/models/chart-page-count';
import ChartPageTime from './services/models/chart-page-time';
import ChartHour from './services/models/chart-hour';
import ChartDay from './services/models/chart-day';
import MessageList from './services/models/message-list';
import SendHistoryReceiverGroup from './services/models/send-history-receiver-group';
import Message from './services/models/message';
import MessageSend from './services/models/message-send';
import ReceiverGroupImportHistory from './services/models/receiver-group-import-history';
import MessageReceiverGroup from './services/models/message-receiver-group';
import MessageExport from './services/models/message-export';
import MessageCancel from './services/models/message-cancel';
import ExportHistory from './services/models/export-history';
import Domain from './services/models/domain';
import ChargeCount from './services/models/charge-count';
import SendCompleteHistory from './services/models/send-complete-history';
import Payment from './services/models/payment';
import SenderHistory from './services/models/sender-history';

// filter
import returnPage from './filters/return-page';
import returnOffset from './filters/return-offset';
import returnJoin from './filters/return-join';
import checkArrayValue from './filters/check-array-value';
import attachZero from './filters/attach-zero';
import trustAsHtml from './filters/trust-as-html';
import noData from './filters/no-data';
import toISODate from './filters/to-iso-date';

// style sheet
import 'angularjs-datepicker/dist/angular-datepicker.min.css';
import './assets/stylesheets/_main.scss';

const APP_NAME = 'analytics';

angular.module(APP_NAME, [uiRouter, ngResource, 'ngCookies', '720kb.datepicker', 'angularFileUpload', 'ngLocale'])
    .config(config)
    .config(routing)
    .config(configInput)
    .config(configTextarea)

    .constant("constant", constant)
    .constant("resource", resource)
    .constant("dataValue", dataValue)

    .controller("MainCtrl", MainCtrl)
    .controller("DialogCtrl", DialogCtrl)
    .controller("LoadingCtrl", LoadingCtrl)
    .controller("HeaderCtrl", HeaderCtrl)
    .controller("SubHeaderCtrl", SubHeaderCtrl)
    .controller("NavigationCtrl", NavigationCtrl)
    .controller("IndexCtrl", IndexCtrl)
    .controller("ReceiverGroupManageCtrl", ReceiverGroupManageCtrl)
    .controller("ReceiverManageCtrl", ReceiverManageCtrl)
    .controller("UserManageCtrl", UserManageCtrl)
    .controller("SendManageCtrl", SendManageCtrl)
    .controller("MessageManageCtrl", MessageManageCtrl)
    .controller("SendAnalysisCtrl", SendAnalysisCtrl)
    .controller("SenderAnalysisCtrl", SenderAnalysisCtrl)
    .controller("ChartCtrl", ChartCtrl)
    .controller("CalculationManageCtrl", CalculationManageCtrl)
    .controller("SentMessageListCtrl", SentMessageListCtrl)
    .controller("SentManageCtrl", SentManageCtrl)


    .controller("SubmitReceiverCtrl", SubmitReceiverCtrl)
    .controller("UpdateReceiverCtrl", UpdateReceiverCtrl)
    .controller("SubmitReceiverGroupCtrl", SubmitReceiverGroupCtrl)
    .controller("SubmitReceiverGroupWithFileCtrl", SubmitReceiverGroupWithFileCtrl)
    .controller("UpdateReceiverGroupCtrl", UpdateReceiverGroupCtrl)
    .controller("ReceiverGroupCtrl", ReceiverGroupCtrl)
    .controller("RegisterReceiverGroupCtrl", RegisterReceiverGroupCtrl)
    .controller("SentCountCtrl", SentCountCtrl)
    .controller("InsertPhoneNumCtrl", InsertPhoneNumCtrl)
    .controller("SelectSendTypeCtrl", SelectSendTypeCtrl)
    .controller("SelectFunctionCtrl", SelectFunctionCtrl)
    .controller("DetailOpenerCountCtrl", DetailOpenerCountCtrl)
    .controller("DetailOpenerCountDiagramCtrl", DetailOpenerCountDiagramCtrl)
    .controller("DetailHourDiagramCtrl", DetailHourDiagramCtrl)
    .controller("DetailDayDiagramCtrl", DetailDayDiagramCtrl)
    .controller("DetailPerHourDiagramCtrl", DetailPerHourDiagramCtrl)
    .controller("DetailPerDayDiagramCtrl", DetailPerDayDiagramCtrl)
    .controller("DetailGenderDiagramCtrl", DetailGenderDiagramCtrl)
    .controller("DetailAgeDiagramCtrl", DetailAgeDiagramCtrl)
    .controller("DetailAddressDiagramCtrl", DetailAddressDiagramCtrl)
    .controller("DetailPageCountDiagramCtrl", DetailPageCountDiagramCtrl)
    .controller("DetailPageTimeDiagramCtrl", DetailPageTimeDiagramCtrl)
    .controller("DetailPerHourCouponDiagramCtrl", DetailPerHourCouponDiagramCtrl)
    .controller("DetailPerDayCouponDiagramCtrl", DetailPerDayCouponDiagramCtrl)
    .controller("DetailCouponCountDiagramCtrl", DetailCouponCountDiagramCtrl)
    .controller("DetailCouponTypeCountDiagramCtrl", DetailCouponTypeCountDiagramCtrl)
    .controller("ReceiverListCtrl", ReceiverListCtrl)
    .controller("UpdateUserCtrl", UpdateUserCtrl)
    .controller("MyProfileCtrl", MyProfileCtrl)
    .controller("CalculationPaymentCtrl", CalculationPaymentCtrl)
    .controller("CalculationSentMessageCtrl", CalculationSentMessageCtrl)

    .directive("input", fileread)
    .directive("selectBox", selectBox)
    .directive("checkBox", checkBox)
    .directive("radioBox", radioBox)
    .directive("searchInput", searchInput)
    .directive("pageNumber", pageNumber)
    .directive("listHeader", listHeader)
    .directive("fileOver", fileOver)
    .directive("fileUpload", fileUpload)
    .directive("preventTouch", preventTouch)
    .directive("ready", ready)

    .provider("metaManager", metaManager)

    .service("navigator", navigator)
    .service("dialogHandler", dialogHandler)
    .service("loadingHandler", loadingHandler)
    .service("stateHandler", stateHandler)
    .service("orderHandler", orderHandler)
    .service("modalHandler", modalHandler)
    .service("checkHandler", checkHandler)
    .service("enumHandler", enumHandler)
    .service("messageHandler", messageHandler)
    .service("chartHandler", chartHandler)
    .service("dateHandler", dateHandler)
    .service("addressHandler", addressHandler)

    .service("sessionManager", sessionManager)
    .service("receiverManager", receiverManager)
    .service("userManager", userManager)
    .service("receiverGroupManager", receiverGroupManager)
    .service("sendHistoryManager", sendHistoryManager)
    .service("addressManager", addressManager)
    .service("chartManager", chartManager)
    .service("messageManager", messageManager)
    .service("messageListManager", messageListManager)
    .service("receiverGroupImportHistoryManger", receiverGroupImportHistoryManger)
    .service("messageReceiverGroupManager", messageReceiverGroupManager)
    .service("domainManager", domainManager)
    .service("calculationManager", calculationManager)
    .service("senderHistoryManager", senderHistoryManager)

    .factory("Session", Session)
    .factory("Receiver", Receiver)
    .factory("User", User)
    .factory("ReceiverGroup", ReceiverGroup)
    .factory("SendHistory", SendHistory)
    .factory("Sender", Sender)
    .factory("Address", Address)
    .factory("ChartOpenerCount", ChartOpenerCount)
    .factory("ChartGender", ChartGender)
    .factory("ChartAge", ChartAge)
    .factory("ChartAddress", ChartAddress)
    .factory("ChartPageCount", ChartPageCount)
    .factory("ChartPageTime", ChartPageTime)
    .factory("ChartHour", ChartHour)
    .factory("ChartDay", ChartDay)
    .factory("MessageList", MessageList)
    .factory("Message", Message)
    .factory("SendHistoryReceiverGroup", SendHistoryReceiverGroup)
    .factory("ReceiverGroupImportHistory", ReceiverGroupImportHistory)
    .factory("MessageReceiverGroup", MessageReceiverGroup)
    .factory("MessageSend", MessageSend)
    .factory("MessageExport", MessageExport)
    .factory("MessageCancel", MessageCancel)
    .factory("ExportHistory", ExportHistory)
    .factory("Domain", Domain)
    .factory("ChargeCount", ChargeCount)
    .factory("SendCompleteHistory", SendCompleteHistory)
    .factory("Payment", Payment)
    .factory("SenderHistory", SenderHistory)

    .filter("returnPage", returnPage)
    .filter("returnOffset", returnOffset)
    .filter("returnJoin", returnJoin)
    .filter("checkArrayValue", checkArrayValue)
    .filter("attachZero", attachZero)
    .filter("noData", noData)
    .filter("toISODate", toISODate)
    .filter("trustAsHtml", trustAsHtml);

if (window.location.hash === '#_=_') window.location.hash = '';

angular.element(document).ready(() => {
    angular.bootstrap(document, [APP_NAME]);
});

export default APP_NAME;