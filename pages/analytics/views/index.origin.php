<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 8. 31.
 * Time: AM 11:19
 */
require_once(dirname(__FILE__).'/../../../server/config/meta.php');
require_once(dirname(__FILE__).'/../../../server/db/ButtonPresets.php');

$ButtonPresets = new ButtonPresets();
$buttonPresets = $ButtonPresets->getsByDomainId($_SESSION['domainId']);
$ButtonPresets->close();

?>
<!DOCTYPE html>
<html xmlns:ng="http://angularjs.org">
<head>
    <meta charset="UTF-8" lang="ko">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta http-equiv=”Expires” content=”Mon, 06 Jan 1990 00:00:01 GMT”>
    <meta http-equiv=”Expires” content=”-1″>
    <meta http-equiv=”Pragma” content=”no-cache”>
    <meta http-equiv=”Cache-Control” content=”no-cache”>
    <base href="/routes/analytics/">
    <title>ANALYTICS</title>
    <script type="text/javascript">
        window.session = <?php echo $_SESSION['id'] ? json_encode($_SESSION, JSON_UNESCAPED_UNICODE) : null ?>;
        window.meta = <?php echo META ? json_encode(META, JSON_UNESCAPED_UNICODE) : null ?>;
        window.buttonPresets = <?php echo json_encode($buttonPresets, JSON_UNESCAPED_UNICODE) ?>;
    </script>
    <!-- inject:css -->
    <!-- endinject -->
</head>
<body data-ng-controller="MainCtrl">
<div data-ng-include="templatePath + 'layouts/dialog.html'"></div>
<div data-ng-include="templatePath + 'layouts/loading.html'"></div>
<div data-ng-include="templatePath + 'layouts/header.html'"></div>
<div data-ng-include="templatePath + 'layouts/sub-header.html'"></div>
<div data-ng-include="templatePath + 'layouts/navigation.html'"></div>
<div data-ng-include="templatePath + 'modals/index.html'"></div>
<article id="mainContentsWrap">
    <div id="mainContents" ui-view="contents"></div>
</article>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- inject:js -->
<!-- endinject -->
</body>
</html>