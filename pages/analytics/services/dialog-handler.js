export default function dialogHandler(constant) {
    'ngInject';

    const dialogKey = constant.dialogKey;
    let rootScope;

    this.init = init;
    this.show = show;
    this.alertError = alertError;

    function init($rootScope) {
        rootScope = $rootScope;
    }

    function show(text, submitOptions, cancelOptions) {
        rootScope.$broadcast(dialogKey.show, {
            text: text,
            submitOptions: submitOptions,
            cancelOptions: cancelOptions
        });
    }

    function alertError(status, data) {
        rootScope.$broadcast(dialogKey.alertError, {
            status: status,
            data: data
        });
    }
}