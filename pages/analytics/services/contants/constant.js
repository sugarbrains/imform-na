const constant = {
    groupTitle: {
        index: {},
        receiverGroupManage: {
            group: '기능',
            title: '발송 그룹 관리'
        },
        receiverManage: {
            group: '기능',
            title: '수신자 관리'
        },
        userManage: {
            group: '기능',
            title: '사용자 관리'
        },
        messageManage: {
            group: '기능',
            title: '발송 메시지 관리'
        },
        sendAnalysis: {
            group: '분석',
            title: '발송 분석'
        },
        senderAnalysis: {
            group: '분석',
            title: '발송자 분석'
        },
        calculationManage: {
            group: '기능',
            title: '정산 관리'
        }
    },
    enumQueries: {
        receiverGroupManage: ['orderBy', 'sort', 'size', 'offset', 'searchField', 'searchItem'],
        receiverManage: ['orderBy', 'sort', 'size', 'offset', 'searchField', 'searchItem', 'receiverGroupId', 'startDate', 'endDate', 'genders', 'ages', 'sido', 'sigungu', 'eupmyeondong'],
        userManage: ['orderBy', 'sort', 'size', 'offset', 'searchField', 'searchItem'],
        sendManage: ['state', 'id'],
        messageManage: ['orderBy', 'sort', 'size', 'offset', 'searchField', 'searchItem', 'startDate', 'endDate'],
        sendAnalysis: ['orderBy', 'sort', 'size', 'offset', 'startDate', 'endDate', 'searchField', 'searchItem', 'senderIds'],
        senderAnalysis: ['orderBy', 'sort', 'size', 'offset', 'searchField', 'searchItem'],
        chart: ['state', 'senderIds', 'sendHistoryIds', 'searchField', 'searchItem', 'genders', 'ages', 'sido', 'sigungu', 'eupmyeondong', 'receiverCount', 'openCount', 'diagramHour', 'diagramDay'],
        calculationManage: ['orderBy', 'sort', 'size', 'offset', 'activeTab', 'startDate', 'endDate']
    },
    defaultQuery: {
        receiverGroupManage: {
            orderBy: 'createdAt',
            sort: 'DESC',
            searchField: 'name',
            searchItem: '',
            offset: 0,
        },
        receiverManage: {
            orderBy: 'createdAt',
            sort: 'DESC',
            searchField: 'title',
            searchItem: '',
            offset: 0,
            receiverGroupId: '',
            sido: '',
            sigungu: '',
            eupmyeondong: '',
            startDate: '',
            endDate: '',
            ages: '',
            genders: ''
        },
        userManage: {
            orderBy: 'createdAt',
            sort: 'DESC',
            searchField: 'uid',
            searchItem: '',
            offset: 0
        },
        sendManage: {
            sendHistoryId: ''
        },
        messageManage: {
            orderBy: 'createdAt',
            sort: 'DESC',
            offset: 0,
            searchField: 'title',
            searchItem: '',
            startDate: '',
            endDate: ''
        },
        sendAnalysis: {
            orderBy: 'activeDate',
            sort: 'DESC',
            offset: 0,
            startDate: '',
            endDate: '',
            searchField: 'title',
            searchItem: '',
            senderIds: '',
        },
        senderAnalysis: {
            orderBy: 'id',
            sort: 'DESC',
            offset: 0,
            searchField: 'name',
            searchItem: ''
        },
        chart: {
            searchField: '',
            searchItem: '',
            sido: '',
            sigungu: '',
            eupmyeondong: '',
            ages: 'age20,age30,age40,age50,ageNone',
            genders: 'genderMale,genderFemale,genderNone',
            receiverCount: '',
            openCount: 0,
            diagramHour: 72,
            diagramDay: 7
        },
        calculationManage: {
            orderBy: 'createdAt',
            sort: 'DESC',
            offset: 0,
            startDate: '',
            endDate: '',
            activeTab: 'sentMessages'
        }
    },
    dialogKey: {
        show: 'dialog.show',
        alertError: 'dialog.alert-error'
    },
    loadingKey: {
        startLoading: 'loading.start',
        endLoading: 'loading.end',
        loadingText: 'loading.text',
        alertError: 'loading.alert-error'
    },
    modalOverlayClose: true,
    listHeaders: {
        receiverGroupManage: ['No.', '그룹 이름', '수신자 수', '발송 횟수', '등록일'],
        receiverManage: ['No.', 'key', '전화번호', '소속 그룹', '발송 횟수', '등록일'],
        receiverGroup: ['No.', '그룹 이름', '소속일', '관리'],
        receivers: ['No.', 'KEY', '이름', '성별', '나이', '전화번호유무', '주소유무'],
        sentMessageList: ['No.', '메시지 명', '등록일', '발송일'],
        sentCount: ['No.', '메시지 이름', '발송일'],
        userManage: ['No.', '이름', '아이디', '권한', 'URL', '수신거부 메시지', '등록일'],
        messageManage: ['No.', '상태', '메시지 이름', '수신자 수', '발송 수', '기능', '등록일', '(예약)발송일'],
        calcSentMessage: ['No.', '메시지 이름', '발송일', '성공 발송 건수', '총 금액', '등록일'],
        calcPayment: ['No.', '금액', '입금일'],
        sendAnalysis: ['No.', '상태', '발송자', '메시지 이름', '발송일', '수신자 수', '발송 수', '열람율'],
        senderAnalysis: ['No.', '이름', 'URL', '발송 횟수', '발송일', '등록일'],
        hourDiagram: ['시간', '열람 수'],
        dayDiagram: ['일수', '열람 수'],
        perHourDiagram: ['시간', '접속량', '접속률'],
        perDayDiagram: ['요일', '접속량', '접속률'],
        detailOpenerCountDiagram: ['발송 수', '열람자 수', '미열람자 수', '재열람자 수'],
        detailOpenerCount: ['URL 접속 횟수', 'URL 재접속 횟수', '평균 URL 접속 횟수'],
        detailGender: ['남자', '여자', '알 수 없음'],
        detailAges: ['나이', '인원'],
        detailPageCounts: ['페이지 명', '열람 수'],
        detailPageTimes: ['페이지 명', '체류시간(초)'],
        detailAddresses: ['주소', '열람 수'],
        detailDays: ['일자', '열람 수'],
        detailHours: ['시간', '열람 수'],
    },
    listKeys: {
        receiverGroupManage: [
            'id',
            'name',
            'receiverCount',
            'sentCount',
            'createdAt'
        ],
        receiverManage: [
            'serialNum',
            'key',
            'phoneNum',
            'groupCount',
            'sentCount',
            'createdAt'
        ],
        receiverGroup: [
            '',
            '',
            ''
        ],
        receiver: [
            'id',
            'key',
            'name'
        ],
        sentMessageList: [
            'id',
            'redirectUrl',
            'activeDate',
            'createdAt'
        ],
        sentCount: [
            'serialNum',
            'title',
            'sentAt'
        ],
        userManage: [
            'id',
            'name',
            'uid',
            '',
            '',
            '',
            'createdAt'
        ],
        messageManage: [
            'id',
            '',
            'title',
            '',
            '',
            '',
            'createdAt',
            ''
        ],
        calcSentMessage: [
            'id',
            'title',
            'activeDate',
            '',
            '',
            'createdAt',
        ],
        calcPayment: [
            'id',
            'price',
            'createdAt'
        ],
        sendAnalysis: [
            'id',
            '',
            'name',
            'title',
            'activeDate',
            '',
            '',
            ''
        ],
        senderAnalysis: [
            'id',
            'name',
            '',
            '',
            'activeDate',
            'createdAt'
        ],
        hourDiagram: [
            'hour',
            'openCount',
            'openRate'
        ],
        dayDiagram: [
            'day',
            'openCount',
            'openRate'
        ],
        perHourDiagram: [
            'hour',
            'connectCount',
            'connectRate'
        ],
        perDayDiagram: [
            'day',
            'connectCount',
            'connectRate'
        ],
        detailOpenerCount: [
            '',
            '',
            '',
        ]
    },
    orderKeys: {
        receiverGroupManage: [
            'receiver-group-manage.id',
            'receiver-group-manage.name',
            'receiver-group-manage.receiverCount',
            'receiver-group-manage.sentCount',
            'receiver-group-manage.createdAt',
        ],
        receiverManage: [
            'receiver-manage.serialNum',
            'receiver-manage.key',
            'receiver-manage.phoneNum',
            'receiver-manage.groupCount',
            'receiver-manage.sentCount',
            'receiver-manage.createdAt'
        ],
        receiverGroup: [
            '',
            '',
            '',
            ''
        ],
        receivers: [
            'receivers.id',
            'receivers.key',
            'receivers.name',
            '',
            '',
            '',
            '',
        ],
        sentMessageList: [
            'sentMessageList.id',
            'sentMessageList.title',
            'sentMessageList.activeDate',
            'sentMessageList.createdAt',
        ],
        sentCount: [
            'sent-count.serialNum',
            'sent-count.title',
            'sent-count.sentAt'
        ],
        userManage: [
            'user-manage.id',
            'user-manage.name',
            'user-manage.uid',
            '',
            '',
            '',
            'user-manage.createdAt'
        ],
        messageManage: [
            'message-manage.id',
            '',
            'message-manage.title',
            '',
            '',
            '',
            'message-manage.createdAt',
            ''
        ],
        calcSentMessage: [
            'calc-sent-message.id',
            'calc-sent-message.title',
            'calc-sent-message.activeDate',
            '',
            '',
            'calc-sent-message.createdAt',
        ],
        calcPayment: [
            'calc-payment.id',
            'calc-payment.price',
            'calc-payment.createdAt',
        ],
        sendAnalysis: [
            'send-analysis.id',
            '',
            'send-analysis.name',
            'send-analysis.title',
            'send-analysis.activeDate',
            '',
            '',
            ''
        ],
        senderAnalysis: [
            'sender-analysis.id',
            'sender-analysis.name',
            '',
            '',
            'sender-analysis.activeDate',
            'sender-analysis.createdAt',
        ],
        hourDiagram: [
            'hour-diagram.hour',
            'hour-diagram.openCount',
            'hour-diagram.openRate'
        ],
        dayDiagram: [
            'day-diagram.day',
            'day-diagram.openCount',
            'day-diagram.openRate'
        ],
        perHourDiagram: [
            'per-hour-diagram.hour',
            'per-hour-diagram.connectCount',
            'per-hour-diagram.connectRate'
        ],
        perDayDiagram: [
            'per-day-diagram.day',
            'per-day-diagram.connectCount',
            'per-day-diagram.connectRate'
        ],
        detailOpenerCount: [
            '',
            '',
            '',
        ]
    },
    modalKey: {
        sentManage: 'modal-open.sent-manage',
        submitReceiver: 'modal-open.submit-receiver',
        submitReceiverGroup: 'modal-open.submit-receiver-group',
        submitReceiverGroupWithFile: 'modal-open.submit-receiver-group-with-file',
        updateReceiverGroup: 'modal-open.update-receiver-group',
        updateUser: 'modal-open.update-user',
        myProfile: 'modal-open.my-profile',
        updateReceiver: 'modal-open.update-receiver',
        receiverGroup: 'modal-open.receiver-group',
        registerReceiverGroup: 'modal-open.register-receiver-group',
        sentCount: 'modal-open.sent-count',
        sentMessageList: 'modal-open.sent-message-list',
        receiverList: 'modal-open.receiver-list',
        insertPhoneNum: 'modal-open.insert-phone-num',
        selectSendType: 'modal-open.select-send-type',
        selectFunction: 'modal-open.select-function',
        detailOpenerCount: 'modal-open.detail-opener-count',
        detailOpenerCountDiagram: 'modal-open.detail-opener-count-diagram',
        detailHourDiagram: 'modal-open.detail-hour-diagram',
        detailDayDiagram: 'modal-open.detail-day-diagram',
        detailPerHourDiagram: 'modal-open.detail-per-hour-diagram',
        detailPerDayDiagram: 'modal-open.detail-per-day-diagram',
        detailGenderDiagram: 'modal-open.detail-gender-diagram',
        detailAgeDiagram: 'modal-open.detail-age-diagram',
        detailAddressDiagram: 'modal-open.detail-address-diagram',
        detailPageCountDiagram: 'modal-open.detail-page-count-diagram',
        detailPageTimeDiagram: 'modal-open.detail-page-time-diagram',
        detailPerHourCouponDiagram: 'modal-open.detail-per-hour-coupon-diagram',
        detailPerDayCouponDiagram: 'modal-open.detail-per-day-coupon-diagram',
        detailCouponCountDiagram: 'modal-open.detail-coupon-count-diagram',
        detailCouponTypeCountDiagram: 'modal-open.detail-coupon-type-count-diagram'
    },
    eventKey: {
        submitReceiver: 'event.submit-receiver',
        updateReceiver: 'event.update-receiver',
        deleteReceiver: 'event.delete-receiver',
        submitReceiverGroup: 'event.submit-receiver-group',
        submitReceiverGroupWithFile: 'event.submit-receiver-group-with-file',
        updateReceiverGroup: 'event.update-receiver-group',
        updateUser: 'event.update-user',
        removeReceiverGroup: 'event.remove-receiver-group',
        withdrawReceiverGroup: 'event.withdraw-receiver-group',
        registerReceiverGroup: 'event.register-receiver-group',
        insertPhoneNum: 'event.insert-phone-num',
        selectFunction: 'event.select-function',
        sendPost: 'event.send-post'
    },
    chartKey: {
        openCount: 'chartOpenCount',
        reopenCount: 'chartReopenCount',
        hourDiagram: 'chartHourDiagram',
        dayDiagram: 'chartDayDiagram',
        perHourDiagram: 'chartPetHourDiagram',
        perDayDiagram: 'chartPerDayDiagram',
        genderDiagram: 'chartGenderDiagram',
        ageDiagram: 'chartAgeDiagram',
        addressDiagram: 'chartAddressDiagram',
        pageCountDiagram: 'chartPageCountDiagram',
        pageTimeDiagram: 'chartPageTimeDiagram'
    },
    chartLabels: {
        openCount: ['열람자 수', '비열람자 수'],
        reopenCount: ['재열람자 수', '비재열람자 수'],
        perDayDiagram: ['월', '화', '수', '목', '금', '토', '일'],
        genderDiagram: ['남자', '여자', '모름'],
        ageDiagram: ['20대', '30대', '40대', '50대 이상', '모름']
    },
    enumMessageManageStates: {
        standby: "기본",
        send: "발송예정",
        sendComplete: "발송완료",
        cancel: "발송취소",
        export: "URL추출"
    },
    genderStates: {
        m: '남자',
        f: '여자',
        noGender: '알 수 없음'
    },
    possibleAdministrationRoles: {
        ADMIN: true,
        MANAGER: true,
        USER: false
    },
    checkItemStates: {
        sendAnalysis: {
            size: 50,
            key: 'sendAnalysisCheckItem'
        },
        senderAnalysis: {
            size: 10,
            key: 'senderAnalysisCheckItem',
            objKey: 'senderHistoryItems'
        }
    },
    enumRolesByRole: {
        ADMIN: ["ADMIN", "MANAGER", "USER", "API"],
        MANAGER: ["MANAGER", "USER", "API"],
    }
};

export default constant;