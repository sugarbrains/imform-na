const dataValue = {
    defaultDialogSubmitText: '확인',
    STATES: {
        500: 'SERVER ERROR',
        400: 'UNEXPECTED ERROR',
        401: 'SESSION ERROR',
        403: 'AUTHORIZATION ERROR',
        404: 'EMPTY DATA'
    },
    ENUM_SIZES: [{
        value: '25',
        text: '25개씩'
    }, {
        value: '50',
        text: '50개씩'
    }],
    SENDER_ANALYSIS_ENUM_SIZES: [{
        value: '10',
        text: '10개씩'
    }, {
        value: '25',
        text: '25개씩'
    }, {
        value: '50',
        text: '50개씩'
    }],
    DEFAULT_SIZE: '25',
    SENDER_ANALYSIS_DEFAULT_SIZE: '10',

    ENUM_SELECT_GENDERS: [{
        value: 'genderMale',
        text: '남자'
    }, {
        value: 'genderFemale',
        text: '여자'
    }, {
        value: 'genderNone',
        text: '없음'
    }],
    ENUM_GENDERS: [
        'genderTotal',
        'genderMale',
        'genderFemale',
        'genderNone'
    ],
    GENDER_TOTAL: 'genderTotal',
    GENDER_MALE: 'genderMale',
    GENDER_FEMALE: 'genderFemale',
    GENDER_NONE: 'genderNone',

    ENUM_AGES: [
        'ageTotal',
        'age20',
        'age30',
        'age40',
        'age50',
        'ageNone'
    ],
    AGE_TOTAL: 'ageTotal',
    AGE_20: 'age20',
    AGE_30: 'age30',
    AGE_40: 'age40',
    AGE_50: 'age50',
    AGE_NONE: 'ageNone',

    PERIOD_MINUTE: 5,

    MAGIC_URL: '[:URL:]',
    SIZE_URL: 20
};

export default dataValue;