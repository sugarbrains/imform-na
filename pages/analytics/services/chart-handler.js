import Chart from 'chart.js';

export default function chartHandler(constant) {
    'ngInject';

    const DOUGHNUT = 'doughnut';
    const DOUGHNUT_NO_TOOLTIP = 'doughnutNoToolTip';
    const LINE = 'line';
    const BAR = 'bar';
    const HORIZONTAL_BAL = 'horizontalBar';

    const CHART = {
        doughnut: 'doughnut',
        doughnutNoToolTip: 'doughnut',
        line: 'line',
        bar: 'bar',
        horizontalBar: 'horizontalBar'
    };
    const DATA_OPTION = {
        line: {
            borderWidth: 2,
            borderColor: '#EF4C20',
            hoverBorderColor: '#EF4C20',
            pointBackgroundColor: '#EF4C20',
            pointHoverBackgroundColor: '#EF4C20',
            pointBorderColor: '#EF4C20',
            pointHoverBorderColor: '#EF4C20',
            backgroundColor: 'rgba(239, 76, 32, 0.16)',
            hoverBackgroundColor: 'rgba(239, 76, 32, 0.16)'
        },
        bar: {
            backgroundColor: '#FF952C',
            hoverBackgroundColor: '#EF4C20'
        },
        horizontalBar: {
            backgroundColor: '#FF952C',
            hoverBackgroundColor: '#EF4C20'
        }
    };
    const OPTION = {
        doughnut: {
            legend: {
                display: false
            },
            cutoutPercentage: 55
        },
        doughnutNoToolTip: {
            tooltips: {
                enabled: false
            },
            legend: {
                display: false
            },
            cutoutPercentage: 55
        },
        line: {
            legend: {
                display: false
            },
            elements: {
                line: {
                    tension: 0
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        },
        bar: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                xAxes: [{
                    barPercentage: 0.7
                }]
            }
        },
        horizontalBar: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }],
                yAxes: [{
                    barThickness: 18
                }]
            }
        }
    };

    const chartKey = constant.chartKey;
    const chartLabels = constant.chartLabels;

    let ready = {};

    this.init = init;
    this.setReady = setReady;
    this.getReady = getReady;
    this.drawChart = drawChart;
    this.drawOpenCount = drawOpenCount;
    this.drawReopenCount = drawReopenCount;
    this.drawHourDiagram = drawHourDiagram;
    this.drawDayDiagram = drawDayDiagram;
    this.drawPerHourDiagram = drawPerHourDiagram;
    this.drawPerDayDiagram = drawPerDayDiagram;
    this.drawGenderDiagram = drawGenderDiagram;
    this.drawAgeDiagram = drawAgeDiagram;
    this.drawAddressDiagram = drawAddressDiagram;
    this.drawPageCountDiagram = drawPageCountDiagram;
    this.drawPageTimeDiagram = drawPageTimeDiagram;

    function init(scope, ids) {
        ready = {};
        ids.forEach((id) => {
            ready[id] = false;
            scope.$on(id, () => {
                setReady(id);
            });
        });
    }

    function setReady(id, callback) {
        if (callback) {
            ready[id] = callback;
        } else if (ready[id]) {
            ready[id]();
        } else {
            ready[id] = true;
        }
    }

    function getReady(id) {
        return ready[id];
    }

    function drawChart(id, option) {
        let element = document.getElementById(id);
        return new Chart(element, option);
    }

    function returnValue(data) {
        if (data instanceof Array) {
            if (data[0] instanceof Object) {
                let values = [];
                data.forEach((item) => {
                    Object.keys(item).forEach((key) => {
                        values.push(item[key]);
                    });
                });
                return values;
            } else {
                return data;
            }
        } else {
            let values = [];
            Object.keys(data).forEach((key) => {
                values.push(data[key]);
            });
            return values;
        }
    }

    function draw(id, type, data, labels, dataOption, options) {
        if (getReady(id)) {
            drawChart(id, {
                type: type,
                data: {
                    datasets: [Object.assign({
                        data: data
                    }, dataOption ? dataOption() : {})],
                    labels: labels ? labels : undefined
                },
                options: options ? options : undefined
            });
        } else {
            setReady(id, () => {
                drawChart(id, {
                    type: type,
                    data: {
                        datasets: [Object.assign({
                            data: data
                        }, dataOption ? dataOption() : {})],
                        labels: labels ? labels : undefined
                    },
                    options: options ? options : undefined
                });
            });
        }
    }

    function drawOpenCount(data) {
        let id = chartKey.openCount;
        let labels = chartLabels.openCount;
        draw(id, CHART[DOUGHNUT], returnValue(data), labels, () => {
            return {
                backgroundColor: [gradient(id, '#FF952C', '#EF4C20'), 'rgba(239, 76, 32, 0.16)'],
                hoverBackgroundColor: [gradient(id, '#FF952C', '#EF4C20'), 'rgba(239, 76, 32, 0.16)'],
                borderWidth: 0,
                hoverBorderWidth: 0
            };
        }, {
            tooltips: {
                callbacks: {
                    labelColor: function(tooltipItem, chart) {
                        if(tooltipItem.index === 0) {
                            return {
                                borderColor: '#f15723',
                                backgroundColor: '#f15723'
                            }
                        } else if(tooltipItem.index === 1) {
                            return {
                                borderColor: '#fce6e0',
                                backgroundColor: '#fce6e0'
                            }
                        }
                    }
                }
            },
            legend: {
                display: false
            },
            cutoutPercentage: 55
        });
    }

    function drawReopenCount(data) {
        let id = chartKey.reopenCount;
        let labels = chartLabels.reopenCount;
        draw(id, CHART[DOUGHNUT], returnValue(data), labels, () => {
            return {
                backgroundColor: [gradient(id, '#FF952C', '#EF4C20'), 'rgba(239, 76, 32, 0.16)'],
                hoverBackgroundColor: [gradient(id, '#FF952C', '#EF4C20'), 'rgba(239, 76, 32, 0.16)'],
                borderWidth: 0,
                hoverBorderWidth: 0
            };
        }, {
            tooltips: {
                callbacks: {
                    labelColor: function(tooltipItem, chart) {
                        if(tooltipItem.index === 0) {
                            return {
                                borderColor: '#f15723',
                                backgroundColor: '#f15723'
                            }
                        } else if(tooltipItem.index === 1) {
                            return {
                                borderColor: '#fce6e0',
                                backgroundColor: '#fce6e0'
                            }
                        }
                    }
                }
            },
            legend: {
                display: false
            },
            cutoutPercentage: 55
        });
    }

    function drawHourDiagram(data) {
        let id = chartKey.hourDiagram;
        let labels = [];
        for (let i=0; i<24; i++) {
            labels.push(((i+1) * 3).toString());
        }
        draw(id, CHART[LINE], returnValue(data), labels, () => {
            return DATA_OPTION[LINE];
        }, OPTION[LINE]);
    }

    function drawDayDiagram(data) {
        let id = chartKey.dayDiagram;
        let labels = [];
        for (let i=0; i<7; i++) {
            labels.push(i.toString());
        }
        labels[0] = '발송일';
        draw(id, CHART[LINE], returnValue(data), labels, () => {
            return DATA_OPTION[LINE];
        }, OPTION[LINE]);
    }

    function drawPerHourDiagram(data) {
        let id = chartKey.perHourDiagram;
        let labels = [];
        for (let i=0; i<24; i++) {
            labels.push((i + 1).toString());
        }
        draw(id, CHART[BAR], returnValue(data), labels, () => {
            return DATA_OPTION[BAR];
        }, OPTION[BAR]);
    }

    function drawPerDayDiagram(data) {
        let id = chartKey.perDayDiagram;
        let labels = chartLabels.perDayDiagram;
        draw(id, CHART[BAR], returnValue(data), labels, () => {
            return DATA_OPTION[BAR];
        }, OPTION[BAR]);
    }

    function drawGenderDiagram(data) {
        let id = chartKey.genderDiagram;
        let labels = chartLabels.genderDiagram;
        draw(id, CHART[DOUGHNUT], returnValue(data), labels, () => {
            return {
                backgroundColor: ['#BFEEF1', '#EF4C20', '#FF952C'],
                hoverBackgroundColor: ['#BFEEF1', '#EF4C20', '#FF952C'],
                borderWidth: 0,
                hoverBorderWidth: 0
            };
        }, OPTION[DOUGHNUT]);
    }

    function drawAgeDiagram(data) {
        let id = chartKey.ageDiagram;
        let labels = chartLabels.ageDiagram;
        draw(id, CHART[HORIZONTAL_BAL], returnValue(data), labels, () => {
            return DATA_OPTION[HORIZONTAL_BAL];
        }, OPTION[HORIZONTAL_BAL]);
    }

    function drawAddressDiagram(data, labels) {
        let id = chartKey.addressDiagram;
        draw(id, CHART[HORIZONTAL_BAL], returnValue(data), labels, () => {
            return DATA_OPTION[HORIZONTAL_BAL];
        }, OPTION[HORIZONTAL_BAL]);
    }

    function drawPageCountDiagram(data, labels) {
        let id = chartKey.pageCountDiagram;
        draw(id, CHART[HORIZONTAL_BAL], returnValue(data), labels, () => {
            return DATA_OPTION[HORIZONTAL_BAL];
        }, OPTION[HORIZONTAL_BAL]);
    }

    function drawPageTimeDiagram(data, labels) {
        let id = chartKey.pageTimeDiagram;
        draw(id, CHART[HORIZONTAL_BAL], returnValue(data), labels, () => {
            return DATA_OPTION[HORIZONTAL_BAL];
        }, OPTION[HORIZONTAL_BAL]);
    }
    
    function gradient(id, start, end, isLine) {
        let ctx = document.getElementById(id).getContext('2d');
        if (isLine) {
            let gradient = ctx.createLinearGradient(0, 0, 200, 0);
            gradient.addColorStop(0, start);
            gradient.addColorStop(1, end);
            return gradient;
        } else {
            let gradient = ctx.createLinearGradient(0, 0, 200, 200);
            gradient.addColorStop(0, start);
            gradient.addColorStop(1, end);
            return gradient;
        }
    }
}