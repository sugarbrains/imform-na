export default function addressHandler() {
    'ngInject';

    this.generateAddresses = generateAddresses;

    function generateAddresses(data) {
        let addresses = {};
        data.forEach((item) => {
            if (!addresses[item.address1]) {
                addresses[item.address1] = {};
            }
            if (!addresses[item.address1][item.address2]) {
                addresses[item.address1][item.address2] = {};
            }
            if (!addresses[item.address1][item.address2][item.address3]) {
                addresses[item.address1][item.address2][item.address3] = true;
            }
        });
        return addresses;
    }
}