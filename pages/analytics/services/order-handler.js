export default function orderHandler () {
    'ngInject';

    const FORM = 'form';
    const DESC = 'DESC';
    const ASC = 'ASC';

    this.init = init;
    this.setOrderBy = setOrderBy;
    this.setSort = setSort;

    function init (scope, events, scopeKey) {
        if (scopeKey) {
            scope[scopeKey].events = events;
        } else {
            scope.events = events;
        }
        let orderBys = [];
        for (let i=0; i<events.length; i++) {
            orderBys.push('');
        }
        if (scopeKey) {
            scope[scopeKey].orderBys = orderBys;
        } else {
            scope.orderBys = orderBys;
        }
    }

    function setOrderBy (scope, keys, callback, scopeKey, formKey) {
        if (scopeKey) {
            scope[scopeKey].events.forEach((key, index) => {
                scope.$on(key, () => {
                    scope[formKey ? formKey : FORM].sort = scope[scopeKey].orderBys[index] == DESC ? ASC : DESC;
                    scope[formKey ? formKey : FORM].orderBy = keys[index];
                    if (callback) callback();
                });
            });
        } else {
            scope.events.forEach((key, index) => {
                scope.$on(key, () => {
                    scope[formKey ? formKey : FORM].sort = scope.orderBys[index] == DESC ? ASC : DESC;
                    scope[formKey ? formKey : FORM].orderBy = keys[index];
                    if (callback) callback();
                });
            });
        }

    }

    function setSort (scope, keys, scopeKey, formKey) {
        for (let i=0; i<keys.length; i++) {
            if (scopeKey) {
                if (keys[i] == scope[formKey ? formKey : FORM].orderBy) {
                    scope[scopeKey].orderBys[i] = scope[formKey ? formKey : FORM].sort;
                } else {
                    scope[scopeKey].orderBys[i] = '';
                }
            } else {
                if (keys[i] == scope[formKey ? formKey : FORM].orderBy) {
                    scope.orderBys[i] = scope[formKey ? formKey : FORM].sort;
                } else {
                    scope.orderBys[i] = '';
                }
            }
        }
    }
}