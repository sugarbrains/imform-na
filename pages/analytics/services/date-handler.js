export default function dateHandler () {
    'ngInject';

    this.toISO = toISO;

    function toISO(data) {
        return new Date(data.replace(' ', 'T') + '.000Z');
    }
}