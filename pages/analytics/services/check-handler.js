export default function checkHandler (dataValue) {
    'ngInject';

    this.check = check;

    function check (scope, objectKey, key) {
        return (data) => {
            if (dataValue.ENUM_GENDERS.indexOf(key) != -1) {
                totalHandle(scope, objectKey, key, data, dataValue.ENUM_GENDERS, dataValue.GENDER_TOTAL);
            } else if (dataValue.ENUM_AGES.indexOf(key) != -1) {
                totalHandle(scope, objectKey, key, data, dataValue.ENUM_AGES, dataValue.AGE_TOTAL);
            }
        };
    }

    function totalHandle(scope, objectKey, key, data, enumKeys, totalKey) {
        if (key == totalKey) {
            for (let i=1; i<enumKeys.length; i++) {
                scope[objectKey][enumKeys[i]] = data;
            }
        } else {
            scope[objectKey][totalKey] = totalCheck(scope, objectKey, key, data, enumKeys);
        }
    }

    function totalCheck(scope, objectKey, key, data, enumKeys) {
        if (data) {
            for (let i=1; i<enumKeys.length; i++) {
                if (enumKeys[i] != key && !scope[objectKey][enumKeys[i]]) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
}