export default function loadingHandler(constant) {
    'ngInject';

    const loadingKey = constant.loadingKey;
    let rootScope;

    this.init = init;
    this.startLoading = startLoading;
    this.endLoading = endLoading;
    this.loadingText = loadingText;

    function init($rootScope) {
        rootScope = $rootScope;
    }

    function startLoading(key) {
        rootScope.$broadcast(loadingKey.startLoading, {
            key: key
        });
    }

    function endLoading(key) {
        rootScope.$broadcast(loadingKey.endLoading, {
            key: key
        })
    }

    function loadingText(key, loadingText) {
        rootScope.$broadcast(loadingKey.loadingText, {
            key: key,
            loadingText: loadingText
        });
    }
}