export default function messageHandler(dataValue) {
    'ngInject';

    const MESSAGE_ID = 'message';
    const TAG_URL = '<span>URL</span>';
    const urlExp = /^(((http(s?))\:\/\/)?)([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}(\:[0-9]+)?(\/\S*)?$/;
    let currentPosition = null;

    this.getPosition = getPosition;
    this.setPosition = setPosition;
    this.getUrlMessage = getUrlMessage;
    this.checkHasUrl = checkHasUrl;
    this.generatePreview = generatePreview;
    this.getBytes = getBytes;
    this.checkUrl = checkUrl;

    function setPosition() {
        currentPosition = $('#' + MESSAGE_ID).prop("selectionStart") || 0;
    }

    function getPosition() {
        return currentPosition;
    }

    function getUrlMessage(message) {
        if (currentPosition !== null) {
            return [
                message.substr(0, currentPosition),
                dataValue.MAGIC_URL,
                message.substr(currentPosition)
            ].join('');
        } else {
            return false;
        }
    }

    function checkUrl(url) {
        return urlExp.test(url);
    }

    function checkHasUrl(message) {
        return message.indexOf(dataValue.MAGIC_URL) != -1;
    }

    function generatePreview(message, replaceText = TAG_URL, attachMessage = '', prevMessage = '') {
        return (prevMessage ? (prevMessage + '\n') : '') + message.replace(dataValue.MAGIC_URL, replaceText) + (attachMessage ? (message ? '\n' : '') + attachMessage : '');
    }

    function getBytes(message, attachMessage = '', prevMessage = '') {
        const bytes = (message.split(dataValue.MAGIC_URL).length - 1) * dataValue.SIZE_URL;
        return bytes + getTextLength(prevMessage) + (prevMessage ? 1 : 0) + getTextLength(generatePreview(message, '')) + getTextLength(attachMessage) + (attachMessage ? 1 : 0);
    }

    function getTextLength(str) {
        var len = 0;
        for (var i = 0; i < str.length; i++) {
            if (escape(str.charAt(i)).length == 6) {
                len++;
            }
            len++;
        }
        return len;
    }
}