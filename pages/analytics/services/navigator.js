export default function navigator ($state, $document, $cookies, constant) {
    'ngInejct';

    let defaultQuery = constant.defaultQuery;
    const SEND_ANALYSIS_KEY = constant.checkItemStates.sendAnalysis.key;
    const SENDER_ANALYSIS_KEY = constant.checkItemStates.senderAnalysis.key;
    const SENDER_ANALYSIS_OBJ_KEY = constant.checkItemStates.senderAnalysis.objKey;

    this.goToIndex = goToIndex;
    this.goToReceiverGroupManage = goToReceiverGroupManage;
    this.goToReceiverManage = goToReceiverManage;
    this.goToUserManage = goToUserManage;
    this.goToSendManage = goToSendManage;
    this.goToMessageManage = goToMessageManage;
    this.goToSendAnalysis = goToSendAnalysis;
    this.goToSenderAnalysis = goToSenderAnalysis;
    this.goToChart = goToChart;
    this.goToCalculationManage = goToCalculationManage;

    function go(name, param, reload, callback) {
        $state.go(name, param, {
            reload: reload
        }).then(() => {
            goToTop();
            if (callback) {
                callback();
            }
        });
    }

    function goToTop() {
        $document.scrollTop(0, 500);
    }
    
    function goToIndex() {
        go('index');
    }

    function goToReceiverGroupManage(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.receiverGroupManage);
        }
        removeCookieData();
        go('receiverGroupManage', param, reload);
    }

    function goToReceiverManage(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.receiverManage);
        }
        removeCookieData();
        go('receiverManage', param, reload);
    }

    function goToUserManage(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.userManage);
        }
        removeCookieData();
        go('userManage', param, reload);
    }

    function goToSendManage(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.sendManage);
        }
        go('sendManage', param, reload);
    }

    function goToMessageManage(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.messageManage);
        }
        removeCookieData();
        go('messageManage', param, reload);
    }

    function goToSendAnalysis(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.sendAnalysis);
        }
        removeCookieData();
        go('sendAnalysis', param, reload);
    }

    function goToSenderAnalysis(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.senderAnalysis);
        }
        removeCookieData();
        go('senderAnalysis', param, reload);
    }

    function goToChart(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.chart);
        } else {
            let defaultParam = angular.copy(defaultQuery.chart);
            Object.assign(defaultParam, param);
            param = defaultParam;
        }
        go('chart', param, reload);
    }

    function goToCalculationManage(param, reload) {
        if (!param) {
            param = angular.copy(defaultQuery.calculationManage)
        }
        go('calculationManage', param, reload);
    }

    function removeCookieData() {
        $cookies.remove(SEND_ANALYSIS_KEY);
        $cookies.remove(SENDER_ANALYSIS_KEY);
        $cookies.remove(SENDER_ANALYSIS_OBJ_KEY);
    }
}