export default function metaManager() {
    'ngInject';

    const meta = window.meta;

    return {
        $get: () => {
            return meta;
        }
    }
}