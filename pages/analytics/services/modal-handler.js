export default function modalHandler(constant) {
    'ngInject';

    const modalKey = constant.modalKey;
    const eventKey = 'keydown';
    let rootScope;

    this.init = init;
    this.focus = focus;
    this.eventBind = eventBind;
    this.openSubmitReceiver = openSubmitReceiver;
    this.openUpdateReceiver = openUpdateReceiver;
    this.openReceiverGroupManage = openReceiverGroupManage;
    this.openSubmitReceiverGroup = openSubmitReceiverGroup;
    this.openSubmitReceiverGroupWithFile = openSubmitReceiverGroupWithFile;
    this.openUpdateReceiverGroup = openUpdateReceiverGroup;
    this.openReceiverGroup = openReceiverGroup;
    this.openRegisterReceiverGroup = openRegisterReceiverGroup;
    this.openSentCount = openSentCount;
    this.openInsertPhoneNum = openInsertPhoneNum;
    this.openSelectSendType = openSelectSendType;
    this.openSelectFunction = openSelectFunction;
    this.openDetailOpenerCount = openDetailOpenerCount;
    this.openDetailOpenerCountDiagram = openDetailOpenerCountDiagram;
    this.openDetailHourDiagram = openDetailHourDiagram;
    this.openDetailDayDiagram = openDetailDayDiagram;
    this.openDetailPerHourDiagram = openDetailPerHourDiagram;
    this.openDetailPerDayDiagram = openDetailPerDayDiagram;
    this.openDetailGenderDiagram = openDetailGenderDiagram;
    this.openDetailAgeDiagram = openDetailAgeDiagram;
    this.openDetailAddressDiagram = openDetailAddressDiagram;
    this.openDetailPageCountDiagram = openDetailPageCountDiagram;
    this.openDetailPageTimeDiagram = openDetailPageTimeDiagram;
    this.openDetailPerHourCouponDiagram = openDetailPerHourCouponDiagram;
    this.openDetailPerDayCouponDiagram = openDetailPerDayCouponDiagram;
    this.openDetailCouponCountDiagram = openDetailCouponCountDiagram;
    this.openDetailCouponTypeCountDiagram = openDetailCouponTypeCountDiagram;
    this.openSentMessageListModal = openSentMessageListModal;
    this.openReceiverListModal = openReceiverListModal;
    this.openSentManage = openSentManage;
    this.openUpdateUser = openUpdateUser;
    this.openMyProfile = openMyProfile;

    function init($rootScope) {
        rootScope = $rootScope;
    }

    function focus($target) {
        let y = window.pageYOffset;
        $target.focus();
        $target.focusin();
        window.scrollTo(0, y);
    }

    function eventBind($target, close) {
        $target.unbind(eventKey);
        $target.bind(eventKey, function (e) {
            let keyCode = (e.keyCode ? e.keyCode : e.which);
            if (keyCode == 27) close();
        });
    }

    function openSubmitReceiver() {
        if (rootScope) {
            rootScope.$broadcast(modalKey.submitReceiver);
        }
    }

    function openUpdateUser(userId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.updateUser, {
                userId: userId
            });
        }
    }

    function openMyProfile(userId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.myProfile, {
                userId: userId
            });
        }
    }

    function openUpdateReceiver(receiver) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.updateReceiver, {
                receiver: receiver
            });
        }
    }

    function openReceiverGroupManage(receiverId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.receiverGroupManage, {
                receiverId: receiverId
            });
        }
    }

    function openSubmitReceiverGroup(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.submitReceiverGroup, {
                query: query
            });
        }
    }

    function openSubmitReceiverGroupWithFile() {
        if (rootScope) {
            rootScope.$broadcast(modalKey.submitReceiverGroupWithFile);
        }
    }

    function openUpdateReceiverGroup(receiverGroupId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.updateReceiverGroup, {
                receiverGroupId: receiverGroupId
            });
        }
    }

    function openReceiverGroup(receiverId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.receiverGroup, {
                receiverId: receiverId
            });
        }
    }

    function openRegisterReceiverGroup(receiverId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.registerReceiverGroup, {
                receiverId: receiverId
            });
        }
    }

    function openSentCount(receiverId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.sentCount, {
                receiverId: receiverId
            });
        }
    }

    function openInsertPhoneNum() {
        if (rootScope) {
            rootScope.$broadcast(modalKey.insertPhoneNum);
        }
    }

    function openSelectSendType(sendHistoryId) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.selectSendType, {
                sendHistoryId: sendHistoryId
            });
        }
    }

    function openSelectFunction() {
        if (rootScope) {
            rootScope.$broadcast(modalKey.selectFunction);
        }
    }

    function openDetailOpenerCount(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailOpenerCount, {
                query: query
            });
        }
    }

    function openDetailOpenerCountDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailOpenerCountDiagram, {
                query: query
            });
        }
    }

    function openDetailHourDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailHourDiagram, {
                query: query
            });
        }
    }

    function openDetailDayDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailDayDiagram, {
                query: query
            });
        }
    }

    function openDetailPerHourDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailPerHourDiagram, {
                query: query
            });
        }
    }

    function openDetailPerDayDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailPerDayDiagram, {
                query: query
            });
        }
    }

    function openDetailGenderDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailGenderDiagram, {
                query: query
            });
        }
    }

    function openDetailAgeDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailAgeDiagram, {
                query: query
            });
        }
    }

    function openDetailAddressDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailAddressDiagram, {
                query: query
            });
        }
    }

    function openDetailPageCountDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailPageCountDiagram, {
                query: query
            });
        }
    }

    function openDetailPageTimeDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailPageTimeDiagram, {
                query: query
            });
        }
    }

    function openDetailPerHourCouponDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailPerHourCouponDiagram, {
                query: query
            });
        }
    }

    function openDetailPerDayCouponDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailPerDayCouponDiagram, {
                query: query
            });
        }
    }

    function openDetailCouponCountDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailCouponCountDiagram, {
                query: query
            });
        }
    }

    function openDetailCouponTypeCountDiagram(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.detailCouponTypeCountDiagram, {
                query: query
            });
        }
    }

    function openSentMessageListModal(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.sentMessageList, {
                query: query
            });
        }
    }

    function openReceiverListModal(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.receiverList, {
                query: query
            });
        }
    }

    function openSentManage(query) {
        if (rootScope) {
            rootScope.$broadcast(modalKey.sentManage, {
                query: query
            });
        }
    }
}