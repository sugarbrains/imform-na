export default function domainManager(Domain) {
    "ngInject";

    this.findAll = findAll;

    function findAll(callback) {
        Domain.query((data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}