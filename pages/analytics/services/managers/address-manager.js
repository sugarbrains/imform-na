export default function addressManager(Address) {
    'ngInject';

    this.getAddresses = getAddresses;

    function getAddresses(query, callback) {
        Address.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}