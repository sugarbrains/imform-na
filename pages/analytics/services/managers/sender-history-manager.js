export default function senderHistoryManager (SenderHistory) {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;

    function findAll(query, callback) {
        SenderHistory.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findById(id, callback) {
        SenderHistory.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}