export default function receiverGroupImportHistoryManger(ReceiverGroupImportHistory) {
    "ngInject";

    this.findById = findById;

    function findById(id, callback) {
        ReceiverGroupImportHistory.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}