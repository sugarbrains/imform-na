export default function usersManager (User) {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;
    this.create = create;
    this.updateById = updateById;
    this.remove = remove;

    function findAll(query, callback) {
        User.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findById(id, callback) {
        User.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function create(data, callback) {
        let user = new User(data);
        user.$save((data) => {
            callback(201, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function updateById(data, callback) {
        let where = {id: data.id};
        let body = {};

        if(data.domainId !== undefined) body.domainId = data.domainId;
        if(data.uid !== undefined) body.uid = data.uid;
        if(data.role !== undefined) body.role = data.role;
        if(data.name !== undefined) body.name = data.name;
        if(data.phoneNum !== undefined) body.phoneNum = data.phoneNum;
        if(data.url !== undefined) body.url = data.url;
        if(data.refuseMessage !== undefined) body.refuseMessage = data.refuseMessage;
        if(data.password !== undefined) body.password = data.password;
        if(data.sms !== undefined) body.sms = data.sms;
        if(data.lms !== undefined) body.lms = data.lms;

        User.update(where, body, (data) => {
            callback(204, data);
        }, data => {
            callback(data.status, data.data);
        })
    }

    function remove(data, callback) {
        let user = new User(data);
        user.$remove(() => {
            callback(204);
        }, data => {
            callback(data.status, data.data);
        })
    }
}