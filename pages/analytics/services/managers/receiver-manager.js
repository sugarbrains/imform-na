export default function receiversManager (Receiver) {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;
    this.create = create;
    this.updateById = updateById;
    this.deleteById = deleteById;

    function findAll(query, callback) {
        Receiver.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findById(id, callback) {
        Receiver.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function create(data, callback) {
        let receiver = new Receiver(data);
        receiver.$save((data) => {
            callback(201, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function updateById(data, callback) {
        let where = {id: data.id};
        let body = {};
        Receiver.update(where, body, () => {
            callback(204);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function deleteById(data, callback) {
        let receiver = new Receiver(data);
        receiver.$remove(() => {
            callback(204);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}