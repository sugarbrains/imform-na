export default function sessionManager (Session, resource) {
    'ngInject';

    this.getSessionData = getSessionData;
    this.getSession = getSession;
    this.signOut = signOut;

    let session = window.session || null;

    function getSessionData() {
        return session;
    }

    function getSession(callback) {
        Session.get({}, (data) => {
            session = data;
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function signOut() {
        location.href = resource.SIGN_OUT;
    }
}