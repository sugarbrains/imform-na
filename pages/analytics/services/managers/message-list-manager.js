export default function messageListManager(MessageList) {
    "ngInject";

    this.findByIds = findByIds;

    function findByIds(ids, callback) {
        MessageList.query({
            ids: ids
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}