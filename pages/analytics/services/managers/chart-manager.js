export default function chartManager(ChartOpenerCount, ChartGender, ChartAge, ChartAddress, ChartPageCount, ChartPageTime, ChartDay, ChartHour) {
    'ngInject';

    this.getChartOpenerCount = getChartOpenerCount;
    this.getChartGender = getChartGender;
    this.getChartAge = getChartAge;
    this.getChartAddress = getChartAddress;
    this.getChartPageCount = getChartPageCount;
    this.getChartPageTime = getChartPageTime;
    this.getChartDay = getChartDay;
    this.getChartHour = getChartHour;

    function getChartOpenerCount(query, callback) {
        ChartOpenerCount.get(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getChartGender(query, callback) {
        ChartGender.get(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getChartAge(query, callback) {
        ChartAge.get(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getChartAddress(query, callback) {
        ChartAddress.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getChartPageCount(query, callback) {
        ChartPageCount.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getChartPageTime(query, callback) {
        ChartPageTime.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getChartDay(query, callback) {
        ChartDay.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getChartHour(query, callback) {
        ChartHour.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}