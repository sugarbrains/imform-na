export default function calculationManager(ChargeCount, SendCompleteHistory, Payment) {
    "ngInject";

    this.findAllChargeCount = findAllChargeCount;
    this.findAllSentMessages = findAllSentMessages;
    this.findAllPayments = findAllPayments;

    function findAllChargeCount(query, callback) {
        ChargeCount.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findAllSentMessages(query, callback) {
        SendCompleteHistory.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findAllPayments(query, callback) {
        Payment.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}