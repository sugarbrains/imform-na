export default function receiversManager () {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;
    this.create = create;
    this.updateById = updateById;
    this.deleteById = deleteById;

    function findAll(query, callback) {

    }

    function findById(id, callback) {

    }

    function create(data, callback) {
        let body = {};
    }

    function updateById(data, callback) {
        let where = {id: data.id};
        let body = {};
    }

    function deleteById(data, callback) {

    }
}