export default function sendHistoryManager (SendHistory, SendHistoryReceiverGroup) {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;
    this.sendReceiverGroup = sendReceiverGroup;

    function findAll(query, callback) {
        SendHistory.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findById(id, callback) {
        SendHistory.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
    
    function sendReceiverGroup(body, callback) {
        let historyReceiverGroup = new SendHistoryReceiverGroup(body);
        historyReceiverGroup.$save((data) => {
            callback(201, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}