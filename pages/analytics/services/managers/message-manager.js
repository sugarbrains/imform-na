export default function messageManager(Message, MessageSend, MessageExport, ExportHistory, MessageCancel) {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;
    this.send = send;
    this.extract = extract;
    this.cancel = cancel;
    this.remove = remove;
    this.getExtractHistory = getExtractHistory;
    this.create = create;
    this.update = update;

    function findAll(query, callback) {
        Message.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findById(id, callback) {
        Message.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function create(body, callback) {
        let message = new Message(body);
        message.$save((data) => {
            callback(201, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function update(id, item, callback) {
        let where = {
            id: id
        };

        let body = {};
        if (item.title !== undefined) body.title = item.title;
        if (item.title2 !== undefined) body.title2 = item.title2;
        if (item.message !== undefined) body.message = item.message;
        if (item.refuseMessage !== undefined) body.refuseMessage = item.refuseMessage;
        if (item.redirectUrl !== undefined) body.redirectUrl = item.redirectUrl;
        if (item.receiverGroupIds !== undefined) body.receiverGroupIds = item.receiverGroupIds;
        if (item.buttons !== undefined) body.buttons = item.buttons;

        Message.update(where, body, () => {
            callback(204);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function send(data, callback) {
        let body = {
            id: data.id
        };

        if (data.activeDate !== undefined) body.activeDate = data.activeDate;

        MessageSend.update(body, (data) => {
            callback(204, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function extract(data, callback) {
        let body = {
            id: data.id
        };

        MessageExport.update(body, (data) => {
            callback(204, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function getExtractHistory(id, callback) {
        ExportHistory.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function cancel(data, callback) {
        let body = {
            id: data.id
        };

        MessageCancel.update(body, (data) => {
            callback(204, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function remove(data, callback) {
        let message = new Message(data);
        message.$remove(() => {
            callback(204);
        }, (data) => {
            callback(data.status, data.data);
        });
    }


}