export default function receiverGroupManager (SendHistoryReceiverGroup, ReceiverGroup, metaManager) {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;
    this.create = create;
    this.createHistoryReceiverGroup = createHistoryReceiverGroup;
    this.updateById = updateById;
    this.remove = remove;

    function findAll(query, callback) {
        ReceiverGroup.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findById(id, callback) {
        ReceiverGroup.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function create(body, callback) {
        // let receiverGroup = new ReceiverGroup(body);
        ReceiverGroup.create(body, (data) => {
            callback(201, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function createHistoryReceiverGroup(body, callback) {
        let receiverGroup = new SendHistoryReceiverGroup(body);
        receiverGroup.$save((data) => {
            callback(201, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function updateById(data, callback) {
        let where = {id: data.id};
        let body = {
            name: data.name
        };
        ReceiverGroup.update(where, body, () => {
            callback(204);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function remove(data, callback) {
        let receiverGroup = new ReceiverGroup(data);
        receiverGroup.$remove(() => {
            callback(204);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}