export default function messageReceiverGroupManager (MessageReceiverGroup) {
    'ngInject';

    this.findAll = findAll;
    this.findById = findById;

    function findAll(query, callback) {
        MessageReceiverGroup.query(query, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }

    function findById(id, callback) {
        MessageReceiverGroup.get({
            id: id
        }, (data) => {
            callback(200, data);
        }, (data) => {
            callback(data.status, data.data);
        });
    }
}