Message.$inject = ['$resource', 'resource'];

export default function Message($resource, resource) {
    "ngInject";

    return $resource(resource.MESSAGE + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}