ReceiverGroupImportHistory.$inject = ['$resource', 'resource'];

export default function ReceiverGroupImportHistory($resource, resource) {
    "ngInject";

    return $resource(resource.RECEIVER_GROUP_IMPORT_HISTORY + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}