SendHistory.$inject = ['$resource', 'resource'];

export default function SendHistory($resource, resource) {
    "ngInject";

    return $resource(resource.SEND_HISTORY + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}