ChartOpenerCount.$inject = ['$resource', 'resource'];

export default function ChartOpenerCount($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_OPENER_COUNT, {}, {})
}