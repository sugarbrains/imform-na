ChartAddress.$inject = ['$resource', 'resource'];

export default function ChartAddress($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_ADDRESS, {}, {
        query: {
            isArray: true
        }
    })
}