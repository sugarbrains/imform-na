ReceiverGroup.$inject = ['$resource', 'resource'];

export default function ReceiverGroup($resource, resource) {
    "ngInject";

    return $resource(resource.RECEIVER_GROUP + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        create: {
            method: 'POST',
            headers: { 'Content-Type': undefined, 'enctype': 'multipart/form-data' }
        },
        query: {
            isArray: false
        }
    })
}