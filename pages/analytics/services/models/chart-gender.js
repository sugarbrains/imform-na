ChartGender.$inject = ['$resource', 'resource'];

export default function ChartGender($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_GENDER, {}, {})
}