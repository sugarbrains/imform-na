ChartPageCount.$inject = ['$resource', 'resource'];

export default function ChartPageCount($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_PAGE_COUNT, {}, {
        query: {
            isArray: true
        }
    })
}