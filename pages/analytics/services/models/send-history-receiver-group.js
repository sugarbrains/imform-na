SendHistoryReceiverGroup.$inject = ['$resource', 'resource'];

export default function SendHistoryReceiverGroup($resource, resource) {
    "ngInject";

    return $resource(resource.SEND_HISTORY_RECEIVER_GROUP + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}