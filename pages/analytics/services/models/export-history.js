ExportHistory.$inject = ['$resource', 'resource'];

export default function ExportHistory($resource, resource) {
    "ngInject";

    return $resource(resource.EXPORT_HISTORY + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}