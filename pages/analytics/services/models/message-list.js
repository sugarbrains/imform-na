MessageList.$inject = ['$resource', 'resource'];

export default function MessageList($resource, resource) {
    "ngInject";

    return $resource(resource.MESSAGE_LIST, {}, {})
}