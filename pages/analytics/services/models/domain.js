Domain.$inject = ['$resource', 'resource'];

export default function Domain($resource, resource) {
    "ngInject";

    return $resource(resource.DOMAIN + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}