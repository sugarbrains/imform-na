Session.$inject = ['$resource', 'resource'];

export default function Session($resource, resource) {
    "ngInject";

    return $resource(resource.SESSION, {}, {});
}