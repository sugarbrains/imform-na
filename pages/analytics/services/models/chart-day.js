ChartDay.$inject = ['$resource', 'resource'];

export default function ChartDay($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_DAY, {}, {
        query: {
            isArray: true
        }
    })
}