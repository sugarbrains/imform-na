MessageCancel.$inject = ['$resource', 'resource'];

export default function MessageCancel($resource, resource) {
    "ngInject";

    return $resource(resource.MESSAGE_CANCEL + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}