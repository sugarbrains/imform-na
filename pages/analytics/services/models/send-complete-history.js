SendCompleteHistory.$inject = ['$resource', 'resource'];

export default function SendCompleteHistory($resource, resource) {
    "ngInject";

    return $resource(resource.SEND_COMPLETE_HISTORY + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}