ChartAge.$inject = ['$resource', 'resource'];

export default function ChartAge($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_AGE, {}, {})
}