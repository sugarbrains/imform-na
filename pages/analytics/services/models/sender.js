Sender.$inject = ['$resource', 'resource'];

export default function Sender($resource, resource) {
    "ngInject";

    return $resource(resource.SENDER + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}