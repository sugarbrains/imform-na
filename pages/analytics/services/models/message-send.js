MessageSend.$inject = ['$resource', 'resource'];

export default function MessageSend($resource, resource) {
    "ngInject";

    return $resource(resource.MESSAGE_SEND + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}