MessageExport.$inject = ['$resource', 'resource'];

export default function MessageExport($resource, resource) {
    "ngInject";

    return $resource(resource.MESSAGE_EXPORT + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}