User.$inject = ['$resource', 'resource'];

export default function User($resource, resource) {
    "ngInject";

    return $resource(resource.USER + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}