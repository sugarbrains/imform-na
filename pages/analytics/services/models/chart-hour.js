ChartHour.$inject = ['$resource', 'resource'];

export default function ChartHour($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_HOUR, {}, {
        query: {
            isArray: true
        }
    })
}