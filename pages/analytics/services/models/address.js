Address.$inject = ['$resource', 'resource'];

export default function Address($resource, resource) {
    "ngInject";

    return $resource(resource.ADDRESS, {}, {
        query: {
            isArray: true
        }
    })
}