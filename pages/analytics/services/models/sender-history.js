SenderHistory.$inject = ['$resource', 'resource'];

export default function SenderHistory($resource, resource) {
    "ngInject";

    return $resource(resource.SENDER_HISTORY + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}