ChartPageTime.$inject = ['$resource', 'resource'];

export default function ChartPageTime($resource, resource) {
    "ngInject";

    return $resource(resource.CHART_PAGE_TIME, {}, {
        query: {
            isArray: true
        }
    })
}