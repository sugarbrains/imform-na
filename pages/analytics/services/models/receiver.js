Receiver.$inject = ['$resource', 'resource'];

export default function Receiver($resource, resource) {
    "ngInject";

    return $resource(resource.RECEIVER + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}