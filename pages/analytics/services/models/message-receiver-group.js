MessageReceiverGroup.$inject = ['$resource', 'resource'];

export default function MessageReceiverGroup($resource, resource) {
    "ngInject";

    return $resource(resource.MESSAGE_RECEIVER_GROUP + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}