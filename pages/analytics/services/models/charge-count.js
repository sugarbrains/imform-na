ChargeCount.$inject = ['$resource', 'resource'];

export default function ChargeCount($resource, resource) {
    "ngInject";

    return $resource(resource.CHARGE_COUNT + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}