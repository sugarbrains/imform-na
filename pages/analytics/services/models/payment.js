Payment.$inject = ['$resource', 'resource'];

export default function Payment($resource, resource) {
    "ngInject";

    return $resource(resource.PAYMENT + '/:id', {
        id: '@id'
    }, {
        update: {
            method: 'PUT'
        },
        query: {
            isArray: false
        }
    })
}