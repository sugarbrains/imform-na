export default function stateHandler ($state, constant) {
    this.setGroupTitle = setGroupTitle;
    this.setState = setState;

    function setGroupTitle (vm, stateParams = {}) {
        var state = $state.current.name;
        if (state == 'chart' || state == 'sendManage') {
            vm.groupTitle = constant.groupTitle[stateParams.state];
        } else {
            vm.groupTitle = state ? constant.groupTitle[state] : {};
        }
    }

    function setState (vm, stateParams = {}) {
        var state = $state.current.name;
        if (state == 'chart' || state == 'sendManage') {
            vm.state = stateParams.state;
        } else {
            vm.state = state ? state : '';
        }
    }
}