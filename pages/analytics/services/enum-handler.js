export default function enumHandler ($filter, dataValue) {
    'ngInject';

    let attachZero = $filter('attachZero');

    this.returnHours = returnHours;
    this.returnMinutes = returnMinutes;

    function returnHours() {
        let enums = [];
        for (let i=0; i<24; i++) {
            enums.push(attachZero(i));
        }
        return enums;
    }

    function returnMinutes() {
        let enums = [];
        for (let i=0; i<60 / dataValue.PERIOD_MINUTE; i++) {
            enums.push(attachZero(i * dataValue.PERIOD_MINUTE));
        }
        return enums;
    }
}