<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 8. 1.
 * Time: PM 8:23
 */
?>
    <article id="sign-up-wrap" class="modal-overlay" tabindex="0">
        <div class="vertical-align-wrap">
            <div class="vertical-align">
                <div id="sign-up" class="modal-item">
                    <button id="sign-up-close-button" type="button" class="modal-close-button"></button>
                    <p class="title">사용자 등록</p>
                    <form id="sign-up-form" action="/api/accounts/users.php" method="post">
                        <div class="input-wrap">
                            <label for="sign-up-id">아이디</label>
                            <div class="input-id-wrap">
                                <input id="sign-up-id" type="text" placeholder="아이디 입력" required>
                                <button id="check-duplication-button" type="button">중복체크</button>
                            </div>
                        </div>
                        <div class="input-wrap half">
                            <label for="sign-up-pw">비밀번호</label>
                            <input id="sign-up-pw" type="password" placeholder="비밀번호 입력" required>
                        </div>
                        <div class="input-wrap half last">
                            <label for="sign-up-re-pw">비밀번호 확인</label>
                            <input id="sign-up-re-pw" type="password" placeholder="비밀번호 확인 입력" required>
                        </div>
                        <div class="input-wrap half">
                            <label for="sign-up-name">이름</label>
                            <input id="sign-up-name" type="text" placeholder="이름 입력" required>
                        </div>
                        <div class="input-wrap half last">
                            <label for="sign-up-phone-num">전화번호</label>
                            <input id="sign-up-phone-num" type="tel" placeholder="01xxxxxxxxx" required>
                        </div>
                        <button id="sign-up-button" type="submit">등록</button>
                    </form>
                </div>
            </div>
        </div>
    </article>
<?php
/**
 * controller
 */
require_once(dirname(__FILE__).'/../../controllers/modals/sign-up.php');
?>