<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 7. 27.
 * Time: AM 1:33
 */

$CURRENT_NAV = 'sign-in';
require_once(dirname(__FILE__).'/../base/top.php');

$uid = $_GET['uid'] ? $_GET['uid'] : '';
$password = $_GET['password'] ? $_GET['password'] : '';
?>
    <div id="sign-vertical-align-wrap">
        <div id="sign-vertical-align">
            <article id="sign-wrap" class="sign-in">
                <h1 class="logo"><span>Analytics sign in</span></h1>
                <p class="title">통합 로그인</p>
                <form id="form" action="/api/redirect/sign-in.php" method="post">
                    <label for="input-id" class="label">아이디</label>
                    <input id="input-id" type="text" name="uid" value="<?php echo $uid ?>" placeholder="아이디" required>
                    <label for="input-pw" class="label">비밀번호</label>
                    <input id="input-pw" type="password" name="password" value="<?php echo $password ?>" placeholder="비밀번호" required>
                    <div class="option-wrap">
                        <label class="check-container">
                            아이디 저장하기
                            <input id="checkbox" class="check-box" type="checkbox">
                            <span class="check-mark"></span>
                        </label>
<!--                        <button type="button" id="find-id-pw">아이디/비밀번호 찾기</button>-->
                    </div>
                    <button type="submit" id="sign-in">로그인</button>
<!--                    <button type="button" id="open-sign-up">사용자 등록</button>-->
                </form>
            </article>
        </div>
    </div>
<?php
/**
 * controller
 */
require_once(dirname(__FILE__).'/../../../../lib/jquery.php');
require_once(dirname(__FILE__).'/../../../../lib/http.php');
require_once(dirname(__FILE__).'/../../../../lib/error.php');
require_once(dirname(__FILE__).'/../../controllers/contents/sign-in.php');


/**
 * modal view
 */
require_once(dirname(__FILE__).'/../modals/sign-up.php');


/**
 * bottom view
 */
require_once(dirname(__FILE__).'/../base/bottom.php');

