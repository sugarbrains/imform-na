<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 7. 30.
 * Time: AM 12:19
 */

$CURRENT_NAV = 'select-service';
require_once(dirname(__FILE__).'/../base/top.php');
?>
    <header>
        <h1 id="logo"></h1>
    </header>
    <article id="select-service-wrap">
        <p class="title">사용할 서비스를 선택해주세요</p>
        <div class="button-wrap">
            <button id="select-analytics" type="button">Analytics</button>
            <button id="select-dimsy" type="button">DIMSY</button>
        </div>
    </article>
<?php
/**
 * controller
 */
require_once(dirname(__FILE__).'/../../../../lib/jquery.php');
require_once(dirname(__FILE__).'/../../controllers/contents/select-service.php');


/**
 * bottom view
 */
require_once(dirname(__FILE__).'/../base/bottom.php');
?>
