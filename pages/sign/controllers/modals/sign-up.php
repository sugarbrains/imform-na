<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 8. 1.
 * Time: PM 8:46
 */
?>
    <script type="text/javascript">
        $( document ).ready(function() {
            setEvent();
        });

        var isChecked = false;
        var isCheckedValue = null;
        var phoneNumExp = new RegExp("^01[016789]{1}[0-9]{7,8}$");

        function setEvent () {
            var $overlay = $('#sign-up-wrap');
            var $openButton = $('#open-sign-up');
            var $closeButton = $('#sign-up-close-button');
            var $checkDuplicationButton = $('#check-duplication-button');
//            var $signUpButton = $('#sign-up-button');
            var $signUpForm = $('#sign-up-form');

            $openButton.click(function (e) {
                e.stopPropagation();
                modalOpen($overlay);
                modalInit();
            });

            $closeButton.click(function (e) {
                e.stopPropagation();
                modalClose();
            });

            $checkDuplicationButton.click(function (e) {
                e.stopPropagation();
                checkDuplication();
            });

            $signUpForm.submit(function (e) {
                return signUp();
            });

//            $signUpButton.click(function (e) {
//                e.stopPropagation();
//                signUp();
//            });

            $overlay.bind('keydown', function (e) {
                var keyCode = (e.keyCode ? e.keyCode : e.which);
                if (keyCode == 27) modalClose();
            });
        }

        function modalOpen ($target) {
            if (!$target.hasClass('active')) {
                $('html, body').scrollTop(0);
                $('body').css('overflow-y', 'hidden');
                $target.addClass('active');
                setTimeout(function () {
                    $target.focus();
                    $target.focusin();
                }, 350);
            }
        }

        function modalInit () {
            isChecked = false;
            isCheckedValue = null;
            var inputIdList = [
                'sign-up-id',
                'sign-up-pw',
                'sign-up-re-pw',
                'sign-up-name',
                'sign-up-phone-num'
            ];
            inputIdList.forEach(function (idKey) {
                $('#' + idKey).val('');
            });
        }

        function modalClose () {
            var $overlay = $('#sign-up-wrap');
            if ($overlay.hasClass('active')) {
                $('body').css('overflow-y', 'auto');
                $overlay.removeClass('active');
            }
        }

        function checkDuplication () {
            var uid = $('#sign-up-id').val();
            if (!isChecked || isCheckedValue !== uid) {
                isCheckedValue = null;
                if (uid) {
                    get('/api/accounts/check-duplicate-id', {
                        uid: uid
                    }, function (status, data) {
                        if (status === 204) {
                            isCheckedValue = uid;
                            isChecked = true;
                            openDialog('사용 가능한 아이디입니다.');
                        } else if (status === 409) {
                            openDialog('중복된 아이디입니다.');
                        } else {
                            alertError(status, data);
                        }
                    }, 204);
                } else {
                    openDialog('아이디를 입력해주세요.');
                }
            } else {
                openDialog('이미 중복체크를 하셨습니다.');
            }
        }

        function signUp () {
            if (!isChecked || !isCheckedValue) {
                openDialog('아이디 중복체크를 해주세요.');
                return false;
            } else if (!$('#sign-up-name').val()) {
                openDialog('이름을 입력해주세요.');
                return false;
            } else if ($('#sign-up-pw').val() !== $('#sign-up-re-pw').val()) {
                openDialog('비밀번호가 다릅니다.');
                return false;
            } else if (!phoneNumExp.test($('#sign-up-phone-num').val())) {
                openDialog('잘못된 전화번호입니다.');
                return false;
            } else {
                post('/api/accounts/users.php', {
                    uid: $('#sign-up-id').val(),
                    password: $('#sign-up-pw').val(),
                    name: $('#sign-up-name').val(),
                    phoneNum: $('#sign-up-phone-num').val()
                }, function (status, data) {
                    console.log(data);
                    if (status === 201) {
                        modalClose();
                        openDialog('사용자 등록되었습니다.\r\n로그인해주세요.');
                    } else {
                        alertError(status, data);
                    }
                }, 201);
                return false;
            }
        }
    </script>
