<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 7. 30.
 * Time: PM 5:45
 */
?>
    <script type="text/javascript">
        $( document ).ready(function() {

            function open (url) {
                window.open(url, '_blank');
            }

            $('#select-analytics').click(function () {
                open('<?php echo ANALYTICS_URL?>');
            });

            $('#select-dimsy').click(function () {
                open('<?php echo DIMSY_URL?>');
            });
        });
    </script>
