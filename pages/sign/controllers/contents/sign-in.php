<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 7. 30.
 * Time: PM 5:48
 */

if (isset($_GET) && $_GET["wrong"]) {
    echo '<script type="text/javascript">openDialog("아이디 혹은 비밀번호가 틀렸습니다.");</script>';
}
if (isset($_GET) && $_GET['api']) {
    echo '<script type="text/javascript">openDialog("서버 점검 중입니다.");</script>';
}
?>
    <script type="text/javascript">
        $( document ).ready(function() {
            var cookieKey = 'analytics_id';
            var $inputId = $('#input-id');
            var $checkbox = $('#checkbox');
            var storedId = getCookie(cookieKey);
            if (storedId) {
                $inputId.val(storedId || '');
                $checkbox.prop("checked", true);
            }

            $('#form').submit(function() {
                document.cookie = cookieKey + '=' + ($("#checkbox").is(":checked") ? $inputId.val() : '');
                return true;
            });
        });

        function getCookie (name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        }
    </script>
