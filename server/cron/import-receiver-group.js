const fs = require('fs');
const path = require('path');
const async = require('async');
const events = require('events');
const Converter = require('csvtojson/v1').Converter;
const iconv = require('iconv-lite');
const Iconv = require('iconv').Iconv;
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const UTIL = require('../utils');
const IMPORT_UTIL = UTIL.import;
const EXPORT_UTIL = UTIL.export;
const FILE_UTIL = UTIL.file;
const HASH_UTIL = UTIL.hash;

const maxSize = 500;

let isImporting = false;

module.exports = (finish) => {
    if (isImporting) {
        return finish(400, 'import receiver-group processing');
    } else {
        isImporting = true;
    }

    let funcs = [],
        splitTimes = 0,
        author = null,
        receiverGroup = null,
        receiverGroupImportHistory = null;

    funcs.push((callback) => {
        initReceivers(callback);
    });

    funcs.push((callback) => {
        getReceiverGroupImportHistory(callback);
    });

    funcs.push((callback) => {
        splitFile(callback);
    });

    funcs.push((callback) => {
        readSplitFileNUpsert(callback);
    });

    funcs.push((callback) => {
        removeSplitFiles(callback);
    });

    funcs.push((callback) => {
        createReceiverGroupRels(callback);
    });

    async.series(funcs, (error, results) => {
        isImporting = false;
        if (error) {
            if (receiverGroupImportHistory) {
                sequelize.models.ReceiverGroupImportHistory.update({
                    state: 'error',
                    errorMessage: (error + '').slice(0, 191)
                }, {
                    where: {
                        id: receiverGroupImportHistory.id
                    }
                }).then(() => {
                    finish(400, error);
                }).catch(() => {
                    finish(400, error);
                });
            } else {
                finish(400, error);
            }
        } else {
            sequelize.models.ReceiverGroupImportHistory.update({
                state: 'complete',
                progress: 100
            }, {
                where: {
                    id: receiverGroupImportHistory.id
                }
            }).then(() => {
                finish(204);
            }).catch(() => {
                finish(204);
            });
        }
    });

    async function initReceivers(callback) {
        await sequelize.models.Receiver.update({
            target: false
        }, {
            where: {
                receiverGroupTarget: true
            }
        });
        callback(null, true);
    }

    async function getReceiverGroupImportHistory(callback) {
        try {
            receiverGroupImportHistory = await sequelize.models.ReceiverGroupImportHistory.findOne({
                include: [{
                    model: sequelize.models.User,
                    as: 'author',
                    required: true
                }, {
                    model: sequelize.models.ReceiverGroup,
                    as: 'receiverGroup',
                    required: true
                }],
                order: [['id', 'ASC']],
                where: {
                    state: 'standby'
                }
            });
            if (receiverGroupImportHistory) {
                author = receiverGroupImportHistory.author;
                receiverGroup = receiverGroupImportHistory.receiverGroup;
                await sequelize.models.ReceiverGroupImportHistory.update({
                    state: 'progress'
                }, {
                    where: {
                        id: receiverGroupImportHistory.id
                    }
                });
                callback(null, true);
            } else {
                callback('import receiver group empty', false);
            }
        } catch (e) {
            console.error('get import receiver group error', e);
            callback('get import receiver group error', false);
        }
    }

    function splitFile(callback) {
        const converter = new Converter({});
        const eventEmitter = new events.EventEmitter();
        const euckr2utf8 = new Iconv('EUC-KR', 'UTF-8');
        const utf82utf8 = new Iconv('UTF-8', 'UTF-8');

        const importedFile = path.join(__dirname, '../../', receiverGroupImportHistory.fileName);
        const splitDirectory = '/uploads/import/';
        const fileName = receiverGroupImportHistory.fileName.split('/')[2];

        let receiverList = [];

        let inputError = false;
        let checkFinish = false;
        let checkIndex = {};

        fs.createReadStream(importedFile)
            .pipe(euckr2utf8)
            .on('error', () => {
                fs.createReadStream(importedFile)
                    .pipe(utf82utf8)
                    .on('error', () => {
                        inputError = true;
                        callback("잘못된 형식의 CSV 파일입니다.(charset)", false);
                    })
                    .pipe(converter)
                    .on('error', () => {
                        inputError = true;
                        callback("parse csv fail", false);
                    });
            })
            .pipe(converter)
            .on('error', () => {
                if (!inputError) {
                    inputError = true;
                    callback('parse csv fail', false);
                }
            });

        converter.on('record_parsed', (resultRow) => {
            receiverList.push(IMPORT_UTIL.returnReceiver(resultRow, (errorMessage) => {
                if (!inputError) {
                    inputError = true;
                    eventEmitter.emit('inputError', errorMessage);
                }
            }, null, null, null, []));
            if (receiverList.length === maxSize) {
                let array = receiverList;
                receiverList = [];
                splitTimes++;
                ((currentTime, list) => {
                    const splitFilePath = splitDirectory + currentTime + fileName;
                    IMPORT_UTIL.writeSplitFile(list, currentTime, splitFilePath, () => {
                        eventEmitter.emit('checkFinish', currentTime);
                    }, (errorMessage) => {
                        if (!inputError) {
                            inputError = true;
                            eventEmitter.emit('inputError', errorMessage);
                        }
                    });
                })(splitTimes - 1, array);
            }
        });

        converter.on('end_parsed', () => {
            checkFinish = true;
            if (receiverList.length > 0) {
                splitTimes++;
                ((currentTime) => {
                    const splitFilePath = splitDirectory + currentTime + fileName;
                    IMPORT_UTIL.writeSplitFile(receiverList, currentTime, splitFilePath, () => {
                        eventEmitter.emit('checkFinish', currentTime);
                    }, (errorMessage) => {
                        if (!inputError) {
                            inputError = true;
                            eventEmitter.emit('inputError', errorMessage);
                        }
                    });
                })(splitTimes - 1);
            }
            eventEmitter.emit('checkFinish');
        });

        eventEmitter.on('inputError', (errorMessage) => {
            callback(errorMessage, false);
        });

        eventEmitter.on('checkFinish', (currentIndex) => {
            if (currentIndex !== undefined) checkIndex[currentIndex] = true;
            if (checkFinish) {
                let isFinish = true;
                for (let i=0; i<splitTimes; i++) {
                    if (!checkIndex[i]) {
                        isFinish = false;
                        break;
                    }
                }
                if (isFinish) {
                    if (!inputError) {
                        callback(null, true);
                    }
                }
            }
        });
    }

    async function readSplitFileNUpsert(callback) {
        const nextYear = (new Date()).getFullYear() + 1;
        const fileName = receiverGroupImportHistory.fileName.split('/')[2];

        let subFuncs = [];

        for (let i=0; i<splitTimes; i++) {
            ((currentTime) => {
                let update = {};
                let receiverList = [];
                let receiverKeyList = [];
                let createReceiverList = null;
                subFuncs.push((subCallback) => {
                    let converter = new Converter({});
                    let filePath = path.join(__dirname, "../../uploads/import/" + currentTime + fileName);
                    fs.createReadStream(filePath)
                        .pipe(iconv.decodeStream('utf8'))
                        .pipe(iconv.encodeStream('utf8'))
                        .pipe(converter);
                    converter.on('record_parsed', (resultRow) => {
                        receiverKeyList.push(resultRow.key);
                        resultRow = IMPORT_UTIL.returnResultRow(resultRow, nextYear);
                        receiverList.push(Object.assign({
                            target: true
                        }, IMPORT_UTIL.returnReceiver(resultRow, null, receiverGroupImportHistory.authorId, update, (updateArg) => {
                            update = updateArg;
                        }, ["name"])));
                    });
                    converter.on('end_parsed', () => {
                        sequelize.models.Receiver.findExistReceivers(receiverKeyList, receiverGroupImportHistory.authorId, (status, data) => {
                            if (status === 200 || status === 404) {
                                createReceiverList = IMPORT_UTIL.returnCreateReceiverArray(receiverList, data);
                                subCallback(null, true);
                            } else {
                                subCallback('findExistReceivers fail', false);
                            }
                        });
                    });
                });

                subFuncs.push((subCallback) => {
                    if (createReceiverList && createReceiverList.length > 0) {
                        sequelize.models.Receiver.createReceivers(createReceiverList, (status, data) => {
                            if (status === 204) {
                                subCallback(null, true);
                            } else {
                                subCallback('createReceivers fail', false);
                            }
                        });
                    } else {
                        subCallback(null, true);
                    }
                });

                subFuncs.push((subCallback) => {
                    sequelize.models.Receiver.updateReceivers(receiverList, update, receiverGroupImportHistory.authorId, (status, data) => {
                        if (status === 204) {
                            let progress = Math.floor(currentTime / splitTimes * 100);
                            sequelize.models.ReceiverGroupImportHistory.update({
                                progress
                            }, {
                                where: {
                                    id: receiverGroupImportHistory.id
                                }
                            }).then(() => {
                                console.log('import receiver group progress:', progress);
                                subCallback(null, true);
                            }).catch(() => {
                                subCallback(null, true);
                            });
                        } else {
                            subCallback('updateReceivers fail', false);
                        }
                    }, 'receiverGroupTarget');
                });
            })(i);
        }

        async.series(subFuncs, (error, results) => {
            if (error) {
                callback(error, false);
            } else {
                callback(null, true);
            }
        });
    }

    async function removeSplitFiles(callback) {
        const fileName = receiverGroupImportHistory.fileName.split('/')[2];

        let promises = [];
        for (let i=0; i<splitTimes; i++) {
            ((currentTime) => {
                let filePath = path.join(__dirname, '../../uploads/import/' + currentTime + fileName);
                promises.push(FILE_UTIL.removeFile(filePath));
            })(i);
        }
        try {
            await Promise.all(promises);
        } catch (e) {}
        callback(null, true);
    }

    async function createReceiverGroupRels(callback) {
        try {
            const result = await sequelize.models.ReceiverGroupRel.createReceiverGroupRels(receiverGroupImportHistory.authorId, receiverGroup.id);
            if (result) {
                callback(null, true);
            } else {
                callback('create receiver group rels fail', false);
            }
        } catch (e) {
            callback('create receiver group rels fail', false);
        }
    }
};