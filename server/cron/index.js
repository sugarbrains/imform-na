const cron = require('node-cron');

let perFiveSeconds = [];
for (let i=0; i<12; i++) {
    perFiveSeconds.push(i * 5);
}
perFiveSeconds = perFiveSeconds.join(',');

module.exports = () => {
    cron.schedule(`${perFiveSeconds} * * * * *`, () => {
        require('./export')((status, data) => {
            if (status === 204) {
                console.log("export success");
            } else {
                if (data !== 'export empty') console.log(data);
            }
        });
    });

    cron.schedule(`${perFiveSeconds} * * * * *`, () => {
        require('./export-eland')((status, data) => {
            if (status === 204) {
                console.log("export success");
            } else {
                if (data !== 'export empty') console.log(data);
            }
        });
    });

    cron.schedule(`${perFiveSeconds} * * * * *`, () => {
        require('./import-receiver-group')((status, data) => {
            if (status === 204) {
                console.log("import receiver group success");
            } else {
                if (data !== 'import receiver group empty') console.log(data);
            }
        });
    });

    cron.schedule(`${perFiveSeconds} * * * * *`, () => {
        require('./send')((status, data) => {
            if (status === 204) {
                console.log("send success");
            } else {
                if (data !== 'send empty') console.log(data);
            }
        });
    });
};