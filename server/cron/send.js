const async = require('async');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const sequelize = require('../methods/sequelize').sequelize;
const STD = require('../metadata/standards');
const MESSAGE = STD.message;
const UTIL = require('../utils');
const MESSAGE_UTIL = UTIL.message;
const HASH_UTIL = UTIL.hash;

const maxSize = 100;

let isSending = false;

module.exports = (finish) => {
    if (isSending) {
        return finish(400, 'send processing');
    } else {
        isSending = true;
    }

    let funcs = [],
        sends = [],
        author = null,
        domain = null,
        message = null;
        // metaPrice = null;

    funcs.push((callback) => {
        getMessage(callback);
    });

    // funcs.push((callback) => {
    //     getMetaPrice(callback);
    // });

    funcs.push((callback) => {
        getSends(callback);
    });

    funcs.push((callback) => {
        sendAll(callback);
    });

    funcs.push((callback) => {
        updateMessage(callback);
    });

    async.series(funcs, (error, results) => {
        isSending = false;
        if (error) {
            finish(400, error);
        } else {
            finish(204);
        }
    });

    async function getMessage(callback) {
        try {
            message = await sequelize.models.Message.findOne({
                include: [{
                    model: sequelize.models.User,
                    as: 'author',
                    required: true,
                    include: [{
                        model: sequelize.models.Domain,
                        as: 'domain',
                        required: true
                    }]
                }],
                order: [['id', 'ASC']],
                where: {
                    state: 'send',
                    activeDate: {
                        [Op.lt]: new Date().toISOString()
                    }
                }
            });
            if (message) {
                author = message.author;
                domain = author.domain;
                callback(null, true);
            } else {
                callback('send empty', false);
            }
        } catch (e) {
            console.error('get message error', e);
            callback('get message error', false);
        }
    }

    // async function getMetaPrice(callback) {
    //     try {
    //         metaPrice = await sequelize.models.MetaPrice.findOne({
    //             order: [['createdAt', 'DESC']]
    //         });
    //         if (metaPrice) {
    //             callback(null, true);
    //         } else {
    //             callback('empty meta price', false);
    //         }
    //     } catch (e) {
    //         console.error('get meta price error', e);
    //         callback('get meta price error', false);
    //     }
    // }

    async function getSends(callback) {
        try {
            sends = await sequelize.models.Send.findAll({
                include: [{
                    model: sequelize.models.Receiver,
                    as: 'receiver'
                }],
                limit: maxSize,
                where: {
                    messageId: message.id,
                    state: 'standby'
                }
            });
            if (sends && sends.length) {
                callback(null, true);
            } else {
                await sequelize.models.Message.update({
                    state: 'sendComplete'
                }, {
                    where: {
                        id: message.id
                    }
                });
                callback('send empty', false);
            }
        } catch (e) {
            console.error('get sends error', e);
            callback('get sends error', false);
        }
    }

    function sendAll(callback) {
        const AD_MESSAGE = '(광고)';
        let host;
        if (author.url) {
            host = author.url;
        } else {
            host = 'http://' + domain.url;
        }

        const url = host + '/Z' + HASH_UTIL.idToHash(message.id) + '/';

        let subFuncs = [];
        let sendMessage = message.message;
        if (message.refuseMessage) sendMessage += ('\n' + message.refuseMessage);

        for (let i=0; i<sends.length; i++) {
            ((currentTime) => {
                subFuncs.push((subCallback) => {
                    const send = sends[currentTime];
                    const receiver = send.receiver;
                    const userUrl = url + HASH_UTIL.idToHash(receiver.id);
                    const userMessage = sendMessage.replace(MESSAGE.magicUrl, userUrl);
                    const bytes = MESSAGE_UTIL.bytes(userMessage);
                    if (bytes <= 80) {
                        MESSAGE_UTIL.sendSms({
                            id: send.id,
                            phoneNum: receiver.phoneNum,
                            message: userMessage
                        }).then(() => {
                            return sequelize.models.Send.update({
                                type: 'sms',
                                state: 'success',
                                price: author.sms
                            }, {
                                where: {
                                    id: send.id
                                }
                            });
                        }).then(() => {
                            subCallback(null, true);
                            return true;
                        }).catch((e) => {
                            sequelize.models.Send.update({
                                state: 'fail',
                                errorMessage: (e + '').slice(0, 191)
                            }, {
                                where: {
                                    id: send.id
                                }
                            }).then(() => {
                                subCallback(null, true);
                                return true;
                            }).catch(() => {
                                subCallback(null, true);
                            });
                        })
                    } else if (bytes <= 2000) {
                        MESSAGE_UTIL.sendLms({
                            id: send.id,
                            phoneNum: receiver.phoneNum,
                            title: (userMessage.indexOf(AD_MESSAGE) === 0 ? AD_MESSAGE : '') + message.title.slice(0, 191),
                            message: userMessage
                        }).then(() => {
                            return sequelize.models.Send.update({
                                type: 'lms',
                                state: 'success',
                                price: author.lms
                            }, {
                                where: {
                                    id: send.id
                                }
                            });
                        }).then(() => {
                            subCallback(null, true);
                        }).catch((e) => {
                            sequelize.models.Send.update({
                                state: 'fail',
                                errorMessage: (e + '').slice(0, 191)
                            }, {
                                where: {
                                    id: send.id
                                }
                            }).then(() => {
                                subCallback(null, true);
                                return true;
                            }).catch(() => {
                                subCallback(null, true);
                            });
                        })
                    } else {
                        sequelize.models.Send.update({
                            state: 'fail',
                            errorMessage: '메세지 길이 초과'
                        }, {
                            where: {
                                id: send.id
                            }
                        }).then(() => {
                            subCallback(null, true);
                            return true;
                        }).catch(() => {
                            subCallback(null, true);
                        });
                    }
                });
            })(i);
        }

        async.series(subFuncs, (error, results) => {
            if (error) {
                callback(error, false);
            } else {
                callback(null, true);
            }
        });
    }

    async function updateMessage(callback) {
        try {
            if (sends.length < maxSize) {
                await sequelize.models.Message.update({
                    state: 'sendComplete'
                }, {
                    where: {
                        id: message.id
                    }
                });
            }
            callback(null, true);
        } catch (e) {
            callback(null, true);
        }
    }
};