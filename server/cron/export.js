const async = require('async');
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const STD = require('../metadata/standards');
const MESSAGE = STD.message;
const UTIL = require('../utils');
const EXPORT_UTIL = UTIL.export;
const HASH_UTIL = UTIL.hash;

const maxSize = 500;

let isExporting = false;

module.exports = (finish) => {
    if (isExporting) {
        return finish(400, 'export processing');
    } else {
        isExporting = true;
    }

    let funcs = [],
        receiverCount = 0,
        splitTimes = 0,
        exportHistory = null,
        author = null,
        domain = null,
        message = null;

    funcs.push((callback) => {
        getMessage(callback);
    });

    funcs.push((callback) => {
        getReceiverCount(callback);
    });

    funcs.push((callback) => {
        findReceiversNWriteFile(callback);
    });

    async.series(funcs, (error, results) => {
        isExporting = false;
        if (error) {
            if (exportHistory) {
                sequelize.models.ExportHistory.update({
                    state: 'error',
                    errorMessage: (error + '').slice(0, 191)
                }, {
                    where: {
                        id: exportHistory.id
                    }
                }).then(() => {
                    finish(400, error);
                }).catch(() => {
                    finish(400, error);
                })
            } else {
                finish(400, error);
            }
        } else {
            sequelize.models.ExportHistory.update({
                state: 'complete',
                progress: 100
            }, {
                where: {
                    id: exportHistory.id
                }
            }).then(() => {
                finish(204);
            }).catch(() => {
                finish(204);
            });
        }
    });

    async function getMessage(callback) {
        try {
            const query = `SELECT Messages.* FROM Messages
INNER JOIN ExportHistories ON ExportHistories.messageId = Messages.id
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.messageId = Messages.id
LEFT JOIN ImportHistories ON ImportHistories.messageId = Messages.id
WHERE Messages.deletedAt IS NULL AND ExportHistories.state = 'standby' AND ImportHistories.id IS NULL 
ORDER BY Messages.id ASC LIMIT 1`;
            const result = await sequelize.query(query, {
                type: Sequelize.QueryTypes.SELECT
            });
            if (result && result.length) {
                message = result[0];
                exportHistory = await sequelize.models.ExportHistory.findOne({
                    include: [{
                        model: sequelize.models.User,
                        as: 'author',
                        required: true,
                        include: [{
                            model: sequelize.models.Domain,
                            as: 'domain'
                        }]
                    }],
                    where: {
                        messageId: message.id
                    }
                });
                author = exportHistory.author;
                domain = author.domain;
                if (message && exportHistory && author && domain) {
                    callback(null, true);
                } else {
                    callback('get info error', false);
                }
            } else {
                callback('export empty', false);
            }
        } catch (e) {
            console.error('get message error', e);
            callback('get message error', false);
        }
    }

    async function getReceiverCount(callback) {
        try {
            const query = `SELECT COUNT(*) AS count FROM (SELECT ReceiverGroupRels.receiverId FROM ReceiverGroupRels
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId AND MessageReceiverGroups.messageId = ${message.id}
GROUP BY ReceiverGroupRels.receiverId) a`;
            const result = await sequelize.query(query, {
                type: Sequelize.QueryTypes.SELECT
            });
            if (result && result.length) {
                receiverCount = result[0].count;
                splitTimes = Math.floor(receiverCount / maxSize) + (receiverCount % maxSize ? 1 : 0);
                callback(null, true);
            } else {
                callback('get receiver count error', false);
            }
        } catch (e) {
            console.error('get receiver count error', e);
            callback('get receiver count error', false);
        }
    }

    function findReceiversNWriteFile(callback) {
        let host;
        if (author.url) {
            host = author.url;
        } else {
            host = 'http://' + domain.url;
        }

        const url = host + '/Z' + HASH_UTIL.idToHash(message.id) + '/';

        let subFuncs = [];
        let options = {
            messageId: message.id,
            last: null,
            orderBy: 'id',
            sort: 'ASC',
            size: maxSize
        };
        const exportFields = ["key", "message", "url", "userHash"];

        let sendMessage = message.message;
        if (message.refuseMessage) sendMessage += ('\n' + message.refuseMessage);

        for (let i=0; i<splitTimes; i++) {
            ((currentTime) => {
                subFuncs.push((subCallback) => {
                    sequelize.models.Receiver.findMessageReceivers(options, (status, data) => {
                        if (status === 200) {
                            if (data && data.length) {
                                let foundData = [];
                                for (let i=0; i<data.length; i++) {
                                    const userHash = HASH_UTIL.idToHash(data[i].id);
                                    let body = {
                                        key: data[i].key,
                                        message: sendMessage.replace(MESSAGE.magicUrl, url + userHash),
                                        url: url + userHash,
                                        userHash
                                    };
                                    foundData.push(body);
                                }
                                options.last = data[data.length - 1].id;
                                EXPORT_UTIL.appendExportFile(foundData, currentTime, exportHistory.fileName, exportFields, () => {
                                    let progress = parseInt(currentTime / splitTimes * 100);
                                    sequelize.models.ExportHistory.update({
                                        progress
                                    }, {
                                        where: {
                                            id: exportHistory.id
                                        }
                                    }).then(() => {
                                        console.log('csv export progress:', progress);
                                        subCallback(null, true);
                                        return true;
                                    }).catch(() => {
                                        subCallback(null, true);
                                    });
                                }, (errorMessage) => {
                                    subCallback(errorMessage, false);
                                });
                            } else {
                                let progress = parseInt(currentTime / splitTimes * 100);
                                sequelize.models.ExportHistory.update({
                                    progress
                                }, {
                                    where: {
                                        id: exportHistory.id
                                    }
                                }).then(() => {
                                    console.log('csv export progress:', progress);
                                    subCallback(null, true);
                                    return true;
                                }).catch(() => {
                                    subCallback(null, true);
                                });
                            }
                        } else {
                            subCallback('findReceivers fail', false);
                        }
                    });
                });
            })(i);
        }

        async.series(subFuncs, (error, results) => {
            if (error) {
                callback(error, false);
            } else {
                callback(null, true);
            }
        });
    }
};