const fs = require('fs');
const path = require('path');
const async = require('async');
const events = require('events');
const Converter = require('csvtojson/v1').Converter;
const iconv = require('iconv-lite');
const Iconv = require('iconv').Iconv;
const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const UTIL = require('../utils');
const IMPORT_UTIL = UTIL.import;
const EXPORT_UTIL = UTIL.export;
const FILE_UTIL = UTIL.file;
const HASH_UTIL = UTIL.hash;

const maxSize = 500;

let isExporting = false;

const errorCodes = {
    '잘못된 형식의 CSV 파일입니다.(charset)': '400_0010',
    'parse csv fail': '400_0011',
    '잘못된 형식의 CSV 파일입니다. (\" 포함)': '400_0014',
    'key 값이 없습니다.': '400_0003',
    '잘못된 형식의 birth 데이터입니다.': '400_0015',
    'json2csv fail': '400_0005',
    'writeFile fail': '400_0005',
    'findExistReceivers fail': '400_0009',
    'createReceivers fail': '400_0009',
    'updateReceivers fail': '400_0009',
    'createReceiverGroup fail': '400_0009',
    'findReceivers fail': '400_0009',
    'get export history error': '400_0009'
};
const defaultErrorCode = '400_0009';

module.exports = (finish) => {
    if (isExporting) {
        return finish(400, 'export processing');
    } else {
        isExporting = true;
    }

    let funcs = [],
        splitTimes = 0,
        exportHistory = null,
        author = null,
        domain = null,
        message = null,
        importHistory = null,
        receiverGroup = null;

    funcs.push((callback) => {
        initReceivers(callback);
    });

    funcs.push((callback) => {
        getExportHistory(callback);
    });

    funcs.push((callback) => {
        splitFile(callback);
    });

    funcs.push((callback) => {
        readSplitFileNUpsert(callback);
    });

    funcs.push((callback) => {
        removeSplitFiles(callback);
    });

    funcs.push((callback) => {
        createReceiverGroup(callback);
    });

    funcs.push((callback) => {
        findReceiversNWriteFile(callback);
    });

    async.series(funcs, (error, results) => {
        isExporting = false;
        if (error) {
            if (exportHistory) {
                sequelize.models.ExportHistory.update({
                    state: 'error',
                    errorCode: errorCodes[error] ? errorCodes[error] : defaultErrorCode,
                    errorMessage: (error + '').slice(0, 191)
                }, {
                    where: {
                        id: exportHistory.id
                    }
                }).then(() => {
                    if (importHistory) {
                        return sequelize.models.ImportHistory.update({
                            errorCode: errorCodes[error] ? errorCodes[error] : defaultErrorCode,
                            errorMessage: (error + '').slice(0, 191)
                        }, {
                            where: {
                                id: importHistory.id
                            }
                        }).then(() => {
                            finish(400, error);
                        });
                    } else {
                        finish(400, error);
                    }
                }).catch(() => {
                    finish(400, error);
                });
            } else {
                finish(400, error);
            }
        } else {
            sequelize.models.ExportHistory.update({
                state: 'complete',
                progress: 100
            }, {
                where: {
                    id: exportHistory.id
                }
            }).then(() => {
                finish(204);
            }).catch(() => {
                finish(204);
            });
        }
    });

    async function initReceivers(callback) {
        await sequelize.models.Receiver.update({
            target: false
        }, {
            where: {
                target: true
            }
        });
        callback(null, true);
    }

    async function getExportHistory(callback) {
        try {
            exportHistory = await sequelize.models.ExportHistory.findOne({
                include: [{
                    model: sequelize.models.Message,
                    as: 'message',
                    required: true,
                    include: [{
                        model: sequelize.models.ImportHistory,
                        as: 'importHistory',
                        required: true
                    }]
                }, {
                    model: sequelize.models.User,
                    as: 'author',
                    required: true,
                    include: [{
                        model: sequelize.models.Domain,
                        as: 'domain'
                    }]
                }],
                order: [['id', 'ASC']],
                where: {
                    state: 'standby'
                }
            });
            if (exportHistory) {
                author = exportHistory.author;
                domain = author.domain;
                message = exportHistory.message;
                importHistory = message.importHistory;
                await sequelize.models.ExportHistory.update({
                    state: 'progress'
                }, {
                    where: {
                        id: exportHistory.id
                    }
                });
                callback(null, true);
            } else {
                callback('export empty', false);
            }
        } catch (e) {
            console.error('get export history error', e);
            callback('get export history error', false);
        }
    }
    
    function splitFile(callback) {
        const converter = new Converter({});
        const eventEmitter = new events.EventEmitter();
        const euckr2utf8 = new Iconv('EUC-KR', 'UTF-8');
        const utf82utf8 = new Iconv('UTF-8', 'UTF-8');

        const importedFile = path.join(__dirname, '../../', importHistory.fileName);
        const splitDirectory = '/uploads/import/';
        const fileName = importHistory.fileName.split('/')[2];

        let receiverList = [];

        let inputError = false;
        let checkFinish = false;
        let checkIndex = {};

        fs.createReadStream(importedFile)
            .pipe(euckr2utf8)
            .on('error', () => {
                fs.createReadStream(importedFile)
                    .pipe(utf82utf8)
                    .on('error', () => {
                        inputError = true;
                        callback("잘못된 형식의 CSV 파일입니다.(charset)", false);
                    })
                    .pipe(converter)
                    .on('error', () => {
                        inputError = true;
                        callback("parse csv fail", false);
                    });
            })
            .pipe(converter)
            .on('error', () => {
                if (!inputError) {
                    inputError = true;
                    callback('parse csv fail', false);
                }
            });

        converter.on('record_parsed', (resultRow) => {
            receiverList.push(IMPORT_UTIL.returnReceiver(resultRow, (errorMessage) => {
                if (!inputError) {
                    inputError = true;
                    eventEmitter.emit('inputError', errorMessage);
                }
            }, null, null, null, []));
            if (receiverList.length === maxSize) {
                let array = receiverList;
                receiverList = [];
                splitTimes++;
                ((currentTime, list) => {
                    const splitFilePath = splitDirectory + currentTime + fileName;
                    IMPORT_UTIL.writeSplitFile(list, currentTime, splitFilePath, () => {
                        eventEmitter.emit('checkFinish', currentTime);
                    }, (errorMessage) => {
                        if (!inputError) {
                            inputError = true;
                            eventEmitter.emit('inputError', errorMessage);
                        }
                    });
                })(splitTimes - 1, array);
            }
        });

        converter.on('end_parsed', () => {
            checkFinish = true;
            if (receiverList.length > 0) {
                splitTimes++;
                ((currentTime) => {
                    const splitFilePath = splitDirectory + currentTime + fileName;
                    IMPORT_UTIL.writeSplitFile(receiverList, currentTime, splitFilePath, () => {
                        eventEmitter.emit('checkFinish', currentTime);
                    }, (errorMessage) => {
                        if (!inputError) {
                            inputError = true;
                            eventEmitter.emit('inputError', errorMessage);
                        }
                    });
                })(splitTimes - 1);
            }
            eventEmitter.emit('checkFinish');
        });

        eventEmitter.on('inputError', (errorMessage) => {
            callback(errorMessage, false);
        });

        eventEmitter.on('checkFinish', (currentIndex) => {
            if (currentIndex !== undefined) checkIndex[currentIndex] = true;
            if (checkFinish) {
                let isFinish = true;
                for (let i=0; i<splitTimes; i++) {
                    if (!checkIndex[i]) {
                        isFinish = false;
                        break;
                    }
                }
                if (isFinish) {
                    if (!inputError) {
                        callback(null, true);
                    }
                }
            }
        });
    }

    async function readSplitFileNUpsert(callback) {
        const nextYear = (new Date()).getFullYear() + 1;
        const fileName = importHistory.fileName.split('/')[2];

        let subFuncs = [];

        for (let i=0; i<splitTimes; i++) {
            ((currentTime) => {
                let update = {};
                let receiverList = [];
                let receiverKeyList = [];
                let createReceiverList = null;
                subFuncs.push((subCallback) => {
                    let converter = new Converter({});
                    let filePath = path.join(__dirname, "../../uploads/import/" + currentTime + fileName);
                    fs.createReadStream(filePath)
                        .pipe(iconv.decodeStream('utf8'))
                        .pipe(iconv.encodeStream('utf8'))
                        .pipe(converter);
                    converter.on('record_parsed', (resultRow) => {
                        receiverKeyList.push(resultRow.key);
                        resultRow = IMPORT_UTIL.returnResultRow(resultRow, nextYear);
                        receiverList.push(Object.assign({
                            target: true
                        }, IMPORT_UTIL.returnReceiver(resultRow, null, exportHistory.authorId, update, (updateArg) => {
                            update = updateArg;
                        }, [])));
                    });
                    converter.on('end_parsed', () => {
                        sequelize.models.Receiver.findExistReceivers(receiverKeyList, exportHistory.authorId, (status, data) => {
                            if (status === 200 || status === 404) {
                                createReceiverList = IMPORT_UTIL.returnCreateReceiverArray(receiverList, data);
                                subCallback(null, true);
                            } else {
                                subCallback('findExistReceivers fail', false);
                            }
                        });
                    });
                });

                subFuncs.push((subCallback) => {
                    if (createReceiverList && createReceiverList.length > 0) {
                        sequelize.models.Receiver.createReceivers(createReceiverList, (status, data) => {
                            if (status === 204) {
                                let progress = Math.floor(currentTime * 33.333 / splitTimes * 2);
                                sequelize.models.ExportHistory.update({
                                    progress
                                }, {
                                    where: {
                                        id: exportHistory.id
                                    }
                                }).then(() => {
                                    console.log('csv export progress:', progress);
                                    subCallback(null, true);
                                }).catch(() => {
                                    subCallback(null, true);
                                });
                            } else {
                                subCallback('createReceivers fail', false);
                            }
                        });
                    } else {
                        subCallback(null, true);
                    }
                });

                subFuncs.push((subCallback) => {
                    sequelize.models.Receiver.updateReceivers(receiverList, update, exportHistory.authorId, (status, data) => {
                        if (status === 204) {
                            let progress = Math.floor(currentTime * 33.333 / splitTimes * 2) + 1;
                            sequelize.models.ExportHistory.update({
                                progress
                            }, {
                                where: {
                                    id: exportHistory.id
                                }
                            }).then(() => {
                                console.log('csv export progress:', progress);
                                subCallback(null, true);
                            }).catch(() => {
                                subCallback(null, true);
                            });
                        } else {
                            subCallback('updateReceivers fail', false);
                        }
                    });
                });
            })(i);
        }

        async.series(subFuncs, (error, results) => {
            if (error) {
                callback(error, false);
            } else {
                callback(null, true);
            }
        });
    }

    async function removeSplitFiles(callback) {
        const fileName = importHistory.fileName.split('/')[2];

        let promises = [];
        for (let i=0; i<splitTimes; i++) {
            ((currentTime) => {
                let filePath = path.join(__dirname, '../../uploads/import/' + currentTime + fileName);
                promises.push(FILE_UTIL.removeFile(filePath));
            })(i);
        }
        try {
            await Promise.all(promises);
        } catch (e) {}
        callback(null, true);
    }

    async function createReceiverGroup(callback) {
        receiverGroup = await sequelize.models.ReceiverGroup.createReceiverGroup(exportHistory.authorId, message.id);
        if (receiverGroup) {
            callback(null, true);
        } else {
            callback('createReceiverGroup fail', false);
        }
    }

    function findReceiversNWriteFile(callback) {
        let host;
        if (author.url) {
            host = author.url;
        } else {
            host = 'http://' + domain.url;
        }

        const url = host + '/Z' + HASH_UTIL.idToHash(message.id) + '/';

        let subFuncs = [];
        let options = {
            receiverGroupId: receiverGroup.id,
            last: null,
            orderBy: 'id',
            sort: 'ASC',
            size: maxSize
        };
        const exportFields = ["campaignId", "key", "url"];

        for (let i=0; i<splitTimes; i++) {
            ((currentTime) => {
                subFuncs.push((subCallback) => {
                    sequelize.models.Receiver.findReceivers(options, (status, data) => {
                        if (status === 200) {
                            if (data && data.length) {
                                let foundData = [];
                                for (let i=0; i<data.length; i++) {
                                    const userHash = HASH_UTIL.idToHash(data[i].id);
                                    let body = {
                                        campaignId: exportHistory.campaignId,
                                        key: data[i].key,
                                        url: url + userHash
                                    };
                                    foundData.push(body);
                                }
                                options.last = data[data.length - 1].id;
                                EXPORT_UTIL.appendExportFile(foundData, currentTime, exportHistory.fileName, exportFields, () => {
                                    let progress = 66 + parseInt(currentTime * 33.333 / splitTimes);
                                    sequelize.models.ExportHistory.update({
                                        progress
                                    }, {
                                        where: {
                                            id: exportHistory.id
                                        }
                                    }).then(() => {
                                        console.log('csv export progress:', progress);
                                        subCallback(null, true);
                                        return true;
                                    }).catch(() => {
                                        subCallback(null, true);
                                    });
                                }, (errorMessage) => {
                                    subCallback(errorMessage, false);
                                });
                            } else {
                                let progress = 66 + parseInt(currentTime * 33.333 / splitTimes);
                                sequelize.models.ExportHistory.update({
                                    progress
                                }, {
                                    where: {
                                        id: exportHistory.id
                                    }
                                }).then(() => {
                                    console.log('csv export progress:', progress);
                                    subCallback(null, true);
                                    return true;
                                }).catch(() => {
                                    subCallback(null, true);
                                });
                            }
                        } else {
                            subCallback('findReceivers fail', false);
                        }
                    });
                });
            })(i);
        }

        async.series(subFuncs, (error, results) => {
            if (error) {
                callback(error, false);
            } else {
                callback(null, true);
            }
        });
    }
};