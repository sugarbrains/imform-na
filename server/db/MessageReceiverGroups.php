<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 20.
 * Time: AM 2:04
 */

require_once(dirname(__FILE__).'/Model.php');

class MessageReceiverGroups extends Model
{
    public function count($data, $authorId) {
        $query = "SELECT COUNT(*) AS count FROM Messages
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.messageId = Messages.id AND MessageReceiverGroups.receiverGroupId = {$data['receiverGroupId']}
LEFT JOIN ExportHistories ON ExportHistories.messageId = Messages.id
WHERE Messages.deletedAt IS NULL
AND (Messages.state = 'sendComplete' OR (Messages.state = 'export' AND ExportHistories.state = 'complete'))";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND Messages.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function gets($data, $authorId) {
        $query = "SELECT Messages.* FROM Messages
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.messageId = Messages.id AND MessageReceiverGroups.receiverGroupId = {$data['receiverGroupId']}
LEFT JOIN ExportHistories ON ExportHistories.messageId = Messages.id
WHERE Messages.deletedAt IS NULL
AND (Messages.state = 'sendComplete' OR (Messages.state = 'export' AND ExportHistories.state = 'complete'))";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND Messages.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }

        if ($data['orderBy']) {
            $query .= " ORDER BY {$data['orderBy']}";
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY Messages.createdAt DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }


        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }
}