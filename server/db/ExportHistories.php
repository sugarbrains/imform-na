<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 6:20
 */

require_once(dirname(__FILE__).'/Model.php');

class ExportHistories extends Model
{
    public function get($id) {
        $query = "SELECT * FROM ExportHistories WHERE id = {$id} LIMIT 1";
        $result = parent::query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }
}