<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: PM 3:22
 */

require_once(dirname(__FILE__).'/Model.php');
require_once(dirname(__FILE__).'/../utils/Filter.php');
require_once(dirname(__FILE__).'/../utils/Session.php');

class Domains extends Model
{
    public function count($data) {
        $query = "SELECT COUNT(*) AS count FROM Domains";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function gets($data) {
        $query = "SELECT * FROM Domains";

        if ($data['orderBy']) {
            $query .= " ORDER BY {$data['orderBy']}";
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY createdAt DESC";
        }

        if ($data['size']) {
            $query .= " LIMIT {$data['size']}";
        }

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function get($id) {
        $query = "SELECT * FROM Domains WHERE id = {$id} LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                unset($row['password']);
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function getIdByUrl($url) {
        $query = "SELECT * FROM Domains WHERE url = '{$url}' LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['id'];
            }
            return null;
        } else {
            $createQuery = "INSERT INTO Domains (`url`, `createdAt`, `updatedAt`) VALUES ('{$url}', NOW(), NOW())";
            if (parent::query($createQuery) === TRUE) {
                $id = parent::getConn()->insert_id;
                return $id;
            } else {
                return null;
            }
        }
    }
}