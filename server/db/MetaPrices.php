<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 11:10
 */

require_once(dirname(__FILE__).'/Model.php');

class MetaPrices extends Model
{
    public function get() {
        $query = "SELECT * FROM MetaPrices ORDER BY createdAt DESC LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }
}