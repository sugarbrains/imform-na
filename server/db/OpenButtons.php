<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 1:53
 */

require_once(dirname(__FILE__).'/Model.php');
require_once(dirname(__FILE__).'/../utils/Filter.php');
require_once(dirname(__FILE__).'/../utils/Session.php');

class OpenButtons extends Model
{
    public function create($buttonId, $receiverId) {
        $query = "INSERT INTO OpenButtons (`buttonId`, `receiverId`, `userAgent`, `createdAt`, `updatedAt`)
VALUES ({$buttonId}, {$receiverId}, '{$_SERVER['HTTP_USER_AGENT']}', NOW(), NOW())";

        if (parent::query($query) === TRUE) {
            return true;
        } else {
            return null;
        }
    }

    public function getPageCounts($data) {
        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "AND openCount > {$openCount}" : '';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.address3, Receivers.birth, COUNT(OpenMessages.id) AS openCount FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= " LEFT JOIN OpenMessages ON OpenMessages.messageId = MessageReceiverGroups.messageId AND OpenMessages.receiverId = Receivers.id
WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id";

        $query = "SELECT * FROM (SELECT COUNT(*) AS count, Buttons.title FROM ({$subQuery}) AS total
INNER JOIN OpenButtons ON OpenButtons.receiverId = total.id
INNER JOIN Buttons ON OpenButtons.buttonId = Buttons.id";
        if ($data['sendHistoryIds']) {
            $query .= " AND Buttons.messageId IN({$data['sendHistoryIds']})";
        }
        $query .= " WHERE total.id > 0 {$openCountQuery} GROUP BY Buttons.id) AS a ORDER BY count DESC";

        if ($data['size']) {
            $query .= " LIMIT {$data['size']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];

            // 전체 합계
            $query_sum = "SELECT SUM(count) AS totalCount FROM (SELECT COUNT(*) AS count, Buttons.title FROM ({$subQuery}) AS total
            INNER JOIN OpenButtons ON OpenButtons.receiverId = total.id
            INNER JOIN Buttons ON OpenButtons.buttonId = Buttons.id";

            if ($data['sendHistoryIds']) {
                $query_sum .= " AND Buttons.messageId IN({$data['sendHistoryIds']})";
            }
            $query_sum .= " WHERE total.id > 0 {$openCountQuery} GROUP BY Buttons.id) AS a ORDER BY count DESC";

            $result_sum = parent::query($query_sum);
            $row_sum = $result_sum->fetch_assoc();

            while($row = $result->fetch_assoc()) {
                $row['totalCount'] = $row_sum['totalCount'];
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }
}