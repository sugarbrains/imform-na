<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 7.
 * Time: AM 2:25
 */

require_once(dirname(__FILE__).'/Model.php');

class Receivers extends Model
{
    public function count($data, $authorId) {
        $query = "SELECT COUNT(*) AS count FROM Receivers";
        if ($data['receiverGroupId']) {
            $query .= " INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id AND ReceiverGroupRels.receiverGroupId = {$data['receiverGroupId']}";
        }
        $query .= " WHERE authorId = {$authorId}";
        if ($data['searchField'] && $data['searchItem']) {
            if ($data['searchField'] === 'key') {
                $query .= " AND Receivers.{$data['searchField']} = '{$data['searchItem']}'";
            } else {
                $query .= " AND Receivers.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
            }
        }
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function gets($data, $authorId) {
        $query = "SELECT * FROM Receivers";
        if ($data['receiverGroupId']) {
            $query .= " INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id AND ReceiverGroupRels.receiverGroupId = {$data['receiverGroupId']}";
        }
        $query .= " WHERE authorId = {$authorId}";
        if ($data['searchField'] && $data['searchItem']) {
            if ($data['searchField'] === 'key') {
                $query .= " AND Receivers.{$data['searchField']} = '{$data['searchItem']}'";
            } else {
                $query .= " AND Receivers.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
            }
        }
        if ($data['orderBy']) {
            $query .= " ORDER BY Receivers.{$data['orderBy']}";
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY Receivers.createdAt DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function get($id, $authorId) {
        $query = "SELECT * FROM Receivers WHERE id = {$id} AND authorId = {$authorId} LIMIT 1";
        $result = parent::query($query);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function getAddresses($data) {
        $query = "SELECT Receivers.address1, Receivers.address2, Receivers.address3 FROM Receivers";
        if ($data['sendHistoryIds']) {
            $query .= " INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $query .= " GROUP BY Receivers.address1, Receivers.address2, Receivers.address3
ORDER BY Receivers.address1, Receivers.address2, Receivers.address3";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }
}