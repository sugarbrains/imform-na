<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 12. 22.
 * Time: PM 2:21
 */

require_once(dirname(__FILE__).'/../config/index.php');

class Model
{
    private $SERVER_NAME;
    private $USER_NAME;
    private $PASSWORD;
    private $DATABASE;

    private $conn;

    function __construct() {
        $this->SERVER_NAME = $GLOBALS['DB_SERVER_NAME'];
        $this->USER_NAME = $GLOBALS['DB_USER_NAME'];
        $this->PASSWORD = $GLOBALS['DB_PASSWORD'];
        $this->DATABASE = $GLOBALS['DB_DATABASE'];

        $this->conn = new mysqli($this->SERVER_NAME, $this->USER_NAME, $this->PASSWORD, $this->DATABASE);

        if ($this->conn->connect_error) {
            die("connection failed: " . $this->conn->connect_error);
        } else {
            $this->conn->query('set names utf8');
            $this->conn->query("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
        }
    }

    function getConn() {
        return $this->conn;
    }

    function query($query) {
        return $this->conn->query($query);
    }

    public function close() {
        $this->conn->close();
    }
}