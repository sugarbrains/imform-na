<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 2:02
 */

require_once(dirname(__FILE__).'/Model.php');

class Buttons extends Model
{
    public function getsByMessageId($messageId) {
        $query = "SELECT * FROM Buttons WHERE messageId = {$messageId} ORDER BY id ASC";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }
}