<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 18.
 * Time: AM 2:35
 */

require_once(dirname(__FILE__).'/Model.php');

class ReceiverGroupImportHistories extends Model
{
    public function get($id, $authorId) {
        $query = "SELECT * FROM ReceiverGroupImportHistories WHERE id = {$id} AND authorId = {$authorId} LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }
}