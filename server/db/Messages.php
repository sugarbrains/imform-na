<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 1:20
 */

require_once(dirname(__FILE__).'/Model.php');

class Messages extends Model
{
    public function countSendCompleteHistories($data, $authorId) {
        $query = "SELECT COUNT(*) AS count FROM Messages
WHERE authorId = {$authorId} AND deletedAt IS NULL AND state = 'sendComplete'";
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND activeDate > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND activeDate < '{$endDate}'";
        }
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND {$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function getsSendCompleteHistories($data, $authorId) {
        $query = "SELECT Messages.*
, (SELECT COUNT(*) FROM Sends WHERE messageId = Messages.id AND state = 'success') AS sendCount
, (SELECT COALESCE(SUM(price), 0) FROM Sends WHERE messageId = Messages.id AND state = 'success') AS totalPrice
FROM Messages
WHERE authorId = {$authorId} AND deletedAt IS NULL AND state = 'sendComplete'";
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND activeDate > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND activeDate < '{$endDate}'";
        }
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND {$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }

        if ($data['orderBy']) {
            $query .= " ORDER BY {$data['orderBy']}";
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY activeDate DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function getSendCompleteHistory($id, $authorId) {
        $query = "SELECT Messages.*
, (SELECT COUNT(*) FROM Sends WHERE messageId = {$id} AND state = 'success') AS sendCount
, (SELECT COALESCE(SUM(price), 0) FROM Sends WHERE messageId = {$id} AND state = 'success') AS totalPrice
FROM Messages
WHERE id = {$id} AND authorId = {$authorId} AND state = 'sendComplete' AND deletedAt IS NULL LIMIT 1";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function count($data, $authorId) {
        $query = "SELECT COUNT(*) AS count FROM (SELECT Messages.id FROM Messages
LEFT JOIN MessageReceiverGroups ON MessageReceiverGroups.messageId = Messages.id
LEFT JOIN ReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroups.id
WHERE Messages.deletedAt IS NULL AND Messages.authorId = {$authorId}";
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND Messages.createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND Messages.createdAt < '{$endDate}'";
        }
        if ($data['searchField'] && $data['searchItem']) {
            if ($data['searchField'] === 'title') {
                $query .= " AND Messages.title LIKE '%{$data['searchItem']}%'";
            } else if ($data['searchField'] === 'name') {
                $query .= " AND ReceiverGroups.name LIKE '%{$data['searchItem']}%'";
            }
        }
        if ($data['state']) {
            $query .= " AND Messages.state = '{$data['state']}'";
        }
        $query .= " GROUP BY Messages.id) a";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function gets($data, $authorId) {
        $query = "SELECT a.* FROM (SELECT Messages.* FROM Messages
LEFT JOIN MessageReceiverGroups ON MessageReceiverGroups.messageId = Messages.id
LEFT JOIN ReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroups.id
WHERE Messages.deletedAt IS NULL AND Messages.authorId = {$authorId}";
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND Messages.createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND Messages.createdAt < '{$endDate}'";
        }
        if ($data['searchField'] && $data['searchItem']) {
            if ($data['searchField'] === 'title') {
                $query .= " AND Messages.title LIKE '%{$data['searchItem']}%'";
            } else if ($data['searchField'] === 'name') {
                $query .= " AND ReceiverGroups.name LIKE '%{$data['searchItem']}%'";
            }
        }
        if ($data['state']) {
            $query .= " AND Messages.state = '{$data['state']}'";
        }
        $query .= " GROUP BY Messages.id) a";

        if ($data['orderBy']) {
            if ($data['orderBy'] === 'id' || $data['orderBy'] === 'title' || $data['orderBy'] === 'createdAt') {
                $query .= " ORDER BY a.{$data['orderBy']}";
            } else {
                $query .= " ORDER BY {$data['orderBy']}";
            }
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY a.createdAt DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                $receiverCountQuery = "SELECT COUNT(*) AS count FROM (SELECT ReceiverGroupRels.receiverId FROM ReceiverGroupRels
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId
INNER JOIN ReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroups.id AND ReceiverGroups.deletedAt IS NULL
WHERE MessageReceiverGroups.messageId = {$row['id']} GROUP BY ReceiverGroupRels.receiverId) a";
                $receiverCountResult = parent::query($receiverCountQuery);
                if ($receiverCountResult->num_rows > 0) {
                    while($receiverCount = $receiverCountResult->fetch_assoc()) {
                        $row['receiverCount'] = $receiverCount['count'];
                    }
                } else {
                    $row['receiverCount'] = 0;
                }
                if ($row['state'] === 'export') {
                    $row['sendCount'] = $row['receiverCount'];
                } else if ($row['state'] === 'sendComplete') {
                    $sendCountQuery = "SELECT COUNT(*) AS count FROM Sends WHERE messageId = {$row['id']} AND state = 'success'";
                    $sendCountResult = parent::query($sendCountQuery);
                    if ($sendCountResult->num_rows > 0) {
                        while($sendCount = $sendCountResult->fetch_assoc()) {
                            $row['sendCount'] = $sendCount['count'];
                        }
                    } else {
                        $row['sendCount'] = 0;
                    }
                } else {
                    $row['sendCount'] = 0;
                }
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function get($id) {
        $query = "SELECT * 
, (SELECT COUNT(*) FROM (SELECT ReceiverGroupRels.receiverId FROM ReceiverGroupRels
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId
INNER JOIN ReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroups.id AND ReceiverGroups.deletedAt IS NULL
WHERE MessageReceiverGroups.messageId = {$id} GROUP BY ReceiverGroupRels.receiverId) a) AS receiverCount
FROM Messages WHERE id = {$id} LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $row['receiverGroups'] = [];
                $receiverGroupQuery = "SELECT ReceiverGroups.* FROM ReceiverGroups
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroups.id AND MessageReceiverGroups.messageId = {$id}
ORDER BY MessageReceiverGroups.id ASC";
                $receiverGroupResult = parent::query($receiverGroupQuery);
                if ($receiverGroupResult->num_rows > 0) {
                    while($receiverGroup = $receiverGroupResult->fetch_assoc()) {
                        array_push($row['receiverGroups'], $receiverGroup);
                    }
                }
                $row['buttons'] = [];
                $buttonQuery = "SELECT * FROM Buttons WHERE messageId = {$row['id']} ORDER BY id ASC";
                $buttonResult = parent::query($buttonQuery);
                if ($buttonResult->num_rows > 0) {
                    while($button = $buttonResult->fetch_assoc()) {
                        array_push($row['buttons'], $button);
                    }
                }
                $row['exportHistory'] = null;
                $exportHistoryQuery = "SELECT * FROM ExportHistories WHERE messageId = {$id} LIMIT 1";
                $exportHistoryResult = parent::query($exportHistoryQuery);
                if ($exportHistoryResult->num_rows > 0) {
                    while($exportHistory = $exportHistoryResult->fetch_assoc()) {
                        $row['exportHistory'] = $exportHistory;
                    }
                }
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function create($data, $authorId) {
        $buttons = [];
        if ($data['buttons']) {
            $buttons = explode(',', $data['buttons']);
        }

        $refuseMessage = $data['refuseMessage'] ? "'{$data['refuseMessage']}'" : 'NULL';

        $messageQuery = "INSERT INTO Messages (`authorId`, `title`, `title2`, `message`, `refuseMessage`, `redirectUrl`, `createdAt`, `updatedAt`)
VALUES ({$authorId}, '{$data['title']}', '{$data['title2']}', '{$data['message']}', {$refuseMessage}, '{$data['redirectUrl']}', NOW(), NOW())";

        if ($data['receiverGroupIds']) {
            $receiverGroupIds = explode(',', $data['receiverGroupIds']);

            parent::getConn()->autocommit(FALSE);

            if (parent::query($messageQuery) === TRUE) {
                $messageId = parent::getConn()->insert_id;
                $messageReceiverGroupQuery = "INSERT INTO MessageReceiverGroups (`messageId`, `receiverGroupId`, `createdAt`, `updatedAt`) VALUES ";
                foreach($receiverGroupIds AS $index => $value) {
                    if ($index > 0) {
                        $messageReceiverGroupQuery .= ", ";
                    }
                    $messageReceiverGroupQuery .= "({$messageId}, {$value}, NOW(), NOW())";
                }
                if ($data['buttons']) {
                    $buttonQuery = "INSERT INTO Buttons (`messageId`, `title`, `createdAt`, `updatedAt`) VALUES ";
                    foreach($buttons AS $index => $title) {
                        if ($index > 0) {
                            $buttonQuery .= ", ";
                        }
                        $buttonQuery .= "({$messageId}, '{$title}', NOW(), NOW())";
                    }
                    if (parent::query($buttonQuery) !== TRUE) {
                        parent::getConn()->rollback();
                        return null;
                    }
                }
                if (parent::query($messageReceiverGroupQuery) === TRUE) {
                    parent::getConn()->commit();
                    $message = $this->get($messageId);
                    return $message;
                } else {
                    parent::getConn()->rollback();
                    return null;
                }
            } else {
                parent::getConn()->rollback();
                return null;
            }
        } else {
            if (parent::query($messageQuery) === TRUE) {
                $id = parent::getConn()->insert_id;
                $message = $this->get($id);
                return $message;
            } else {
                return null;
            }
        }
    }

    public function update($id, $authorId, $data) {
        $refuseMessage = $data['refuseMessage'] ? "'{$data['refuseMessage']}'" : 'NULL';

        $messageQuery = "UPDATE Messages SET title = '{$data['title']}', title2 = '{$data['title2']}', message = '{$data['message']}', redirectUrl = '{$data['redirectUrl']}'
, refuseMessage = {$refuseMessage} WHERE id = {$id} AND authorId = {$authorId}";
        $deleteQuery = "DELETE FROM MessageReceiverGroups WHERE messageId = {$id}";

        $receiverGroupIds = null;
        $messageReceiverGroupQuery = null;
        if ($data['receiverGroupIds']) {
            $receiverGroupIds = explode(',', $data['receiverGroupIds']);
            $messageReceiverGroupQuery = "INSERT INTO MessageReceiverGroups (`messageId`, `receiverGroupId`, `createdAt`, `updatedAt`) VALUES ";
            foreach($receiverGroupIds AS $index => $value) {
                if ($index > 0) {
                    $messageReceiverGroupQuery .= ", ";
                }
                $messageReceiverGroupQuery .= "({$id}, {$value}, NOW(), NOW())";
            }
        }

        $buttonDeleteQuery = "DELETE FROM Buttons WHERE messageId = {$id}";
        $buttonQuery = null;
        if ($data['buttons']) {
            $buttons = explode(',', $data['buttons']);
            $buttonQuery = "INSERT INTO Buttons (`messageId`, `title`, `createdAt`, `updatedAt`) VALUES ";
            foreach($buttons AS $index => $title) {
                if ($index > 0) {
                    $buttonQuery .= ", ";
                }
                $buttonQuery .= "({$id}, '{$title}', NOW(), NOW())";
            }
        }

        parent::getConn()->autocommit(FALSE);

        if (parent::query($messageQuery) === TRUE && parent::query($deleteQuery) === TRUE && parent::query($buttonDeleteQuery) === TRUE) {
            if ($messageReceiverGroupQuery) {
                if (parent::query($messageReceiverGroupQuery) === TRUE) {
                    if ($buttonQuery) {
                        if (parent::query($buttonQuery) === TRUE) {
                            parent::getConn()->commit();
                            return true;
                        } else {
                            parent::getConn()->rollback();
                            return false;
                        }
                    } else {
                        parent::getConn()->commit();
                        return true;
                    }
                } else {
                    parent::getConn()->rollback();
                    return false;
                }
            } else {
                if ($buttonQuery) {
                    if (parent::query($buttonQuery) === TRUE) {
                        parent::getConn()->commit();
                        return true;
                    } else {
                        parent::getConn()->rollback();
                        return false;
                    }
                } else {
                    parent::getConn()->commit();
                    return true;
                }
            }
        } else {
            parent::getConn()->rollback();
            return false;
        }
    }

    public function delete($id, $authorId) {
        $query = "UPDATE Messages SET deletedAt = NOW() WHERE id = {$id} AND authorId = {$authorId}";
        if (parent::query($query) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function getSendHistory($id) {
        $query = "SELECT Messages.* FROM Messages
INNER JOIN ExportHistories ON ExportHistories.messageId = Messages.id AND ExportHistories.state = 'complete'
WHERE Messages.id {$id} AND Messages.deletedAt IS NULL LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $authorQuery = "SELECT * FROM Users WHERE id = {$row['authorId']} LIMIT 1";
                $authorResult = parent::query($authorQuery);
                if ($authorResult->num_rows > 0) {
                    while($author = $authorResult->fetch_assoc()) {
                        unset($author['password']);
                        $row['author'] = $author;
                    }
                }
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function countSendHistories($data, $session) {
        $query = "SELECT COUNT(*) AS count FROM Messages
LEFT JOIN ExportHistories ON ExportHistories.messageId = Messages.id
INNER JOIN Users ON Messages.authorId = Users.id
WHERE Messages.deletedAt IS NULL
AND (Messages.state = 'sendComplete' OR (Messages.state = 'export' AND ExportHistories.state = 'complete'))";

        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND Messages.activeDate > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND Messages.activeDate < '{$endDate}'";
        }
        if ($data['searchField'] && $data['searchItem']) {
            if ($data['searchField'] === 'name') {
                $query .= " AND Users.name LIKE '%{$data['searchItem']}%'";
            } else {
                $query .= " AND Messages.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
            }
        }
        if ($data['senderIds']) {
            $query .= " AND Messages.authorId IN({$data['senderIds']})";
        }
        if ($session['role'] === 'MANAGER') {
            $query .= " AND Users.domainId = {$session['domainId']}";
        } else if ($session['role'] === 'USER') {
            $query .= " AND Users.id = {$session['id']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return null;
        } else {
            return null;
        }
    }

    public function getsSendHistories($data, $session) {
        $query = "SELECT Users.name, Messages.* 
, (SELECT COUNT(*) FROM (SELECT messageId, receiverId FROM OpenMessages WHERE receiverId > 0 GROUP BY receiverId, messageId) AS openMessages WHERE messageId = Messages.id) AS openCount
FROM Messages
LEFT JOIN ExportHistories ON ExportHistories.messageId = Messages.id
INNER JOIN Users ON Messages.authorId = Users.id
WHERE Messages.deletedAt IS NULL
AND (Messages.state = 'sendComplete' OR (Messages.state = 'export' AND ExportHistories.state = 'complete'))";

        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND Messages.activeDate > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND Messages.activeDate < '{$endDate}'";
        }
        if ($data['searchField'] && $data['searchItem']) {
            if ($data['searchField'] === 'name') {
                $query .= " AND Users.name LIKE '%{$data['searchItem']}%'";
            } else {
                $query .= " AND Messages.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
            }
        }
        if ($data['senderIds']) {
            $query .= " AND Messages.authorId IN({$data['senderIds']})";
        }
        if ($session['role'] === 'MANAGER') {
            $query .= " AND Users.domainId = {$session['domainId']}";
        } else if ($session['role'] === 'USER') {
            $query .= " AND Users.id = {$session['id']}";
        }

        if ($data['orderBy']) {
            if ($data['orderBy'] === 'name') {
                $query .= " ORDER BY Users.name";
            } else if ($data['orderBy'] === 'receiverCount') {
                $query .= " ORDER BY receiverCount";
            } else {
                $query .= " ORDER BY Messages.{$data['orderBy']}";
            }
        } else {
            $query .= " ORDER BY Messages.createdAt DESC";
        }

        if ($data['sort']) {
            $query .= " {$data['sort']}";
        } else {
            $query .= " DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                $receiverCountQuery = "SELECT COUNT(*) AS count FROM (SELECT ReceiverGroupRels.receiverId FROM ReceiverGroupRels
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId
INNER JOIN ReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroups.id AND ReceiverGroups.deletedAt IS NULL
WHERE MessageReceiverGroups.messageId = {$row['id']} GROUP BY ReceiverGroupRels.receiverId) a";
                $receiverCountResult = parent::query($receiverCountQuery);
                if ($receiverCountResult->num_rows > 0) {
                    while($receiverCount = $receiverCountResult->fetch_assoc()) {
                        $row['receiverCount'] = $receiverCount['count'];
                    }
                } else {
                    $row['receiverCount'] = 0;
                }
                if ($row['state'] === 'export') {
                    $row['sendCount'] = $row['receiverCount'];
                } else if ($row['state'] === 'sendComplete') {
                    $sendCountQuery = "SELECT COUNT(*) AS count FROM Sends WHERE messageId = {$row['id']} AND state = 'success'";
                    $sendCountResult = parent::query($sendCountQuery);
                    if ($sendCountResult->num_rows > 0) {
                        while($sendCount = $sendCountResult->fetch_assoc()) {
                            $row['sendCount'] = $sendCount['count'];
                        }
                    } else {
                        $row['sendCount'] = 0;
                    }
                } else {
                    $row['sendCount'] = 0;
                }
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function getsMessageList($data) {
        $query = "SELECT id, title FROM Messages WHERE deletedAt IS NULL AND id IN({$data['ids']}) ORDER BY FIELD(id,{$data['ids']})";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function checkOwnMessages($ids, $authorId) {
        $query = "SELECT COUNT(*) AS count FROM Messages WHERE id IN({$ids}) AND authorId = {$authorId}";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return ($row['count'] == count(explode(',', $ids)));
            }
            return false;
        } else {
            return false;
        }
    }

    public function cancel($id) {
        $query = "UPDATE Messages SET state = 'cancel' WHERE id = {$id}";
        if (parent::query($query) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function export($id, $authorId) {
        $timestamp = round(microtime(true) * 1000);
        $fileName = "uploads/export/{$authorId}_{$timestamp}.csv";
        $messageQuery = "UPDATE Messages SET state = 'export', activeDate = NOW() WHERE id = {$id}";
        $exportHistoryQuery = "INSERT INTO ExportHistories (`authorId`, `messageId`, `fileName`, `createdAt`, `updatedAt`)
VALUES ({$authorId}, {$id}, '{$fileName}', NOW(), NOW())";
        
        parent::getConn()->autocommit(FALSE);

        if (parent::query($messageQuery) === TRUE && parent::query($exportHistoryQuery) === TRUE) {
            $exportHistoryId = parent::getConn()->insert_id;
            parent::getConn()->commit();
            $query = "SELECT * FROM ExportHistories WHERE id = {$exportHistoryId} LIMIT 1";
            $result = parent::query($query);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    return $row;
                }
                return null;
            } else {
                return null;
            }
        } else {
            parent::getConn()->rollback();
            return null;
        }
    }

    public function send($id, $authorId, $data) {
        date_default_timezone_set('UTC');
        $temp = null;
        if ($data['activeDate']) {
            $temp = date('Y-m-d H:i:s', strtotime($data['activeDate'].'-9 hours'));
        }
        $activeDate = $data['activeDate'] ? "'{$temp}'" : 'NOW()';
        $messageQuery = "UPDATE Messages SET state = 'send', activeDate = {$activeDate} WHERE id = {$id}";
        $sendQuery = "INSERT INTO Sends (messageId, receiverId, createdAt, updatedAt)
SELECT {$id}, Receivers.id, NOW(), NOW() FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId AND MessageReceiverGroups.messageId = {$id}
GROUP BY Receivers.id";

        parent::getConn()->autocommit(FALSE);
        if (parent::query($messageQuery) === TRUE && parent::query($sendQuery) === TRUE) {
            parent::getConn()->commit();
            return true;
        } else {
            parent::getConn()->rollback();
            return false;
        }
    }
}