<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 12:57
 */

require_once(dirname(__FILE__).'/Model.php');
require_once(dirname(__FILE__).'/../utils/Filter.php');
require_once(dirname(__FILE__).'/../utils/Session.php');

class OpenMessages extends Model
{
    public function create($messageId, $receiverId) {
        $query = "INSERT INTO OpenMessages (`messageId`, `receiverId`, `userAgent`, `createdAt`, `updatedAt`)
VALUES ({$messageId}, {$receiverId}, '{$_SERVER['HTTP_USER_AGENT']}', NOW(), NOW())";

        if (parent::query($query) === TRUE) {
            return true;
        } else {
            return null;
        }
    }

    static public function generateOpenerQuery($data) {
        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "WHERE openCount > {$openCount}" : 'WHERE openCount > 0';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT * FROM (SELECT a.*, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= "WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) a
LEFT JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) GROUP BY a.id) a {$openCountQuery}";

        return $subQuery;
    }

    public function getChartOpenerCount($data) {
        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "AND openCount > {$openCount}" : '';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT a.*, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= "WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) a
LEFT JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) GROUP BY a.id";

        $receiverQuery = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE id > 0 {$openCountQuery}";
        $openerQuery = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE openCount > 0 {$openCountQuery}";
        $reopenerQuery = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE openCount > 1 {$openCountQuery}";
        $openQuery = "SELECT COALESCE(SUM(openCount), 0) FROM ({$subQuery}) AS total WHERE total.id > 0 {$openCountQuery}";
        $reopenQuery = "SELECT COALESCE(SUM(openCount), 0) FROM ({$subQuery}) AS total WHERE openCount > 1 {$openCountQuery}";

        $query = "SELECT ($receiverQuery) AS receiverCount, ({$openerQuery}) AS openerCount, ({$openQuery}) AS openCount, ({$reopenerQuery}) AS reopenerCount, ({$reopenQuery}) AS reopenCount";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $row['reopenCount'] = $row['reopenCount'] - $row['reopenerCount'];
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function getChartGender($data) {
        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "AND openCount > {$openCount}" : '';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT a.*, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= "WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) a
LEFT JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) GROUP BY a.id";

        $query = "SELECT (SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE gender = 'm' {$openCountQuery}) AS genderMale
, (SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE gender = 'f' {$openCountQuery}) AS genderFemale
, (SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE gender = 'noGender' {$openCountQuery}) AS genderNone";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function getChartAge($data) {
        $currentYear = intval(date("Y"));
        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "AND openCount > {$openCount}" : '';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT a.*, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= "WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) a
LEFT JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) GROUP BY a.id";

        $age20Start = $currentYear - 29;
        $age20End = $currentYear - 18;
        $age30Start = $currentYear - 39;
        $age30End = $currentYear - 28;
        $age40Start = $currentYear - 49;
        $age40End = $currentYear - 38;
        $age50End = $currentYear - 48;

        $age20Query = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE (birth > {$age20Start} AND birth < {$age20End}) {$openCountQuery}";
        $age30Query = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE (birth > {$age30Start} AND birth < {$age30End}) {$openCountQuery}";
        $age40Query = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE (birth > {$age40Start} AND birth < {$age40End}) {$openCountQuery}";
        $age50Query = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE birth < {$age50End} {$openCountQuery}";
        $ageNoneQuery = "SELECT COUNT(*) FROM ({$subQuery}) AS total WHERE birth = 9999 {$openCountQuery}";

        $query = "SELECT ({$age20Query}) AS age20, ({$age30Query}) AS age30, ({$age40Query}) AS age40, ({$age50Query}) AS age50, ({$ageNoneQuery}) AS ageNone";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function getChartAddress($data) {
        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "AND openCount > {$openCount}" : '';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT a.*, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= "WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) a
LEFT JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) GROUP BY a.id";

        $addressQuery = "";

        if ($data['sido'] && $data['sigungu']) {
            $addressQuery .= " address1, address2, address3";
        } else if ($data['sido']) {
            $addressQuery .= " address1, address2";
        } else {
            $addressQuery .= " address1";
        }

        $query = "SELECT * FROM (SELECT {$addressQuery}, COUNT(*) AS count FROM ({$subQuery}) AS total WHERE id > 0 {$openCountQuery} GROUP BY {$addressQuery}) a ORDER BY count DESC";

        if ($data['size']) {
            $query .= " LIMIT {$data['size']}";
        }

        if ($data['offset']) {
            $query .= " LIMIT {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function getSentTime($data) {
        $query = "SELECT MIN(OpenMessages.createdAt) AS sentTime FROM OpenMessages";
        if ($data['sendHistoryIds']) {
            $query .= " WHERE OpenMessages.messageId IN({$data['sendHistoryIds']})";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['sentTime'];
            }
            return null;
        } else {
            return null;
        }
    }

    public function getChartHour($data, $sentTime) {
        $zeroTime = strtotime($sentTime);
        $maxTime = date('Y-m-d H:i:s', strtotime($sentTime.'+72 hours'));

        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "WHERE openCount > {$openCount}" : '';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT a.id, FLOOR((UNIX_TIMESTAMP(a.createdAt) - {$zeroTime}) / 10800) * 3 AS createdAt FROM (SELECT OpenMessages.id, OpenMessages.createdAt, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.address3, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= " WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) AS a
INNER JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) AND OpenMessages.createdAt < '{$maxTime}' GROUP BY OpenMessages.id) a {$openCountQuery}";

        $query = "SELECT createdAt AS hour, COUNT(id) AS openCount FROM ({$subQuery}) a GROUP BY createdAt ORDER BY createdAt ASC LIMIT 24";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function getChartDay($data, $sentTime) {
        $zeroTime = strtotime($sentTime);
        $maxTime = date('Y-m-d H:i:s', strtotime($sentTime.'+7 days'));

        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "WHERE openCount > {$openCount}" : '';

        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }

        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT a.id, FLOOR((UNIX_TIMESTAMP(a.createdAt) - {$zeroTime}) / 86400) AS createdAt FROM (SELECT OpenMessages.id, OpenMessages.createdAt, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.address3, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= " WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) AS a
INNER JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) AND OpenMessages.createdAt < '{$maxTime}' GROUP BY OpenMessages.id) a {$openCountQuery}";

        $query = "SELECT createdAt AS day, COUNT(id) AS openCount FROM ({$subQuery}) a GROUP BY createdAt ORDER BY createdAt ASC LIMIT 7";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }
}