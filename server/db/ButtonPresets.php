<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 22.
 * Time: AM 3:14
 */

require_once(dirname(__FILE__).'/Model.php');

class ButtonPresets extends Model
{
    public function getsByDomainId($domainId) {
        $query = "SELECT * FROM ButtonPresets WHERE domainId = {$domainId} ORDER BY id ASC";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }
}