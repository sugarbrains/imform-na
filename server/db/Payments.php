<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 28.
 * Time: AM 3:08
 */

require_once(dirname(__FILE__).'/Model.php');

class Payments extends Model
{
    public function count($data, $userId) {
        $query = "SELECT COUNT(*) AS count FROM Payments WHERE userId = {$userId}";
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND createdAt < '{$endDate}'";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function gets($data, $userId) {
        $query = "SELECT * FROM Payments WHERE userId = {$userId}";
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND createdAt < '{$endDate}'";
        }

        if ($data['orderBy']) {
            $query .= " ORDER BY {$data['orderBy']}";
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY createdAt DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while ($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function get($id, $userId) {
        $query = "SELECT * FROM Payments WHERE id = {$id} AND userId = {$userId} LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function getChargeCount($userId) {
        $query = "SELECT
(SELECT COALESCE(SUM(price), 0) FROM Sends
INNER JOIN Messages ON Sends.messageId = Messages.id AND Messages.authorId = {$userId} AND Messages.state = 'sendComplete'
WHERE Sends.state = 'success') AS totalSendPrice
, (SELECT COALESCE(SUM(price), 0) FROM Payments WHERE userId = {$userId}) AS totalPaymentPrice";

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }
}