<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: AM 10:49
 */

require_once(dirname(__FILE__).'/Model.php');
require_once(dirname(__FILE__).'/../utils/Filter.php');
require_once(dirname(__FILE__).'/../utils/Session.php');

class Users extends Model
{
    public function getSenderHistory($id, $domainId = null) {
        $query = "SELECT a.*
, (SELECT MAX(Messages.activeDate) FROM Messages WHERE Messages.authorId = {$id} AND Messages.state IN('sendComplete', 'export') AND Messages.deletedAt IS NULL) AS activeDate
FROM (SELECT Users.*, COUNT(Messages.id) AS sendCount FROM Users
INNER JOIN Messages ON Messages.authorId = Users.id AND Messages.state IN('sendComplete', 'export') AND Messages.deletedAt IS NULL
WHERE Users.deletedAt IS NULL AND Users.id = {$id}";
        if ($domainId) {
            $query .= " AND Users.domainId = {$domainId}";
        }
        $query .= " GROUP BY Users.id) a";
        $query .= " LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                unset($row['password']);
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function countSenderHistories($data, $domainId = null) {
        $query = "SELECT COUNT(*) AS count FROM (SELECT Users.id FROM Users
INNER JOIN Messages ON Messages.authorId = Users.id AND Messages.state IN('sendComplete', 'export') AND Messages.deletedAt IS NULL
WHERE Users.deletedAt IS NULL";
        if ($domainId) {
            $query .= " AND Users.domainId = {$domainId}";
        }
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND Users.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
        $query .= " GROUP BY Users.id) a";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function getsSenderHistories($data, $domainId = null) {
        $query = "SELECT a.*
, (SELECT MAX(Messages.activeDate) FROM Messages WHERE Messages.authorId = a.id AND Messages.state IN('sendComplete', 'export') AND Messages.deletedAt IS NULL) AS activeDate
FROM (SELECT Users.*, COUNT(Messages.id) AS sendCount FROM Users
INNER JOIN Messages ON Messages.authorId = Users.id AND Messages.state IN('sendComplete', 'export') AND Messages.deletedAt IS NULL
WHERE Users.deletedAt IS NULL";
        if ($domainId) {
            $query .= " AND Users.domainId = {$domainId}";
        }
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND Users.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
        $query .= " GROUP BY Users.id) a";

        if ($data['orderBy']) {
            if ($data['orderBy'] === 'activeDate') {
                $query .= " ORDER BY activeDate";
            } else {
                $query .= " ORDER BY a.{$data['orderBy']}";
            }
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY activeDate DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function countByManager($data, $domainId) {
        $query = "SELECT COUNT(*) AS count FROM Users WHERE deletedAt IS NULL AND role IN('MANAGER', 'USER', 'API') AND domainId = {$domainId}";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND {$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND createdAt < '{$endDate}'";
        }
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function getsByManager($data, $domainId) {
        $query = "SELECT id, domainId, uid, role, name, phoneNum, url, refuseMessage, createdAt, updatedAt, deletedAt
FROM Users WHERE deletedAt IS NULL AND role IN('MANAGER', 'USER', 'API') AND domainId = {$domainId}";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND {$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00'));
            $query .= " AND createdAt < '{$endDate}'";
        }

        if ($data['orderBy']) {
            $query .= " ORDER BY {$data['orderBy']}";
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY createdAt DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function count($data) {
        $query = "SELECT COUNT(*) AS count FROM Users WHERE deletedAt IS NULL";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND {$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt < '{$endDate}'";
        }
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function gets($data) {
        $query = "SELECT id, domainId, uid, role, name, phoneNum, url, refuseMessage, createdAt, updatedAt, deletedAt
FROM Users WHERE deletedAt IS NULL";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND {$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
        if ($data['startDate']) {
            $startDate = date('Y-m-d H:i:s', strtotime($data['startDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt > '{$startDate}'";
        }
        if ($data['endDate']) {
            $endDate = date('Y-m-d H:i:s', strtotime($data['endDate'].' 15:00:00-1 days'));
            $query .= " AND createdAt < '{$endDate}'";
        }

        if ($data['orderBy']) {
            $query .= " ORDER BY {$data['orderBy']}";
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY createdAt DESC";
        }

        $query .= " LIMIT {$data['size']}";

        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function getByManager($id, $domainId) {
        $query = "SELECT * FROM Users WHERE id = {$id} AND domainId = {$domainId} AND deletedAt IS NULL";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                unset($row['password']);
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function checkDuplicateId($uid) {
        $query = "SELECT COUNT(*) AS count FROM Users WHERE uid = '{$uid}' AND deletedAt IS NULL";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'] === '0';
            }
            return false;
        } else {
            return false;
        }
    }

    public function get($id) {
        $query = "SELECT * FROM Users WHERE id = {$id} AND deletedAt IS NULL LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                unset($row['password']);
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function update($id, $data) {
        $url = $data['url'] ? "'{$data['url']}'" : 'NULL';
        $refuseMessage = $data['refuseMessage'] ? "'{$data['refuseMessage']}'" : 'NULL';

        $query = "UPDATE Users SET `uid` = '{$data['uid']}', `role` = '{$data['role']}', `name` = '{$data['name']}', `phoneNum` = '{$data['phoneNum']}'
, `url` = {$url}, `refuseMessage` = {$refuseMessage}";

        if ($data['domainId']) {
            $query .= ", `domainId` = {$data['domainId']}";
        }

        if ($data['password']) {
            $password = Filter::password($data['password']);
            $query .= ", `password` = '{$password}'";
        }

        if ($data['sms']) {
            $query .= ", `sms` = {$data['sms']}";
        }

        if ($data['lms']) {
            $query .= ", `lms` = {$data['lms']}";
        }

        $query .= " WHERE id = {$id}";

        if (parent::query($query) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function create($body, $domainId) {
        $password = Filter::password($body['password']);

        $query = "INSERT INTO Users (`domainId`, `uid`, `password`, `name`, `phoneNum`, `createdAt`, `updatedAt`)
VALUES ({$domainId}, '{$body['uid']}', '{$password}', '{$body['name']}', '{$body['phoneNum']}', NOW(), NOW())";

        if (parent::query($query) === TRUE) {
            $id = parent::getConn()->insert_id;
            $user = $this->get($id);
            return $user;
        } else {
            return null;
        }
    }

    public function login($body, $domain) {
        $query = "SELECT Users.*, Domains.url AS domainUrl FROM Users
INNER JOIN Domains ON Users.domainId = Domains.id
WHERE Users.uid = '{$body['uid']}' AND Users.deletedAt IS NULL LIMIT 1";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                if ($row['role'] !== 'ADMIN' && $row['domainUrl'] !== $domain) {
                    return null;
                }
                if (password_verify($body['password'], $row['password'])) {
                    if ($row['role'] === 'API') {
                        return 'API';
                    } else {
                        Session::setSession($row);
                        return $row;
                    }
                }
            }
            return null;
        } else {
            return null;
        }
    }
}