<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 12.
 * Time: PM 9:53
 */

require_once(dirname(__FILE__).'/Model.php');
require_once(dirname(__FILE__).'/../utils/Filter.php');

class ReceiverGroups extends Model
{
    public function count($data, $authorId) {
        $query = "SELECT COUNT(*) AS count FROM ReceiverGroups
INNER JOIN Users ON ReceiverGroups.authorId = Users.id
LEFT JOIN ReceiverGroupImportHistories ON ReceiverGroupImportHistories.ReceiverGroupId = ReceiverGroups.id 
WHERE (ReceiverGroupImportHistories.id IS NULL OR ReceiverGroupImportHistories.state = 'complete') AND ReceiverGroups.deletedAt IS NULL AND ReceiverGroups.authorId = {$authorId}";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND ReceiverGroups.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
//        if ($session['role'] === 'MANAGER') {
//            $query .= " AND Users.domainId = {$session['id']}";
//        } else if ($session['role'] === 'USER') {
//            $query .= " AND ReceiverGroups.authorId = {$session['id']}";
//        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row['count'];
            }
            return 0;
        } else {
            return 0;
        }
    }

    public function gets($data, $authorId) {
        $query = "SELECT ReceiverGroups.*
, (SELECT COUNT(*) FROM ReceiverGroupRels WHERE ReceiverGroupId = ReceiverGroups.id) AS receiverCount
, (SELECT COUNT(*) FROM Messages
LEFT JOIN ExportHistories ON ExportHistories.messageId = Messages.id
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.messageId = Messages.id
WHERE MessageReceiverGroups.receiverGroupId = ReceiverGroups.id AND Messages.deletedAt IS NULL
AND (Messages.state = 'sendComplete' OR (Messages.state = 'export' AND ExportHistories.state = 'complete'))) AS sentCount
FROM ReceiverGroups
INNER JOIN Users ON ReceiverGroups.authorId = Users.id
LEFT JOIN ReceiverGroupImportHistories ON ReceiverGroupImportHistories.ReceiverGroupId = ReceiverGroups.id 
WHERE (ReceiverGroupImportHistories.id IS NULL OR ReceiverGroupImportHistories.state = 'complete') AND ReceiverGroups.deletedAt IS NULL AND ReceiverGroups.authorId = {$authorId}";
        if ($data['searchField'] && $data['searchItem']) {
            $query .= " AND ReceiverGroups.{$data['searchField']} LIKE '%{$data['searchItem']}%'";
        }
//        if ($session['role'] === 'MANAGER') {
//            $query .= " AND Users.domainId = {$session['id']}";
//        } else if ($session['role'] === 'USER') {
//            $query .= " AND ReceiverGroups.authorId = {$session['id']}";
//        }
        if ($data['orderBy']) {
            if ($data['orderBy'] === 'id' || $data['orderBy'] === 'name' || $data['orderBy'] === 'createdAt') {
                $query .= " ORDER BY ReceiverGroups.{$data['orderBy']}";
            } else {
                $query .= " ORDER BY {$data['orderBy']}";
            }
            if ($data['sort']) {
                $query .= " {$data['sort']}";
            } else {
                $query .= " DESC";
            }
        } else {
            $query .= " ORDER BY ReceiverGroups.createdAt DESC";
        }
        if ($data['size']) {
            $query .= " LIMIT {$data['size']}";
        }
        if ($data['offset']) {
            $query .= " OFFSET {$data['offset']}";
        }

        $result = parent::query($query);

        if ($result->num_rows > 0) {
            $rows = [];
            while($row = $result->fetch_assoc()) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return [];
        }
    }

    public function get($id, $authorId) {
        $query = "SELECT ReceiverGroups.*
, (SELECT COUNT(*) FROM ReceiverGroupRels WHERE ReceiverGroupId = ReceiverGroups.id) AS receiverCount
, (SELECT COUNT(*) FROM Messages
LEFT JOIN ExportHistories ON ExportHistories.messageId = Messages.id
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.messageId = Messages.id
WHERE MessageReceiverGroups.receiverGroupId = ReceiverGroups.id AND Messages.deletedAt IS NULL
AND (ExportHistories.id > 0)) AS sentCount
FROM ReceiverGroups WHERE ReceiverGroups.deletedAt IS NULL AND ReceiverGroups.id = {$id} AND ReceiverGroups.authorId = {$authorId}";
        $result = parent::query($query);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                return $row;
            }
            return null;
        } else {
            return null;
        }
    }

    public function create($name, $fileName, $authorId) {
        $receiverGroupQuery = "INSERT INTO ReceiverGroups (`authorId`, `name`, `createdAt`, `updatedAt`) VALUES ({$authorId}, '{$name}', NOW(), NOW())";
        parent::getConn()->autocommit(FALSE);

        if (parent::query($receiverGroupQuery) === TRUE) {
            $receiverGroupId = parent::getConn()->insert_id;
            $receiverGroupImportHistoryQuery = "INSERT INTO ReceiverGroupImportHistories (`authorId`, `receiverGroupId`, `fileName`, `createdAt`, `updatedAt`) VALUES ({$authorId}, {$receiverGroupId}, '{$fileName}', NOW(), NOW())";
            if (parent::query($receiverGroupImportHistoryQuery) === TRUE) {
                $receiverGroupImportHistoryId = parent::getConn()->insert_id;
                parent::getConn()->commit();
                $query = "SELECT * FROM ReceiverGroupImportHistories WHERE id = {$receiverGroupImportHistoryId} LIMIT 1";
                $result = parent::query($query);
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        return $row;
                    }
                    return null;
                } else {
                    return null;
                }
            } else {
                parent::getConn()->rollback();
                return null;
            }
        } else {
            parent::getConn()->rollback();
            return null;
        }
    }

    public function update($id, $authorId, $data) {
        $query = "UPDATE ReceiverGroups SET `name` = '{$data['name']}' WHERE id = ${id} AND authorId = {$authorId}";
        if (parent::query($query) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id, $authorId) {
        $query = "UPDATE ReceiverGroups SET deletedAt = NOW() WHERE id = {$id} AND authorId = {$authorId}";
        if (parent::query($query) === TRUE) {
            return true;
        } else {
            return false;
        }
    }

    public function sendHistoryReceiverGroup($data, $authorId) {
        $createReceiverGroupQuery = "INSERT INTO ReceiverGroups (`authorId`, `name`, `createdAt`, `updatedAt`) VALUES ({$authorId}, '{$data['name']}', NOW(), NOW())";

        $openCount = $data['openCount'] ? intval($data['openCount']) - 1 : '';
        $openCountQuery = $data['openCount'] ? "WHERE a.openCount > {$openCount}" : "WHERE a.openCount > 0";
        $address1Query = $data['sido'] ? "AND Receivers.address1 = '{$data['sido']}'" : '';
        $address2Query = $data['sigungu'] ? "AND Receivers.address2 = '{$data['sigungu']}'" : '';
        $birthQuery = '';
        if ($data['ages']) {
            $birthQuery .= ' AND (';
            $currentYear = intval(date("Y"));
            if (strpos($data['ages'],'age20') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 29;
                $endBirth = $currentYear - 18;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age30') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 39;
                $endBirth = $currentYear - 28;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age40') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $startBirth = $currentYear - 49;
                $endBirth = $currentYear - 38;
                $birthQuery .= "(Receivers.birth > {$startBirth} AND Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'age50') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $endBirth = $currentYear - 48;
                $birthQuery .= "(Receivers.birth < {$endBirth})";
            }
            if (strpos($data['ages'],'ageNone') !== FALSE) {
                $birthQuery .= $birthQuery === ' AND (' ? '' : ' OR ';
                $birthQuery .= "(Receivers.birth = 9999)";
            }
            $birthQuery .= ')';
        }
        $genderQuery = '';
        if ($data['genders']) {
            $genderQuery .= ' AND (';
            if (strpos($data['genders'],'genderMale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'm')";
            }
            if (strpos($data['genders'],'genderFemale') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'f')";
            }
            if (strpos($data['genders'],'genderNone') !== FALSE) {
                $genderQuery .= $genderQuery === ' AND (' ? '' : ' OR ';
                $genderQuery .= "(Receivers.gender = 'noGender')";
            }
            $genderQuery .= ')';
        }
        $searchQuery = '';
        if ($data['searchField'] === 'key' && $data['searchItem']) {
            $searchQuery .= " AND Receivers.key = '{$data['searchItem']}'";
        }
        $whereQuery = "{$address1Query} {$address2Query}{$birthQuery}{$genderQuery}{$searchQuery}";

        $subQuery = "SELECT a.*, COUNT(OpenMessages.id) AS openCount FROM (SELECT Receivers.id, Receivers.key, Receivers.gender, Receivers.address1, Receivers.address2, Receivers.birth FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON ReceiverGroupRels.receiverGroupId = MessageReceiverGroups.receiverGroupId";
        if ($data['sendHistoryIds']) {
            $subQuery .= " AND MessageReceiverGroups.messageId IN({$data['sendHistoryIds']})";
        }
        $subQuery .= "WHERE Receivers.id > 0 {$whereQuery} GROUP BY Receivers.id) a
LEFT JOIN OpenMessages ON OpenMessages.receiverId = a.id AND OpenMessages.messageId IN({$data['sendHistoryIds']}) GROUP BY a.id";

        parent::getConn()->autocommit(FALSE);

        if (parent::query($createReceiverGroupQuery) === TRUE) {
            $receiverGroupId = parent::getConn()->insert_id;
            $createReceiverGroupRelQuery = "INSERT INTO ReceiverGroupRels (`receiverGroupId`, `receiverId`, `createdAt`, `updatedAt`)
SELECT {$receiverGroupId}, a.id, NOW(), NOW() FROM ({$subQuery}) AS a {$openCountQuery}";
            if (parent::query($createReceiverGroupRelQuery) === TRUE) {
                parent::getConn()->commit();
                return true;
            } else {
                parent::getConn()->rollback();
                return false;
            }
        } else {
            parent::getConn()->rollback();
            return false;
        }
    }
}