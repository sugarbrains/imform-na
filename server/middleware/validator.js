const intExp = new RegExp('^[-]?[0-9]*$');
const floatExp = new RegExp('^[-]?[0-9]*[\.]?[0-9]+$');
const emailExp = new RegExp('^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$');

module.exports = async (ctx, next) => {

    ctx.check = check(ctx);

    await next();
};

function check(ctx) {

    const response = (status, data) => {
        ctx.throw(status, JSON.stringify(data));
    };

    return {
        isInt: (key) => {
            const param = getParams(ctx, key);
            if (param && !intExp.test(param)) {
                response(400, {
                    code: '400_1'
                });
            }
        },
        isFloat: (key) => {
            const param = getParams(ctx, key);
            if (param && !floatExp.test(param)) {
                response(400, {
                    code: '400_2'
                });
            }
        },
        isEmail: (key) => {
            const param = getParams(ctx, key);
            if (param && !emailExp.test(param)) {
                response(400, {
                    code: '400_3'
                });
            }
        },
        isBoolean: (key) => {
            const param = getParams(ctx, key);
            if (param && param !== 'true' && param !== 'false') {
                response(400, {
                    code: '400_4'
                });
            }
        },
        isDate: (key) => {
            const param = getParams(ctx, key);
            if (param) {
                const date = new Date(param);
                if (isNaN(date.getTime())) {
                    response(400, {
                        code: '400_5'
                    });
                }
            }
        },
        len: (key, min = null, max = null) => {
            const param = getParams(ctx, key);
            if (typeof min === 'number' && typeof max === 'number') {
                if (param && (param.length < min || param.length > max)) {
                    response(400, {
                        code: '400_6'
                    });
                }
            } else {
                throw new Error('argument is essential and integer type');
            }
        },
        isRegExp: (key, RegExp) => {
            const param = getParams(ctx, key);
            if (param && !RegExp.test(param)) {
                response(400, {
                    code: '400_7'
                });
            }
        }
    };
}

function getParams(ctx, key) {
    if (ctx.params && ctx.params[key] !== undefined && ctx.params[key] !== undefined) {
        return ctx.params[key] + '';
    } else if (ctx.query && ctx.query[key] !== undefined && ctx.query[key] !== null) {
        return ctx.query[key] + '';
    } else if (ctx.request && ctx.request.body && ctx.request.body[key] !== undefined && ctx.request.body !== null) {
        return ctx.request.body[key] + '';
    } else {
        return null;
    }
}
