const sequelize = require('../methods/sequelize').sequelize;

module.exports = async (ctx, next) => {
    ctx.models = sequelize.models;

    return next();
};
