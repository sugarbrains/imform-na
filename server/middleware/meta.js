const std = require('../metadata/standards');

module.exports = async (ctx, next) => {
    ctx.meta = {
        std
    };

    await next();
};