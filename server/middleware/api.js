const path = require('path');
const Router = require('koa-router');
const recursive = require('recursive-readdir-synchronous');
const router = new Router();

const apiPath = path.join(__dirname, '../api');
const ignorePath = ['del.js', 'get.js', 'gets.js', 'post.js', 'put.js'];
const splitItem = process.platform === "win32" ? '\\' : '/';

const paths = recursive(apiPath, ignorePath).map((file) => {
    let temp = file.replace(apiPath, '').split(splitItem);
    return temp.splice(0, temp.length - 1).join('/');
});

paths.forEach((path) => {
    const subRouter = require(apiPath + path + '/define').router;
    router.use('/api' + path, subRouter.routes(), subRouter.allowedMethods());
});

module.exports = router.routes();
