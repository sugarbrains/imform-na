<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 12. 22.
 * Time: PM 11:55
 */
require_once(dirname(__FILE__).'/Response.php');
require_once(dirname(__FILE__).'/../db/Users.php');

class Session
{
    public static function setSession($data) {
        session_start();
        $_SESSION['id'] = $data['id'];
        $_SESSION['domainId'] = $data['domainId'];
        $_SESSION['role'] = $data['role'];
        $_SESSION['uid'] = $data['uid'];
        $_SESSION['name'] = $data['name'];
        $_SESSION['phoneNum'] = $data['phoneNum'];
        $_SESSION['url'] = $data['url'];
        $_SESSION['refuseMessage'] = $data['refuseMessage'];
        $_SESSION['sms'] = $data['sms'];
        $_SESSION['lms'] = $data['lms'];
    }

    public static function logout() {
        session_start();
        $_SESSION = array();
        session_destroy();
    }

    public static function isLoggedIn() {
        session_start();
        if (!$_SESSION['id']) {
            $response = new Response(401,null,true);
            die();
        } else {
            $Users = new Users();
            $user = $Users->get($_SESSION['id']);
            $Users->close();
            self::setSession($user);
        }
    }
}