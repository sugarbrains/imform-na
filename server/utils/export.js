const fs = require('fs');
const json2csv = require('json2csv');

module.exports = {
    appendExportFile: function (data, currentTime, exportFilePath, fields, successCallback, failCallback) {
        let replaceTarget = '';
        for (let i=0; i<fields.length; i++) {
            if (i) replaceTarget += ',';
            replaceTarget += '"' + fields[i] + '"';
        }
        json2csv({
            data: data,
            field: fields
        }, function (err, csvString) {
            if (err) {
                if (failCallback) {
                    failCallback('json2csv fail');
                }
            } else {
                if (currentTime) {
                    csvString = csvString.replace(replaceTarget, '');
                } else {
                    csvString = "\ufeff" + csvString;
                }
                fs.appendFile(exportFilePath, csvString, function (err) {
                    if (err) {
                        if (failCallback) {
                            failCallback('appendFile fail');
                        }
                    } else {
                        if (successCallback) {
                            successCallback();
                        }
                    }
                });
            }
        });
    }
};