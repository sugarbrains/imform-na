const STD = require('../metadata/standards');
const hashFields = STD.track.hashFields;

const convertToHash = (input) => {
    input = parseInt(input);
    if (input >= hashFields.length) {
        return "" + convertToHash(input/hashFields.length) + hashFields[input % hashFields.length];
    } else {
        return hashFields[input];
    }
};

const convertToId = (input) => {
    input = input.toString();
    let result = 0;
    for (let i=0; i<input.length; i++) {
        result += hashFields.indexOf(input.slice(i, i + 1)) * Math.pow(hashFields.length, input.length - (i + 1));
    }
    return result;
};

module.exports = {
    idToHash: (id) => {
        return convertToHash(id);
    },
    hashToId: (hash) => {
        return convertToId(hash);
    }
};