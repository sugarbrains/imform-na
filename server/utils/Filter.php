<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 12. 22.
 * Time: PM 4:53
 */
require_once(dirname(__FILE__).'/../config/index.php');

class Filter
{
    public static function attachZero($data) {
        if (isset($data) && is_numeric($data)) {
            if (gettype($data) === 'string' && strlen($data) < 2) {
                return '0'.$data;
            } else if (gettype($data) === 'integer') {
                $temp = strval($data);
                if (strlen($temp) < 2) {
                    return '0'.$temp;
                } else {
                    return strval($data);
                }
            } else {
                return strval($data);
            }
        } else {
            return null;
        }
    }

    public static function password($data) {
        return password_hash($data,PASSWORD_DEFAULT, [
            'cost' => 12
        ]);
    }

    public static function loginPassword($data) {
        $password = crypt($data);

        return crypt($data, $password);
    }

    public static function generateOrderNum() {
        return round(microtime(true) * 1000);
    }

    public static function minusToZero($data) {
        return $data < 0 ? 0 : $data;
    }

    public static function base64_urlencode($str) {
        return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
    }

    public static function jwt_encode($payload, $key) {
        $jwtHead = self::base64_urlencode(json_encode(array('typ' => 'JWT', 'alg' => 'HS256')));
        $jsonPayload = self::base64_urlencode(json_encode($payload));
        $signature = self::base64_urlencode(hash_hmac('SHA256', $jwtHead . '.' . $jsonPayload, $key, true));

        return $jwtHead . '.' . $jsonPayload . '.' . $signature;
    }

    public static function generateJwtToken($payload, $secret) {
        return self::jwt_encode($payload, $secret);
    }

    public static function mb_basename($path) { return end(explode('/',$path)); }
    public static function utf2euc($str) { return iconv("UTF-8","cp949//IGNORE", $str); }
    public static function is_ie() {
        if(!isset($_SERVER['HTTP_USER_AGENT']))return false;
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) return true; // IE8
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows NT 6.1') !== false) return true; // IE11
        return false;
    }
    public static function fileDownload($filePath) {
        $filesize = filesize($filePath);
        $filename = self::mb_basename($filePath);
        if(self::is_ie()) $filename = self::utf2euc($filename);

        header("Pragma: public");
        header("Expires: 0");
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: $filesize");

        readfile($filePath);
    }

    public static function isDynamic($url) {
        $list = explode('/', $url);
        $requestList = explode('/', $_SERVER['REQUEST_URI']);
        if (count($list) !== count($requestList)) return false;
        for ($i=0; $i<count($list) - 1; $i++) {
            if ($requestList[$i] && $list[$i]) {
                if (strpos($requestList[$i], $list[$i]) !== 0) return false;
            } else {
                if ($list[$i] !== $requestList[$i]) return false;
            }
        }
        return true;
    }

    public static function requestId() {
        $requestList = explode('/', $_SERVER['REQUEST_URI']);
        $id = $requestList[count($requestList) - 1];
        return explode('?', $id)[0];
    }
}