const attachZero = (data) => {
    let temp = Number(data);
    if (temp < 10 && temp >= 0) {
        return '0' + data;
    } else {
        return data;
    }
};

module.exports = {
    attachZero: attachZero,
    date: (data, pattern) => {
        let temp = new Date(data);

        const yyyy = temp.getFullYear();
        const HH = temp.getHours();
        const dateObject = {
            yyyy: yyyy,
            MM: attachZero(temp.getMonth() + 1),
            dd: attachZero(temp.getDate()),
            yy: yyyy.toString().substr(2, 4),
            HH: attachZero(HH),
            hh: attachZero(HH % 12),
            mm: attachZero(temp.getMinutes()),
            ss: attachZero(temp.getSeconds())
        };

        Object.keys(dateObject).forEach(function (key) {
            pattern = pattern.replace(key, dateObject[key]);
        });

        return pattern;
    }
};