<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 12:39
 */

require_once(dirname(__FILE__).'/../config/meta.php');

class Hash
{
    public static function hashToId($hash) {
        $hashFields = META['track']['hashFields'];
        $result = 0;
        for ($i = 0; $i<strlen($hash); $i++) {
            $result += array_search(substr($hash, $i,1), $hashFields) * pow(count($hashFields), strlen($hash) - ($i + 1));
        }
        return $result;
    }
}