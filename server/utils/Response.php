<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 8. 12.
 * Time: PM 9:56
 */

const STATUS_ERROR_SERVER = 500;
const STATUS_ERROR = 400;
const STATUS_SUCCESS_WITH_DATA = 200;
const STATUS_SUCCESS_WITHOUT_DATA = 204;
const STATUS_SUCCESS_CREATE = 201;
const STATUS_REDIRECT = 301;

class Response
{
    private $status;
    private $response;

    public function __construct($status, $response = null, $isDirectResponse = true) {
        $this->status = $status;
        if (isset($response)) {
            $this->response = $response;
        } else {
            $this->response = null;
        }
        if ($isDirectResponse) {
            $this->json();
        }
    }

    public function setResponse($response = null) {
        $this->response = $response;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getResponse($isJson = false) {
        if ($isJson) {
            return json_encode($this->response,JSON_UNESCAPED_UNICODE);
        } else {
            return $this->response;
        }
    }

    public function getStatus() {
        return $this->status;
    }

    public function json() {
        http_response_code($this->status);
        echo json_encode($this->response,JSON_UNESCAPED_UNICODE);
    }
}