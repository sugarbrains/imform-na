module.exports = {
    setNoCache: (ctx) => {
        ctx.set('Cache-Control', 'no-cache, no-store, must-revalidate');
        ctx.set('pragma',  'no-cache');
        ctx.set('expires', 0);
    }
};