const fs = require('fs');
const json2csv = require('json2csv');
const RECEIVER = require('../metadata/standards').receiver;

const optionalReceiverFields = ["gender", "birth", "address1", "address2", "address3", "phoneNum"];

const regNum = /^[0-9]+$/;
const changeExp = /[\r\n\s!@#$&%^*()\-=+\\\|\[\]{};:\'`"~,.<>\/?]/g;

const correctPhoneNum = new RegExp("^[+]{1}821[016789]{1}[0-9]{7,8}$");
const phoneNum1 = new RegExp("^1[016789]{1}[0-9]{7,8}$");
const phoneNum2 = new RegExp("^821[016789]{1}[0-9]{7,8}$");
const phoneNum3 = new RegExp("^8201[016789]{1}[0-9]{7,8}$");
const phoneNum4 = new RegExp("^01[016789]{1}[0-9]{7,8}$");

module.exports = {
    returnReceiver: function (resultRow, failCallback, authorId, update, updateCallback, appendFields) {
        let receiverFields = ["key"];
        if (appendFields.length) {
            receiverFields = receiverFields.concat(appendFields);
            if(update){
                for (var i=0; i < appendFields.length; i++) {
                    update[appendFields[i]] = true;
                }
            }
        }


        let csvUser = {};
        for (let i=0; i<receiverFields.length; i++) {
            if (resultRow[receiverFields[i]]) {
                if (failCallback && (resultRow[receiverFields[i]] + '').indexOf('"') !== -1) {
                    failCallback('잘못된 형식의 CSV 파일입니다. (\\" 포함)');
                }
                csvUser[receiverFields[i]] = resultRow[receiverFields[i]];
            } else {
                if (failCallback) {
                    failCallback('key 값이 없습니다.');
                }
            }
        }
        for (let i=0; i<optionalReceiverFields.length; i++) {
            if (resultRow[optionalReceiverFields[i]]) {
                csvUser[optionalReceiverFields[i]] = resultRow[optionalReceiverFields[i]];
                if (failCallback && (resultRow[optionalReceiverFields[i]] + '').indexOf('"') !== -1) {
                    failCallback('잘못된 형식의 CSV 파일입니다. (\\" 포함)');
                }
                if (failCallback && resultRow.birth && !regNum.test(resultRow.birth + '')) {
                    failCallback('잘못된 형식의 birth 데이터입니다.');
                }
                if (failCallback && optionalReceiverFields[i] === 'gender' && resultRow.gender) {
                    // if (!RECEIVER.possibleGenders[resultRow.gender]) {
                    //     failCallback('잘못된 형식의 gender 데이터입니다.');
                    // }
                }
                if (update) {
                    update[optionalReceiverFields[i]] = true;
                }
            }
        }
        if (updateCallback) {
            updateCallback(update);
        }
        if (authorId) {
            csvUser.authorId = authorId;
        }
        return csvUser;
    },
    returnCreateReceiverArray: function (receiverArray, existArray) {
        let createReceiverArray = receiverArray.slice();
        if (existArray !== undefined && existArray.length > 0) {
            for (let i=0; i<createReceiverArray.length; i++) {
                for (let j=0; j<existArray.length; j++) {
                    if (createReceiverArray[i].key === existArray[j].key) {
                        createReceiverArray.splice(i, 1);
                        i--;
                        existArray.splice(j, 1);
                        j--;
                        break;
                    }
                }
            }
        }
        return createReceiverArray;
    },
    writeSplitFile: function (data, currentTime, splitFilePath, successCallback, failCallback) {
        const receiverFields = ["key"];
        json2csv({
            data: data,
            field: receiverFields.concat(optionalReceiverFields)
        }, function (err, csvString) {
            if (err) {
                if (failCallback) {
                    failCallback('json2csv fail');
                }
            } else {
                fs.writeFile('.' + splitFilePath, csvString, function (err) {
                    if (err) {
                        if (failCallback) {
                            failCallback('writeFile fail');
                        }
                    } else {
                        if (successCallback) {
                            successCallback();
                        }
                    }
                });
            }
        });
    },
    returnResultRow: function (resultRow, currentYear) {
        if (resultRow.birth && parseInt(resultRow.birth) < 1000) {
            resultRow.birth = currentYear - parseInt(resultRow.birth);
        }
        if (resultRow.phoneNum) {
            resultRow.phoneNum = this.returnPhoneNum(resultRow.phoneNum);
        }
        if (resultRow.gender) {
            if (RECEIVER.maleFields[resultRow.gender]) {
                resultRow.gender = 'm';
            } else if (RECEIVER.femaleFields[resultRow.gender]) {
                resultRow.gender = 'f';
            } else {
                resultRow.gender = 'noGender';
            }
        }
        return resultRow;
    },
    returnPhoneNum: function (phoneNum) {
        let temp = phoneNum + '';
        temp = temp.replace(changeExp, '');

        if (phoneNum1.test(temp)) {
            temp = '+82' + temp;
        }

        if (phoneNum2.test(temp)) {
            temp = temp.replace('82', '+82');
        }

        if (phoneNum3.test(temp)) {
            temp = temp.replace('820', '+82');
        }

        if (phoneNum4.test(temp)) {
            temp = temp.replace('01', '+821');
        }

        if (correctPhoneNum.test(temp)) {
            return temp;
        } else {
            return "wrongPhoneNum";
        }
    }
};