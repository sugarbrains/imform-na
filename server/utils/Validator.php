<?php

/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2018. 9. 2.
 * Time: PM 11:52
 */

require_once(dirname(__FILE__) . '/Response.php');

const RESPONSE_KEY_MESSAGE = "message";

const ERROR_MESSAGE_ACCEPTABLE = "IS NOT ACCEPTABLE";
const ERROR_MESSAGE_ESSENTIAL = "IS EMPTY";
const ERROR_MESSAGE_IS_NOT_EMAIL = "IS NOT EMAIL";
const ERROR_MESSAGE_IS_NOT_INTEGER = "IS NOT INTEGER";
const ERROR_MESSAGE_IS_NOT_FLOAT = "IS NOT FLOAT";
const ERROR_MESSAGE_IS_NOT_BOOLEAN = "IS NOT FLOAT";
const ERROR_MESSAGE_IS_NOT_STRING = "IS NOT STRING";
const ERROR_MESSAGE_STRING_LENGTH = "IS WRONG LENGTH";
const ERROR_MESSAGE_WRONG_PATTERN = "IS WRONG PATTERN";
const ERROR_MESSAGE_WRONG_ENUM = "IS NOT ACCEPTABLE";

class Validator
{
    private $method;
    private $params;
    private $acceptable;
    private $essential;
    private $isError;
    private $activePath;

    public function __construct($acceptable = Array(), $essential = Array(), $activePath = null) {
        $this->method = $_SERVER['REQUEST_METHOD'];

//        $putdata = fopen("php://input", "r");
//        while ($data = fread($putdata, 1024)) {
//            echo json_encode($data);
//        }

        $putData = [];
        if ($this->method === "PUT") {
            parse_str(file_get_contents("php://input"),$putData);
            $putData['id'] = $_POST['id'];
        }

        $content = json_decode(file_get_contents("php://input"), true);

        if (count($_GET)) {
            $this->params = $_GET;
        } else if (count($content)) {
            if (count($_POST)) {
                $temp = [];
                foreach($_POST AS $key => $value) {
                    $temp[$key] = $value;
                }
                foreach($content AS $key => $value) {
                    $temp[$key] = $value;
                }
                $_POST = $temp;
                $this->params = $temp;
            } else {
                $_POST = $content;
                $this->params = $content;
            }
        } else if (count($putData)) {
            $_POST = $putData;
            $this->params = $putData;
        } else if (count($_POST)) {
            $this->params = $_POST;
        } else {
            $this->params = Array();
        }

        $this->acceptable = $acceptable;
        $this->essential = $essential;
        $this->activePath = $activePath;

        if ($this->checkAcceptable()) {
            if ($this->checkEssential()) {
                $this->isError = false;
            } else {
                $this->isError = true;
            }
        } else {
            $this->isError = true;
        }
    }

    public function checkAcceptable() {
        foreach ($this->params as $key => $value) {
            if (array_search($key, $this->acceptable) === false) {
                new Response(400, $this->returnMessage($key, ERROR_MESSAGE_ACCEPTABLE));
                return false;
            }
        }
        return true;
    }

    public function checkEssential() {
        foreach ($this->essential AS $key) {
            if (array_search($key, array_keys($this->params)) === false) {
                new Response(400, $this->returnMessage($key, ERROR_MESSAGE_ESSENTIAL));
                return false;
            }
        }
        return true;
    }

    public function isExist($key) {
        return !$this->isError && isset($this->params[$key]);
    }

    public function isEmail($key, $message = null) {
        if ($this->isExist($key) && !filter_var($this->params[$key], FILTER_VALIDATE_EMAIL)) {
            new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_IS_NOT_EMAIL));
            $this->isError = true;
        }
    }

    public function isInt($key, $message = null) {
        if ($this->isExist($key) && filter_var($this->params[$key], FILTER_VALIDATE_INT) === false && filter_var($this->params[$key], FILTER_VALIDATE_INT) !== 0) {
            new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_IS_NOT_INTEGER));
            $this->isError = true;
        }
    }

    public function isFloat($key, $message = null) {
        if ($this->isExist($key) && filter_var($this->params[$key], FILTER_VALIDATE_FLOAT) === false && filter_var($this->params[$key], FILTER_VALIDATE_FLOAT) !== 0) {
            new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_IS_NOT_FLOAT));
            $this->isError = true;
        }
    }

    public function isBoolean($key, $message = null) {
        if ($this->isExist($key) && is_bool($this->params[$key]) === false && $this->params[$key] !== 'true' && $this->params[$key] !== 'false') {
            new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_IS_NOT_BOOLEAN));
            $this->isError = true;
        }
    }

    public function len($key, $min, $max, $message = null) {
        if ($this->isExist($key)) {
            if (is_string($this->params[$key]) === false) {
                new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_IS_NOT_STRING));
                $this->isError = true;
            } else {
                $length = strlen($this->params[$key]);
                if ($length < $min || $length > $max) {
                    new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_STRING_LENGTH));
                    $this->isError = true;
                }
            }
        }
    }

    public function isExp($key, $pattern, $message = null) {
        if ($this->isExist($key) && preg_match($pattern, $this->params[$key]) === false) {
            new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_WRONG_PATTERN));
            $this->isError = true;
        }
    }

    public function isEnum($key, $enum, $message = null) {
        if ($this->isExist($key) && array_search($this->params[$key], $enum) === false) {
            new Response(400, $this->returnMessage($key, $message ? $message : ERROR_MESSAGE_WRONG_ENUM));
            $this->isError = true;
        }
    }

    public function getParam($key) {
        return $this->params[$key];
    }

    public function returnMessage($key, $message) {
        return Array(RESPONSE_KEY_MESSAGE => $key . ' ' . $message);
    }

    public function active() {
        if (!$this->isError && $this->activePath) {
            require_once ($this->activePath);
        }
    }
}