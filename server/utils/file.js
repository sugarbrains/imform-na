const fs = require('fs');
const path = require('path');
const rootPath = path.join(__dirname, '../../');

module.exports = {
    removeUploadFiles: async (ctx) => {
        const files = Object.keys(ctx.request.files);
        if (ctx.request.files && Object.keys(ctx.request.files)) {
            let funcs = [];
            files.forEach((key) => {
                const file = ctx.request.files[key];
                funcs.push(new Promise((resolve, reject) => {
                    fs.unlink(path.join(rootPath, file.path), (err) => {
                        if (err) console.error('remove file fail', err);
                        resolve();
                    });
                }));
            });
            await Promise.all(funcs);
        }
    },
    removeFile: (path) => {
        return new Promise((resolve, reject) => {
            fs.unlink(path, (err) => {
                if (err) console.error('remove file fail', err);
                resolve();
            });
        });
    },
    moveFile: (oldPath, newPath) => {
        return new Promise((resolve, reject) => {
            fs.rename(oldPath, newPath, (err) => {
                if (err) console.error('move file fail', err);
                resolve();
            });
        });
    },
    getFileName: (path) => {
        return path.replace('uploads/', '');
    }
};