const request = require('request');
const CONFIG = require('../config');
const API_STORE = CONFIG.apiStore;
const DIRECT_SEND = CONFIG.directSend;

// const API_URL = 'https://api.apistore.co.kr/ppurio/';
// const VERSION_1 = '1';
// const VERSION_2 = '2';

const phoneNumExp = new RegExp("^[+]{1}821[016789]{1}[0-9]{7,8}$");
const errorMessages = {
    '100': 'POST validation 실패',
    '101': 'sender 유효한 번호가 아님',
    '102': 'recipient 유효한 번호가 아님',
    '103': '회원정보가 일치하지 않음',
    '104': '받는 사람이 없습니다',
    '105': 'message length = 0, message length >= 2000, title >= 20',
    '106': 'message validation 실패',
    '107': '이미지 업로드 실패',
    '108': '이미지 갯수 초과',
    '109': 'return_url이 유효하지 않습니다',
    '110': '이미지 용량 300kb 초과',
    '111': '이미지 확장자 오류',
    '112': 'euckr 인코딩 에러 발생',
    '205': '잔액부족',
    '999': 'Internal Error.',
};

module.exports = {
    bytes,
    sendSms: ({id, phoneNum, message}) => {
        return new Promise((resolve, reject) => {
            if (bytes(message) > 80) {
                reject('문자 내용이 80bytes 를 넘었습니다.');
            } else if (!phoneNumExp.test(phoneNum)) {
                reject('잘못된 핸드폰번호입니다.');
            } else {
                request.post({
                    url: DIRECT_SEND.url,
                    form: {
                        message: Buffer.from(message).toString('base64'),
                        sender: DIRECT_SEND.sendPhoneNum,
                        username: DIRECT_SEND.username,
                        recipients: phoneNum.replace('+82', '0'),
                        key: encodeURIComponent(DIRECT_SEND.key)
                    }
                }, (err, res, body) => {
                    if (err) {
                        reject(err);
                    } else {
                        try {
                            let data = JSON.parse(body);
                            if (data.status === '0') {
                                resolve(true);
                            } else {
                                reject(errorMessages[data.status] || data.status);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            }
        });
    },
    sendLms: ({id, phoneNum, title, message}) => {
        return new Promise((resolve, reject) => {
            if (bytes(message) > 2000) {
                reject('문자 내용이 2000bytes 를 넘었습니다.');
            } else if (bytes(title) > 40) {
                reject('제목이 40bytes 를 넘었습니다.');
            } else if (!phoneNumExp.test(phoneNum)) {
                reject('잘못된 핸드폰번호입니다.');
            } else {
                request.post({
                    url: DIRECT_SEND.url,
                    form: {
                        title: Buffer.from(title).toString('base64'),
                        message: Buffer.from(message).toString('base64'),
                        sender: DIRECT_SEND.sendPhoneNum,
                        username: DIRECT_SEND.username,
                        recipients: phoneNum.replace('+82', '0'),
                        key: encodeURIComponent(DIRECT_SEND.key)
                    }
                }, (err, res, body) => {
                    if (err) {
                        reject(err);
                    } else {
                        try {
                            let data = JSON.parse(body);
                            if (data.status === '0') {
                                resolve(true);
                            } else {
                                reject(errorMessages[data.status] || data.status);
                            }
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
            }
        });
    },
    // sendSms: ({phoneNum, message}) => {
    //     return new Promise((resolve, reject) => {
    //         if (bytes(message) >= 90) {
    //             reject('문자 내용이 90bytes 를 넘었습니다.');
    //         } else if (!phoneNumExp.test(phoneNum)) {
    //             reject('잘못된 핸드폰번호입니다.');
    //         } else {
    //             request.post({
    //                 url: `${API_URL}/${VERSION_1}/message/sms/${API_STORE.clientId}`,
    //                 headers: {
    //                     'x-waple-authorization': API_STORE.key
    //                 },
    //                 form: {
    //                     send_phone: API_STORE.sendPhoneNum,
    //                     dest_phone: phoneNum.replace('+82', '0'),
    //                     msg_body: message
    //                 }
    //             }, (err, res, body) => {
    //                 if (err) { // todo response parsing
    //                     reject(err);
    //                 } else {
    //                     resolve();
    //                 }
    //             });
    //         }
    //     });
    // },
    // sendLms: ({phoneNum, title, message}) => {
    //     return new Promise((resolve, reject) => {
    //         if (bytes(message) >= 2000) {
    //             reject('문자 내용이 2000bytes 를 넘었습니다.');
    //         } else if (bytes(title) >= 60) {
    //             reject('제목이 60bytes 를 넘었습니다.');
    //         } else if (!phoneNumExp.test(phoneNum)) {
    //             reject('잘못된 핸드폰번호입니다.')
    //         } else {
    //             request.post({
    //                 url: `${API_URL}/${VERSION_1}/message/lms/${API_STORE.clientId}`,
    //                 headers: {
    //                     'x-waple-authorization': API_STORE.key
    //                 },
    //                 form: {
    //                     send_phone: API_STORE.sendPhoneNum,
    //                     dest_phone: phoneNum.replace('+82', '0'),
    //                     subject: title,
    //                     msg_body: message
    //                 }
    //             }, (err, res, body) => {
    //                 if (err) { // todo response parsing
    //                     reject(err);
    //                 } else {
    //                     resolve();
    //                 }
    //             });
    //         }
    //     });
    // }
};

function bytes(text){
    let codeByte = 0;
    for (let i=0; i<text.length; i++) {
        const oneChar = escape(text.charAt(i));
        if ( oneChar.length === 1 ) {
            codeByte ++;
        } else if (oneChar.indexOf("%u") !== -1) {
            codeByte += 2;
        } else if (oneChar.indexOf("%") !== -1) {
            codeByte ++;
        }
    }
    return codeByte;
}