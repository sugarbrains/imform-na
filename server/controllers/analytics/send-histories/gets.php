<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: PM 3:57
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$count = $Messages->countSendHistories($_GET, $_SESSION);
$messages = $Messages->getsSendHistories($_GET, $_SESSION);
$Messages->close();

if ($count) {
    $response = new Response(200, [
        'count' => $count,
        'rows' => $messages
    ],true);
} else {
    $response = new Response(404,null,true);
}
