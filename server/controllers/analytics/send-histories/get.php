<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: PM 3:57
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

$Messages = new Messages();
$message = $Messages->getSendHistory($_GET['id']);
$Messages->close();

if ($message) {
    $response = new Response(200, $message,true);
} else {
    $response = new Response(404,null,true);
}