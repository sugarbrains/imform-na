<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 12.
 * Time: PM 9:52
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');
require_once(dirname(__FILE__).'/../../../db/ReceiverGroups.php');

Session::isLoggedIn();

$Messages = new Messages();
$check = $Messages->checkOwnMessages($_POST['sendHistoryIds'], $_SESSION['id']);
$Messages->close();

if ($check) {
    $ReceiverGroups = new ReceiverGroups();
    $result = $ReceiverGroups->sendHistoryReceiverGroup($_POST, $_SESSION['id']);
    $ReceiverGroups->close();

    if ($result) {
        $response = new Response(204,null,true);
    } else {
        $response = new Response(400, [
            'message' => '수신자그룹 생성 실패'
        ],true);
    }
} else {
    $response = new Response(400, [
        'message' => '타 회원의 수신자가 포함되어 있습니다.'
    ],true);
}