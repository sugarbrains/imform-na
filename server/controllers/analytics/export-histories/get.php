<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 6:19
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/ExportHistories.php');

Session::isLoggedIn();

$ExportHistories = new ExportHistories();
$exportHistory = $ExportHistories->get($_GET['id']);
$ExportHistories->close();

if ($exportHistory) {
    $response = new Response(200, $exportHistory,true);
} else {
    $response = new Response(404,null,true);
}