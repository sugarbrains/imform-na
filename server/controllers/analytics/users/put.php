<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 20.
 * Time: AM 5:33
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Users.php');

Session::isLoggedIn();

if ($_SESSION['role'] === 'USER') {
    if ($_POST['role'] || $_POST['id'] !== $_SESSION['id'] || isset($_POST['sms']) || isset($_POST['lms'])) {
        $response = new Response(403, [
            'message' => '권한이 없습니다.'
        ],true);
    } else {
        $Users = new Users();
        $result = $Users->update($_POST['id'], $_POST);
        $user = $Users->get($_POST['id']);
        $Users->close();
        if ($result) {
            Session::setSession($user);
            $response = new Response(204,null,true);
        } else {
            $response = new Response(400,[
                'message' => '수정 실패.'
            ],true);
        }
    }
} else if ($_SESSION['role'] === 'MANAGER') {
    if ($_POST['role'] === 'ADMIN' || $_POST['domainId'] || isset($_POST['sms']) || isset($_POST['lms'])) {
        $response = new Response(403, [
            'message' => '권한이 없습니다.'
        ],true);
    } else {
        $Users = new Users();
        $user = $Users->getByManager($_POST['id'], $_SESSION['domainId']);
        if ($user) {
            $result = $Users->update($_POST['id'], $_POST);
            if ($result) {
                if ($_POST['id'] === $_SESSION['id']) {
                    $user = $Users->get($_POST['id']);
                    Session::setSession($user);
                }
                $response = new Response(204,null,true);
            } else {
                $response = new Response(400,[
                    'message' => '수정 실패.'
                ],true);
            }
        } else {
            $response = new Response(404,null,true);
        }
        $Users->close();
    }
} else if ($_SESSION['role'] === 'ADMIN') {
    $Users = new Users();
    $result = $Users->update($_POST['id'], $_POST);
    if ($result) {
        if ($_POST['id'] === $_SESSION['id']) {
            $user = $Users->get($_POST['id']);
            Session::setSession($user);
        }
        $response = new Response(204,null,true);
    } else {
        $response = new Response(400,[
            'message' => '수정 실패.'
        ],true);
    }
    $Users->close();
} else {
    $response = new Response(403, [
        'message' => '권한이 없습니다.'
    ],true);
}