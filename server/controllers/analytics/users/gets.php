<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 20.
 * Time: AM 5:33
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Users.php');

Session::isLoggedIn();

if ($_SESSION['role'] === 'MANAGER') {
    $Users = new Users();
    $count = $Users->countByManager($_GET, $_SESSION['domainId']);
    $users = $Users->getsByManager($_GET, $_SESSION['domainId']);
    $Users->close();

    if ($count) {
        $response = new Response(200, [
            'count' => $count,
            'rows' => $users
        ],true);
    } else {
        $response = new Response(404,null,true);
    }
} else if ($_SESSION['role'] === 'ADMIN') {
    $Users = new Users();
    $count = $Users->count($_GET);
    $users = $Users->gets($_GET);
    $Users->close();

    if ($count) {
        $response = new Response(200, [
            'count' => $count,
            'rows' => $users
        ],true);
    } else {
        $response = new Response(404,null,true);
    }
} else {
    $response = new Response(403, [
        'message' => '권한이 없습니다.'
    ],true);
}