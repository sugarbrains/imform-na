<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 20.
 * Time: AM 5:33
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Users.php');

Session::isLoggedIn();

if ($_SESSION['role'] === 'MANAGER') {
    $Users = new Users();
    $user = $Users->getByManager($_GET['id'], $_SESSION['domainId']);
    $Users->close();
    if ($user) {
        $response = new Response(200, $user,true);
    } else {
        $response = new Response(404,null,true);
    }
} else if ($_SESSION['role'] === 'ADMIN') {
    $Users = new Users();
    $user = $Users->get($_GET['id']);
    $Users->close();
    if ($user) {
        $response = new Response(200, $user,true);
    } else {
        $response = new Response(404,null,true);
    }
} else if ($_SESSION['role'] === 'USER' && $_SESSION['id'] === $_GET['id']) {
    $Users = new Users();
    $user = $Users->get($_GET['id']);
    $Users->close();
    if ($user) {
        $response = new Response(200, $user,true);
    } else {
        $response = new Response(404,null,true);
    }
} else {
    $response = new Response(403, [
        'message' => '권한이 없습니다.'
    ],true);
}