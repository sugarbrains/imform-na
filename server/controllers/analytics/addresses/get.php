<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 7.
 * Time: AM 1:34
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/Receivers.php');

$Receivers = new Receivers();
$result = $Receivers->getAddresses($_GET);
$Receivers->close();

if (is_array($result)) {
    $response = new Response(200, $result,true);
} else {
    $response = new Response(400, [
        'message' => '조회 실패'
    ],true);
}