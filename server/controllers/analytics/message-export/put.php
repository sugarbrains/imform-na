<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 5:29
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$message = $Messages->get($_POST['id']);

if ($message['receiverCount'] <= 0) {
    $response = new Response(400, [
        'message' => '수신자가 없습니다.'
    ],true);
} else if ($message['state'] === 'standby' && $message['authorId'] = $_SESSION['id']) {
    $exportHistory = $Messages->export($_POST['id'], $_SESSION['id']);
    if ($exportHistory) {
        $response = new Response(200, $exportHistory,true);
    } else {
        $response = new Response(400, [
            'message' => '추출 실패.'
        ],true);
    }
} else {
    $response = new Response(400, [
        'message' => '추출할 수 없는 상태의 메세지입니다.'
    ],true);
}

$Messages->close();