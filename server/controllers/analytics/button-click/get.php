<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 1:52
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Hash.php');
require_once(dirname(__FILE__).'/../../../db/Buttons.php');
require_once(dirname(__FILE__).'/../../../db/OpenButtons.php');

$messageId = Hash::hashToId(substr($_GET['messageHash'],1,strlen($_GET['messageHash'])));
$receiverId = Hash::hashToId($_GET['userHash']);

$Buttons = new Buttons();
$buttons = $Buttons->getsByMessageId($messageId);
$Buttons->close();

$buttonCount = count($buttons);

if (count($buttons) >= $_GET['buttonId'] && $_GET['buttonId'] > 0) {
    $buttonId = $buttons[$_GET['buttonId'] - 1]['id'];

    $OpenButtons = new OpenButtons();
    $result = $OpenButtons->create($buttonId, $receiverId);
    $OpenButtons->close();

    if ($result) {
        echo $_GET['callback'] . '("success")';
    } else {
        echo $_GET['callback'] . "(\"create open button fail, {$_GET['buttonId']}, {$messageId}, {$receiverId}, {$buttonCount}\")";
    }
} else {
    echo $_GET['callback'] . "(\"get buttons fail, {$_GET['buttonId']}, {$messageId}, {$receiverId}, {$buttonCount}\")";
}
