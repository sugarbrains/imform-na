<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 28.
 * Time: AM 2:52
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$count = $Messages->countSendCompleteHistories($_GET, $_SESSION['id']);
$sendCompleteHistories = $Messages->getsSendCompleteHistories($_GET, $_SESSION['id']);
$Messages->close();

if ($count) {
    $response = new Response(200, [
        'count' => $count,
        'rows' => $sendCompleteHistories
    ],true);
} else {
    $response = new Response(404,null,true);
}