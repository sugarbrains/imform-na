<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 18.
 * Time: AM 2:33
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/ReceiverGroupImportHistories.php');

Session::isLoggedIn();

$ReceiverGroupImportHistory = new ReceiverGroupImportHistories();
$receiverGroupImportHistory = $ReceiverGroupImportHistory->get($_GET['id'], $_SESSION['id']);
$ReceiverGroupImportHistory->close();

if ($receiverGroupImportHistory) {
    $response = new Response(200, $receiverGroupImportHistory,true);
} else {
    $response = new Response(404,null,true);
}