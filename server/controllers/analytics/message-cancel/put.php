<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 5:55
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$message = $Messages->get($_POST['id']);

if ($message['state'] === 'standby') {
    $response = new Response(400, [
        'message' => '취소할 수 없는 상태의 메세지입니다.'
    ],true);
} else if ($message['state'] === 'export') {
    $response = new Response(400, [
        'message' => '취소할 수 없는 상태의 메세지입니다.'
    ],true);
} else if ($message['state'] === 'send' && strtotime($message['activeDate']) < strtotime(date('c'))) {
    $response = new Response(400, [
        'message' => '이미 발송된 발송메세지를 취소할 수 없습니다.'
    ],true);
} else {
    $result = $Messages->cancel($_POST['id']);
    if ($result) {
        $response = new Response(204,null,true);
    } else {
        $response = new Response(400, [
            'message' => '취소 실패.'
        ],true);
    }
}

$Messages->close();