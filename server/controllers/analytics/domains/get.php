<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 23.
 * Time: PM 2:59
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Domains.php');

Session::isLoggedIn();

if ($_SESSION['role'] === 'ADMIN') {
    $Domains = new Domains();
    $domain = $Domains->get($_GET['id']);
    $Domains->close();
    if ($domain) {
        $response = new Response(200, $domain,true);
    } else {
        $response = new Response(404,null,true);
    }
} else {
    $response = new Response(403, [
        'message' => '권한이 없습니다.'
    ],true);
}