<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: PM 9:27
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/OpenMessages.php');

$OpenMessages = new OpenMessages();
$result = $OpenMessages->getChartGender($_GET);
$OpenMessages->close();

if ($result) {
    $response = new Response(200, $result,true);
} else {
    $response = new Response(400, [
        'message' => '조회 실패'
    ],true);
}