<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 12.
 * Time: PM 10:43
 */
require_once(dirname(__FILE__).'/../../../config/index.php');
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/OpenMessages.php');

Session::isLoggedIn();

use PhpOffice\PhpSpreadsheet\Spreadsheet AS Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx AS Xlsx;

require_once(dirname(__FILE__).'/../../../../vendor/autoload.php');

if ($_GET['sendHistoryIds']) {
    if ($_GET['openCount'] && $_GET['openCount'] < 0) {
        $response = new Response(400, [
            'message' => '잘못된 요청입니다.'
        ],true);
        die();
    } else if ($_GET['openCount'] == 0) {
        $_GET['openCount'] = 1;
    }
    $conn = new mysqli($GLOBALS['DB_SERVER_NAME'], $GLOBALS['DB_USER_NAME'], $GLOBALS['DB_PASSWORD'], $GLOBALS['DB_DATABASE']);

    if ($conn->connect_error) {
        die("connection failed: " . $conn->connect_error);
    } else {
        $conn->query('set names utf8');
        $conn->query("SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
    }

    $keys = [
        'key' => 'key',
        'openCount' => '열람수'
    ];

    $fileName = '열람자목록_'.date('Y-m-d');
    if (isIe())  $fileName = utf2euc($fileName);

    $spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();

    $index = 0;
    foreach($keys as $key => $value) {
        $cell = getNameFromNumber($index);
        $cellName = $cell . '1';
        $sheet->getColumnDimension($cell)->setWidth(20);
        $sheet->setCellValue($cellName, $value);
        $index++;
    }

    $query = OpenMessages::generateOpenerQuery($_GET);
    $result = $conn->query($query);

    if ($result->num_rows > 0) {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fileName.'.xlsx"');
        $valueIndex = 2;
        while($row = $result->fetch_assoc()) {
            $sheet->setCellValue(getNameFromNumber(0).$valueIndex, $row['key']);
            $sheet->setCellValue(getNameFromNumber(1).$valueIndex, $row['openCount']);
            $valueIndex++;
        }
        $writer = new Xlsx($spreadsheet);
        return $writer->save('php://output');
    } else {
        $response = new Response(404, null,true);
    }
} else {
    $response = new Response(400, [
        'message' => '잘못된 요청입니다.'
    ],true);
}

function returnType($value) {
    if (is_string($value)) {
        return '"'.$value.'"';
    } else {
        return $value;
    }
}

function getNameFromNumber($num) {
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2 - 1) . $letter;
    } else {
        return $letter;
    }
}

function utf2euc($str) {
    return iconv("UTF-8","cp949//IGNORE", $str);
}

function isIe() {
    if (!isset($_SERVER['HTTP_USER_AGENT'])) return false;
    if (strpos($_SERVER['HTTP_USER_AGENT'],'MSIE') !== false) return true; // IE8
    if (strpos($_SERVER['HTTP_USER_AGENT'],'Windows NT 6.1') !== false) return true; // IE11
    return false;
}