<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:08
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/ReceiverGroups.php');

Session::isLoggedIn();

$path = dirname(__FILE__).'/../../../../uploads/import/';

if ($_FILES['file']) {
    $format = explode('.', $_FILES['file']['name'])[1];
    if ($format === 'csv' || $format === 'CSV') {
        $fileName = uniqid().'.'.$format;
        $filePath = '/uploads/import/'.$fileName;
        if (move_uploaded_file($_FILES['file']['tmp_name'],$path.$fileName)) {
            $ReceiverGroups = new ReceiverGroups();
            $receiverGroupImportHistory = $ReceiverGroups->create($_POST['name'], $filePath, $_SESSION['id']);
            $ReceiverGroups->close();
            if ($receiverGroupImportHistory) {
                new Response(201, $receiverGroupImportHistory,true);
            } else {
                new Response(400,[
                    'message' => '등록 실패.'
                ],true);
            }
        } else {
            new Response(400,[
                'message' => '파일 업로드 실패.'
            ],true);
        }
    } else {
        new Response(400,[
            'message' => 'csv 파일만 업로드 가능합니다.'
        ],true);
    }
} else {
    new Response(400,[
        'message' => '업로드된 파일이 없습니다.'
    ],true);
}