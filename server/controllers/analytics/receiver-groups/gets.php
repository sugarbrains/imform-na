<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:08
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/ReceiverGroups.php');

Session::isLoggedIn();

$ReceiverGroups = new ReceiverGroups();
$count = $ReceiverGroups->count($_GET, $_SESSION['id']);
$receiverGroups = $ReceiverGroups->gets($_GET, $_SESSION['id']);
$ReceiverGroups->close();

if ($count) {
    $response = new Response(200, [
        'count' => $count,
        'rows' => $receiverGroups
    ],true);
} else {
    $response = new Response(404,null,true);
}