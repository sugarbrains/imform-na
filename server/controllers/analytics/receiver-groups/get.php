<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:08
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/ReceiverGroups.php');

Session::isLoggedIn();

$ReceiverGroups = new ReceiverGroups();
$receiverGroup = $ReceiverGroups->get($_GET['id'], $_SESSION['id']);
$ReceiverGroups->close();

if ($receiverGroup) {
    $response = new Response(200, $receiverGroup,true);
} else {
    $response = new Response(404,null,true);
}