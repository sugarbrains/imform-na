<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:08
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/ReceiverGroups.php');

Session::isLoggedIn();

$ReceiverGroups = new ReceiverGroups();
$result = $ReceiverGroups->delete($_POST['id'], $_SESSION['id']);
$ReceiverGroups->close();

if ($result) {
    $response = new Response(204,null,true);
} else {
    $response = new Response(400, [
        'message' => '삭제 실패'
    ],true);
}