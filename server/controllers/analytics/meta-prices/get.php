<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 11:09
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/MetaPrices.php');

$MetaPrices = new MetaPrices();
$metaPrice = $MetaPrices->get();
$MetaPrices->close();

if ($metaPrice) {
    $response = new Response(200, $metaPrice,true);
} else {
    $response = new Response(404,null,true);
}