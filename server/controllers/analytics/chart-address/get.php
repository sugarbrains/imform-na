<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 7.
 * Time: AM 1:10
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/OpenMessages.php');

$OpenMessages = new OpenMessages();
$result = $OpenMessages->getChartAddress($_GET);
$OpenMessages->close();

if (is_array($result)) {
    $response = new Response(200, $result,true);
} else {
    $response = new Response(400, [
        'message' => '조회 실패'
    ],true);
}