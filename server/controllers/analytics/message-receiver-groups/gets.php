<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 20.
 * Time: AM 2:03
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/MessageReceiverGroups.php');

Session::isLoggedIn();

$MessageReceiverGroups = new MessageReceiverGroups();
$count = $MessageReceiverGroups->count($_GET, $_SESSION['id']);
$messageReceiverGroups = $MessageReceiverGroups->gets($_GET, $_SESSION['id']);
$MessageReceiverGroups->close();

if ($count) {
    $response = new Response(200, [
        'count' => $count,
        'rows' => $messageReceiverGroups
    ],true);
} else {
    $response = new Response(404,null,true);
}