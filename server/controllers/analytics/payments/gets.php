<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 28.
 * Time: AM 4:46
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Payments.php');

Session::isLoggedIn();

$Payments = new Payments();
$count = $Payments->count($_GET['id'], $_SESSION['id']);
$payments = $Payments->gets($_GET['id'], $_SESSION['id']);
$Payments->close();

if ($count) {
    $response = new Response(200, [
        'count' => $count,
        'rows' => $payments
    ],true);
} else {
    $response = new Response(404,null,true);
}