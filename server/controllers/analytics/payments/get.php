<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 28.
 * Time: AM 4:46
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Payments.php');

Session::isLoggedIn();

$Payments = new Payments();
$payment = $Payments->get($_GET['id'], $_SESSION['id']);
$Payments->close();

if ($payment) {
    $response = new Response(200, $payment,true);
} else {
    $response = new Response(404,null,true);
}