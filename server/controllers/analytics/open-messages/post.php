<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: AM 1:37
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Hash.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');
require_once(dirname(__FILE__).'/../../../db/OpenMessages.php');

$messageId = $_POST['messageId'];

if ($_POST['userHash']) {
    $receiverId = Hash::hashToId($_POST['userHash']);
} else {
    $receiverId = 'NULL';
}

$OpenMessages = new OpenMessages();
$result = $OpenMessages->create($messageId, $receiverId);
$OpenMessages->close();

$Messages = new Messages();
$message = $Messages->get($messageId);
$Messages->close();

if ($message) {
    $response = new Response(200, [
        'url' => $message['redirectUrl']
    ],true);
} else {
    $response = new Response(404,null,true);
}