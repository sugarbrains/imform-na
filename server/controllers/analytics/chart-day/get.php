<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 12.
 * Time: AM 5:46
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/OpenMessages.php');

$OpenMessages = new OpenMessages();
$sentTime = $OpenMessages->getSentTime($_GET);
if ($sentTime) {
    $result = $OpenMessages->getChartDay($_GET, $sentTime);
    $OpenMessages->close();

    if (is_array($result)) {
        $response = new Response(200, $result,true);
    } else {
        $response = new Response(400, [
            'message' => '조회 실패'
        ],true);
    }
} else {
    $OpenMessages->close();
    $response = new Response(200, [],true);
}
