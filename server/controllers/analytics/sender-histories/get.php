<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 3. 5.
 * Time: AM 4:22
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Users.php');

Session::isLoggedIn();

if ($_SESSION['role'] === 'ADMIN') {
    $Users = new Users();
    $senderHistory = $Users->getSenderHistory($_GET['id']);
    $Users->close();

    if ($senderHistory) {
        $response = new Response(200, $senderHistory,true);
    } else {
        $response = new Response(404,null,true);
    }
} else if ($_SESSION['role'] === 'MANAGER') {
    $Users = new Users();
    $senderHistory = $Users->getSenderHistory($_GET['id'], $_SESSION['domainId']);
    $Users->close();

    if ($senderHistory) {
        $response = new Response(200, $senderHistory,true);
    } else {
        $response = new Response(404,null,true);
    }
} else {
    $response = new Response(403, [
        'message' => '권한이 없습니다.'
    ],true);
}