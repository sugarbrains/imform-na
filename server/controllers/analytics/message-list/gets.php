<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 12.
 * Time: PM 9:34
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$messages = $Messages->getsMessageList($_GET);
$Messages->close();

if (count($messages)) {
    $response = new Response(200, $messages,true);
} else {
    $response = new Response(404,null,true);
}