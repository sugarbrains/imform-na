<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 19.
 * Time: AM 7:32
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Receivers.php');

Session::isLoggedIn();

$Receivers = new Receivers();
$receiver = $Receivers->get($_GET['id'], $_SESSION['id']);
$Receivers->close();

if ($receiver) {
    $response = new Response(200, $receiver,true);
} else {
    $response = new Response(404,null,true);
}