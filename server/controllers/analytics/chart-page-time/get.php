<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 7.
 * Time: AM 2:23
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/OpenButtonTimes.php');

$OpenButtonTimes = new OpenButtonTimes();
$result = $OpenButtonTimes->getPageTimes($_GET);
$OpenButtonTimes->close();

if (is_array($result)) {
    $response = new Response(200, $result,true);
} else {
    $response = new Response(400, [
        'message' => '조회 실패'
    ],true);
}