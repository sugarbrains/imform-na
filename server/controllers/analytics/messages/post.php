<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:47
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

if (strpos($_POST['message'],'[:URL:]') !== FALSE) {
    $Messages = new Messages();
    $message = $Messages->create($_POST, $_SESSION['id']);
    $Messages->close();

    if ($message) {
        $response = new Response(201, $message,true);
    } else {
        $response = new Response(400, [
            'message' => '등록 실패.'
        ],true);
    }
} else {
    $response = new Response(400, [
        'message' => 'URL link 삽입 필요.'
    ],true);
}