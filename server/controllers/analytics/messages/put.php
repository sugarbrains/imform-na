<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:47
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$message = $Messages->get($_POST['id']);

if ($message['state'] === 'cancel') {
    $response = new Response(400, [
        'message' => '취소상태의 메세지를 수정할 수 없습니다.'
    ],true);
} else if ($message['state'] === 'export') {
    $response = new Response(400, [
        'message' => '추출상태의 메세지를 수정할 수 없습니다.'
    ],true);
} else if ($message['state'] === 'send') {
    $response = new Response(400, [
        'message' => '발송상태의 메세지를 수정할 수 없습니다.'
    ],true);
} else {
    $result = $Messages->update($_POST['id'], $_SESSION['id'], $_POST);

    if ($result) {
        $response = new Response(204,null,true);
    } else {
        $response = new Response(400, [
            'message' => '수정 실패.'
        ],true);
    }
}

$Messages->close();