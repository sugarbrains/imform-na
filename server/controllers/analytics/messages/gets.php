<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:47
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$count = $Messages->count($_GET, $_SESSION['id']);
$messages = $Messages->gets($_GET, $_SESSION['id']);
$Messages->close();

if ($count) {
    $response = new Response(200, [
        'count' => $count,
        'rows' => $messages
    ],true);
} else {
    $response = new Response(404,null,true);
}