<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 14.
 * Time: PM 10:47
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../utils/Session.php');
require_once(dirname(__FILE__).'/../../../db/Messages.php');

Session::isLoggedIn();

$Messages = new Messages();
$result = $Messages->delete($_POST['id'], $_SESSION['id']);
$Messages->close();

if ($result) {
    $response = new Response(204,null,true);
} else {
    $response = new Response(400, [
        'message' => '삭제 실패'
    ],true);
}