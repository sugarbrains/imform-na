<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: PM 12:29
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/Domains.php');
require_once(dirname(__FILE__).'/../../../db/Users.php');

$Domains = new Domains();
$domainId = $Domains->getIdByUrl($_SERVER['HTTP_HOST']);
$Domains->close();

if ($domainId) {
    $Users = new Users();
    $user = $Users->create($_POST, $domainId);
    $Users->close();

    if ($user) {
        $response = new Response(201, $user,true);
    } else {
        $response = new Response(400, [
            'message' => '회원가입 실패'
        ],true);
    }
} else {
    $response = new Response(400, [
        'message' => '회원가입 실패'
    ],true);
}