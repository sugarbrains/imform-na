<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 6.
 * Time: PM 3:40
 */
session_start();
if ($_SESSION['id']) {
    $response = new Response(200, $_SESSION,true);
} else {
    $response = new Response(401,null,true);
}