<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: AM 10:48
 */
require_once(dirname(__FILE__).'/../../../utils/Response.php');
require_once(dirname(__FILE__).'/../../../db/Users.php');

$Users = new Users();
$result = $Users->checkDuplicateId($_GET['uid']);
$Users->close();

if ($result) {
    $response = new Response(204,null,true);
} else {
    $response = new Response(409,null,true);
}