const fs = require('fs');
const path = require('path');
const sequelize = require('../methods/sequelize').sequelize;

const uploadsPath = path.join(__dirname, '../../uploads');
const importPath = path.join(uploadsPath, 'import');
const exportPath = path.join(uploadsPath, 'export');

const database = () => {
    return sequelize.models.ExportHistory.update({
        state: 'error',
        errorCode: '400',
        errorMessage: '서버 재시작'
    }, {
        where: {
            state: 'progress',
            errorMessage: null
        }
    }).then(() => {
        return sequelize.models.ReceiverGroupImportHistory.update({
            state: 'error',
            errorMessage: '서버 재시작'
        }, {
            where: {
                state: 'progress',
                errorMessage: null
            }
        });
    });
};

module.exports = async (app) => {
    if (!fs.existsSync(uploadsPath)) fs.mkdirSync(uploadsPath);
    if (!fs.existsSync(importPath)) fs.mkdirSync(importPath);
    if (!fs.existsSync(exportPath)) fs.mkdirSync(exportPath);

    await database();
};