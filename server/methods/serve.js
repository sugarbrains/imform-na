const path = require('path');
const serve = require('koa-static');
const mount = require('koa-mount');

module.exports = (app) => {
    app.use(mount('/public', serve(path.join(__dirname, '../../public'))));
};
