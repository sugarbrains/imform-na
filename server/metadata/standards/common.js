const standards = {
    track: {
        "hashFields": [
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z'
        ],
        "prefixHash": "Z"
    },
    message: {
        magicUrl: '[:URL:]'
    },
    receiver: {
        maleFields: {
            'M': true,
            'male': true,
            'm': true,
            'man': true,
            'Mr': true,
            'mr': true,
            'boy': true,
            '남': true,
            '남성': true,
            '남자': true,
            'Man': true,
            'Male': true,
            'MR': true,
            'men': true,
            'MAN': true,
            'MEN': true
        },
        femaleFields: {
            'F': true,
            'female': true,
            'f': true,
            'woman': true,
            'Mrs': true,
            'mrs': true,
            'girl': true,
            'lady': true,
            '여': true,
            '여성': true,
            '여자': true,
            'Woman': true,
            'Female': true,
            'MRS': true,
            'women': true,
            'WOMAN': true,
            'WOMEN': true,
            'Miss': true,
            'miss': true,
            'MISS': true
        },
        possibleGenders: {
            'M': true,
            'male': true,
            'm': true,
            'man': true,
            'Mr': true,
            'mr': true,
            'boy': true,
            '남': true,
            '남성': true,
            '남자': true,
            'Man': true,
            'Male': true,
            'MR': true,
            'men': true,
            'MAN': true,
            'MEN': true,
            'F': true,
            'female': true,
            'f': true,
            'woman': true,
            'Mrs': true,
            'mrs': true,
            'girl': true,
            'lady': true,
            '여': true,
            '여성': true,
            '여자': true,
            'Woman': true,
            'Female': true,
            'MRS': true,
            'women': true,
            'WOMAN': true,
            'WOMEN': true,
            'Miss': true,
            'miss': true,
            'MISS': true
        }
    }
};

module.exports = standards;
