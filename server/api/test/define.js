const Router = require('koa-router');
const router = new Router();

const post = require('./post');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'uid',
                'password'
            ],
            essential: [],
            explains: {
                'uid': 'uid',
                'password': 'password'
            },
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await post.validate(ctx, next);
            await post.findUser(ctx, next);
            await post.login(ctx, next);
        } else {
            return params;
        }
    }
};

router.post('/', api.post());

module.exports.router = router;
module.exports.api = api;
