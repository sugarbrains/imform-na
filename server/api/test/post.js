let post = {};

const Password = require("node-php-password");

post.validate = async (ctx, next) => {
    ctx.check.len('uid', 5, 191);
    ctx.check.len('password', 6, 20);
};

post.findUser = async (ctx, next) => {
    try {
        const body = ctx.request.body;
        const user = await ctx.models.User.findOne({
            where: {
                uid: body.uid
            }
        });
        if (user) {
            ctx.user = user;
        } else {
            ctx.throw(404);
        }
    } catch (e) {
        ctx.throw(400, e);
    }
};

post.login = async (ctx, next) => {
    if (Password.verify(ctx.request.body.password, ctx.user.password)) {
        ctx.status = 204;
    } else {
        ctx.throw(404);
    }
};

module.exports = post;