let get = {};

get.validate = async (ctx, next) => {
    ctx.check.isInt('id');
};

get.findExportHistory = async (ctx, next) => {
    try {
        const exportHistory = await ctx.models.ExportHistory.findOne({
            include: [{
                model: ctx.models.Message,
                as: 'message',
                required: true,
                where: {
                    id: ctx.params.id
                }
            }]
        });

        if (exportHistory) {
            ctx.utils.response.setNoCache(ctx);

            let sendCount = 0;
            if (exportHistory.state === 'complete') {
                try {
                    sendCount = await ctx.models.Message.getSendCount(ctx.params.id);
                } catch (e) {}
            }

            attachDummyData(exportHistory.message, sendCount);
            attachDummyData(exportHistory.message.dataValues, sendCount);

            attachExportHistoryData(exportHistory);
            attachExportHistoryData(exportHistory.dataValues);

            ctx.body = [exportHistory];

        } else {
            ctx.throw(404);
        }
    } catch (e) {
        ctx.throw(400, e);
    }
};

module.exports = get;

function attachDummyData(message, sendCount = 0) {
    message.mmsTitle = null;
    message.sendMethod = null;
    message.noPhoneNumCount = null;
    message.wrongPhoneNumCount = null;
    message.sendFailCount = null;
    message.priceSendMessage = 0;
    message.checkPayment = 0;
    message.ageStats = JSON.stringify({"9999":0});
    message.address1Stats = JSON.stringify({"noAddress":0});
    message.address2Stats = JSON.stringify({"noAddress":0});
    message.address3Stats = JSON.stringify({"noAddress":0});
    message.redirectTitle = null;
    message.redirectDescription = null;
    message.redirectImageId = null;
    message.sendCount = sendCount;
    message.totalMessageCount = {
        count: 0
    };
    delete message.state;
    delete message.activeDate;
    delete message.refuseMessage;
}

function attachExportHistoryData(exportHistory) {
    delete exportHistory.state;
    delete exportHistory.errorMessage;
    delete exportHistory.campaignId;
}