const Router = require('koa-router');
const router = new Router();

const get = require('./get');

const api = {
    get: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [],
            essential: [],
            explains: {
                'id': 'ID'
            },
            param: 'id',
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await get.validate(ctx, next);
            await get.findExportHistory(ctx, next);
        } else {
            return params;
        }
    },
};

router.get('/:id', api.get());

module.exports.router = router;
module.exports.api = api;
