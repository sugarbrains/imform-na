const Router = require('koa-router');
const router = new Router();

const post = require('./post');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'email',
                'pass',
            ],
            essential: [
                'email',
                'pass',
            ],
            explains: {
                'email': '사용자 email',
                'pass': '사용자 pass',
            },
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await post.validate(ctx, next);
            await post.getUser(ctx, next);
            await post.redirect(ctx, next);
        } else {
            return params;
        }
    }
};

router.post('/', api.post());

module.exports.router = router;
module.exports.api = api;
