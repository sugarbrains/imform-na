let post = {};
const Password = require("node-php-password");

post.validate = async (ctx) => {
    ctx.check.len('email', 1, 191);
    ctx.check.len('pass', 6, 20);
};

post.getUser = async (ctx) => {
    try {
        const body = ctx.request.body;
        const user = await ctx.models.User.findOne({
            where: {
                uid: body.email
            }
        });
        if (user && Password.verify(body.pass, user.password)) {
            ctx.user = user;
        } else {
            ctx.throw(400, JSON.stringify({
                code: '400_0025'
            }));
        }
    } catch (e) {
        ctx.throw(400, JSON.stringify({
            code: '400_0025'
        }));
    }
};

post.redirect = async (ctx) => {
    const host = 'http://localhost:3001' || ctx.request.origin;
    ctx.status = 302;
    ctx.redirect(`${host}/routes/sign-in.php?uid=${ctx.request.body.email}&password=${ctx.request.body.pass}`);
};

module.exports = post;