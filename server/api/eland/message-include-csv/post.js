let post = {};
const path = require('path');
const Password = require("node-php-password");
const rootPath = path.join(__dirname, '../../../../');

post.validate = async (ctx) => {
    const MESSAGE = ctx.meta.std.message;
    const FILE_UTIL = ctx.utils.file;

    ctx.check.len('email', 1, 191);
    ctx.check.len('pass', 6, 20);
    ctx.check.len('title', 1, 191);
    ctx.check.len('redirectUrl', 1, 191);

    if (ctx.request.body.message.indexOf(MESSAGE.magicUrl) === -1) {
        FILE_UTIL.removeUploadFiles(ctx);
        ctx.throw(400, JSON.stringify({
            code: '400_0007'
        }));
    }

    if (ctx.request.body.redirectUrl.indexOf('http') !== 0) {
        FILE_UTIL.removeUploadFiles(ctx);
        ctx.throw(400, JSON.stringify({
            code: '400_0026'
        }));
    }

    if (!ctx.request.files || Object.keys(ctx.request.files).length !== 1) {
        FILE_UTIL.removeUploadFiles(ctx);
        ctx.throw(400, JSON.stringify({
            code: '400_0021'
        }));
    } else if (!ctx.request.files['file0']) {
        FILE_UTIL.removeUploadFiles(ctx);
        ctx.throw(400, JSON.stringify({
            code: '400_0021'
        }));
    }
};

post.getUser = async (ctx) => {
    const FILE_UTIL = ctx.utils.file;
    try {
        const body = ctx.request.body;
        const user = await ctx.models.User.findOne({
            where: {
                uid: body.email
            }
        });
        if (user && Password.verify(body.pass, user.password)) {
            ctx.user = user;
        } else {
            FILE_UTIL.removeUploadFiles(ctx);
            ctx.throw(400, JSON.stringify({
                code: '400_0025'
            }));
        }
    } catch (e) {
        FILE_UTIL.removeUploadFiles(ctx);
        ctx.throw(400, JSON.stringify({
            code: '400_0025'
        }));
    }
};

post.getButtonPresets = async (ctx) => {
    ctx.buttons = [];
    try {
        const buttonPresets = await ctx.models.ButtonPresets.findAll({
            where: {
                domainId: ctx.user.domainId
            },
            order: [['id', 'ASC']]
        });
        buttonPresets.forEach((buttonPreset) => {
            ctx.buttons.push({
                title: buttonPreset.title
            });
        });
    } catch (e) {}
};

post.create = async (ctx) => {
    const FILE_UTIL = ctx.utils.file;
    try {
        const body = ctx.request.body;
        const progressMessage = await ctx.models.Message.findOne({
            where: {
                authorId: ctx.user.id
            },
            include: [{
                model: ctx.models.ExportHistory,
                as: 'exportHistory',
                where: {
                    authorId: ctx.user.id,
                    state: 'progress',
                    errorMessage: null
                }
            }]
        });

        if (progressMessage) {
            FILE_UTIL.removeUploadFiles(ctx);
            ctx.throw(400, JSON.stringify({
                code: '400_0009'
            }));
        } else {
            const now = new Date().getTime();
            const data = await ctx.models.Message.createMessageByEland({
                ...body,
                state: 'export',
                activeDate: new Date(),
                buttons: ctx.buttons,
                authorId: ctx.user.id,
                exportFileName: `uploads/export/${ctx.user.id}_${now}.csv`,
                importFileName: 'uploads/import/' + FILE_UTIL.getFileName(ctx.request.files['file0'].path) + '.csv',
            });
            const message = await data.reload();
            if (message) {

                attachDummyData(message);
                attachDummyData(message.dataValues);

                attachExportHistoryData(message.exportHistory);
                attachExportHistoryData(message.exportHistory.dataValues);

                attachImportHistoryData(message.importHistory);
                attachImportHistoryData(message.importHistory.dataValues);

                ctx.utils.response.setNoCache(ctx);
                ctx.status = 200;
                ctx.body = message;
            } else {
                FILE_UTIL.removeUploadFiles(ctx);
                ctx.throw(400, JSON.stringify({
                    code: '400_0009'
                }));
            }
        }
    } catch (e) {
        FILE_UTIL.removeUploadFiles(ctx);
        ctx.throw(400, JSON.stringify({
            code: '400_0009'
        }));
    }
};

post.moveImportFile = async (ctx) => {
    const FILE_UTIL = ctx.utils.file;
    FILE_UTIL.moveFile(path.join(rootPath, ctx.request.files['file0'].path), path.join(rootPath, 'uploads/import', FILE_UTIL.getFileName(ctx.request.files['file0'].path) + '.csv'));
};

module.exports = post;

function attachDummyData(message) {
    message.mmsTitle = null;
    message.sendMethod = null;
    message.noPhoneNumCount = null;
    message.wrongPhoneNumCount = null;
    message.sendFailCount = null;
    message.priceSendMessage = 0;
    message.checkPayment = 0;
    message.ageStats = JSON.stringify({"9999":0});
    message.address1Stats = JSON.stringify({"noAddress":0});
    message.address2Stats = JSON.stringify({"noAddress":0});
    message.address3Stats = JSON.stringify({"noAddress":0});
    message.redirectTitle = null;
    message.redirectDescription = null;
    message.redirectImageId = null;
    message.sendCount = 0;
    message.totalMessageCount = {
        count: 0
    };
    delete message.state;
    delete message.activeDate;
    delete message.refuseMessage;
}

function attachExportHistoryData(exportHistory) {
    delete exportHistory.state;
    delete exportHistory.errorMessage;
    delete exportHistory.campaignId;
}

function attachImportHistoryData(importHistory) {
    delete importHistory.errorMessage;
}