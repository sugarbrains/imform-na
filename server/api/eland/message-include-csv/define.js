const Router = require('koa-router');
const router = new Router();

const post = require('./post');

const api = {
    post: (isParam) => async (ctx, next) => {
        const params = {
            acceptable: [
                'email',
                'pass',
                'title',
                'message',
                'redirectUrl',
                'fileUniqueNum',
                'fileName'
            ],
            essential: [
                'email',
                'pass',
                'title',
                'message',
                'redirectUrl',
                'fileUniqueNum',
                'fileName'
            ],
            explains: {
                'email': '사용자 email',
                'pass': '사용자 pass',
                'title': '메세지 타이틀',
                'message': '메세지 [:URL:] 필요',
                'redirectUrl': '리다이렉트 URL',
                'fileUniqueNum': '이랜드 측 고유 번호',
                'fileName': '이랜드 측 파일 이름'
            },
            title: 'title',
            state: 'develop'
        };

        if (!isParam) {
            await post.validate(ctx, next);
            await post.getUser(ctx, next);
            await post.getButtonPresets(ctx, next);
            await post.create(ctx, next);
            await post.moveImportFile(ctx, next);
        } else {
            return params;
        }
    }
};

router.post('/', api.post());

module.exports.router = router;
module.exports.api = api;
