const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'receiverId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'receiverGroupId': {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    options: {
        tableName: 'ReceiverGroupRels',
        indexes: [{
            name: 'unique_key',
            fields: ['receiverId', 'receiverGroupId'],
            unique: true
        }],
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Receiver',
        foreignKey: 'receiverId',
        targetKey: 'id',
        as: 'receiver'
    }, {
        belongsTo: 'ReceiverGroup',
        foreignKey: 'receiverGroupId',
        targetKey: 'id',
        as: 'receiverGroup'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            createReceiverGroupRels: (authorId, receiverGroupId) => {
                return new Promise(async (resolve, reject) => {
                    const query = `INSERT INTO ReceiverGroupRels (receiverId, receiverGroupId, createdAt, updatedAt) SELECT Receivers.id, ${receiverGroupId}, NOW(), NOW() FROM Receivers WHERE Receivers.receiverGroupTarget IS TRUE AND Receivers.authorId = ${authorId}`;
                    return sequelize.transaction((t) => {
                        return sequelize.query(query, {
                            type: Sequelize.QueryTypes.INSERT,
                            transaction: t
                        }).then(() => {
                            return sequelize.models.Receiver.update({
                                receiverGroupTarget: false
                            }, {
                                where: {
                                    receiverGroupTarget: true
                                },
                                transaction: t
                            });
                        });
                    }).then(() => {
                        resolve(true);
                    }).catch(() => {
                        resolve(false);
                    });
                });
            }
        }
    }
};
