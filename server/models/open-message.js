const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'messageId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'receiverId': {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        'userAgent': {
            type: Sequelize.TEXT('long'),
            allowNull: true
        }
    },
    options: {
        tableName: 'OpenMessages',
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Message',
        foreignKey: 'messageId',
        targetKey: 'id',
        as: 'message'
    }, {
        belongsTo: 'Receiver',
        foreignKey: 'receiverId',
        targetKey: 'id',
        as: 'receiver'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
