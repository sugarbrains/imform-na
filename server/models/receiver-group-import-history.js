const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'authorId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'receiverGroupId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['standby', 'progress', 'complete', 'error'],
            defaultValue: 'standby',
            allowNull: false
        },
        'progress': {
            type: Sequelize.STRING(getDBStringLength()),
            defaultValue: 0,
            allowNull: true
        },
        'fileName': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'errorMessage': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        }
    },
    options: {
        tableName: 'ReceiverGroupImportHistories',
        indexes: [{
            name: 'unique_key',
            fields: ['authorId', 'receiverGroupId'],
            unique: true
        }, {
            name: 'state',
            fields: ['state']
        }, {
            name: 'progress',
            fields: ['progress']
        }, {
            name: 'errorMessage',
            fields: ['errorMessage']
        }],
        timestamps: true,
        paranoid: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'User',
        foreignKey: 'authorId',
        targetKey: 'id',
        as: 'author'
    }, {
        belongsTo: 'ReceiverGroup',
        foreignKey: 'receiverGroupId',
        targetKey: 'id',
        as: 'receiverGroup'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
