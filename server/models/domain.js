const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'url': {
            type: Sequelize.STRING(getDBStringLength()),
            unique: true,
            allowNull: false
        }
    },
    options: {
        tableName: 'Domains',
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
