const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'buttonId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'receiverId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'time': {
            type: Sequelize.INTEGER,
            defaultValue: 0,
            allowNull: false
        },
        'userAgent': {
            type: Sequelize.TEXT('long'),
            allowNull: true
        }
    },
    options: {
        tableName: 'OpenButtonTimes',
        timestamps: true,
        paranoid: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Button',
        foreignKey: 'buttonId',
        targetKey: 'id',
        as: 'button'
    }, {
        belongsTo: 'Receiver',
        foreignKey: 'receiverId',
        targetKey: 'id',
        as: 'receiver'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
