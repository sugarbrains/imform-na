const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'userId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'price': {
            type: Sequelize.INTEGER,
            defaultValue: 0,
            allowNull: false
        },
        'memo': {
            type: Sequelize.TEXT('long'),
            allowNull: true
        }
    },
    options: {
        tableName: 'Payments',
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'User',
        foreignKey: 'userId',
        targetKey: 'id',
        as: 'user'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
