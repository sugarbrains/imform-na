const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'sms': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: false
        },
        'lms': {
            type: Sequelize.DOUBLE(8, 2),
            allowNull: false
        }
    },
    options: {
        tableName: 'MetaPrices',
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
