const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'authorId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'messageId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'fileName': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['standby', 'progress', 'complete', 'error'],
            defaultValue: 'standby',
            allowNull: false
        },
        'progress': {
            type: Sequelize.STRING(getDBStringLength()),
            defaultValue: 0,
            allowNull: true
        },
        'errorCode': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'errorMessage': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'campaignId': {
            type: Sequelize.TEXT('long'),
            allowNull: true
        }
    },
    options: {
        tableName: 'ExportHistories',
        indexes: [{
            name: 'unique_key',
            fields: ['authorId', 'messageId'],
            unique: true
        }, {
            name: 'state',
            fields: ['state']
        }, {
            name: 'progress',
            fields: ['progress']
        }, {
            name: 'errorMessage',
            fields: ['errorMessage']
        }],
        timestamps: true,
        paranoid: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'User',
        foreignKey: 'authorId',
        targetKey: 'id',
        as: 'author'
    }, {
        belongsTo: 'Message',
        foreignKey: 'messageId',
        targetKey: 'id',
        as: 'message'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
