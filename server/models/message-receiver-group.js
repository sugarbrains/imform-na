const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'messageId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'receiverGroupId': {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    },
    options: {
        tableName: 'MessageReceiverGroups',
        indexes: [{
            name: 'unique_key',
            fields: ['messageId', 'receiverGroupId'],
            unique: true
        }],
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Message',
        foreignKey: 'messageId',
        targetKey: 'id',
        as: 'message'
    }, {
        belongsTo: 'ReceiverGroup',
        foreignKey: 'receiverGroupId',
        targetKey: 'id',
        as: 'receiverGroup'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
