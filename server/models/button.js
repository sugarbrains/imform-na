const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'messageId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'title': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        }
    },
    options: {
        tableName: 'Buttons',
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Message',
        foreignKey: 'messageId',
        targetKey: 'id',
        as: 'message'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
