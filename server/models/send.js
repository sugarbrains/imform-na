const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'messageId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'receiverId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'type': {
            type: Sequelize.ENUM,
            values: ['sms', 'lms'],
            allowNull: true
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['standby', 'progress', 'success', 'fail'],
            defaultValue: 'standby',
            allowNull: false
        },
        'price': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 0,
            allowNull: false
        },
        'errorMessage': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        }
    },
    options: {
        tableName: 'Sends',
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Message',
        foreignKey: 'messageId',
        targetKey: 'id',
        as: 'message'
    }, {
        belongsTo: 'Receiver',
        foreignKey: 'receiverId',
        targetKey: 'id',
        as: 'receiver'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
