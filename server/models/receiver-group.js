const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;
const FILTER_UTIL = require('../utils').filter;

module.exports = {
    fields: {
        'authorId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        }
    },
    options: {
        tableName: 'ReceiverGroups',
        timestamps: true,
        paranoid: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'User',
        foreignKey: 'authorId',
        targetKey: 'id',
        as: 'author'
    }, {
        hasOne: 'ReceiverGroupImportHistory',
        foreignKey: 'receiverGroupId',
        as: 'receiverGroupImportHistory'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            createReceiverGroup: (authorId, messageId) => {
                let createdData = null;
                return new Promise(async (resolve, reject) => {
                    return sequelize.transaction((t) => {
                        return sequelize.models.ReceiverGroup.create({
                            authorId,
                            name: FILTER_UTIL.date(new Date(), 'yyyy-MM-dd') + ' 발송 그룹'
                        }, {
                            transaction: t
                        }).then((data) => {
                            createdData = data;
                            const query = `INSERT INTO ReceiverGroupRels (receiverId, receiverGroupId, createdAt, updatedAt) SELECT Receivers.id, ${data.id}, NOW(), NOW() FROM Receivers WHERE Receivers.target IS TRUE AND Receivers.authorId = ${authorId}`;
                            return sequelize.query(query, {
                                type: Sequelize.QueryTypes.INSERT,
                                transaction: t
                            });
                        }).then(() => {
                            return sequelize.models.Receiver.update({
                                target: false
                            }, {
                                where: {
                                    target: true
                                },
                                transaction: t
                            });
                        }).then(() => {
                            return sequelize.models.MessageReceiverGroup.create({
                                messageId,
                                receiverGroupId: createdData.id
                            }, {
                                transaction: t
                            });
                        });
                    }).then(() => {
                        resolve(createdData);
                    }).catch((e) => {
                        resolve(null);
                    });
                });
            }
        }
    }
};
