const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'authorId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'target': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'receiverGroupTarget': {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        'key': {
            type: Sequelize.STRING,
            allowNull: false
        },
        'gender': {
            type: Sequelize.ENUM,
            values: ['m', 'f', 'noGender'],
            defaultValue: 'noGender',
            allowNull: false
        },
        'address1': {
            type: Sequelize.STRING,
            defaultValue: 'noAddress',
            allowNull: false
        },
        'address2': {
            type: Sequelize.STRING,
            defaultValue: 'noAddress',
            allowNull: false
        },
        'address3': {
            type: Sequelize.STRING,
            defaultValue: 'noAddress',
            allowNull: false
        },
        'birth': {
            type: Sequelize.INTEGER,
            defaultValue: 9999,
            allowNull: false
        },
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'phoneNum': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        }
    },
    options: {
        tableName: 'Receivers',
        indexes: [{
            name: 'unique_key',
            fields: ['authorId', 'key'],
            unique: true
        }, {
            name: 'target',
            fields: ['target']
        }, {
            name: 'receiverGroupTarget',
            fields: ['receiverGroupTarget']
        }, {
            name: 'key',
            fields: ['key']
        }, {
            name: 'gender',
            fields: ['gender']
        }, {
            name: 'address1',
            fields: ['address1']
        }, {
            name: 'address2',
            fields: ['address2']
        }, {
            name: 'birth',
            fields: ['birth']
        }, {
            name: 'name',
            fields: ['name']
        }],
        timestamps: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'User',
        foreignKey: 'authorId',
        targetKey: 'id',
        as: 'user'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            findExistReceivers: (receiverKeyList, authorId, callback) => {
                let loadedData = null;
                sequelize.models.Receiver.findAll({
                    where: {
                        key: receiverKeyList,
                        authorId: authorId
                    },
                    attributes: ['key']
                }).then((data) => {
                    loadedData = data;
                    return true;
                }).catch(() => {
                    callback(400);
                }).done((isSuccess) => {
                    if (isSuccess) {
                        if (loadedData.length) {
                            callback(200, loadedData);
                        } else {
                            callback(404, []);
                        }
                    }
                });
            },
            createReceivers: (receiverList, callback) => {
                sequelize.models.Receiver.bulkCreate(receiverList, {
                    ignoreDuplicates: true
                }).then(() => {
                    return true;
                }).catch((e) => {
                    callback(400, e);
                }).done((isSuccess) => {
                    if (isSuccess) {
                        callback(204);
                    }
                });
            },
            updateReceivers: (receiverList, update, authorId, callback, key) => {
                const length = receiverList.length;
                if (length > 0) {
                    let query = `UPDATE Receivers SET ${key ? key : 'target'} = (CASE`;
                    for (let i=0; i<length; i++) {
                        query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN TRUE`;
                    }
                    query += ' END)';
                    if (update.gender) {
                        query += ', gender = (CASE';
                        for (let i=0; i<length; i++) {
                            if (receiverList[i].gender) {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "${receiverList[i].gender}"`;
                            } else {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "noGender"`;
                            }
                        }
                        query += ' END)';
                    }
                    if (update.birth) {
                        query += ', birth = (CASE';
                        for (let i=0; i<length; i++) {
                            if (receiverList[i].birth) {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN ${receiverList[i].birth}`;
                            } else {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN 9999`;
                            }
                        }
                        query += ' END)';
                    }
                    if (update.address1) {
                        query += ', address1 = (CASE';
                        for (let i=0; i<length; i++) {
                            if (receiverList[i].address1) {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "${receiverList[i].address1}"`;
                            } else {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "noAddress"`;
                            }
                        }
                        query += ' END)';
                    }
                    if (update.address2) {
                        query += ', address2 = (CASE';
                        for (let i=0; i<length; i++) {
                            if (receiverList[i].address2) {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "${receiverList[i].address2}"`;
                            } else {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "noAddress"`;
                            }
                        }
                        query += ' END)';
                    }
                    if (update.address3) {
                        query += ', address3 = (CASE';
                        for (let i=0; i<length; i++) {
                            if (receiverList[i].address3) {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "${receiverList[i].address3}"`;
                            } else {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "noAddress"`;
                            }
                        }
                        query += ' END)';
                    }
                    if (update.phoneNum) {
                        query += ', phoneNum = (CASE';
                        for (let i=0; i<length; i++) {
                            if (receiverList[i].phoneNum) {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "${receiverList[i].phoneNum}"`;
                            } else {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN NULL`;
                            }
                        }
                        query += ' END)';
                    }
                    if (update.name) {
                        query += ', name = (CASE';
                        for (let i=0; i<length; i++) {
                            if (receiverList[i].name) {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN "${receiverList[i].name}"`;
                            } else {
                                query += ` WHEN Receivers.key = "${receiverList[i].key}" AND authorId = ${authorId} THEN NULL`;
                            }
                        }
                        query += ' END)';
                    }
                    query += ' WHERE Receivers.key IN("';
                    for (let i=0; i<length - 1; i++) {
                        query += receiverList[i].key + '","';
                    }
                    query += receiverList[length - 1].key + `") AND authorId = ${authorId}`;
                    sequelize.query(query, {
                        type: Sequelize.QueryTypes.UPDATE
                    }).then(() => {
                        return true;
                    }).catch((e) => {
                        callback(400, e);
                    }).done((isSuccess) => {
                        if (isSuccess) {
                            callback(204);
                        }
                    })
                } else {
                    callback(204);
                }
            },
            findReceivers: (options, callback) => {
                let loadedData = null;
                let query = `SELECT Receivers.* FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id AND ReceiverGroupRels.receiverGroupId = ${options.receiverGroupId}`;
                if (options.last) {
                    if (options.sort === 'ASC') {
                        query += ` WHERE Receivers.${options.orderBy} > ${options.last}`;
                    } else {
                        query += ` WHERE Receivers.${options.orderBy} < ${options.last}`;
                    }
                }
                query += ` ORDER BY Receivers.${options.orderBy} ${options.sort} LIMIT ${options.size}`;
                sequelize.query(query, {
                    type: Sequelize.QueryTypes.SELECT
                }).then((data) => {
                    loadedData = data;
                    return true;
                }).catch((e) => {
                    callback(400, e);
                }).done((isSuccess) => {
                    if (isSuccess) {
                        callback(200, loadedData);
                    }
                });
            },
            findMessageReceivers: (options, callback) => {
                let loadedData = null;
                let query = `SELECT Receivers.* FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId AND MessageReceiverGroups.messageId = ${options.messageId}
WHERE Receivers.id > ${options.last ? options.last : 0}
GROUP BY Receivers.id ORDER BY Receivers.id ASC LIMIT ${options.size}`;
                sequelize.query(query, {
                    type: Sequelize.QueryTypes.SELECT
                }).then((data) => {
                    loadedData = data;
                    return true;
                }).catch((e) => {
                    callback(400, e);
                }).done((isSuccess) => {
                    if (isSuccess) {
                        callback(200, loadedData);
                    }
                });
            }
        }
    }
};
