const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'domainId': {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        'title': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        }
    },
    options: {
        tableName: 'ButtonPresets',
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Domain',
        foreignKey: 'domainId',
        targetKey: 'id',
        as: 'domain'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
