const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'domainId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'uid': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'password': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false,
        },
        'role': {
            type: Sequelize.ENUM,
            values: ['ADMIN', 'MANAGER', 'USER', 'API'],
            defaultValue: 'USER',
            allowNull: false
        },
        'name': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'phoneNum': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'url': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: true
        },
        'refuseMessage': {
            type: Sequelize.TEXT('long'),
            allowNull: true
        },
        'sms': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 9.5,
            allowNull: false
        },
        'lms': {
            type: Sequelize.DOUBLE(8, 2),
            defaultValue: 27,
            allowNull: false
        }
    },
    options: {
        tableName: 'Users',
        indexes: [{
            name: 'unique_key',
            fields: ['domainId', 'uid'],
            unique: true
        }, {
            name: 'name',
            fields: ['name']
        }, {
            name: 'createdAt',
            fields: ['createdAt']
        }],
        timestamps: true,
        paranoid: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'Domain',
        foreignKey: 'domainId',
        targetKey: 'id',
        as: 'domain'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {

        }
    }
};
