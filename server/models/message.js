const Sequelize = require('sequelize');
const sequelize = require('../methods/sequelize').sequelize;
const DB = require('../config').db;
const getDBStringLength = require('../methods/sequelize').getDBStringLength;

module.exports = {
    fields: {
        'authorId': {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        'state': {
            type: Sequelize.ENUM,
            values: ['standby', 'send', 'sendComplete', 'cancel', 'export'],
            defaultValue: 'standby',
            allowNull: false
        },
        'title': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'title2': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'image': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'message': {
            type: Sequelize.TEXT('long'),
            allowNull: false
        },
        'refuseMessage': {
            type: Sequelize.TEXT('long'),
            allowNull: true
        },
        'redirectUrl': {
            type: Sequelize.STRING(getDBStringLength()),
            allowNull: false
        },
        'activeDate': {
            type: Sequelize.DATE,
            allowNull: true
        }
    },
    options: {
        tableName: 'Messages',
        indexes: [{
            name: 'title',
            fields: ['title']
        }, {
            name: 'state',
            fields: ['state']
        }, {
            name: 'activeDate',
            fields: ['activeDate']
        }, {
            name: 'createdAt',
            fields: ['createdAt']
        }],
        timestamps: true,
        paranoid: true,
        charset: DB.charset,
        collate: DB.collate
    },
    associations: [{
        belongsTo: 'User',
        foreignKey: 'authorId',
        targetKey: 'id',
        as: 'author'
    }, {
        hasOne: 'ExportHistory',
        foreignKey: 'messageId',
        as: 'exportHistory'
    }, {
        hasOne: 'ImportHistory',
        foreignKey: 'messageId',
        as: 'importHistory'
    }, {
        hasMany: 'Button',
        foreignKey: 'messageId',
        as: 'buttons'
    }, {
        hasMany: 'MessageReceiverGroup',
        foreignKey: 'messageId',
        as: 'messageReceiverGroups'
    }],
    methods: {
        instanceMethods: {},
        classMethods: {
            createMessageByEland: ({authorId, state, title, message, redirectUrl, activeDate, buttons, exportFileName, importFileName, fileUniqueNum, fileName}) => {
                let createdData = null;
                return new Promise(async (resolve, reject) => {
                    return sequelize.transaction((t) => {
                        let messageBody = {
                            authorId,
                            title,
                            message,
                            redirectUrl
                        };
                        if (state) messageBody.state = state;
                        if (activeDate) messageBody.activeDate = activeDate;
                        if (buttons.length) messageBody.buttons = buttons;
                        return sequelize.models.Message.create(messageBody, {
                            include: [{
                                model: sequelize.models.ExportHistory,
                                as: 'exportHistory'
                            }, {
                                model: sequelize.models.ImportHistory,
                                as: 'importHistory'
                            }, {
                                model: sequelize.models.Button,
                                as: 'buttons'
                            }],
                            transaction: t
                        }).then((data) => {
                            createdData = data;
                            return sequelize.models.ExportHistory.create({
                                authorId,
                                messageId: createdData.id,
                                fileName: exportFileName,
                                campaignId: fileUniqueNum ? fileUniqueNum : null
                            }, {
                                transaction: t
                            });
                        }).then(() => {
                            return sequelize.models.ImportHistory.create({
                                authorId,
                                messageId: createdData.id,
                                fileName: importFileName
                            }, {
                                transaction: t
                            });
                        });
                    }).then(() => {
                        resolve(createdData);
                    }).catch((e) => {
                        resolve(null);
                    });
                });
            },
            getSendCount: (id) => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const query = `SELECT COUNT(*) AS count FROM Receivers
INNER JOIN ReceiverGroupRels ON ReceiverGroupRels.receiverId = Receivers.id
INNER JOIN MessageReceiverGroups ON MessageReceiverGroups.receiverGroupId = ReceiverGroupRels.receiverGroupId AND MessageReceiverGroups.messageId = ${id}`;
                        const result = await sequelize.query(query, {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        if (result.length && result[0].count) {
                            resolve(result[0].count);
                        } else {
                            resolve(0);
                        }
                    } catch (e) {
                        reject(e);
                    }
                });
            }
        }
    }
};
