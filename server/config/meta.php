<?php
/**
 * Created by PhpStorm.
 * User: KJY
 * Date: 2019. 2. 5.
 * Time: PM 1:12
 */
const META = [
    "track" => [
        "hashFields" => [
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z'
        ],
        "prefixHash" => "Z"
    ],
    "magic" => [
        "reset" => ":RESET:",
        "empty" => ":EMPTY:"
    ]
];
